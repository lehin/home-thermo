# OpenTherm™ home controller

The controller is OpenTherm™ room unit that can be connected to variety of OT/+ boiler units.

It implements a multi-point outdoor compensated temperature PID-regulation. 

The device is equipped with 2.4" TFT color display to indicate its and also the connected boiler state.

The device can be monitored, configured and also flashed by air since it uses RAK439 WiFi module for wireless communications. Any telnet client can be used to control the unit. The flashing can be performed using simple protocol.

This controller implements also MQTT client to enable receiving temperature measurements also by air.

