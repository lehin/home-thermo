////////////////////////////////////////////////////////////////////////////////////////////
//
// commands.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <avr/pgmspace.h>

static constexpr uint16_t BOOTLOADER_UDP_PORT = 756 ;

////////////////////////////////////////////////////////////////////////////////////////////

struct SockAddr
{
   uint16_t m_port ;
   uint16_t m_family ;
   uint32_t m_addr ;

   static constexpr uint32_t ANY = 0 ;

   constexpr SockAddr (uint32_t addr, uint16_t port) : m_port{ port }, m_family{ 2 }, m_addr{ addr } {}
   constexpr SockAddr (void) : m_port{ 0 }, m_family{ 0 }, m_addr{ 0 } {}

   inline friend bool operator< (const SockAddr& lhs, const SockAddr& rhs)
   {
      if (lhs.m_addr != rhs.m_addr)
         return lhs.m_addr < rhs.m_addr ;

      return lhs.m_port < rhs.m_port ;
   }

   inline friend bool operator== (const SockAddr& lhs, const SockAddr& rhs)
   {
      return
         lhs.m_addr == rhs.m_addr &&
         lhs.m_port == rhs.m_port ;
   }

   inline friend bool operator!= (const SockAddr& lhs, const SockAddr& rhs)
      { return !operator== (lhs, rhs) ; }

} ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace cmds
{

////////////////////////////////////////////////////////////////////////////////////////////

struct CmdBase ;

void _send_command (const CmdBase& cmd) ;
void _read_command (void *buf, uint32_t pgm, uint8_t size) ;

struct CmdBase
{
   uint16_t m_size ;
} ;

template<uint16_t _cmdid, typename _Struct>
   struct Cmd : CmdBase
   {
      struct WmiHdr
      {
         uint16_t m_cmd ;
         uint8_t padding[4] = { 0 } ;

         constexpr WmiHdr (uint16_t cmdid) : m_cmd{ cmdid } {}
      } m_hdr ;

      _Struct m_data ;

      template<typename ... _Args>
         constexpr Cmd (_Args ... args) :
            CmdBase{ sizeof (_Struct) + sizeof (WmiHdr) },
            m_hdr{ _cmdid },
            m_data{ args... }
         {
         }

      void send (void) const { _send_command (*this) ; }

      static Cmd from_pgm (void)
      {
         static constexpr Cmd s_pgm_instance PROGMEM ;

         uint8_t raw[sizeof (Cmd)] ;
         _read_command (raw, pgm_get_far_address (s_pgm_instance), sizeof (Cmd)) ;

         return *reinterpret_cast<Cmd*> (raw) ;
      }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

enum class SockCmdType : uint8_t
{
   Open, Close, Connect, Bind, Listen, Accept, Select,
   SetSockOpt, GetSockOpt, Errno, IpConfig, Ping, StackInit,
   StackMisc, Ping6, Ip6Config, IpConfigDhcpPool, Ip6ConfigRouterPrefix,
   SetTcpExpBackoffRetry, IpSetIp6Status, IpDhcpRelease
} ;

struct SocketCmdHeader
{
   const uint32_t m_type ;
   const uint32_t m_length ;

   constexpr SocketCmdHeader (SockCmdType type, uint16_t len) :
      m_type{ static_cast<uint32_t> (type) }, m_length{ len } {}
} ;

template<SockCmdType _Cmd, typename _Struct>
   struct SocketCmdCore
   {
      SocketCmdHeader m_hdr ;
      _Struct m_data ;

      template<typename ... _Args>
         constexpr SocketCmdCore (_Args ... args) : m_hdr{ _Cmd, sizeof (_Struct) }, m_data{ args... } {}
   } ;

template<SockCmdType _Cmd, typename _Struct>
   using SocketCmd = Cmd< 0xF055, SocketCmdCore<_Cmd, _Struct> > ;

////////////////////////////////////////////////////////////////////////////////////////////

struct StackInitParms
{
   uint8_t m_stack_enabled = 1 ;
   uint8_t m_num_sockets = 8 ;
   uint8_t m_num_buffers = 2 ;
   uint8_t m_reserved = 0 ;
} ;

struct SetPasswordParms
{
   char m_ssid[32] = { 0 } ;
   char m_pass[64] = { 0 } ;
   uint8_t m_ssid_len = 0 ;
   uint8_t m_pass_len = 0 ;
} ;

struct ConnectParms
{
   uint8_t m_network_type = 1 ;
   uint8_t m_dot11_auth_mode = 1 ;
   uint8_t m_auth_mode = 0x10 ;
   uint8_t m_pairwise_crypto_type = 8 ;
   uint8_t m_pairwise_crypto_len = 0 ;
   uint8_t m_group_crypto_type = 8 ;
   uint8_t m_group_crypto_len = 0 ;
   uint8_t m_ssid_len = 0 ;
   char m_ssid[32] = { 0 } ;
   uint16_t m_channel = 0 ;
   uint8_t m_bssid[6] = { 0 } ;
   uint32_t m_ctrl_flags = 0x44 ;
} ;

struct DhcpParms
{
   struct Ip6Addr { uint8_t m_data[16] ; } ;

   uint32_t m_mode = 2 ;
   uint32_t m_ipv4 = 0 ;
   uint32_t m_subnet_mask = 0 ;
   uint32_t m_gateway4 = 0 ;
   uint32_t m_dnsrv1 = 0 ;
   uint32_t m_dnsrv2 = 0 ;
   char m_hostname[33] = "rak439" ;
   Ip6Addr m_ipv6_link_addr = { 0 } ;
   Ip6Addr m_ipv6_global_addr = { 0 } ;
   Ip6Addr m_ipv6_def_gw = { 0 } ;
   Ip6Addr m_ipv6_link_addr_extd = { 0 } ;
   int32_t m_link_prefix = 0 ;
   int32_t m_glb_prefix = 0 ;
   int32_t m_def_gw_prefix = 0 ;
   int32_t m_glb_prefix_extd = 0 ;
} ;

struct OpenParms
{
   uint32_t m_domain = 2 ;
   uint32_t m_type = 2 ; // UDP socket
   uint32_t m_protocol = 0 ;
} ;

struct SockAddrParms
{
   uint32_t m_sock ;
   SockAddr m_addr = { 0, BOOTLOADER_UDP_PORT } ;
   uint8_t m_padding[20] = { 0 } ; // it should be used for IPv6
   uint16_t m_addrlen = sizeof (m_addr) ;

   constexpr SockAddrParms (uint32_t sock) : m_sock{ sock } {}
} ;

struct CloseParms
{
   uint32_t m_sock ;

   constexpr CloseParms (uint32_t sock) : m_sock{ sock } {}
} ;

////////////////////////////////////////////////////////////////////////////////////////////

using SetPasswd = Cmd< 0xF048, SetPasswordParms> ;
using Connect = Cmd<0x0001,ConnectParms> ;
using StackInit = SocketCmd< SockCmdType::StackInit, StackInitParms > ;
using Dhcp = SocketCmd< SockCmdType::IpConfig, DhcpParms > ;
using Open = SocketCmd< SockCmdType::Open, OpenParms > ;
using Bind = SocketCmd< SockCmdType::Bind, SockAddrParms > ;
using Close = SocketCmd< SockCmdType::Close, CloseParms > ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace cmds
