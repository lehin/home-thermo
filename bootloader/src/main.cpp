#include "spi.h"
#include "gpio.h"
#include "hci.h"
#include "commands.h"
#include <avr/sleep.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include <avr/boot.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <string.h>

static constexpr uint16_t EEPROM_CREDS_ADDR = 15 ;

////////////////////////////////////////////////////////////////////////////////////////////

static void _blink (void) ;

void cmds::_send_command (const CmdBase& cmd)
{
   uint16_t full = hw::rak439::Hci::send_header (1, cmd.m_size) ;

   hw::rak439::Hci::Cs _cs ;

   auto begin = reinterpret_cast<const uint8_t*> (&cmd + 1) ;
   hw::Spi::send_range (hw::Spi::lsb{}, aux::range (begin, begin + cmd.m_size)) ;
   hw::Spi::fill (full - cmd.m_size) ;
}

void cmds::_read_command (void *buf, uint32_t pgm, uint8_t size)
{
   auto dst = reinterpret_cast<uint8_t*> (buf) ;

   while (size--)
      *dst++ = pgm_read_byte_far (pgm++) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

static void _boot (void) ;

////////////////////////////////////////////////////////////////////////////////////////////

static bool _check_reset (void)
{
   if (eeprom_read_byte ((uint8_t*)EEPROM_CREDS_ADDR) == 0xff)
      return true ;

   if (hw::gpio::Miso::read())
      return false ;

   _delay_ms (5) ;

   return !hw::gpio::Miso::read() ;
}

bool _wait_interrupt (uint8_t wait_cnt)
{
   TCNT1 = 0 ;
   TIFR1 = _BV(TOV1) ;

   if (wait_cnt)
      TCCR1B = _BV(CS10) | _BV (CS12) ;

   bool result = true ;

   while (hw::gpio::Wlirq::read())
   {
      hw::rak439::StatusReg{}.get() ;

      if (!wait_cnt)
         continue ;

      wdt_reset() ;

      if (TIFR1 & _BV(TOV1))
      {
         hw::gpio::Led::turn() ;

         TIFR1 = _BV(TOV1) ;

         if (!--wait_cnt)
         {
            result = false ;
            break ;
         }
      }
   }

   TCNT1 = 0 ;
   TIFR1 = _BV(TOV1) ;
   TCCR1B = 0 ;

   return result ;
}

bool _wait_event (uint8_t wait_cnt, uint16_t evtid, void *buf = nullptr, uint8_t bufsize = 0)
{
   bool done = false ;

   do
   {
      using namespace hw::rak439 ;

      if (!_wait_interrupt (wait_cnt))
         return false ;

      Interrupts::clear (Interrupts::PktAvail) ;

      auto info = Hci::read_header (1) ;
      if (!info.m_total)
         continue ;

      Hci::Cs _cs ;

      struct WmiHdr
      {
         uint16_t evtid ;
         uint8_t padding[4] ;
      } hdr ;

      hw::Spi::receive (hw::Spi::lsb{}, hdr) ;
      info.m_total -= sizeof (hdr) ;

      if (hdr.evtid == evtid)
      {
         if (!bufsize)
            done = true ;
         else
         {
            done = (info.m_total >= bufsize) ;

            if (done)
            {
               auto first = reinterpret_cast<uint8_t*> (buf) ;
               hw::Spi::receive_range (hw::Spi::lsb{}, aux::range (first, first + bufsize)) ;

               info.m_total -= bufsize;
            }
         }
      }

      hw::Spi::skip (info.m_total) ;

   } while (!done) ;

   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

uint8_t _string_from_eeprom (uint16_t addr, char *dst, uint8_t maxlen)
{
   auto src = reinterpret_cast<const uint8_t*> (addr) ;

   char ch ;
   uint8_t len = 0 ;

   while (len < maxlen && (ch = eeprom_read_byte (src++)) != -1 && ch)
   {
      *dst++ = ch ;
      ++len ;
   }

   return len ;
}

bool _wifi_init (void)
{
   hw::gpio::Rstp::reset() ;
   _delay_ms (2) ;
   hw::gpio::Rstp::set() ;
   _delay_ms (2) ;

   using namespace hw::rak439 ;

   ConfigReg{} = 1 << 7 ;

   wdt_reset() ;

   while (HostInterest::refclk_hz() != 26000000) ;
   HostInterest::flash_is_present() = 0x102 ;

   Interrupts::set (Interrupts::PktAvail) ;
   _wait_event (0, 0x1001) ;

   wdt_reset() ;

   cmds::StackInit{}.send() ;

   auto sp = cmds::SetPasswd{} ;
   sp.m_data.m_ssid_len = _string_from_eeprom (15, sp.m_data.m_ssid, sizeof (sp.m_data.m_ssid)) ;
   sp.m_data.m_pass_len = _string_from_eeprom (15 + 32, sp.m_data.m_pass, sizeof (sp.m_data.m_pass)) ;

   if (!sp.m_data.m_ssid_len)
      return false ;

   sp.send() ;

   auto conn = cmds::Connect{} ;
   conn.m_data.m_ssid_len = _string_from_eeprom (15, conn.m_data.m_ssid, sizeof (conn.m_data.m_ssid)) ;
   conn.send() ;

   if (!_wait_event (16, 0x1002))
      return false ;

   cmds::Dhcp{}.send() ;
   return _wait_event (16, 0x9007) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

struct DataHeader
{
   int8_t m_rssi ;

   struct
   {
      uint8_t m_msg_type : 2;
      uint8_t m_tid : 3 ;
      uint8_t m_ap_flag : 1 ;
      uint8_t m_hdr_type : 2 ;
   } m_info ;

   struct
   {
      uint16_t m_seqno : 12 ;
      uint16_t m_a_mspdu : 1 ;
      uint16_t m_meta_version : 3 ;
   } m_info2 ;

   uint16_t m_reserved ;

   constexpr DataHeader (void) :
      m_rssi{ 0 },
      m_info { 0, 0, 0, 0 },
      m_info2{ 0, 0, 5 },
      m_reserved{ 0 }
   {
   }
} ;

uint16_t _recv_from (uint32_t sock, uint8_t *buf, size_t bufsize, SockAddr& from)
{
   hw::rak439::Hci::ReadInfo info ;

   using namespace hw::rak439 ;

   if (!_wait_interrupt (1))
      return 0 ;

   Interrupts::clear (Interrupts::PktAvail) ;

   info = Hci::read_header (2) ;

   Hci::Cs _cs ;

   hw::Spi::skip (sizeof (DataHeader) + 4) ;
   hw::Spi::receive (hw::Spi::lsb{}, from) ;
   hw::Spi::skip (8) ;

   info.m_total -= sizeof (DataHeader) + 4 + 8 + sizeof (from) ;
   info.m_data -= sizeof (DataHeader) + 4 + 8 + sizeof (from) ;

   if (info.m_data > bufsize)
      info.m_data = bufsize ;

   hw::Spi::receive_range (hw::Spi::lsb{}, aux::range (buf + 0, buf + info.m_data)) ;
   hw::Spi::skip (info.m_total - info.m_data) ;

   return info.m_data ;
}

////////////////////////////////////////////////////////////////////////////////////////////

void _send_ok (uint32_t sock, uint16_t id, uint16_t ok, const SockAddr& to)
{
   struct SockSend
   {
      uint32_t m_sock ;
      uint16_t m_length ;
      uint16_t m_reserved ;
      uint32_t m_flags ;
      SockAddr m_addr ;
      uint16_t m_addrlen ;
   } ;

   struct Send
   {
      DataHeader hdr ;
      uint8_t pad1[12] ;
      SockSend ss ;
      uint8_t pad2[44-sizeof(SockSend)] ;
      uint16_t ok, id ;
   } s
   {
      {},
      { 0 },
      { sock, 4, 0, 0, to, sizeof (to) },
      { 0 },
      ok, id
   } ;

   constexpr uint16_t req = sizeof (Send) ;

   uint16_t full = hw::rak439::Hci::send_header (2, req) ;

   hw::rak439::Hci::Cs _cs ;

   hw::Spi::send (hw::Spi::lsb{}, s) ;
   hw::Spi::fill (full - req) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

typedef const uint8_t *BufPtr ;

uint8_t _from_hex__ (BufPtr& buf)
{
   uint8_t res ;

   if (*buf <= '9')
      res = *buf - '0' ;
   else
      res = *buf - 'A' + 10 ;

   buf++ ;
   return res ;
}

uint8_t _from_hex (BufPtr& buf, uint8_t& sum)
{
   uint8_t hi = _from_hex__ (buf) ;
   uint8_t lo = _from_hex__ (buf) ;

   uint8_t byte = (hi << 4) | lo ;
   sum += byte ;

   return byte ;
}

void _from_hex (BufPtr& buf, uint16_t& word, uint8_t& sum)
{
   uint8_t hi = _from_hex (buf, sum) ;
   uint8_t lo = _from_hex (buf, sum) ;

   word = ((uint16_t)hi << 8) | lo ;
}

////////////////////////////////////////////////////////////////////////////////////////////

class Flasher
{
public:
   enum class Result { Ok, Completed, Error } ;

public:
   ~Flasher (void) ;

   Result operator() (BufPtr buf, uint16_t bytes) ;

private:
   void _write_row (uint16_t addr, BufPtr buf, uint8_t size) ;
   bool _check_page (uint32_t addr) ;
   void _commit (void) ;

private:
   uint32_t m_last_page = -1 ;
   uint32_t m_offset = 0 ;
} ;


Flasher::~Flasher (void)
{
   _commit() ;
}

Flasher::Result Flasher::operator() (BufPtr buf, uint16_t bytes)
{
   auto end = buf + bytes ;
   uint32_t lastpage = -1 ;

   while (buf < end)
   {
      if ((*buf++ != ':') || buf >= end)
         return Result::Error ;

      uint8_t sum = 0 ;
      uint8_t size = _from_hex (buf, sum) ;

      uint16_t addr ;
      _from_hex (buf, addr, sum) ;

      uint8_t cmd = _from_hex (buf, sum) ;

      if (buf >= end)
         return Result::Error ;

      uint8_t row[16] ;
      for (uint8_t n = 0; n < size; ++n)
         row[n] = _from_hex (buf, sum) ;

      uint8_t add = _from_hex (buf, sum) ;

      if (buf >= end || *buf++ != '\r' || *buf++ != '\n')
         return Result::Error ;

      if (sum)
         return Result::Error ;

      switch (cmd)
      {
         case 0:
            _write_row (addr, row, size) ;
            break ;

         case 1:
            return Result::Completed ;

         case 2:
            m_offset = (uint32_t)(((uint16_t)row[0] << 8) | row[1]) << 4 ;
            break ;
      }
   }

   return Result::Ok ;
}

void Flasher::_write_row (uint16_t addr, BufPtr buf, uint8_t size)
{
   uint32_t dst = addr + m_offset ;
   for (uint8_t offs = 0; offs < size; offs += 2, dst += 2)
   {
      if (_check_page (dst))
         boot_page_fill (dst, *reinterpret_cast<const uint16_t*> (buf + offs)) ;
   }
}

bool Flasher::_check_page (uint32_t addr)
{
   if (addr >= 0x1F000ul)
      return false ;

   auto page = addr & ~(SPM_PAGESIZE - 1) ;
   if (page == m_last_page)
      return true ;

   _commit() ;

   boot_page_erase (m_last_page = page) ;
   boot_spm_busy_wait() ;

   return true ;
}

void Flasher::_commit (void)
{
   if (m_last_page == -1)
      return ;

   boot_page_write (m_last_page) ;
   boot_spm_busy_wait() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

static uint8_t buf[1024] ;
static SockAddr from ;

bool _program (uint32_t sock)
{
   uint16_t sz ;
   if (!(sz = _recv_from (sock, buf, sizeof (buf), from)))
      return false ;

   if (strncmp ((const char*)buf, "UNIFLASH HELLO", 14))
      return false ;

   _send_ok (sock, 0, (uint16_t)'2V', from) ;

   Flasher flasher ;
   Flasher::Result res = Flasher::Result::Error ;

   do
   {
      auto bytes = _recv_from (sock, buf, sizeof (buf), from) ;
      if (bytes < 3)
         break ;

      hw::gpio::Led::reset() ;

      uint16_t id = *(uint16_t*)buf ;
      static uint16_t last_id = -1 ;

      res = Flasher::Result::Ok ;
      if (id != last_id)
      {
         res = flasher (buf + 2, bytes - 2) ;
         if (res == Flasher::Result::Ok)
            last_id = id ;
      }

      _send_ok (sock, id, (res != Flasher::Result::Error ? (uint16_t)'KO' : (uint16_t)'RE'), from) ;

      hw::gpio::Led::set() ;

      wdt_reset() ;

   } while (res == Flasher::Result::Ok) ;

   return res == Flasher::Result::Completed ;
}

////////////////////////////////////////////////////////////////////////////////////////////

void _update_statistics (uint16_t offset)
{
   eeprom_write_word ((uint16_t*)offset, eeprom_read_word ((const uint16_t*)offset) + 1) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

bool _check_boot_flag (void)
{
   auto flag = eeprom_read_byte (0) ;

   if (flag != 0xFF)
   {
      eeprom_write_byte (0, 0xFF) ;
      eeprom_busy_wait() ;
   }

   uint8_t mcusr = MCUSR ;
   MCUSR = 0 ;

   switch (flag)
   {
      case 0: // usual boot
         if (mcusr & _BV(PORF))
            _update_statistics (1) ;
         else if (mcusr & _BV(BORF))
            _update_statistics (5) ;
         else if (mcusr & _BV(EXTRF))
            _update_statistics (3) ;
         else if (mcusr & _BV(WDRF))
            _update_statistics (7) ;

         return (mcusr & _BV(WDRF)) == 0 ;

      case 1: // software reset
         _update_statistics (9) ;
         return true ;

      case 2: // assertion
         _update_statistics (11) ;
         break ;

      case 3: // flash
         _update_statistics (9) ;
         break ;

      default: // boot unsuccessfull
         _update_statistics (13) ;
         break ;
   }

   return false ;

}

////////////////////////////////////////////////////////////////////////////////////////////

static void _blink (void)
{
   for (uint8_t n = 0; !(n & 16); ++n)
   {
      hw::gpio::Led::turn() ;
      _delay_ms (20) ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

extern "C" { void _bootloader (void) __attribute__ ((used,noreturn)) ; }

namespace
{
   struct SockResponseData
   {
      uint32_t m_type ;
      uint32_t m_sock ;
      uint32_t m_error ;
   } ;

   static SockResponseData sockresp ;
}

void _bootloader (void)
{
   hw::gpio::init() ;
   hw::Spi::init() ;

   wdt_enable (WDTO_4S) ;
   wdt_reset() ;

   _blink() ;
   hw::gpio::Led::reset() ;

   if (_check_boot_flag() || _check_reset() || !_wifi_init())
      goto boot ;

   hw::gpio::Led::set() ;

   cmds::Open{}.send() ;

   _wait_event (0, 0x9007, &sockresp, sizeof (sockresp)) ;

   cmds::Bind{ sockresp.m_sock } . send() ;
   _wait_event (0, 0x9007) ;

   if (_program (sockresp.m_sock))
      boot_rww_enable() ;

   cmds::Close{ sockresp.m_sock } . send() ;

boot:
   wdt_reset() ;
   asm volatile ( "jmp 0" ) ;
   while (true) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

void ___init0 (void) __attribute__ ((used,naked)) __attribute__ ((section (".init0"))) ;
void ___init9 (void) __attribute__ ((used,naked)) __attribute__ ((section (".init9"))) ;

static void (*call_firmware) (void) ;

void ___init0 (void)
{
   asm volatile ( "clr __zero_reg__" ) ;  // r1 set to 0

   SPH = 0x40 ;
   SPL = 0xFF ;
}

void ___init9 (void)
{
   asm volatile ( "rjmp _bootloader" ) ;          // jump to main()
}

////////////////////////////////////////////////////////////////////////////////////////////
