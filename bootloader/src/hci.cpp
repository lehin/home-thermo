////////////////////////////////////////////////////////////////////////////////////////////
//
// hci.cpp
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "hci.h"
#include "gpio.h"
#include "spi.h"
#include <util/delay.h>

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

Hci::Cs::~Cs (void)
{
   Spi::flush() ;
   gpio::Wlcs::set() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static constexpr uint16_t HCI_INTREG_FLAG = 1 << 14 ;
   static constexpr uint16_t HCI_READ_FLAG = 1 << 15 ;
}

void Hci::set_internal_register (uint16_t addr, uint16_t val)
{
   Cs _cs ;
   Spi::send_multiple (Spi::msb{}, addr | HCI_INTREG_FLAG, val) ;
}

uint16_t Hci::get_internal_register (uint16_t addr)
{
   Cs _cs ;

   Spi::send (Spi::msb{}, addr | HCI_INTREG_FLAG | HCI_READ_FLAG) ;

   uint16_t res ;
   Spi::receive (Spi::msb{}, res) ;

   return res ;
}

namespace
{
   static constexpr uint16_t SPI_HOST_CTRL_SIZE_FIXED_ADDR = 1 << 6 ;
   static constexpr uint16_t SPI_HOST_CTRL_CONFIG_ENABLE = 1 << 15 ;
   static constexpr uint16_t SPI_HOST_CTRL_CONFIG_DIR_WRITE = 1 << 14 ;
   static constexpr uint16_t SPI_STATUS_HOST_ACCESS_DONE = 1 << 0 ;
}

void Hci::external_read (uint16_t addr, uint8_t size, void* buf)
{
   HostCtrlSizeReg{} = (uint16_t)size ;
   HostCtrlCfgReg{} = addr | SPI_HOST_CTRL_CONFIG_ENABLE ;

   while (!(StatusReg{} & SPI_STATUS_HOST_ACCESS_DONE))
      _delay_ms (1) ;

   {
      Cs _cs ;

      Spi::send (Spi::msb{}, HostCtrlRdPortReg::address | HCI_INTREG_FLAG | HCI_READ_FLAG) ;

      auto bytes = static_cast<uint8_t*> (buf) ;
      Spi::receive_range (Spi::lsb{}, aux::range (bytes, bytes + size)) ;
   }

   Interrupts::clear (Interrupts::HostCtrl) ;
}

void Hci::external_write (uint16_t addr, uint8_t size, const void* buf)
{
   HostCtrlSizeReg{} = (uint16_t)size ;

   {
      Cs _cs ;

      Spi::send (Spi::msb{}, HostCtrlWrPortReg::address  | HCI_INTREG_FLAG) ;

      auto *bytes = static_cast<const uint8_t*> (buf) ;
      Spi::send_range (Spi::lsb{}, aux::range (bytes, bytes + size)) ;
   }

   HostCtrlCfgReg{} = addr | SPI_HOST_CTRL_CONFIG_ENABLE | SPI_HOST_CTRL_CONFIG_DIR_WRITE ;

   while (!(StatusReg{} & SPI_STATUS_HOST_ACCESS_DONE))
      _delay_ms (1) ;

   Interrupts::clear (Interrupts::HostCtrl) ;
}

uint32_t Hci::read_diag_window (uint32_t addr)
{
   WindowRdAddrReg{} = addr ;
   return WindowDataPort{} ;
}

void Hci::write_diag_window (uint32_t addr, uint32_t value)
{
   WindowDataPort{} = value ;
   WindowWrAddrReg{} = addr ;
}

Hci::ReadInfo Hci::read_header (uint8_t ep)
{
   auto hdr = _query_look_ahead() ;

   uint16_t readlen = hdr.m_length + sizeof (FrameHdrFull) ;
   readlen = (readlen + 31) & ~31 ;

   SpiDmaSizeReg{} = readlen ;

   Cs _cs ;

   uint16_t addr = 0x800 + 0x800 - readlen ;
   Spi::send (Spi::msb{}, addr | HCI_READ_FLAG) ;

   if (hdr.m_ep != ep)
   {
      Spi::skip (readlen) ;
      return { 0, 0 } ;
   }

   FrameHdrFull hdrfull ;
   Spi::receive (Spi::lsb{}, hdrfull) ;

   readlen -= sizeof (hdrfull) ;

   auto datalen = hdrfull.m_hdr.m_length ;

   static constexpr uint8_t RECV_TRAILER = 1 << 1 ;
   if (hdrfull.m_hdr.m_flags & RECV_TRAILER)
      datalen -= hdrfull.m_control[0] ;

   return { readlen, datalen } ;
}

uint16_t Hci::send_header (uint8_t ep, uint16_t len)
{
   uint16_t txlen = (len + sizeof (FrameHdrFull) + 31u) & ~31u ;

   SpiDmaSizeReg{} = txlen ;

   Cs _cs ;

   uint16_t addr = 0x800 + 0x800 - txlen ;
   Spi::send (Spi::msb{}, addr) ;
   Spi::send (Spi::lsb{}, FrameHdrFull{ { ep, 0, len }, { 0, 0 } } ) ;

   return txlen - sizeof (FrameHdrFull) ;
}

namespace
{
   uint16_t _swab (uint16_t val)
   {
      return (val << 8) | (val >> 8) ;
   }
}

Hci::FrameHdr Hci::_query_look_ahead (void)
{
   union
   {
      uint16_t bytes[2] ;
      FrameHdr hdr ;
   } cvt ;

   cvt.bytes[0] = _swab (RdBufLookahead1Reg{}) ;
   cvt.bytes[1] = _swab (RdBufLookahead2Reg{}) ;

   return cvt.hdr ;
}

////////////////////////////////////////////////////////////////////////////////////////////

void Interrupts::set (Flags flags)
{
   IntEnableReg{} = flags ;
}

void Interrupts::clear (Flags flags)
{
   IntCauseReg{} = flags ;
}

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
