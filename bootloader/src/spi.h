////////////////////////////////////////////////////////////////////////////////////////////
//
// spi.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "range.h"
#include <stdint.h>
#include <stddef.h>
#include <avr/io.h>

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

class Spi
{
public:
   struct msb {} ;
   struct lsb {} ;

public:
   static void init (void) ;
   static void flush (void) ;

   static void send (uint8_t byte) ;

   // send any struct using MSB-first order
   template<typename _Order, typename _Tp>
      static void send (_Order, const _Tp& data) ;

   template<typename _Order, typename _Tp, typename... _Args>
      static void send_multiple (_Order order, const _Tp& data, const _Args& ... args) ;

   template<typename _Order, typename _Range>
      static void send_range (_Order order, const _Range& range) ;

   template<typename _Tp>
      static void send_range (lsb, const aux::Range<_Tp*>& range) ;

   static uint8_t receive (void) ;

   template<typename _Order, typename _Tp>
      static void receive (_Order, _Tp& data) ;

   template<typename _Order, typename _Range>
      static void receive_range (_Order order, const _Range& range) ;

   template<typename _Tp>
      static void receive_range (lsb, const aux::Range<_Tp*>& range) ;

   static void skip (uint16_t len) ;
   static void fill (uint16_t len) ;

private:
   template<typename _Order>
      static void send_multiple (_Order) {}

   static void _send_bytes_generic (lsb, const uint8_t *p, size_t size) ;
   static void _send_bytes_generic (msb, const uint8_t *p, size_t size) ;

   static void _receive_bytes_generic (lsb, uint8_t *p, size_t size) ;
   static void _receive_bytes_generic (msb, uint8_t *p, size_t size) ;

   static void _send_bytes_2 (lsb, const uint16_t& p) ;
   static void _send_bytes_2 (msb, const uint16_t& p) ;
   static uint16_t _receive_bytes_2 (lsb) ;
   static uint16_t _receive_bytes_2 (msb) ;

   static void _receive_prepare (void) ;
   static uint8_t _receive_byte (void) ;

   template<typename _Order, typename _Tp>
      static void _receive_raw (_Order, _Tp& data) ;

   template<typename _Tp, size_t _Size>
      struct _GenericSender ;

   template<typename _Tp>
      struct _GenericSender<_Tp,1> ;

   template<typename _Tp>
      struct _GenericSender<_Tp,2> ;

   template<typename _Tp, size_t _Size>
      struct _GenericReceiver ;

   template<typename _Tp>
      struct _GenericReceiver<_Tp,1> ;

   template<typename _Tp>
      struct _GenericReceiver<_Tp,2> ;
} ;


template<typename _Tp, size_t _Size>
   struct Spi::_GenericSender
   {
      template<typename _Order> __attribute__ ((always_inline))
         void operator() (_Order order, const _Tp& p) const
            { _send_bytes_generic (order, (const uint8_t*)&p, _Size) ; }
   } ;

template<typename _Tp>
   struct Spi::_GenericSender<_Tp,1>
   {
      __attribute__ ((always_inline))
         void operator() (lsb, const _Tp& p) const { send (reinterpret_cast<const uint8_t&> (p)) ; }

      __attribute__ ((always_inline))
         void operator() (msb, const _Tp& p) const { send (reinterpret_cast<const uint8_t&> (p)) ; }
   } ;

template<typename _Tp>
   struct Spi::_GenericSender<_Tp,2>
   {
      template<typename _Order> __attribute__ ((always_inline))
         void operator() (_Order order, const _Tp& p) const
            { _send_bytes_2 (order, reinterpret_cast<const uint16_t&> (p)) ; }
   } ;

template<typename _Tp, size_t _Size>
   struct Spi::_GenericReceiver
   {
      template<typename _Order> __attribute__ ((always_inline))
         void operator() (_Order order, _Tp& p) const
          { _receive_bytes_generic (order, (uint8_t*)&p, _Size) ; }
   } ;

template<typename _Tp>
   struct Spi::_GenericReceiver<_Tp,1>
   {
      __attribute__ ((always_inline))
         void operator() (lsb, _Tp& p) const
            { reinterpret_cast<uint8_t&> (p) = receive() ; }

      __attribute__ ((always_inline))
         void operator() (msb, _Tp& p) const
            { reinterpret_cast<uint8_t&> (p) = receive() ; }
   } ;

template<typename _Tp>
   struct Spi::_GenericReceiver<_Tp,2>
   {
      template<typename _Order> __attribute__ ((always_inline))
         void operator() (_Order order, _Tp& p) const
            { reinterpret_cast<uint16_t&> (p) = _receive_bytes_2 (order) ; }
   } ;

inline
   void Spi::flush (void)
   {
      loop_until_bit_is_set (UCSR1A, TXC1) ;
   }

inline
   void Spi::send (uint8_t byte)
   {
      loop_until_bit_is_set (UCSR1A, UDRE1) ;
      UDR1 = byte ;
      UCSR1A = _BV (TXC1) ;
   }

template<typename _Order, typename _Tp> __attribute__ ((always_inline))
   inline void Spi::send (_Order, const _Tp& data)
   {
      _GenericSender<_Tp,sizeof(_Tp)>{} (_Order{}, data) ;
   }

template<typename _Order, typename _Tp, typename... _Args>
   inline void Spi::send_multiple (_Order order, const _Tp& data, const _Args& ... args)
   {
      send (order, data) ;
      send_multiple (order, args...) ;
   }

template<typename _Order, typename _Range>
   inline void Spi::send_range (_Order order, const _Range& range)
   {
      for (auto& val : range)
         send (order, val) ;
   }

template<typename _Tp>
   inline void Spi::send_range (lsb, const aux::Range<_Tp*>& range)
   {
      _send_bytes_generic (lsb{}, range.begin(), range.size() * sizeof (_Tp));
   }

inline
   uint8_t Spi::receive (void)
   {
      _receive_prepare() ;
      return _receive_byte() ;
   }

inline
   void Spi::skip (uint16_t len)
   {
      while (len--)
         receive() ;
   }

inline
   void Spi::fill (uint16_t len)
   {
      while (len--)
         send (0) ;
   }

template<typename _Order, typename _Tp> __attribute__ ((always_inline))
   inline void Spi::receive (_Order, _Tp& data)
   {
      _receive_prepare() ;
      _GenericReceiver<_Tp,sizeof(_Tp)>{} (_Order{}, data) ;
   }

template<typename _Order, typename _Range>
   inline void Spi::receive_range (_Order order, const _Range& range)
   {
      _receive_prepare() ;
      for (auto& val : range)
         _receive_raw (order, val) ;
   }

template<typename _Tp>
   inline __attribute__ ((always_inline)) void Spi::receive_range (lsb, const aux::Range<_Tp*>& range)
   {
      _receive_prepare() ;
      _receive_bytes_generic (lsb{}, (uint8_t*)range.begin(), range.size() * sizeof (_Tp)) ;
   }

inline __attribute__ ((always_inline))
   void Spi::_send_bytes_2 (lsb, const uint16_t& p)
   {
      send (p) ;
      send (p >> 8) ;
   }

inline __attribute__ ((always_inline))
   void Spi::_send_bytes_2 (msb, const uint16_t& p)
   {
      send (p >> 8) ;
      send (p) ;
   }

inline __attribute__ ((always_inline))
   uint16_t Spi::_receive_bytes_2 (lsb)
   {
      return _receive_byte() | ((uint16_t)_receive_byte() << 8) ;
   }

inline __attribute__ ((always_inline))
   uint16_t Spi::_receive_bytes_2 (msb)
   {
      return ((uint16_t)_receive_byte() << 8) | _receive_byte() ;
   }

inline
   void Spi::_receive_prepare (void)
   {
      flush() ;

      while ((UCSR1A & _BV(RXC1)))
         UDR1 ;
   }

inline
   uint8_t Spi::_receive_byte (void)
   {
      UDR1 = 0 ;
      loop_until_bit_is_set (UCSR1A, RXC1) ;
      return UDR1 ;
   }

template<typename _Order, typename _Tp>
   void Spi::_receive_raw (_Order, _Tp& data)
   {
      _GenericReceiver<_Tp,sizeof(_Tp)>{} (_Order{}, data) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
