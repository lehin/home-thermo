use <thermo-ctrl-pcb.scad>
use <connectors.scad>
use <rcube.scad>

$fn = 30 ;
delta = 1e-3 ;

face_thickness = 1 ;
back_thickness = 1.6 ;
wall_thickness = 1.6 ;

pcb_size = [ 85.471, 58.547, 1.6 ] ;
assembly_height = 15 ;

gap = 0.4 ;
pcb_gap = 0.4 ;
pcb_offs = [wall_thickness + pcb_gap, wall_thickness + pcb_gap, 4.5 + face_thickness] ;

case_size = 
   [
      pcb_size[0] + pcb_offs[0] + pcb_gap + wall_thickness + 1.5,
      pcb_size[1] + pcb_offs[1] + pcb_gap + wall_thickness,
      pcb_size[2] + pcb_offs[2] + assembly_height + 0.5 + back_thickness + face_thickness
   ] ;

back_sz = [case_size[0] - 2*wall_thickness - gap, case_size[1] - 2*wall_thickness - gap, back_thickness] ;

ot_conn_sz = [9.5658, 11.54, 6.5] ;

switch_sz = [13.5, 8.2, 8.6] ;

module translate_pcb_mm()
   translate ([0, 0, pcb_size[2]/2] + pcb_offs)
      children() ;
         
module place_on_pcb (mil)
{
   function mil2mm (c) = c * 25.4 / 1000 ;
   translate_pcb_mm()
      translate ([mil2mm (mil[0]), pcb_size[1] - mil2mm (mil[1])])
         children() ;
}

module place_ot_conn()
   translate ([65, -0.5, case_size[2]/2])
      children() ;

module place_switch()
   translate ([3.5, 0, 9])
      children() ;


module pcb()
{
   translate_pcb_mm()
      pcb_board() ;
}

module switch()
{
   translate ([0, 0, -switch_sz[2]])
      color ([0.3, 0.3, 0.3])
         cube (switch_sz) ;
   
   full_sz = [15, 10.5, 1.5] ;
   
   color ([0.3, 0.3, 0.3])
      hull()
      {
         translate ([-(full_sz[0] - switch_sz[0]) / 2, -(full_sz[1] - switch_sz[1]) / 2, 0])
            cube ([full_sz[0], full_sz[1], delta]) ;

         translate ([0, 0, full_sz[2]])
            cube ([switch_sz[0], switch_sz[1], delta]) ;
      }

   color ([0.9, 0.9, 0.9])
   {
      translate ([switch_sz[0]/2-0.3, switch_sz[1]/2 - 2, -switch_sz[2] - 6])
         cube ([0.6, 4, 6]) ;

      translate ([1.5, switch_sz[1]/2 - 2, -switch_sz[2] - 6])
         cube ([0.6, 4, 6]) ;
   }
}

module pcb_holders()
{
   module screw_fix()
   {
      translate ([0, 0, -pcb_size[2]/2 - pcb_offs[2]])

      difference()
      {
         translate ([-3, -3])
            cube ([6, 6, pcb_offs[2]]) ;
         
         translate ([0, 0, -delta])
            cylinder (d = 2.5, h = pcb_offs[2] + 2*delta) ;
      }
   }

   place_on_pcb([100, 2200])
      screw_fix() ;

   place_on_pcb([1450, 100])
      screw_fix() ;

   translate ([35, wall_thickness - delta, 0])
      cube ([5, 2, pcb_offs[2]]) ;

   translate ([57, wall_thickness - delta, 0])
      cube ([20, 2, pcb_offs[2]]) ;

   translate ([41.5, case_size[1] - 2 - wall_thickness + delta, 0])
      cube ([case_size[0] - 41.5 - wall_thickness + delta, 2, pcb_offs[2]]) ;

   translate ([29, case_size[1] - 2 - wall_thickness + delta, 0])
      cube ([7, 2, pcb_offs[2]]) ;

   translate ([10, case_size[1] - 2 - wall_thickness + delta, 0])
      cube ([9, 2, pcb_offs[2]]) ;

   translate ([wall_thickness - delta, 7.5, 0])
      cube ([2, case_size[1] - 7.5 - wall_thickness + delta, pcb_offs[2]]) ;
      
   translate ([case_size[0] - wall_thickness + delta, 0, pcb_offs[2] + 0.1])
   {
      translate ([0, 40, 0])
         hull()
         {
            translate ([-2.5, 0, pcb_size[2] + 1])
               cube ([2.5, 18, delta]) ;
            
            cube ([delta, 18, delta]) ;
         }

      translate ([0, 11, 0])
         hull()
         {
            translate ([-2.5, 0, pcb_size[2] + 1])
               cube ([2.5, 10, delta]) ;
            
            cube ([delta, 10, delta]) ;
         }
   }
}

module back_clicks (enlarge = 0)
{
   module click (D, L)
   {
      translate ([-L/2, 0, 0])
         hull()
         {
            sphere (d = D) ;
            translate ([L, 0, 0])
               sphere (d = D) ;
            
            translate ([-2*D, 0, 0])
               sphere (d = 0.5) ;

            translate ([L+2*D, 0, 0])
               sphere (d = 0.5) ;
         }
   }

   SZ = back_sz + [2*enlarge, 2*enlarge, 0] ;

   D = 0.9 ;
   L1 = 40 + enlarge ;
   L2 = 20 + enlarge ;

   translate ([(case_size[0] - SZ[0]) / 2, (case_size[1] - SZ[1]) / 2, case_size[2] - SZ[2]])
   {
      translate ([SZ[0]/2, 0, SZ[2]/2])
      {
         click (D, L1) ;
         
         translate ([0, SZ[1], 0])
            click (D, L1) ;
      }
      
      translate ([0, SZ[1]/2, SZ[2]/2])
      {
         rotate ([0, 0, 90])
            click (D, L2) ;

         translate ([SZ[0], 0, 0])
            rotate ([0, 0, 90])
               click (D, L2) ;
      }
   }
}

module vents (excludes = [])
{
   function norm (l, n) = len(l) == 3 ? l : [l[0], l[1], n] ;
   function find (p) = len (search (1, [ for (e = excludes) norm (e, p[2]) == p ? 1 : 0 ])) != 0 ;

   module vent (l)
   {
      R = 0.7 ;
      
      rotate ([-90, 0, 0])
         hull()
         {
            translate ([-l/2 + R, 0, -delta])
               cylinder (r = R, h = wall_thickness + 2 * delta) ;

            translate ([l/2 - R, 0, -delta])
               cylinder (r = R, h = wall_thickness + 2 * delta) ;
         }
   }

   h_offs = 1 ;
   h_step = 3 ;
   h_rows = 5 ;
   
   x_cols = 7 ;
   x_step = 12 ;
   x_offs = (case_size[0] - (x_cols-1)*x_step) / 2 ;
   z_base = pcb_offs[2] + pcb_size[2] + 1 ;

   for (i = [0:h_rows-1])
      for (j = [0: x_cols - (i % 2 ? 2 : 1)])
         translate ([j * x_step + (i%2)*(x_step/2) + x_offs, 0, z_base + i * h_step + h_offs])
         {
            if (!find ([0, i, j]))
               vent (7) ;
            
            if (!find ([1, i, j]))
               translate ([0, case_size[1] - wall_thickness, 0])
                  vent (7) ;
         }
}

module case()
{
   difference()
   {
      rcube (case_size, wall_thickness) ;
      
      translate ([wall_thickness, wall_thickness, face_thickness])
         cube (case_size - [2*wall_thickness, 2*wall_thickness, 0]) ;

      translate ([0, 0, -10])
      {
         place_on_pcb([967, 2007])
            translate ([3.03 - 0.5, 3.00 - 0.5])
               cube ([48.96 + 1, 36.72 + 1, 30]) ;
      }
      
      place_ot_conn()
         translate ([-(ot_conn_sz[0] + gap)/2, -delta, -(ot_conn_sz[2] + gap)/2])
         {
            cube ([ot_conn_sz[0] + gap, 10, ot_conn_sz[2] + gap]) ;
            
            translate ([-2, 0, -2])
               cube ([ot_conn_sz[0] + gap + 4, 1, ot_conn_sz[2] + gap + 4]) ;
         }
         
      translate ([7, -delta, case_size[2] - back_thickness - 4])
         cube ([6, wall_thickness + 2*delta, 3]) ;
         
      translate ([0, 0, pcb_offs[2] + pcb_size[2] + 1])
      {
         translate ([19.4, -delta, 0])
            cube ([14.1, wall_thickness + 2*delta, 3]) ;

         translate ([41.6, -delta, 0])
            cube ([10.5, wall_thickness + 2*delta, 3]) ;

         translate ([case_size[0] - wall_thickness - delta, 22.1, 0])
            cube ([wall_thickness + 2*delta, 7, 3]) ;

         translate ([case_size[0] - wall_thickness - delta, 30.3, -0.25])
            cube ([wall_thickness + 2*delta, 7.5 + 0.7, 2.54+0.5]) ;
      }
      
      place_switch()
         translate ([-gap, -delta, -gap/2])
            cube ([switch_sz[0] + 2*gap, wall_thickness + 2*delta, switch_sz[2] + gap]) ;
      
      back_clicks (gap/2) ;

      vent_excludes =
         [ 
            [0, 0, 0], [0, 0, 1], [0, 0, 2], [0, 0, 3], [0, 0, 4], [0, 0, 5],
            [0, 1],
            [0, 2, 0], [0, 2, 1], [0, 2, 4], [0, 2, 5],
            [0, 3, 0], [0, 3, 4], [0, 3, 5],
            [0, 4, 0]
         ] ;

      vents (vent_excludes) ;
   }
   
   bfix_l = case_size[1] ;
   bfix_h = 5 ;
   
   translate ([0, case_size[1] / 2 - bfix_l/2, case_size[2] - back_thickness - bfix_h])
   {
      translate ([wall_thickness - delta, 0])
         hull()
         {
            cube ([delta, bfix_l, delta]) ;
            translate ([0, 0, bfix_h])
               cube ([1, bfix_l, delta]) ;
         }

      translate ([case_size[0] - wall_thickness + delta, 0])
         hull()
         {
            cube ([delta, bfix_l, delta]) ;

            translate ([-1, 0, bfix_h])
               cube ([1, bfix_l, delta]) ;
         }
   }
   
   pcb_holders() ;
}

module back()
{
   module hook (extra_h = delta)
   {
      translate ([-4.5/2, 0, -3])
      {
         cube ([4.5, wall_thickness, 3 + extra_h]) ;

         translate ([0, -1, 3 + extra_h - 1])
            cube ([4.5, wall_thickness + 1, 1]) ;
            
         hull()
         {
            cube ([4.5, wall_thickness, delta]) ;
            translate ([0, 0, 1])
               cube ([4.5, wall_thickness, delta]) ;
            translate ([0, -0.7, 0.7])
               cube ([4.5, wall_thickness + 0.7, delta]) ;
         }
      }
   }
   
   translate ([0, 0, case_size[2] - delta])
      rcube ([case_size[0], case_size[1], 1], wall_thickness) ;

   translate ([wall_thickness + gap/2, 0, case_size[2] - back_thickness])
   {
      translate ([19.6, 0, 0])
      {
         mirror ([0, 1, 0])
            hook (1 + back_thickness) ;

         translate ([0, case_size[1], 0])
            hook (1 + back_thickness) ;
      }

      translate ([67.7, 0, 0])
      {
         mirror ([0, 1, 0])
            hook (1 + back_thickness) ;

         translate ([0, case_size[1], 0])
            hook (1 + back_thickness) ;
      }
   }
   
   translate ([wall_thickness + gap/2, wall_thickness + gap/2, case_size[2] - back_thickness])
   {
      rcube (back_sz, 0.7) ;
      
/*      translate ([19.6, 0, 0])
         hook() ;
         
      translate ([7.5, back_sz[1], 0])
         rotate ([0, 0, 180])
            hook() ;

      translate ([67.7, 0, 0])
      {
         hook() ;

         translate ([0, back_sz[1], 0])
            rotate ([0, 0, 180])
               hook() ;
      }*/
   }

   back_clicks() ;
}

case() ;

translate ([10, 10, 5])
   back() ;

pcb() ;

place_switch()
   rotate ([90, 0, 0])
      switch() ;

place_ot_conn()
   translate ([0, ot_conn_sz[1], 0])
      rotate ([180, 0, 0])
      {
         ot_male (2) ;
         ot_case (2) ;
      }
      