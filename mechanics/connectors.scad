use <rcube.scad>

$fn = 50 ;
delta = 1e-3 ;
ext_h = 2.7 + 1 ;
ext_w = 2.54*1.27 + 1 ;
L = 15 ;
Dint = 0.9 ;
int_w = 2.2 ;
int_h = 2.2 ;

step = 2.54 ;

thick = 1 ;
gap = 0.4 ;

CNT = 2 ;
deep = 9 ;
case_sz = [ext_w + step*(CNT-1) + 2*gap + 2*thick, step + deep, ext_h + 2*gap + 2*thick] ;


module ot_female (cnt)
{
   SZ = [ext_w + step*(cnt-1), L, ext_h] ;
   
   translate([-SZ[0]/2, 0, -SZ[2]/2])
   {
      difference()
      {
         union()
         {
            cube (SZ) ;
            
            hull()
            {
               brim = 2 ;
               cube ([SZ[0], delta, SZ[2]]) ;
               
               translate ([-brim, L - deep, -brim])
                  cube ([SZ[0] + 2*brim, delta, SZ[2] + 2*brim]) ;
            }
         }
         
         for (i = [0 : cnt - 1])
            translate ([i * step, 0])
            {
               translate ([ext_w/2 - Dint/2, -delta, ext_h/2 - Dint/2])
                  cube ([Dint, L + 2*delta, Dint]) ;

               translate ([ext_w / 2,  L - 0.3, ext_h / 2])
                  hull()
                  {
                     cube ([Dint, delta, Dint], center = true) ;
                     translate ([0, 0.5, 0])
                        cube ([ext_w-0.8, delta, ext_h-0.8], center = true) ;
                  }

               translate ([ext_w/2 - int_w/2, -delta, ext_h/2 - int_h/2]) 
                  cube ([int_w, L - 0.5, int_h]) ;

               translate ([ext_w/2 - int_w/2, L - 9, -delta])
                  cube ([int_w, 5, ext_h/2]) ;
            }
      }

      for (i = [0 : cnt - 1])
         translate ([i * step, 0])
            hull()
            {
                  translate ([ext_w/2 - int_w/2 + 1/2, L-9])
                     cube ([int_w - 1, delta, 0.5]) ;

                  translate ([ext_w/2 - int_w/2 + 1/2, L - 9 + 3.2])
                     cube ([int_w - 1, delta, (ext_h - int_h)/2 + 0.4]) ;
            }
   }
}

module ot_female2 (cnt)
{
   SZ = [ext_w + step*(cnt-1), L, ext_h] ;
   SZint = [2.54*cnt, 14, 2.7] + [0.2, 0, 0.2] ;

   translate([-SZ[0]/2, 0, -SZ[2]/2])
      difference()
      {
         union()
         {
            rotate ([-90, 0, 0])
               translate ([0, -SZ[2], 0])
                  rcube ([SZ[0], SZ[2], SZ[1]], 0.5) ;
            
            hull()
            {
               brim = 2 ;
               cube ([SZ[0], delta, SZ[2]]) ;

               translate ([-brim, L - deep, -brim])
               {
                  cube ([SZ[0] + 2*brim, delta, SZ[2] + 2*brim]) ;
                  
                  translate ([0, -1, 0])
                     cube ([SZ[0] + 2*brim, delta, SZ[2] + 2*brim]) ;
               }
            }
         }
         
         translate ([(SZ[0] - SZint[0]) / 2, SZ[1] - SZint[1] + delta, (SZ[2] - SZint[2]) / 2])
            cube (SZint) ;
         
         SZ2 = SZint - [ 0.8, 0, 0.8 ] ;
         translate ([(SZ[0] - SZ2[0]) / 2, -delta, (SZ[2] - SZ2[2]) / 2])
            cube ([SZ2[0], SZ[1] + 2*delta, SZ2[2]]) ;
      }
}

module ot_male (cnt)
{
   translate ([-step*cnt/2, 0, -step/2])
      for (i = [0 : cnt-1])
         translate ([i * step, 0])
         {
            translate ([step / 2 - 0.3, -6+step, step / 2 - 0.3])
               color ([0.85, 0.8, 0])
                  cube ([0.6, 12, 0.6]) ;
            
            color ([0.3, 0.3, 0.3])
               cube ([step, step, step]) ;
         }
}

module ot_case (cnt)
{
   translate ([-case_sz[0]/2, 0, -case_sz[2]/2])
      difference()
      {
         union()
         {
            cube (case_sz) ;

            brim = 2 ;

            translate ([-brim, case_sz[1] - thick, -brim])
               cube ([case_sz[0] + 2*brim, thick, case_sz[2] + 2*brim]) ;
            
            boffs = 0.5 ;

            translate ([0, case_sz[1] - thick - 1.7, case_sz[2]/2])
            {
               translate ([boffs, 0, 0])
                  sphere (d = 2) ;
               
               translate ([case_sz[0] - boffs, 0, 0])
                  sphere (d = 2) ;
            }
         }
         
         translate ([(case_sz[0] - step*cnt)/2 - 0.1, -delta, (case_sz[2] - step)/2])
            cube ([step*cnt + 0.2, case_sz[1] + 2*delta, step]) ;
         
         translate ([thick, step, thick])
            cube ([case_sz[0] - 2*thick, case_sz[1], case_sz[2] - 2*thick]) ;
      }
}

echo ("Case size = ", case_sz[0], " x ", case_sz[1], " x ", case_sz[2]) ;

ot_female2 (CNT) ;

translate ([0, case_sz[1] + L - deep, 0])
   rotate ([180, 0, 0])
   {
      ot_male (CNT) ;
      %ot_case (CNT) ;
   }