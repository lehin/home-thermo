////////////////////////////////////////////////////////////////////////////////////////////
//
// statistics.cpp
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "statistics.h"
#include "declare-pgm-str.h"
#include "util/eeprom.h"
#include "lan/lan-service.h"
#include "lan/tcp-server.h"
#include "app/mqtt-client.h"
#include "hw/ot-comm.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

Statistics STATICSTICS ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   namespace stats
   {
      DECLARE_PGM_STR (power, "Power") ;
      DECLARE_PGM_STR (reset, "Reset") ;
      DECLARE_PGM_STR (brownout, "Brownout") ;
      DECLARE_PGM_STR (watchdog, "Watchdog") ;
      DECLARE_PGM_STR (softreset, "Soft") ;
      DECLARE_PGM_STR (assertion, "Assert") ;
      DECLARE_PGM_STR (booterr, "Booterr") ;
      DECLARE_PGM_STR (wlan_loss, "WLAN loss") ;
      DECLARE_PGM_STR (wlan_clients, "WLAN clients") ;
      DECLARE_PGM_STR (mqtt_reconnections, "MQTT reconnect") ;

      DECLARE_PGM_STR (rs485_success, "Rx success") ;
      DECLARE_PGM_STR (rs485_timeouts, "Rx timeouts") ;
      DECLARE_PGM_STR (rs485_crc_errors, "Rx crc") ;
      DECLARE_PGM_STR (rs485_last_error, "Last failed") ;
      DECLARE_PGM_STR (rs485_last_error_opcode, "Opcode failed") ;

      DECLARE_PGM_STR (opentherm_retrains, "OpenTherm retrains") ;
   }

   const char* const s_boot_stats[util::BootStatistics::_TypeCount] PROGMEM =
   {
      stats::power, stats::reset, stats::brownout, stats::watchdog,
      stats::softreset, stats::assertion, stats::booterr
   } ;
}

////////////////////////////////////////////////////////////////////////////////////////////

void Statistics::query_general (Formatter&& fmt) const
{
   util::BootStatistics stats = util::EEPROM.boot_stats() ;

   for (uint8_t n = 0; n < util::BootStatistics::_TypeCount; ++n)
      fmt ((const char*)pgm_read_word (s_boot_stats + n), _format_value (stats.m_counters[n]), n == 0) ;

   fmt (stats::wlan_loss, _format_value (lan::SERVICE.wlan_loss_count()), false) ;
   fmt (stats::wlan_clients, _format_value (lan::TcpServer::total_connections()), false) ;
   fmt (stats::mqtt_reconnections, _format_value (app::MQTTCLIENT.reconnections_count()), false) ;
}

void Statistics::query_device (Formatter&& fmt) const
{
   #if !defined (SERIALDEBUG)

   auto rs485 = hw::MODBUS.statistics() ;

   fmt (stats::rs485_success, _format_value (rs485.m_success), true) ;
   fmt (stats::rs485_timeouts, _format_value (rs485.m_timeouts), false) ;
   fmt (stats::rs485_crc_errors, _format_value (rs485.m_crc_errors), false) ;

   auto err = bus::MODBUS.last_error() ;
   fmt (stats::rs485_last_error, _format_value ((uint16_t)err.m_addr), false) ;
   fmt (stats::rs485_last_error_opcode, _format_value ((uint16_t)err.m_opcode), false) ;

   #endif

   fmt (stats::opentherm_retrains, _format_value (hw::otcomm::LINK.retrains_counter()), false) ;
}

void Statistics::reset (void)
{
   util::EEPROM.boot_stats() = { 0,0,0,0,0,0,0 } ;
   lan::SERVICE.reset_wlan_loss_count() ;
   app::MQTTCLIENT.reset_reconnections_count() ;
   hw::otcomm::LINK.reset_retrains_counter() ;

   #if !defined (SERIALDEBUG)
   hw::MODBUS.reset_statistics() ;
   bus::MODBUS.reset_last_error() ;
   #endif
}

aux::StringRef Statistics::_format_value (uint16_t val) const
{
   static char buf[5] ;
   return { buf, util::itoa (val, buf) } ;
}

aux::StringRef Statistics::_format_value (uint32_t val) const
{
   static char buf[10] ;
   return { buf, util::itoa (val, buf) } ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
