////////////////////////////////////////////////////////////////////////////////////////////
//
// config-commands.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "cmd-processor.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class ConfigCommands CONFIGCOMMANDS ;

////////////////////////////////////////////////////////////////////////////////////////////

class ConfigCommands : public CmdProcessor
{
   typedef CmdProcessor base_class ;

public:
   ConfigCommands (void) ;

private:
   bool _cmd_discovery (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_dev (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_rtc (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_room (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_mqtt (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_outdoor (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_regulator (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_all_reset (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_wlan_reset (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_mutable_reset (const aux::StringRef&, util::ClientSink& client) const ;

private:
   static const Command s_commands[] PROGMEM ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
