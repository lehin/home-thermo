////////////////////////////////////////////////////////////////////////////////////////////
//
// regulator-commands.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "cmd-processor.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class RegulatorCommands REGULATORCOMMANDS ;

////////////////////////////////////////////////////////////////////////////////////////////

class RegulatorCommands : public CmdProcessor
{
   typedef CmdProcessor base_class ;

public:
   RegulatorCommands (void) ;

private:
   bool _cmd_data (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_setup (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_minmax (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_fallback (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_reset (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_lock (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_unlock (const aux::StringRef&, util::ClientSink& client) const ;

private:
   static const Command s_commands[] PROGMEM ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
