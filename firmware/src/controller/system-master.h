////////////////////////////////////////////////////////////////////////////////////////////
//
// system-master.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "master-commands.h"
#include "aux/string.h"
#include "aux/ring-buffer.h"
#include "lan/tcp-server.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class SystemMaster SYSMASTER ;

////////////////////////////////////////////////////////////////////////////////////////////

class SystemMaster
{
public:
   struct Request
   {
      lan::Socket *m_client ;
      aux::String m_command ;
   } ;

public:
   SystemMaster (void) ;

   void initialize (void) ;

private:
   void _on_connection_accepted (lan::Socket& client) ;

   void _on_receive (Request& current, lan::Socket::RxStream& rx) ;
   void _on_closed (lan::Socket& client) ;
   void _handle_received (void) ;

   lan::TcpServer& _server (void) { return m_server ; }

private:
   typedef aux::RingBuffer<4,Request> Pendings ;
   Pendings m_pendings ;

   lan::TcpServer m_server ;

   class MasterCommands m_master_cmds ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
