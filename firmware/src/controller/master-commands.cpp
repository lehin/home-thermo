////////////////////////////////////////////////////////////////////////////////////////////
//
// master-commands.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "master-commands.h"
#include "master-cmds-tags.h"
#include "bus-commands.h"
#include "config-commands.h"
#include "boiler-commands.h"
#include "system-master.h"
#include "statistics.h"
#include "general-info.h"
#include "peripherals/device-server.h"

#include <avr/eeprom.h>
#include "aux/ll-scheduler.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace tags
{
   DECLARE_PGM_STR (test, "test") ;
}

////////////////////////////////////////////////////////////////////////////////////////////

const CmdProcessor::Command MasterCommands::s_commands[] PROGMEM =
{
   { tags::version, Member<MasterCommands, &MasterCommands::_cmd_version>{} },
   { tags::exit, Member<MasterCommands, &MasterCommands::_cmd_exit>{} },
   { tags::reboot, Member<MasterCommands, &MasterCommands::_cmd_reboot>{} },
   { tags::flash, Member<MasterCommands, &MasterCommands::_cmd_flash>{} },
   { tags::mem, Member<MasterCommands, &MasterCommands::_cmd_mem>{} },
   { tags::uptime, Member<MasterCommands, &MasterCommands::_cmd_uptime>{} },

#ifdef DEBUG_ASSERT
   { tags::debug_assert, Member<MasterCommands, &MasterCommands::_debug_assert>{} },
#endif

   { tags::stats, Member<MasterCommands, &MasterCommands::_cmd_stats>{} },
   { tags::stats_reset, Member<MasterCommands, &MasterCommands::_cmd_stats_reset>{} },
   { tags::bus, Member<MasterCommands, &MasterCommands::_cmd_bus>{} },
   { tags::config, Member<MasterCommands, &MasterCommands::_cmd_config>{} },
   { tags::boiler, Member<MasterCommands, &MasterCommands::_cmd_boiler>{} },

   { tags::test, Member<MasterCommands, &MasterCommands::_cmd_test>{} }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

MasterCommands::MasterCommands (void) :
   base_class (s_commands)
{
}

bool MasterCommands::_cmd_version (const aux::StringRef&, util::ClientSink& client) const
{
   GENERALINFO.ver (client) ;
   client << util::endl{} ;
   GENERALINFO.built (client) ;
   client << util::endl{} << Result::Ok ;
   return true ;
}

bool MasterCommands::_cmd_exit (const aux::StringRef&, util::ClientSink& client) const
{
   client << Result::Ok ;
   return false ;
}

bool MasterCommands::_cmd_reboot (const aux::StringRef& parms, util::ClientSink& client) const
{
   if (parms.empty())
   {
      do_reboot (client, util::BootFlag::SoftReset) ;
      return false ;
   }

   auto itprm = parms.begin() ;
   for (; itprm != parms.end() && *itprm != ' '; ++itprm) ;

   auto devices = peripherals::DEVSERVER.devices() ;
   auto itdev = devices.find ({ parms.begin(), itprm }) ;

   if (itdev == devices.end())
      client << Result::Error ;
   else
      itdev->reboot (aux::forward (client)) ;

   return true ;
}

bool MasterCommands::_cmd_flash (const aux::StringRef&, util::ClientSink& client) const
{
   do_reboot (client, util::BootFlag::TryFlash) ;
   return false ;
}

bool MasterCommands::_cmd_mem (const aux::StringRef&, util::ClientSink& client) const
{
   GENERALINFO.mem (client) ;
   client << util::endl{} << Result::Ok ;

   return true ;
}

bool MasterCommands::_cmd_uptime (const aux::StringRef&, util::ClientSink& client) const
{
   GENERALINFO.uptime (client) ;
   client << util::endl{} << Result::Ok ;

   return true ;
}

#ifdef DEBUG_ASSERT
bool MasterCommands::_debug_assert (const aux::StringRef&, util::ClientSink& client) const
{
   auto info = aux::read_debug_assert_info() ;

   char *end = info.m_ctx ;
   for (int8_t n = 0; n < sizeof (info.m_ctx) && *end; ++n, ++end) ;

   client << aux::range (info.m_ctx + 0, end) << util::endl{} << Result::Ok ;

   aux::write_debug_assert_info (nullptr) ;

   return true ;
}
#endif

bool MasterCommands::_cmd_stats (const aux::StringRef&, util::ClientSink& client) const
{
   auto formatter =
      [&client] (const char *pname, const aux::StringRef& val, bool)
      {
         client << util::pstr{ pname } << ':' << ' ' << val << util::endl{} ;
      } ;

   client << util::divider{} ;
   STATICSTICS.query_general (formatter) ;

   client << util::divider{} ;
   STATICSTICS.query_device (formatter) ;

   client << util::divider{} << Result::Ok ;

   return true ;
}

bool MasterCommands::_cmd_stats_reset (const aux::StringRef&, util::ClientSink& client) const
{
   STATICSTICS.reset() ;
   client << Result::Ok ;
   return true ;
}

bool MasterCommands::_cmd_bus (const aux::StringRef& cmd, util::ClientSink& client) const
{
   return BUSCOMMANDS.on_command (cmd, client) ;
}

bool MasterCommands::_cmd_config (const aux::StringRef& cmd, util::ClientSink& client) const
{
   return CONFIGCOMMANDS.on_command (cmd, client) ;
}

bool MasterCommands::_cmd_boiler (const aux::StringRef& cmd, util::ClientSink& client) const
{
   return BOILERCOMMANDS.on_command (cmd, client) ;
}

bool MasterCommands::_cmd_test (const aux::StringRef& parms, util::ClientSink& client) const
{
/*   char buf[2] ;

   for (uint16_t addr = 0; addr <= E2END; ++addr)
   {
      util::itoah (eeprom_read_byte ((const uint8_t*)addr), buf) ;
      client << aux::range (buf) ;
      if (addr % 32 == 31)
         client << util::endl{} ;
      else
         client << ' ' ;
   }

   client << util::endl{} << Result::Ok ;*/

   aux::LLSCHED.schedule
      (
         [] { aux::debug_assert(false, PSTR ("TEST ASSERT")  ) ; },
         PSTR("TEST SHCHED")
      ) ;

   return false ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
