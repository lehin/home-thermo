////////////////////////////////////////////////////////////////////////////////////////////
//
// config-commands.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "config-commands.h"
#include "rtc-commands.h"
#include "room-commands.h"
#include "mqtt-commands.h"
#include "outdoor-commands.h"
#include "regulator-commands.h"
#include "peripherals/device-server.h"
#include "peripherals/rtc.h"
#include "util/eeprom.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

ConfigCommands CONFIGCOMMANDS ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace tags
{
   DECLARE_PGM_STR (discovery, "discovery") ;
   DECLARE_PGM_STR (dev, "dev") ;
   DECLARE_PGM_STR (rtc, "rtc") ;
   DECLARE_PGM_STR (room, "room") ;
   DECLARE_PGM_STR (mqtt, "mqtt") ;
   DECLARE_PGM_STR (outdoor, "outdoor") ;
   DECLARE_PGM_STR (regulator, "regulator") ;
   DECLARE_PGM_STR (all_reset, "all-reset") ;
   DECLARE_PGM_STR (wlan_reset, "wlan-reset") ;
   DECLARE_PGM_STR (mutable_reset, "mutable-reset") ;
}

////////////////////////////////////////////////////////////////////////////////////////////

const CmdProcessor::Command ConfigCommands::s_commands[] PROGMEM =
{
   { tags::discovery, Member<ConfigCommands, &ConfigCommands::_cmd_discovery>{} },
   { tags::dev, Member<ConfigCommands, &ConfigCommands::_cmd_dev>{} },
   { tags::rtc, Member<ConfigCommands, &ConfigCommands::_cmd_rtc>{} },
   { tags::room, Member<ConfigCommands, &ConfigCommands::_cmd_room>{} },
   { tags::mqtt, Member<ConfigCommands, &ConfigCommands::_cmd_mqtt>{} },
   { tags::outdoor, Member<ConfigCommands, &ConfigCommands::_cmd_outdoor>{} },
   { tags::regulator, Member<ConfigCommands, &ConfigCommands::_cmd_regulator>{} },

   { tags::all_reset, Member<ConfigCommands, &ConfigCommands::_cmd_all_reset>{} },
   { tags::wlan_reset, Member<ConfigCommands, &ConfigCommands::_cmd_wlan_reset>{} },
   { tags::mutable_reset, Member<ConfigCommands, &ConfigCommands::_cmd_mutable_reset>{} },
} ;

////////////////////////////////////////////////////////////////////////////////////////////

ConfigCommands::ConfigCommands (void) :
   base_class (s_commands)
{
}

namespace
{
   static void _send_onewire_id (util::ClientSink& client, const bus::OneWire::DEVID& id)
   {
      client << util::dev_id_to_string (id) << util::endl{} ;
   }
}

bool ConfigCommands::_cmd_discovery (const aux::StringRef&, util::ClientSink& client) const
{
   client << util::divider{} ;

   bool ok = bus::ONEWIRE.discovery ([&client] (const bus::OneWire::DEVID& id) { _send_onewire_id (client, id) ; }) ;

   client << util::divider{} << (ok ? Result::Ok : Result::Error) ;
   return true ;
}

namespace
{
   void _list_devices (util::ClientSink& client)
   {
      client << util::divider{} ;

      for (auto& dev : peripherals::DEVSERVER.devices())
         client << util::pstr{ dev.name() } << util::endl{} ;

      client << util::divider{} << util::ClientSink::Result::Ok ;
   }
}

bool ConfigCommands::_cmd_dev (const aux::StringRef& parms, util::ClientSink& client) const
{
   if (parms.empty())
   {
      _list_devices (client) ;
      return true ;
   }

   auto itprm = parms.begin() ;
   for (; itprm != parms.end() && *itprm != ' '; ++itprm) ;

   auto devices = peripherals::DEVSERVER.devices() ;
   auto itdev = devices.find ({ parms.begin(), itprm }) ;

   if (itdev == devices.end())
      client << Result::Error ;
   else
   {
      for (; itprm != parms.end() && *itprm == ' '; ++itprm) ;
      itdev->command ({ itprm, parms.end() }, aux::forward (client)) ;
   }

   return true ;
}

bool ConfigCommands::_cmd_rtc (const aux::StringRef& cmd, util::ClientSink& client) const
{
   return RTCCOMANDS.on_command (cmd, client) ;
}

bool ConfigCommands::_cmd_room (const aux::StringRef& cmd, util::ClientSink& client) const
{
   return ROOMCOMMANDS.on_command (cmd, client) ;
}

bool ConfigCommands::_cmd_mqtt (const aux::StringRef& cmd, util::ClientSink& client) const
{
   return MQTTCOMMANDS.on_command (cmd, client) ;
}

bool ConfigCommands::_cmd_outdoor (const aux::StringRef& cmd, util::ClientSink& client) const
{
   return OUTDOORCOMMANDS.on_command (cmd, client) ;
}

bool ConfigCommands::_cmd_regulator (const aux::StringRef& cmd, util::ClientSink& client) const
{
   return REGULATORCOMMANDS.on_command (cmd, client) ;
}

namespace
{
   static bool _reboot (const CmdProcessor& proc, util::ClientSink& client)
   {
      client << util::pstr{ PSTR("Rebooting...") } << util::endl{} ;
      proc.do_reboot (client, util::BootFlag::SoftReset) ;
      return false ;
   }
}

bool ConfigCommands::_cmd_all_reset (const aux::StringRef&, util::ClientSink& client) const
{
   util::EEPROM.clear_all() ;
   return _reboot (*this, client) ;
}

bool ConfigCommands::_cmd_wlan_reset (const aux::StringRef&, util::ClientSink& client) const
{
   util::EEPROM.wlan_creds().reset() ;
   return _reboot (*this, client) ;
}

bool ConfigCommands::_cmd_mutable_reset (const aux::StringRef&, util::ClientSink& client) const
{
   util::EEPROM.clear_mutable() ;
   client << Result::Ok ;
   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
