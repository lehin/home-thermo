////////////////////////////////////////////////////////////////////////////////////////////
//
// mqtt-commands.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "cmd-processor.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class MqttCommands MQTTCOMMANDS ;

////////////////////////////////////////////////////////////////////////////////////////////

class MqttCommands : public CmdProcessor
{
   typedef CmdProcessor base_class ;

public:
   MqttCommands (void) ;

private:
   bool _cmd_setup (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_list (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_subscribe (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_unsubscribe (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_state (const aux::StringRef&, util::ClientSink& client) const ;

private:
   static const Command s_commands[] PROGMEM ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
