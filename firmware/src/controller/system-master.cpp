////////////////////////////////////////////////////////////////////////////////////////////
//
// system-master.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "system-master.h"
#include "aux/array.h"
#include "aux/debug-assert.h"
#include "aux/ll-scheduler.h"
#include "aux/shared-ptr.h"
#include "hw/rak439/rxstream.h"

static constexpr aux::String::size_type RECEIVE_BUFFER_SIZE = 128 ;

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

SystemMaster SYSMASTER ;

////////////////////////////////////////////////////////////////////////////////////////////

SystemMaster::SystemMaster (void) :
   m_server{ 755 }
{
}

void SystemMaster::initialize (void)
{
   _server().on_accepted ([] (lan::Socket& sock) { SYSMASTER._on_connection_accepted (sock) ; }) ;
   _server().on_closed ([] (auto& sock) { SYSMASTER._on_closed (sock) ; }) ;
   _server().initialize() ;
}

void SystemMaster::_on_connection_accepted (lan::Socket& client)
{
   client.on_receive
      (
         [r = aux::shared_ptr<SystemMaster::Request>::create (&client)] (auto& rcv)
         {
            SYSMASTER._on_receive (*r, rcv.m_rx) ;
         }
      ) ;
}

void SystemMaster::_on_receive (Request& current, lan::Socket::RxStream& rx)
{
   while (rx.unread())
   {
      char ch = rx.read() ;
      if (ch == '\n')
         continue ;

      if (ch != '\r')
      {
         if (!current.m_command.capacity())
            current.m_command = aux::String{ RECEIVE_BUFFER_SIZE } ;

         current.m_command.push_back (ch) ;
      }
      else
      {
         bool empty = m_pendings.empty() ;

         m_pendings.push_back (current) ;
         current.m_command.reset() ;

         if (empty)
            aux::LLSCHED.schedule ([] { SYSMASTER._handle_received() ; }, PSTR ("SYSMASTER REC")) ;
      }
   }
}

void SystemMaster::_on_closed (lan::Socket& client)
{
   for (auto& item : m_pendings)
      if (item.m_client == &client)
         item.m_client = nullptr ;
}

void SystemMaster::_handle_received (void)
{
   while (!m_pendings.empty())
   {
      auto top = m_pendings.pop_front() ;
      if (!top.m_client)
         continue ;

      util::ClientSink sink{ *top.m_client } ;
      if (!m_master_cmds.on_command (aux::range (top.m_command), sink))
      {
         for (auto& item : m_pendings)
            if (item.m_client == top.m_client)
               item.m_client = nullptr ;

         sink.commit() ;
         top.m_client->close() ;
      }
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
