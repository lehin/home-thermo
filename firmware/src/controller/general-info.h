////////////////////////////////////////////////////////////////////////////////////////////
//
// general-info.h
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "util/client-sink.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

extern struct GeneralInfo GENERALINFO ;

////////////////////////////////////////////////////////////////////////////////////////////

struct GeneralInfo
{
   void ver (util::ClientSink& sink) ;
   void built (util::ClientSink& sink) ;
   void mem (util::ClientSink& sink) ;
   void uptime (util::ClientSink& sink) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
