////////////////////////////////////////////////////////////////////////////////////////////
//
// master-commands.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "cmd-processor.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

class MasterCommands : public CmdProcessor
{
   typedef CmdProcessor base_class ;

public:
   MasterCommands (void) ;

private:
   bool _cmd_version (const aux::StringRef& cmd, util::ClientSink& client) const ;
   bool _cmd_exit (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_reboot (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_flash (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_mem (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_uptime (const aux::StringRef&, util::ClientSink& client) const ;

#ifdef DEBUG_ASSERT
   bool _debug_assert (const aux::StringRef&, util::ClientSink& client) const ;
#endif

   bool _cmd_stats (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_stats_reset (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_bus (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_config (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_boiler (const aux::StringRef&, util::ClientSink& client) const ;

   bool _cmd_test (const aux::StringRef&, util::ClientSink& client) const ;

private:
   static const Command s_commands[] PROGMEM ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
