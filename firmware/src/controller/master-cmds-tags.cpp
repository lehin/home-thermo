////////////////////////////////////////////////////////////////////////////////////////////
//
// master-cmds-tags.cpp
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "master-cmds-tags.h"

namespace controller
{
   namespace tags
   {

////////////////////////////////////////////////////////////////////////////////////////////

DECLARE_PGM_STR (version, "ver") ;
DECLARE_PGM_STR (exit, "exit") ;
DECLARE_PGM_STR (reboot, "reboot") ;
DECLARE_PGM_STR (flash, "flash") ;
DECLARE_PGM_STR (mem, "mem") ;
DECLARE_PGM_STR (uptime, "uptime") ;
DECLARE_PGM_STR (debug_assert, "debug-assert") ;
DECLARE_PGM_STR (stats, "stat") ;
DECLARE_PGM_STR (stats_reset, "stat-reset") ;
DECLARE_PGM_STR (bus, "bus") ;
DECLARE_PGM_STR (config, "config") ;
DECLARE_PGM_STR (boiler, "boiler") ;

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace tags
} // namespace controller
