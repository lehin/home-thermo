////////////////////////////////////////////////////////////////////////////////////////////
//
// general-info.cpp
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "general-info.h"
#include "util/strconv.h"
#include "peripherals/rtc.h"

extern "C" { extern uint16_t __brkval ; }

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

static const char _ver[] PROGMEM = "OpenTherm home controller ver 0.1" ;
static const char _built[] PROGMEM = __DATE__ " " __TIME__ ;

////////////////////////////////////////////////////////////////////////////////////////////

GeneralInfo GENERALINFO ;

////////////////////////////////////////////////////////////////////////////////////////////

void GeneralInfo::ver (util::ClientSink& sink)
{
   sink << util::pstr{ _ver } ;
}

void GeneralInfo::built (util::ClientSink& sink)
{
   sink << util::pstr{ _built } ;
}

void GeneralInfo::mem (util::ClientSink& sink)
{
   size_t start = (size_t)__malloc_heap_start ;
   size_t end = (size_t)__brkval ;
   size_t sp = SP ;

   char res[] = "    -     SP=        " ;

   char *it = util::itoah (start, res) ;
   it = util::itoah (end, it + 1) ;
   util::itoah (sp, it + 4) ;

   sink << res  ;
}

void GeneralInfo::uptime (util::ClientSink& sink)
{
   auto start = peripherals::RTC.start_time() ;

   int32_t diff = peripherals::RTC.date_time() - start ;
   int32_t days = diff / peripherals::Rtc::Time::day_seconds() ;
   diff %= peripherals::Rtc::Time::day_seconds() ;

   char buf[16] ;
   auto end = util::itoa ((unsigned)days, buf) ;
   sink << aux::range (buf + 0, end) << util::pstr{ PSTR(" days ") } ;

   end = peripherals::Rtc::Time::from_seconds (diff) . to_string (buf) ;
   sink << aux::range (buf + 0, end) << util::pstr{ PSTR(" since ") } ;

   end = start.to_string (buf, true) ;
   sink << aux::range (buf + 0, end) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
