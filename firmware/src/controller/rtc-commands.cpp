////////////////////////////////////////////////////////////////////////////////////////////
//
// rtc-commands.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "rtc-commands.h"
#include "misc/ntp-client.h"
#include "peripherals/rtc.h"
#include "util/eeprom.h"
#include "util/strconv.h"
#include "aux/ll-scheduler.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

RtcCommands RTCCOMANDS ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   namespace tags
   {
      DECLARE_PGM_STR (show, "show") ;
      DECLARE_PGM_STR (date, "date") ;
      DECLARE_PGM_STR (time, "time") ;
      DECLARE_PGM_STR (ntp, "ntp") ;
      DECLARE_PGM_STR (sync, "sync") ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

const CmdProcessor::Command RtcCommands::s_commands[] PROGMEM =
{
   { tags::show, Member<RtcCommands, &RtcCommands::_cmd_show>{} },
   { tags::date, Member<RtcCommands, &RtcCommands::_cmd_date>{} },
   { tags::time, Member<RtcCommands, &RtcCommands::_cmd_time>{} },
   { tags::ntp, Member<RtcCommands, &RtcCommands::_cmd_ntp>{} },
   { tags::sync, Member<RtcCommands, &RtcCommands::_cmd_sync>{} }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

RtcCommands::RtcCommands (void) :
   base_class{ s_commands }
{
}

bool RtcCommands::_cmd_show (const aux::StringRef&, util::ClientSink& client) const
{
   char buf[32] ;
   auto end = peripherals::RTC.date_time().to_string (buf, true) ;
   client << aux::range (buf + 0, end) << util::endl{} << Result::Ok ;
   return true ;
}

bool RtcCommands::_cmd_date (const aux::StringRef& parms, util::ClientSink& client) const
{
   if (parms.empty())
   {
      char buf[10] ;
      auto end = peripherals::RTC.date().to_string (buf) ;
      client << aux::range (buf + 0, end) << util::endl{} << Result::Ok ;
      return true ;
   }
   else
   {
      util::Date dt ;

      auto str = parms.begin(),
           end = parms.end() ;

      for (int8_t n = 0; n < 3; ++n)
      {
         if (str == end)
            return  (client << Result::Error, true) ;

         const char *err ;
         const char *last = aux::range(str,end).find ('/') ;

         auto val = util::atoiw (str, last, &err) ;
         if (!val)
            return  (client << Result::Error, true) ;

         switch (n)
         {
            case 0: dt.m_day = val ; break ;
            case 1: dt.m_month = val ; break ;
            case 2: dt.m_year = val ; break ;
            default: ;
         }

         if (err != last)
            return  (client << Result::Error, true) ;

         str = last ;

         if (str != end)
            ++str ;
      }

      peripherals::RTC.date (dt) ;
      client << Result::Ok ;
   }

   return true ;
}

bool RtcCommands::_cmd_time (const aux::StringRef& parms, util::ClientSink& client) const
{
   if (parms.empty())
   {
      char buf[8] ;
      auto end = peripherals::RTC.time().to_string (buf) ;
      client << aux::range (buf + 0, end) << util::endl{} << Result::Ok ;
      return true ;
   }
   else
   {
      util::Time tm ;

      auto str = parms.begin(),
           end = parms.end() ;

      for (int8_t n = 0; n < 3; ++n)
      {
         if (str == end)
            return  (client << Result::Error, true) ;

         const char *err ;
         const char *last = aux::range(str,end).find (':') ;

         auto val = util::atoib (str, last, &err) ;

         switch (n)
         {
            case 0: tm.m_hour = val ; break ;
            case 1: tm.m_min = val ; break ;
            case 2: tm.m_sec = val ; break ;
            default: ;
         }

         if (err != last)
            return  (client << Result::Error, true) ;

         str = last ;

         if (str != end)
            ++str ;
      }

      peripherals::RTC.time (tm) ;
      client << Result::Ok ;
   }

   return true ;
}

bool RtcCommands::_cmd_ntp (const aux::StringRef& cmd, util::ClientSink& client) const
{
   if (cmd.empty())
   {
      char buf[24] ;

      client << util::pstr{ PSTR ("Addr: ") } ;

      util::Eeprom::NtpParms prms = util::EEPROM.ntp_parms() ;
      client << prms.ntp_server.get() << util::endl{} ;

      auto end = util::itoa (prms.ntp_offset, buf) ;
      client << util::pstr{ PSTR ("Zone: ") } << aux::range (buf + 0, end) << util::endl{} ;

      auto stat = misc::NTPCLIENT.status() ;

      switch (stat.m_result)
      {
         case misc::NetQueryHelper::Status::Result::No:
            client << util::pstr{ PSTR ("No sync") } << util::endl{} ;
            break ;

         case misc::NetQueryHelper::Status::Result::Ok:
         {
            auto end = stat.m_tm.to_string (buf) ;
            client << util::pstr{ PSTR ("Sync: ") } << aux::range (buf + 0, end) << util::endl{} ;

            end = misc::NTPCLIENT.next().to_string (buf) ;
            client << util::pstr{ PSTR ("Next: ") } << aux::range (buf + 0, end) << util::endl{} ;

            end = util::itoa ((int32_t)stat.m_offset, buf) ;
            client << util::pstr{ PSTR ("Diff: ") } << aux::range (buf + 0, end) << util::endl{} ;
            break ;
         }

         case misc::NetQueryHelper::Status::Result::Failed:
         {
            auto end = stat.m_tm.to_string (buf) ;
            client << util::pstr{ PSTR ("Failed: ") } << aux::range (buf + 0, end) << util::endl{} ;

            end = misc::NTPCLIENT.next().to_string (buf) ;
            client << util::pstr{ PSTR ("Next: ") } << aux::range (buf + 0, end) << util::endl{} ;
            break ;
         }

         case misc::NetQueryHelper::Status::Result::Pending:
         {
            client << util::pstr{ PSTR ("Pending...") } << util::endl{} ;
            break ;
         }
      }
   }
   else
   {
      const char *src = cmd.begin() ;

      for (; src != cmd.end() && *src != ' '; ++src) ;
      aux::StringRef server{ cmd.begin(), src } ;

      if (server.empty())
         return (client << Result::Error, true) ;

      for (; src != cmd.end() && *src == ' '; ++src) ;

      if (src == cmd.end())
         return (client << Result::Error, true) ;

      auto offset = util::atol (src, cmd.end(), &src) ;

      for (; src != cmd.end() && *src == ' '; ++src) ;

      auto period = util::Time::from_string ({ src, cmd.end() }) ;
      if (!period.m_hour && !period.m_min && !period.m_sec)
         return (client << Result::Error, true) ;

      misc::NTPCLIENT.setup (server, offset, period) ;
   }

   client << Result::Ok ;
   return true ;
}

bool RtcCommands::_cmd_sync (const aux::StringRef&, util::ClientSink& client) const
{
   aux::LLSCHED.schedule ([] { misc::NTPCLIENT.query_now() ; }, PSTR ("RTC CMD SYNC")) ;
   client << Result::Ok ;
   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
