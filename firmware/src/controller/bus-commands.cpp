////////////////////////////////////////////////////////////////////////////////////////////
//
// bus-commands.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "bus-commands.h"
#include "bus-accessor.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

BusCommands BUSCOMMANDS ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace tags
{
   DECLARE_PGM_STR (acquire, "acquire") ;
   DECLARE_PGM_STR (release, "release") ;
}

////////////////////////////////////////////////////////////////////////////////////////////

const CmdProcessor::Command BusCommands::s_commands[] PROGMEM =
{
   { tags::acquire, Member<BusCommands, &BusCommands::_cmd_acquire>{} },
   { tags::release, Member<BusCommands, &BusCommands::_cmd_release>{} }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

BusCommands::BusCommands (void) :
   base_class{ s_commands }
{
}

bool BusCommands::_cmd_acquire (const aux::StringRef& parms, util::ClientSink& client) const
{
   auto bus = static_cast<BusAccessor::Bus> (util::atoib (parms.begin(), parms.end())) ;
   client << (BUSACCESS.acquire (bus) ? Result::Ok : Result::Error) ;
   return true ;
}

bool BusCommands::_cmd_release (const aux::StringRef&, util::ClientSink& client) const
{
   BUSACCESS.release() ;
   client << Result::Ok ;
   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
