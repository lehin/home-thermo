////////////////////////////////////////////////////////////////////////////////////////////
//
// outdoor-commands.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "outdoor-commands.h"
#include "app/outdoor.h"
#include "util/tokenizer.h"
#include "util/utf-8.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

OutdoorCommands OUTDOORCOMMANDS ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   namespace tags
   {
      DECLARE_PGM_STR (temp, "temp") ;
      DECLARE_PGM_STR (setup, "setup") ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

const CmdProcessor::Command OutdoorCommands::s_commands[] PROGMEM =
{
   { tags::temp, Member<OutdoorCommands, &OutdoorCommands::_cmd_temp>{} },
   { tags::setup, Member<OutdoorCommands, &OutdoorCommands::_cmd_setup>{} }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

OutdoorCommands::OutdoorCommands (void) :
   base_class{ s_commands }
{
}

bool OutdoorCommands::_cmd_temp (const aux::StringRef&, util::ClientSink& client) const
{
   client << app::OUTDOOR.temperature().format (true) << util::endl{} << Result::Ok ; ;
   return true ;
}

bool OutdoorCommands::_cmd_setup (const aux::StringRef& parms, util::ClientSink& client) const
{
   bool ok = true ;

   if (parms.empty())
   {
      auto s = app::OUTDOOR.settings() ;

      char buf[3] ;
      char *end = util::itoa (s.m_subnum, buf) ;

      client << util::pstr{ PSTR("Subscription: ") } << aux::range (buf + 0, end) << util::endl{} ;
      client << util::pstr{ PSTR("Key: ") } ;

      if (util::is_ascii_string (range (s.m_key)))
         client << s.m_key ;
      else
         client << util::utf8_encode (range (s.m_key)) ;

       client << util::endl{} ;
   }
   else
   {
      app::Outdoor::Settings s = { 0xff, {} } ;
      uint8_t idx = 0 ;

      ok =
         util::tokenize
            (
               parms,
               [&] (const aux::StringRef& token)
               {
                  switch (idx)
                  {
                     case 0:
                        const char *err ;

                        s.m_subnum = util::atoib (token.begin(), token.end(), &err) ;
                        if (err != token.end())
                           return false ;

                        break ;

                     case 1:
                        if (token.empty())
                           return false ;

                        if (util::is_ascii_string (token))
                           s.m_key = token ;
                        else
                           s.m_key = util::utf8_decode (token) ;

                        break ;
                  }
                  ++idx ;
                  return true ;
               }
            ) ;

      ok = ok && idx == 2 ;
      ok = ok && app::OUTDOOR.setup (s) ;
   }

   client << (ok ? Result::Ok : Result::Error) ;
   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
