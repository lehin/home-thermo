////////////////////////////////////////////////////////////////////////////////////////////
//
// boiler-commands.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "boiler-commands.h"
#include "util/tokenizer.h"
#include "app/boiler.h"
#include "peripherals/opentherm.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

BoilerCommands BOILERCOMMANDS ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   namespace tags
   {
      DECLARE_PGM_STR (mode, "mode") ;
      DECLARE_PGM_STR (dhw, "dhw") ;
      DECLARE_PGM_STR (standby, "standby") ;
      DECLARE_PGM_STR (info, "info") ;
      DECLARE_PGM_STR (reset, "reset") ;
      DECLARE_PGM_STR (fault, "fault") ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

const CmdProcessor::Command BoilerCommands::s_commands[] PROGMEM =
{
   { tags::mode, Member<BoilerCommands, &BoilerCommands::_cmd_mode>{} },
   { tags::dhw, Member<BoilerCommands, &BoilerCommands::_cmd_dhw>{} },
   { tags::standby, Member<BoilerCommands, &BoilerCommands::_cmd_standby>{} },
   { tags::info, Member<BoilerCommands, &BoilerCommands::_cmd_info>{} },
   { tags::reset, Member<BoilerCommands, &BoilerCommands::_cmd_reset>{} },
   { tags::fault, Member<BoilerCommands, &BoilerCommands::_cmd_fault>{} }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

BoilerCommands::BoilerCommands (void) :
   base_class{ s_commands }
{
}

bool BoilerCommands::_cmd_mode (const aux::StringRef& parms, util::ClientSink& client) const
{
   bool ok = true ;

   if (parms.empty())
   {
      static const char _names_off[] PROGMEM = "0: Off" ;
      static const char _names_winter[] PROGMEM = "1: Winter" ;
      static const char _names_summer[] PROGMEM = "2: Summer" ;
      static const char _names_standby[] PROGMEM = "3: Standby" ;
      static const char* const _names[] PROGMEM = { _names_off, _names_winter, _names_summer, _names_standby } ;

      auto mode = app::BOILER.get_mode() ;
      auto name = (const char*)pgm_read_ptr (_names + (uint8_t)mode) ;

      client << util::pstr{ name } << util::endl{} ;
   }
   else
   {
      const char *err ;
      auto mode = (app::Boiler::Mode) util::atoib (parms.begin(), parms.end(), &err) ;
      ok = err == parms.end() ;
      ok = ok && app::BOILER.set_mode (mode) ;
   }

   client << (ok ? Result::Ok : Result::Error) ;
   return true ;
}

namespace
{
   typedef util::TempVal (*BoilerGetTemp) (void) ;
   typedef bool (*BoilerSetTemp) (util::TempVal) ;

   bool _boiler_temp_impl (const aux::StringRef& parms, util::ClientSink& client, BoilerGetTemp getter, BoilerSetTemp setter)
   {
      bool ok = true ;

      if (parms.empty())
         client << getter().format (false, true) << util::endl{} ;
      else
      {
         const char *err ;
         auto t = util::atoib (parms.begin(), parms.end(), &err) ;
         ok = err == parms.end() ;
         ok = ok && setter ({ (int8_t)t, 0 }) ;
      }

      client << (ok ? util::ClientSink::Result::Ok : util::ClientSink::Result::Error) ;
      return true ;
   }
}

bool BoilerCommands::_cmd_dhw (const aux::StringRef& parms, util::ClientSink& client) const
{
   return
      _boiler_temp_impl
         (
            parms, client,
            [] { return app::BOILER.dhw_temp() ; },
            [] (auto t) { return app::BOILER.dhw_temp (t) ; }
         ) ;
}

bool BoilerCommands::_cmd_standby (const aux::StringRef& parms, util::ClientSink& client) const
{
   return
      _boiler_temp_impl
         (
            parms, client,
            [] { return app::BOILER.standby_temp() ; },
            [] (auto t) { return app::BOILER.standby_temp (t) ; }
         ) ;
}

bool BoilerCommands::_cmd_info (const aux::StringRef&, util::ClientSink& client) const
{
   static const char _regulating[] PROGMEM = ", regulating" ;

   auto running_mode = peripherals::OPENTHERM.get_mode() ;

   client << util::pstr{ PSTR ("T(ch): ") } << app::BOILER.ch_temp().format() ;
   if (running_mode & peripherals::OpenTherm::Mode::Ch)
      client << util::pstr{ _regulating } ;
   client << util::endl{} ;

   client << util::pstr{ PSTR ("T(dhw): ") } << app::BOILER.dhw_temp().format() ;
   if (running_mode & peripherals::OpenTherm::Mode::Dhw)
      client << util::pstr{ _regulating } ;
   client << util::endl{} ;

   client << util::pstr{ PSTR ("T(standby): ") } << app::BOILER.standby_temp().format() << util::endl{} ;

   client << util::pstr{ PSTR ("T(actual): ") }
          << peripherals::OPENTHERM.actual_temp().format()
          << util::pstr{ running_mode & peripherals::OpenTherm::Mode::Dhw ? PSTR(" (DHW)") : PSTR(" (CH)") } << util::endl{} ;

   client << util::pstr{ PSTR ("Flame: ") } << util::pstr{ peripherals::OPENTHERM.flame_active() ? PSTR("ACTIVE") : PSTR("NOT ACTIVE") } << util::endl{} ;

   char buf[2] ;
   util::itoah (peripherals::OPENTHERM.status_byte(), buf) ;
   client << util::pstr{ PSTR ("Status: ") } << aux::range (buf) << util::endl{} ;

   client << util::ClientSink::Result::Ok ;
   return true ;
}

bool BoilerCommands::_cmd_reset (const aux::StringRef&, util::ClientSink& client) const
{
   peripherals::OPENTHERM.reset
      (
         [ sock = client.hold() ] (bool ok, uint8_t, uint8_t)
         {
            util::ClientSink{ sock } << (ok ? util::ClientSink::Result::Ok : util::ClientSink::Result::Error) ;
         }
      ) ;

   return true ;
}

bool BoilerCommands::_cmd_fault (const aux::StringRef&, util::ClientSink& client) const
{
   peripherals::OPENTHERM.fault_data
      (
         [ sock = client.hold() ] (bool ok, uint8_t d0, uint8_t d1)
         {
            util::ClientSink cli{ sock } ;

            if (!ok)
               cli << util::ClientSink::Result::Error ;
            else
            {
               char flags[2],
                    code[3] ;

               util::itoah (d0, flags) ;
               auto code_e = util::itoa (d1, code) ;

               cli << util::pstr{ PSTR("Flags: ") } << aux::range (flags) << util::endl{}
                   << util::pstr{ PSTR("OEM code: ") } << aux::range (code + 0, code_e) << util::endl{}
                   << util::ClientSink::Result::Ok ;
            }
         }
      ) ;

   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
