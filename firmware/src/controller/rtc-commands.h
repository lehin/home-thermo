////////////////////////////////////////////////////////////////////////////////////////////
//
// rtc-commands.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "cmd-processor.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class RtcCommands RTCCOMANDS ;

////////////////////////////////////////////////////////////////////////////////////////////

class RtcCommands : public CmdProcessor
{
   typedef CmdProcessor base_class ;

public:
   RtcCommands (void) ;

private:
   bool _cmd_show (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_date (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_time (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_ntp (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_sync (const aux::StringRef&, util::ClientSink& client) const ;

private:
   static const Command s_commands[] PROGMEM ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
