////////////////////////////////////////////////////////////////////////////////////////////
//
// regulator-commands.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "regulator-commands.h"
#include "app/regulator.h"
#include "util/tokenizer.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

RegulatorCommands REGULATORCOMMANDS ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   namespace tags
   {
      DECLARE_PGM_STR (data, "data") ;
      DECLARE_PGM_STR (setup, "setup") ;
      DECLARE_PGM_STR (minmax, "minmax") ;
      DECLARE_PGM_STR (fallback, "fallback") ;
      DECLARE_PGM_STR (reset, "reset") ;
      DECLARE_PGM_STR (lock, "lock") ;
      DECLARE_PGM_STR (unlock, "unlock") ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

const CmdProcessor::Command RegulatorCommands::s_commands[] PROGMEM =
{
   { tags::data, Member<RegulatorCommands, &RegulatorCommands::_cmd_data>{} },
   { tags::setup, Member<RegulatorCommands, &RegulatorCommands::_cmd_setup>{} },
   { tags::minmax, Member<RegulatorCommands, &RegulatorCommands::_cmd_minmax>{} },
   { tags::fallback, Member<RegulatorCommands, &RegulatorCommands::_cmd_fallback>{} },
   { tags::reset, Member<RegulatorCommands, &RegulatorCommands::_cmd_reset>{} },
   { tags::lock, Member<RegulatorCommands, &RegulatorCommands::_cmd_lock>{} },
   { tags::unlock, Member<RegulatorCommands, &RegulatorCommands::_cmd_unlock>{} }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

RegulatorCommands::RegulatorCommands (void) :
   base_class{ s_commands }
{
}

bool RegulatorCommands::_cmd_data (const aux::StringRef&, util::ClientSink& client) const
{
   char buf[16] ;

   for (auto& r : app::REGULATOR.rooms())
   {
      char *end = util::itoa (r.m_num, buf) ;
      client << util::pstr{ PSTR("Room ") } << aux::range (buf+0,end) << util::endl{} ;

      dtostre (-r.m_err, buf, 6, 0) ;
      client << util::pstr{ PSTR("  E: ") } << buf << util::endl{} ;

      dtostre (r.m_diff, buf, 6, 0) ;
      client << util::pstr{ PSTR("  D: ") } << buf << util::endl{} ;
   }

   client << util::pstr{ PSTR("-------------------") } << util::endl{} ;

   auto d = app::REGULATOR.runtime_data() ;

   dtostre (d.integral, buf, 6, 0) ;
   client << util::pstr{ PSTR("I: ") } << buf << util::endl{} ;

   dtostre (-d.error, buf, 6, 0) ;
   client << util::pstr{ PSTR("E: ") } << buf << util::endl{} ;

   dtostre (d.differential, buf, 6, 0) ;
   client << util::pstr{ PSTR("D: ") } << buf << util::endl{} ;

   dtostrf (d.outdoor, 0, 6, buf) ;
   client << util::pstr{ PSTR("T(o): ") } << buf << util::endl{} ;

   dtostrf (d.boiler, 0, 6, buf) ;
   client << util::pstr{ PSTR("T(b): ") } << buf << util::endl{} ;

   char *end = util::itoah ((uint8_t)app::REGULATOR.locks(), buf) ;
   client << util::pstr{ PSTR("Locks: ") } << aux::range (buf + 0,  end) << util::endl{} ;

   client << Result::Ok ;
   return true ;
}

bool RegulatorCommands::_cmd_setup (const aux::StringRef& parms, util::ClientSink& client) const
{
   bool ok = true ;
   if (parms.empty())
   {
      auto s = app::REGULATOR.pid() ;
      char buf[16] ;
      dtostrf (s.p, 0, 2, buf) ;
      client << util::pstr{ PSTR("Proportional: ") } << buf << util::endl{} ;
      dtostrf (s.i, 0, 2, buf) ;
      client << util::pstr{ PSTR("Integral: ") } << buf << util::endl{} ;
      dtostrf (s.d, 0, 2, buf) ;
      client << util::pstr{ PSTR("Differential: ") } << buf << util::endl{} ;
   }
   else
   {
      aux::Array<float,3> pid ;

      ok =
         util::tokenize
            (
               parms,
               [&] (auto token)
               {
                  return pid.push_back (util::atof (token.begin(), token.end())) ;
               }
            ) ;

      ok = ok && pid.size() == 3 ;
      ok = ok && app::REGULATOR.pid ({ pid[0], pid[1], pid[2] }) ;
   }

   client << (ok ? Result::Ok : Result::Error) ;
   return true ;
}

bool RegulatorCommands::_cmd_minmax (const aux::StringRef& parms, util::ClientSink& client) const
{
   bool ok = true ;
   if (parms.empty())
   {
      auto mm = app::REGULATOR.minmax() ;
      char buf[3] ;
      char *end = util::itoa (mm.minimum, buf) ;
      client << util::pstr{ PSTR("Min: ") } << aux::range (buf + 0, end) << util::endl{} ;
      end = util::itoa (mm.maximum, buf) ;
      client << util::pstr{ PSTR("Max: ") } << aux::range (buf + 0, end) << util::endl{} ;
   }
   else
   {
      aux::Array<uint8_t,3> mm ;

      ok =
         util::tokenize
            (
               parms,
               [&] (auto token)
               {
                  const char *end ;
                  if (!mm.push_back (util::atoib (token.begin(), token.end(), &end)))
                     return false ;

                  return end == token.end() ;
               }
            ) ;

      ok = ok && mm.size() == 2 ;
      ok = ok && app::REGULATOR.minmax({ mm[0], mm[1] }) ;
   }

   client << (ok ? Result::Ok : Result::Error) ;
   return true ;
}

bool RegulatorCommands::_cmd_fallback (const aux::StringRef& parms, util::ClientSink& client) const
{
   bool ok = true ;
   if (parms.empty())
   {
      char buf[3] ;
      auto end = util::itoa (app::REGULATOR.fallback_boiler_temp(), buf) ;
      client << aux::range (buf + 0, end) << util::endl{} ;
   }
   else
   {
      const char *err ;
      uint8_t t = util::atoib (parms.begin(), parms.end(), &err) ;

      ok = (err == parms.end()) ;
      ok = ok && app::REGULATOR.fallback_boiler_temp (t) ;
   }

   client << (ok ? Result::Ok : Result::Error) ;
   return true ;
}

bool RegulatorCommands::_cmd_reset (const aux::StringRef&, util::ClientSink& client) const
{
   app::REGULATOR.reset() ;
   client << Result::Ok ;
   return true ;
}

bool RegulatorCommands::_cmd_lock (const aux::StringRef&, util::ClientSink& client) const
{
   app::REGULATOR.user_lock() ;
   client << Result::Ok ;
   return true ;
}

bool RegulatorCommands::_cmd_unlock (const aux::StringRef&, util::ClientSink& client) const
{
   app::REGULATOR.user_unlock() ;
   client << Result::Ok ;
   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
