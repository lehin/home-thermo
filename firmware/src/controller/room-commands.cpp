////////////////////////////////////////////////////////////////////////////////////////////
//
// room-commands.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "room-commands.h"
#include "app/rooms.h"
#include "util/utf-8.h"
#include "util/tokenizer.h"
#include <math.h>

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

RoomCommands ROOMCOMMANDS ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   namespace tags
   {
      DECLARE_PGM_STR (add, "add") ;
      DECLARE_PGM_STR (remove, "remove") ;
      DECLARE_PGM_STR (list, "list") ;
      DECLARE_PGM_STR (target, "target") ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

const CmdProcessor::Command RoomCommands::s_commands[] PROGMEM =
{
   { tags::add, Member<RoomCommands, &RoomCommands::_cmd_add>{} },
   { tags::remove, Member<RoomCommands, &RoomCommands::_cmd_remove>{} },
   { tags::list, Member<RoomCommands, &RoomCommands::_cmd_list>{} },
   { tags::target, Member<RoomCommands, &RoomCommands::_cmd_target>{} }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

RoomCommands::RoomCommands (void) :
   base_class{ s_commands }
{
}

bool RoomCommands::_cmd_add (const aux::StringRef& parms, util::ClientSink& client) const
{
   uint8_t idx = 0 ;
   uint8_t subnum ;
   aux::StringRef name{ nullptr, nullptr } ;
   util::TempVal target = util::TempVal::undefined() ;

   if
      (
         !util::tokenize
            (
               parms,
               [&] (const aux::StringRef& token)
               {
                  switch (idx)
                  {
                     case 0:
                     {
                        const char *err ;
                        subnum = util::atoib (token.begin(), token.end(), &err) ;

                        if (err != token.end())
                           return false ;

                        break ;
                     }

                     case 1:
                        if (token.empty())
                           return false ;
                        name = token ;
                        break ;

                     case 2:
                     {
                        float f = util::atof (token.begin(), token.end()) ;
                        if (isnan (f))
                           return false ;

                        target = util::TempVal{ f } ;
                        break ;
                     }
                  }

                  ++idx ;
                  return true ;
               }
            ) ||
         idx < 2
      )
   {
      client << Result::Error ;
      return true ;
   }

   bool ok ;

   if (util::is_ascii_string (name))
      ok = app::ROOMS.append (subnum, name, target) ;
   else
      ok = app::ROOMS.append (subnum, range (util::utf8_decode (name)), target) ;

   client << (ok ? Result::Ok : Result::Error) ;
   return true ;
}

bool RoomCommands::_cmd_remove (const aux::StringRef& parms, util::ClientSink& client) const
{
   uint8_t idx = 0 ;
   uint8_t subnum ;
   aux::StringRef name{ nullptr, nullptr } ;

   bool ok =
      util::tokenize
         (
            parms,
            [&] (const aux::StringRef& token)
            {
               switch (idx)
               {
                  case 0:
                  {
                     const char *err ;
                     subnum = util::atoib (token.begin(), token.end(), &err) ;

                     if (err != token.end())
                        return false ;

                     break ;
                  }

                  case 1:
                     if (token.empty())
                        return false ;
                     name = token ;
                     break ;
               }

               ++idx ;
               return true ;
            }
         ) ;

   ok = ok && idx == 2 ;

   if (util::is_ascii_string (name))
      ok = ok && app::ROOMS.remove (subnum, name) ;
   else
      ok = ok && app::ROOMS.remove (subnum, range (util::utf8_decode (name))) ;

   client << (ok ? Result::Ok : Result::Error) ;
   return true ;
}

bool RoomCommands::_cmd_list (const aux::StringRef&, util::ClientSink& client) const
{
   app::ROOMS.for_each_room
      (
         [&client] (const auto& r)
         {
            client << (r.m_regulated ? (!r.m_nodata ? '*' : '!') : ' ') ;

            if (util::is_ascii_string (range (r.m_name)))
               client << r.m_name ;
            else
               client << util::utf8_encode (range (r.m_name)) ;

            char buf[3] ;
            char *end = util::itoa (r.m_subnum, buf) ;

            client << '(' << aux::range (buf + 0, end) << util::pstr{ PSTR ("): ") }
               << r.m_target.format() << util::pstr{ PSTR (" / ") }
               << r.m_cur.format() << util::endl{} ;
         }
      ) ;

   client << Result::Ok ;
   return true ;
}

bool RoomCommands::_cmd_target (const aux::StringRef& parms, util::ClientSink& client) const
{
   uint8_t idx = 0 ;
   uint8_t subnum ;
   aux::StringRef name{ nullptr, nullptr } ;
   util::TempVal target ;

   bool ok =
      util::tokenize
         (
            parms,
            [&] (const aux::StringRef& token)
            {
               switch (idx)
               {
                  case 0:
                  {
                     const char *err ;
                     subnum = util::atoib (token.begin(), token.end(), &err) ;

                     if (err != token.end())
                        return false ;

                     break ;
                  }

                  case 1:
                     if (token.empty())
                        return false ;
                     name = token ;
                     break ;

                  case 2:
                  {
                     static const char _off[] PROGMEM = "off" ;
                     if
                        (
                           token.size() == strlen_P (_off) &&
                           !strncmp_P (token.begin(), _off, token.size())
                        )
                        target = util::TempVal::undefined() ;
                     else
                     {
                        float f = util::atof (token.begin(), token.end()) ;
                        if (isnan (f))
                           return false ;

                        target = util::TempVal{ f } ;
                     }
                     break ;
                  }
               }

               ++idx ;
               return true ;
            }
         ) ;

   ok = ok && idx == 3 ;

   if (util::is_ascii_string (name))
      ok = ok && app::ROOMS.target (subnum, name, target) ;
   else
      ok = ok && app::ROOMS.target (subnum, range (util::utf8_decode (name)), target) ;

   client << (ok ? Result::Ok : Result::Error) ;
   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
