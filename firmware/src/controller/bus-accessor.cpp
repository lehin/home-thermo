////////////////////////////////////////////////////////////////////////////////////////////
//
// bus-accessor.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "bus-accessor.h"
#include "peripherals/device-server.h"
#include "peripherals/rtc.h"
#include "bus/one-wire.h"
#include "lan/lan-service.h"
#include "aux/ll-scheduler.h"
#include "hw/rak439/rxstream.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

BusAccessor BUSACCESS ;

////////////////////////////////////////////////////////////////////////////////////////////

bool BusAccessor::acquire (Bus bus)
{
   if (m_bus_request || m_lan_server)
      return false ;

   switch (bus)
   {
      #if !defined (SERIALDEBUG)
      case Bus::Modbus: m_bus_request = &BusAccessor::_modbus_request ; break ;
      #endif
      case Bus::I2c: m_bus_request = &BusAccessor::_i2c_request ; break ;
      case Bus::OneWire: m_bus_request = &BusAccessor::_1wire_request ; break ;
   }

   if (!m_bus_request)
      return false ;

   peripherals::DEVSERVER.stop() ;
   peripherals::RTC.disable() ;

   aux::LLSCHED.schedule ([] { BUSACCESS.m_lan_server = new LanServer ; }, PSTR ("BUSACC ACQUIRE")) ;

   return true ;
}

void BusAccessor::release (void)
{
   aux::LLSCHED.schedule
      (
         []
         {
            if (BUSACCESS.m_lan_server)
            {
               delete BUSACCESS.m_lan_server ;
               BUSACCESS.m_lan_server = nullptr ;
            }

            BUSACCESS.m_bus_request = nullptr ;

            peripherals::RTC.enable() ;
            peripherals::DEVSERVER.start() ;
         },
         PSTR ("BUSACC RELEASE")
      ) ;
}

void BusAccessor::_send_to_peer (const Data& data)
{
   if (m_lan_server)
      m_lan_server->m_socket.sendto (data.begin(), data.size(), m_lan_server->m_peer) ;
}

#if !defined (SERIALDEBUG)

void BusAccessor::_modbus_request (const Data& data)
{
   (hw::MODBUS.tx() << data).send() ;
   hw::MODBUS.receive ([] (auto ok, auto& data) { BUSACCESS._modbus_receive (ok, data) ; }, 500) ;
}

void BusAccessor::_modbus_receive (bool ok, const hw::Modbus::RecBytes& data)
{
   if (ok)
      _send_to_peer (data) ;
}

#endif

void BusAccessor::_i2c_request (const Data& data)
{
   auto it = data.begin() ;

   m_i2c_addr = *it++ ;
   m_i2c_recsize = *it++ ;

   auto tx = hw::I2C.tx (m_i2c_addr, [] (auto errno) { BUSACCESS._i2c_sent (errno) ; }) ;
   tx << aux::range (it, data.end()) ;

   tx.send() ;
}

void BusAccessor::_i2c_sent (uint8_t errno)
{
   bool ok = !errno ;
   uint8_t reply[1] = { ok } ;

   ok = ok && (m_i2c_recsize < 15) ; // temporary buffer (in the _i2c_received) size check

   if (ok && m_i2c_recsize)
      hw::I2C.rx (m_i2c_addr, m_i2c_recsize, [] (auto errno, auto& rx) { BUSACCESS._i2c_received (errno, rx) ; }) ;
   else
      _send_to_peer (aux::range (reply)) ;
}

void BusAccessor::_i2c_received (uint8_t errno, hw::I2c::Rx& rx)
{
   uint8_t buf[16] ;

   if (!(buf[0] = !errno))
      _send_to_peer (aux::range (buf + 0, buf + 1)) ;
   else
   {
      auto end = buf + 1 + m_i2c_recsize ;

      rx >> aux::range (buf + 1, end) ;
      _send_to_peer (aux::range (buf + 0, end)) ;
   }
}

void BusAccessor::_1wire_request (const Data& data)
{
   auto it = data.begin() ;
   uint8_t recsize = *it++ ;

   uint8_t buf[16] ;

   auto& ok = buf[0] ;
   ok = recsize <= sizeof (buf) - 1 ;

   if (ok)
      for (auto byte : aux::range (it, data.end()))
         if (!(ok = bus::ONEWIRE.send (byte)))
            break ;

   if (ok && recsize)
      for (auto& byte : aux::range (buf + 1, buf + recsize + 1))
         if (!(ok = bus::ONEWIRE.read (&byte)))
            break ;

   if (!ok)
      recsize = 0 ;

   _send_to_peer (aux::range (buf + 0, buf + 1 + recsize)) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

BusAccessor::LanServer::LanServer (void) :
   m_socket { lan::Socket::Type::Udp }
{
   m_socket.bind (756) ;
   m_socket.on_receive ([this] (auto& r) { this->_on_receive (r) ; }) ;
}

void BusAccessor::LanServer::_on_receive (const hw::rak439::Sock::ReceiveData& rcv)
{
   auto sz = min (rcv.m_rx.unread(), sizeof (m_buf)) ;
   if (!sz || !BUSACCESS.m_bus_request)
      return ;

   m_peer = rcv.m_addr ;

   auto range = aux::range (m_buf + 0, m_buf + sz) ;
   rcv.m_rx >> range ;

   (BUSACCESS.*BUSACCESS.m_bus_request) (range) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
