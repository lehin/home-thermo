////////////////////////////////////////////////////////////////////////////////////////////
//
// mqtt-commands.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "mqtt-commands.h"
#include "app/mqtt.h"
#include "app/mqtt-client.h"
#include "util/tokenizer.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

MqttCommands MQTTCOMMANDS ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   namespace tags
   {
      DECLARE_PGM_STR (setup, "setup") ;
      DECLARE_PGM_STR (list, "list") ;
      DECLARE_PGM_STR (subscribe, "subscribe") ;
      DECLARE_PGM_STR (unsubscribe, "unsubscribe") ;
      DECLARE_PGM_STR (state, "state") ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

const CmdProcessor::Command MqttCommands::s_commands[] PROGMEM =
{
   { tags::setup, Member<MqttCommands, &MqttCommands::_cmd_setup>{} },
   { tags::list, Member<MqttCommands, &MqttCommands::_cmd_list>{} },
   { tags::subscribe, Member<MqttCommands, &MqttCommands::_cmd_subscribe>{} },
   { tags::unsubscribe, Member<MqttCommands, &MqttCommands::_cmd_unsubscribe>{} },
   { tags::state, Member<MqttCommands, &MqttCommands::_cmd_state>{} }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

MqttCommands::MqttCommands (void) :
   base_class{ s_commands }
{
}

bool MqttCommands::_cmd_setup (const aux::StringRef& parms, util::ClientSink& client) const
{
   if (parms.empty())
   {
      auto s = app::MQTT.settings() ;
      client << util::pstr{ PSTR("Server: ") } << s.m_address << util::endl{} ;

      char buf[5] ;
      char *end = util::itoa (s.m_port, buf) ;
      client << util::pstr{ PSTR("Port: ") } << aux::range (buf + 0, end) << util::endl{} ;
   }
   else
   {
      aux::StringRef server{ nullptr, nullptr } ;
      uint16_t port = 1883 ;
      uint8_t idx = 0 ;

      if
         (
            !util::tokenize
               (
                  parms,
                  [&] (const aux::StringRef& token)
                  {
                     switch (idx)
                     {
                        case 0:
                           if (token.empty())
                              return false ;
                           server = token ;
                           break ;

                        case 1:
                           const char *err ;
                           port  = util::atoiw (token.begin(), token.end(), &err) ;

                           if (err != token.end() || !port)
                              return false ;

                           break ;
                     }
                     ++idx ;
                     return true ;
                  }
               )
         )
      {
         client << Result::Error ;
         return true ;
      }

      app::MQTT.setup (server, port) ;
   }

   client << Result::Ok ;
   return true ;
}

bool MqttCommands::_cmd_list (const aux::StringRef&, util::ClientSink& client) const
{
   app::MQTT.for_each_subscription
      (
         [&client] (auto topic, auto key, auto tempval)
         {
            client
               << topic << util::pstr{ PSTR(": ") }
               << key << util::pstr{ PSTR(" / ") }
               << tempval << util::endl{} ;
         }
      ) ;

   client << Result::Ok ;
   return true ;
}

bool MqttCommands::_cmd_subscribe (const aux::StringRef& parms, util::ClientSink& client) const
{
   aux::Array<aux::StringRef,3> tokens ;

   bool ok =
      util::tokenize
         (
            parms,
            [&] (auto token) { return tokens.push_back (token) ; }
         ) ;

   ok = ok && tokens.size() == 3 ;
   ok = ok && app::MQTT.add_subscription (tokens[0], tokens[1], tokens[2]) ;

   client << (ok ? Result::Ok : Result::Error) ;
   return true ;
}

bool MqttCommands::_cmd_unsubscribe (const aux::StringRef& parms, util::ClientSink& client) const
{
   bool ok = !parms.empty() ;

   const char *err ;
   uint8_t num = util::atoib (parms.begin(), parms.end(), &err) ;

   ok = ok && err == parms.end() ;
   ok = ok && app::MQTT.remove_subscription (num) ;

   client << (ok ? Result::Ok : Result::Error) ;
   return true ;
}

bool MqttCommands::_cmd_state (const aux::StringRef&, util::ClientSink& client) const
{
   client << util::pstr{ app::MQTTCLIENT.state_description() } << util::endl{} << Result::Ok ;
   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
