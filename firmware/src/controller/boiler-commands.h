////////////////////////////////////////////////////////////////////////////////////////////
//
// boiler-commands.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "cmd-processor.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class BoilerCommands BOILERCOMMANDS ;

////////////////////////////////////////////////////////////////////////////////////////////

class BoilerCommands : public CmdProcessor
{
   typedef CmdProcessor base_class ;

public:
   BoilerCommands (void) ;

private:
   bool _cmd_mode (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_dhw (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_standby (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_info (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_reset (const aux::StringRef&, util::ClientSink& client) const ;
   bool _cmd_fault (const aux::StringRef&, util::ClientSink& client) const ;

private:
   static const Command s_commands[] PROGMEM ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
