////////////////////////////////////////////////////////////////////////////////////////////
//
// bus-accessor.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/range.h"
#include "hw/modbus.h"
#include "hw/i2c.h"
#include "lan/lan-socket.h"

namespace controller
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class BusAccessor BUSACCESS ;

////////////////////////////////////////////////////////////////////////////////////////////

class BusAccessor
{
   class LanServer
   {
      static constexpr uint8_t BUFSIZE = 64 ;

      friend class BusAccessor ;

   public:
      LanServer (void) ;

   private:
      void _on_receive (const hw::rak439::Sock::ReceiveData& rcv) ;

   private:
      lan::Socket m_socket ;
      uint8_t m_buf[BUFSIZE] ;
      lan::Address m_peer ;
   } ;

   friend class LanServer ;

public:
   enum class Bus : uint8_t { Modbus, I2c, OneWire } ;

public:
   bool acquire (Bus bus) ;
   void release (void) ;

private:
   typedef aux::Range<const uint8_t*> Data ;
   void _send_to_peer (const Data& data) ;

   #if !defined (SERIALDEBUG)
   void _modbus_request (const Data& data) ;
   void _modbus_receive (bool ok, const hw::Modbus::RecBytes& data) ;
   #endif

   void _i2c_request (const Data& data) ;
   void _i2c_sent (uint8_t errno) ;
   void _i2c_received (uint8_t errno, hw::I2c::Rx& rx) ;

   void _1wire_request (const Data& data) ;

private:
   typedef void (BusAccessor::*BusRequest) (const Data& data) ;
   BusRequest m_bus_request = nullptr ;
   LanServer *m_lan_server = nullptr ;
   uint8_t m_i2c_addr ;
   uint8_t m_i2c_recsize ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace controller
