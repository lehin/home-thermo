////////////////////////////////////////////////////////////////////////////////////////////
//
// regulator.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "init.h"
#include "util/tempval.h"
#include "aux/vector.h"
#include "aux/signal.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Regulator REGULATOR ;

////////////////////////////////////////////////////////////////////////////////////////////

class Regulator : Init<Regulator,&REGULATOR>
{
   struct RoomData
   {
      uint8_t m_num ;
      float m_target, m_cur ;

      unsigned m_no_data_timeout = 0 ;

      // PID
      unsigned m_time_stable = 0 ;
      float m_err = 0, m_diff = 0 ;
   } ;

   typedef aux::Vector<RoomData> Rooms ;

public:
   Regulator (void) ;

   void initialize (void) ;
   void reset (void) ;

   const Rooms& rooms (void) const { return m_rooms ; }

   struct RuntimeData
   {
      float integral, error, differential ;
      float outdoor, boiler ;
   } ;

   RuntimeData runtime_data (void) { return { m_integral, m_err, m_diff, m_outdoor_t, m_boiler_t } ; }

   struct Pid
   {
      float p, i, d ;
   } ;

   Pid pid (void) const ;
   bool pid (Pid s) ;

   struct MinMax
   {
      uint8_t minimum, maximum ;
   } ;

   MinMax minmax (void) const { return m_minmax ; }
   bool minmax (const MinMax& mm) ;

   uint8_t fallback_boiler_temp (void) const { return m_fallback_boiler_temp ; }
   bool fallback_boiler_temp (uint8_t t) ;

   enum class Locks : uint8_t
   {
      None = 0x00,
      ByRoom = 0x01, ByOutdoor = 0x02, ByBoiler = 0x04, ByMode = 0x08, ByUser = 0x10
   } ;

   Locks locks (void) const { return m_locks ; }
   bool is_locked (void) const { return m_locks != Locks::None ; }

   void user_lock (void) { _set_lock (Locks::ByUser) ; }
   void user_unlock (void) { _set_lock (Locks::ByUser, false) ; }

   typedef void (*OnCalcPidValues) (float e, float d, float i) ;
   void on_calc_pid_values (OnCalcPidValues fn) { m_on_calc_pid_values = fn ; }

private:
   void _on_room_temperature (uint8_t num, float t) ;
   void _on_room_target (uint8_t num, float t) ;
   void _on_room_removed (uint8_t num) ;
   void _append_room (uint8_t num) ;
   void _set_lock (Locks lck, bool set = true) ;
   void _set_boiler_fallback (void) ;

   void _on_outdoor_temperature (float t) ;
   void _recalc_boilder_by_temp_change (RoomData& room) ;
   void _recalc_boiler_t (void) ;
   void _set_boiler_t (float t) ;
   void _check_save_runtime_data (void) ;

   void _seconds_tick (void) ;

private:
   Rooms m_rooms ;

   MinMax m_minmax ;
   Pid m_pid ;
   uint8_t m_fallback_boiler_temp ;

   Locks m_locks ;

   // integrap part of PID-regulation
   float m_integral,
         m_err,
         m_diff ;

   float m_outdoor_t,
         m_boiler_t ;

   bool m_using_fallback_boiler_t,
        m_in_saturation ;

   unsigned m_seconds_to_recalc,
            m_seconds_to_save ;

   OnCalcPidValues m_on_calc_pid_values = nullptr ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

inline Regulator::Locks operator| (Regulator::Locks lhs, Regulator::Locks rhs)
   { return static_cast<Regulator::Locks> ((uint8_t)lhs | (uint8_t)rhs) ; }

inline Regulator::Locks& operator|= (Regulator::Locks& l, Regulator::Locks rhs)
   { l = l | rhs ; return l ; }

inline bool operator& (Regulator::Locks lhs, Regulator::Locks rhs)
   { return static_cast<Regulator::Locks> ((uint8_t)lhs & (uint8_t)rhs) != Regulator::Locks::None ; }

inline Regulator::Locks& operator&= (Regulator::Locks& l, Regulator::Locks rhs)
   { l = static_cast<Regulator::Locks> ((uint8_t)l & ~(uint8_t)rhs) ; return l ; }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
