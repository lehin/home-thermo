////////////////////////////////////////////////////////////////////////////////////////////
//
// mqtt-client.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "mqtt-client.h"
#include "mqtt-proto.h"
#include "peripherals/rtc.h"
#include "lan/dn-service.h"
#include "lan/lan-service.h"
#include "hw/rak439/rxstream.h"
#include "aux/ll-scheduler.h"

#include "util/eeprom.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static constexpr uint16_t KEEP_ALIVE_INTERVAL = 300 ;
}

////////////////////////////////////////////////////////////////////////////////////////////

MqttClient MQTTCLIENT ;

////////////////////////////////////////////////////////////////////////////////////////////

MqttClient::MqttClient (void) :
   m_server_ip{ 0 },
   m_reconnect_timeout{ 0 },
   m_reconnections_count{ 0 },
   m_state{ State::Uninitialized },
   m_flags{ false, false },
   m_keep_alive_remaining{ 0 }
{
}

json::ValsCollector& MqttClient::add_subscription (aux::String topic, TopicEventHandler&& evthandler)
{
   m_topics.push_back ({ topic, aux::forward (evthandler) }) ;
   return m_topics.back().m_parser ;
}

void MqttClient::connect (aux::String server, uint16_t port)
{
   peripherals::RTC.signal_seconds_tick().connect ([] { MQTTCLIENT._on_seconds_tick() ; }) ;

   m_server = server ;
   m_port = port ;

   m_sock = new lan::Socket ;
   m_sock->on_receive ([] (auto& rec) { MQTTCLIENT._on_receive (rec) ; }) ;

   m_sock->on_close
      (
         []
         {
            if (MQTTCLIENT.m_state != State::Error)
               MQTTCLIENT._set_state (State::Lost) ;

            MQTTCLIENT._schedule_reconnect() ;
         }
      ) ;

   lan::SERVICE.signal_network_state().connect
      (
         [] (auto s)
         {
            switch (s)
            {
               case lan::WmiState::Connected:
                  if (!MQTTCLIENT.m_flags.m_connect_on_network_ready)
                     break ;

                  MQTTCLIENT.m_flags.m_connect_on_network_ready = false ;
                  MQTTCLIENT._initialize_connection() ;
                  break ;

               case lan::WmiState::Shutdown:
                  MQTTCLIENT.m_sock->close() ;
                  MQTTCLIENT.m_reconnect_timeout = 0 ;
                  MQTTCLIENT.m_flags.m_connect_on_network_ready = true ;
                  break ;

               default:;
            }
         }
      ) ;

   _initialize_connection() ;
}

const char* MqttClient::state_description (void) const
{
   static const char _Uninitialized[] PROGMEM = "Uninitialized" ;
   static const char _Resolving[] PROGMEM = "Resolving" ;
   static const char _Connecting[] PROGMEM = "Connecting" ;
   static const char _Subscribing[] PROGMEM = "Subscribing" ;
   static const char _Ready[] PROGMEM = "Ready" ;
   static const char _Error[] PROGMEM = "Connection error" ;
   static const char _Lost[] PROGMEM = "Connection lost" ;

   static const char* const _Messages[] PROGMEM =
      { _Uninitialized, _Resolving, _Connecting, _Subscribing, _Ready, _Error, _Lost } ;

   return (const char*)pgm_read_ptr (_Messages + (uint8_t)m_state) ;
}

void MqttClient::_set_state (State s)
{
   if (m_state == s)
      return ;

   m_state = s ;

   m_keep_alive_remaining = s == State::Ready ? KEEP_ALIVE_INTERVAL : 0 ;
   m_flags.m_keep_alive_pending = false ;

   m_signal_state_changed (m_state) ;
}

void MqttClient::_initialize_connection (void)
{
   if (!lan::SERVICE.is_connected())
   {
      m_flags.m_connect_on_network_ready = true ;
      _set_state (State::Uninitialized) ;
      return ;
   }

   if (m_server_ip)
      _connect() ;
   else
   {
      _set_state (State::Resolving) ;

      lan::DNSERVICE.request
         (
            m_server,
            [] (uint16_t, uint32_t addr)
            {
               if (!addr)
               {
                  MQTTCLIENT._on_connection_error (true) ;
                  return ;
               }

               MQTTCLIENT.m_server_ip = addr ;
               MQTTCLIENT._connect() ;
            }
         ) ;
   }
}

void MqttClient::_schedule_reconnect (bool fastreconnect)
{
   m_reconnect_timeout = fastreconnect ? 2 : 120 ;
}

void MqttClient::_on_connection_error (bool fastreconnect)
{
   _set_state (State::Error) ;

   if (m_sock->is_valid())
      aux::LLSCHED.schedule ([] { MQTTCLIENT.m_sock->close() ; }, PSTR ("MQTT CONN ERR")) ;
   else
      _schedule_reconnect (fastreconnect) ;
}

void MqttClient::_connect (void)
{
   if
      (
         !m_sock->open (lan::Socket::Type::Tcp) ||
         !m_sock->connect ({ m_server_ip, m_port })
      )
   {
      _on_connection_error() ;
      return ;
   }

   _set_state (State::Connecting) ;

   mqtt::Connect { PSTR("HomeThermo"), KEEP_ALIVE_INTERVAL } . send (*m_sock) ;
}

void MqttClient::_subscribe (void)
{
   _set_state (State::Subscribing) ;

   mqtt::Subscribe pkt ;

   m_subscribe_id = pkt.contents().field().id() ;

   auto& subscriptions = pkt.contents().tail().field() ;
   for (auto& t : m_topics)
      subscriptions.push_back ({ t.m_name, (uint8_t)0 }) ;

   pkt.send (*m_sock) ;
}

void MqttClient::_on_receive (const hw::rak439::Sock::ReceiveData& rec)
{
   struct Sink : mqtt::PacketSink
   {
      virtual void operator() (const mqtt::ConnACK& pkt) const override { MQTTCLIENT._on_conn_ack (pkt) ; }
      virtual void operator() (const mqtt::SubACK& pkt) const override { MQTTCLIENT._on_sub_ack (pkt) ; }
      virtual void operator() (const mqtt::PingResp& pkt) const override { MQTTCLIENT._on_ping_response (pkt) ; }
      virtual void operator() (const mqtt::Publish& pkt) const override { MQTTCLIENT._on_publish (pkt) ; }
   } ;

   auto pkt = mqtt::InboundBase::from_stream (rec.m_rx) ;
   if (!pkt)
      return ;

   pkt->sink (Sink{}) ;
   delete pkt ;
}

void MqttClient::_on_conn_ack (const mqtt::ConnACK& ack)
{
   if (!ack.contents().field().value().ret)
      aux::LLSCHED.schedule ([] { MQTTCLIENT._subscribe() ; }, PSTR ("MQTT CONN ACK")) ;
   else
      _on_connection_error() ;
}

void MqttClient::_on_sub_ack (const mqtt::SubACK& ack)
{
   if (ack.contents().field().id() != m_subscribe_id)
      return ;

   if (!(ack.contents().tail().field().value() & 0x80))
      _set_state (State::Ready) ;
   else
      _on_connection_error() ;
}

void MqttClient::_on_ping_response (const mqtt::PingResp&)
{
   m_flags.m_keep_alive_pending = false ;
}

void MqttClient::_on_publish (const mqtt::Publish& pkt)
{
   auto& topic = pkt.contents().field().str() ;

   for (auto& t : m_topics)
      if (t.m_name == topic)
      {
         for (auto ch : pkt.contents().tail().field().str())
            t.m_parser << ch ;

         if (t.m_parser.is_valid())
            t.m_fn (t.m_parser) ;

         t.m_parser.reset() ;
         break ;
      }
}

void MqttClient::_on_seconds_tick (void)
{
   if (m_keep_alive_remaining && !--m_keep_alive_remaining)
      _on_keep_alive() ;

   if (m_reconnect_timeout && !--m_reconnect_timeout)
   {
      ++m_reconnections_count ;
      MQTTCLIENT._initialize_connection() ;
   }
}

void MqttClient::_on_keep_alive (void)
{
   aux::debug_assert( m_sock && m_sock->is_valid(), PSTR ("MQTT KEEPALIVE")) ;

   if (!m_flags.m_keep_alive_pending && lan::SERVICE.is_connected())
   {
      m_keep_alive_remaining = KEEP_ALIVE_INTERVAL ;
      m_flags.m_keep_alive_pending = true ;

      mqtt::PingReq{} . send (*m_sock) ;
   }
   else
      m_sock->close() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
