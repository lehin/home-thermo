////////////////////////////////////////////////////////////////////////////////////////////
//
// json-parser.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "json-parser.h"
#include <ctype.h>

namespace app
{
   namespace json
   {

////////////////////////////////////////////////////////////////////////////////////////////

Analyzer::Analyzer (Context& ctx) :
   m_context{ ctx },
   m_name{ 32 },
   m_value{ 64 }
{
   reset() ;
}

void Analyzer::reset (void)
{
   m_name.clear() ;
   m_value.clear() ;
   m_ctxtype = Context::Type::Value ;
   _set_sink (&Analyzer::_pre_entity) ;
}

Analyzer& Analyzer::operator<< (char ch)
{
   while (!(this->*m_sink)(ch)) ;
   return *this ;
}

namespace
{
   inline bool isnumeric (char ch) { return ch == '+' || ch == '-' || ch == '.' || isdigit (ch) ; }
}

bool Analyzer::_pre_entity (char ch)
{
   if (ch == '"')
      _set_sink (&Analyzer::_string_val) ;
   else if (isnumeric (ch))
   {
      _set_sink (&Analyzer::_numeric_val) ;
      return false ;
   }
   else if (ch == '[')
      _array_begin() ;
   else if (ch == ']')
      _array_end() ;
   else if (ch == '{')
      _object_begin() ;
   else if (ch == '}')
      _object_end() ;

   return true ;
}

bool Analyzer::_post_entity (char ch)
{
   if (ch == ',')
      _set_sink (m_ctxtype == Context::Type::Field ? &Analyzer::_pre_field_name : &Analyzer::_pre_entity) ;
   else if (ch == ']')
      _array_end() ;
   else if (ch == '}')
      _object_end() ;

   return true ;
}

bool Analyzer::_string_val (char ch)
{
   if (ch != '"')
      m_value.push_back (ch) ;
   else
      _on_value (ValType::String) ;

   return true ;
}

bool Analyzer::_numeric_val (char ch)
{
   if (isnumeric (ch))
      return (m_value.push_back (ch), true) ;

   _on_value (ValType::Numeric) ;
   return false ;
}

bool Analyzer::_pre_field_name (char ch)
{
   if (ch == '}')
      _object_end() ;
   else if (ch == '"')
      _set_sink (&Analyzer::_field_name) ;

   return true ;
}

bool Analyzer::_field_name (char ch)
{
   if (ch == '"')
      _set_sink (&Analyzer::_field_separator) ;
   else
      m_name.push_back (ch) ;

   return true ;
}

bool Analyzer::_field_separator (char ch)
{
   if (ch == ':')
      _set_sink (&Analyzer::_pre_entity) ;

   return true ;
}

void Analyzer::_array_begin (void)
{
   m_ctxtype = Context::Type::Value ;
   m_context.array_begin (range (m_name)) ;
   m_name.clear() ;
}

void Analyzer::_array_end (void)
{
   m_ctxtype = m_context.array_end() ;
   _set_sink (&Analyzer::_post_entity) ;
}

void Analyzer::_object_begin (void)
{
   m_ctxtype = Context::Type::Field ;
   m_context.object_begin (range (m_name)) ;
   m_name.clear() ;
   _set_sink (&Analyzer::_pre_field_name) ;
}

void Analyzer::_object_end (void)
{
   m_ctxtype = m_context.object_end() ;
   _set_sink (&Analyzer::_post_entity) ;
}

void Analyzer::_on_value (ValType type)
{
   m_context.value (type, range (m_name), range (m_value)) ;
   m_value.clear() ;
   m_name.clear() ;
   _set_sink (&Analyzer::_post_entity) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

ValsCollector::ValsCollector (void) :
   m_analyzer{ *this },
   m_collect{ nullptr },
   m_is_valid{ true }
{
}

ValsCollector::ValsCollector (ValsCollector&& org) :
   m_analyzer{ *this },
   m_values{ aux::forward (org.m_values) },
   m_collect{ org.m_collect },
   m_is_valid{ true }
{
   org.m_collect =  nullptr ;
}

ValsCollector::~ValsCollector (void)
{
   if (m_collect)
      delete m_collect ;
}

void ValsCollector::reset (void)
{
   m_is_valid = true ;
   m_analyzer.reset() ;
   m_ctx_stack.clear() ;

   for (auto& v : m_values)
   {
      v.m_type = ValType::Null ;
      v.m_str.clear() ;
   }
}

void ValsCollector::collect (aux::StringRef path)
{
   struct CollectTreeBuilder
   {
      CollectTreeItem** m_collect ;
      CollectTreeItem* m_item = nullptr ;

      CollectTreeBuilder (CollectTreeItem*& collect) : m_collect{ &collect } {}

      void operator() (aux::StringRef str, int8_t idx)
      {
         m_item = *m_collect ;

         while (m_item)
            if (m_item->m_name == str && m_item->m_idx == idx)
            {
               m_collect = &m_item->m_child ;
               return ;
            }
            else
               m_item = m_item->m_sibling ;

         m_item = new CollectTreeItem{ str, idx } ;
         m_item->m_sibling = *m_collect ;
         *m_collect = m_item ;
         m_collect = &m_item->m_child ;
      }
   } ;

   auto builder = Path{ path } . parse (CollectTreeBuilder{ m_collect }) ;
   if (!builder.m_item)
      return ;

   builder.m_item->m_validx = m_values.size() ;
   m_values.push_back (Value{ ValType::Null }) ;
}

void ValsCollector::collect (const char *path)
{
   collect (aux::range (path, path + strlen (path))) ;
}

void ValsCollector::dump_collect_tree (util::ClientSink& sink) const
{
   struct Dumper
   {
      CollectTreeItem *m_item ;
      unsigned m_level ;

      void operator() (util::ClientSink& sink) const
      {
         for (auto item = m_item; item; item = item->m_sibling)
         {
            for (auto n = 0; n < m_level; ++n)
               sink << ' ' << ' ' ;

            char buf[16] ;
            char* end = util::itoa ((int32_t)item->m_idx, buf) ;

            sink << item->m_name << '(' << aux::range (buf + 0, end) << util::pstr{ PSTR(") -> ") } ;

            end = util::itoa ((int32_t)item->m_validx, buf) ;
            sink << aux::range (buf + 0, end) << util::endl() ;

            Dumper{ item->m_child, m_level + 1 } (sink) ;
         }
      }
   } ;

   Dumper{ m_collect, 0 } (sink) ;
}

void ValsCollector::array_begin (aux::StringRef name)
{
   if (!is_valid())
      return ;

   CollectTreeItem *item = nullptr ;
   if (!m_ctx_stack.empty())
      item = m_ctx_stack.back().m_item ;

   m_ctx_stack.push_back ({ 0, name, item }) ;
}

Context::Type ValsCollector::array_end (void)
{
   return _object_array_end ([] (int8_t idx) { return idx >= 0 ; }) ;
}

void ValsCollector::object_begin (aux::StringRef name)
{
   if (!is_valid())
      return ;

   m_ctx_stack.push_back ({ -1, name, _find_in_collect_tree (name) }) ;
}

Context::Type ValsCollector::object_end (void)
{
   return _object_array_end ([] (int8_t idx) { return idx < 0 ; }) ;
}

void ValsCollector::value (ValType type, aux::StringRef name, aux::StringRef val)
{
   if (!is_valid())
      return ;

   auto *collect = _find_in_collect_tree (name) ;
   if (collect)
   {
      auto& found = m_values[collect->m_validx] ;
      found.m_type = type ;
      found.m_str = val ;
   }

   _check_increment_index() ;
}

ValsCollector::CollectTreeItem* ValsCollector::_find_in_collect_tree (aux::StringRef name) const
{
   CollectTreeItem* item = m_collect ;
   int8_t idx = -1 ;

   if (!m_ctx_stack.empty())
   {
      auto& ctx = m_ctx_stack.back() ;

      idx = ctx.m_idx ;
      if (idx >= 0)
         name = aux::range (ctx.m_name) ;

      item = ctx.m_item ;
      if (item)
         item = item->m_child ;
   }

   for (; item; item = item->m_sibling)
      if (item->m_name == name && item->m_idx == idx)
         break ;

   return item ;
}

void ValsCollector::_check_increment_index (void)
{
   if (m_ctx_stack.empty())
      return ;

   auto& idx = m_ctx_stack.back().m_idx ;
   if (idx >= 0)
      ++idx ;
}

Context::Type ValsCollector::_object_array_end (bool (*comp) (int8_t))
{
   if (!is_valid())
      return Type::Value ;

   m_is_valid = !m_ctx_stack.empty() && comp (m_ctx_stack.back().m_idx) ;
   if (!is_valid())
      return Type::Value ;

   m_ctx_stack.pop_back() ;
   _check_increment_index() ;

   return !m_ctx_stack.empty() && m_ctx_stack.back().m_idx < 0 ? Type::Field : Type::Value ;
}

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace json
} // namespace app
