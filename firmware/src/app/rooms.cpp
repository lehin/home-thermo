////////////////////////////////////////////////////////////////////////////////////////////
//
// rooms.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "rooms.h"
#include "mqtt.h"
#include "util/eeprom.h"


namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

Rooms ROOMS ;

////////////////////////////////////////////////////////////////////////////////////////////

Rooms::Rooms (void) :
   m_count{ 0 }
{
}

void Rooms::initialize (void)
{
   (new ui::RoomDataFrameItem{ 25 }) -> show() ;
   (new ui::RoomDataFrameItem{ 25 + 8 + ui::RoomDataItem::visual_size().y * 2 - 1 }) -> show() ;

   for (uint8_t n = 0; n < MAX_ROOMS; ++n)
   {
      util::Eeprom::Room room = util::EEPROM.rooms()[n] ;
      if (room.name.size())
         _append_room (n, room.subnum, room.name, room.target) ;
   }

   MQTT.temp_signal().connect ([] (auto subnum, auto key, auto t) { ROOMS._update (subnum, key, t) ; }) ;
}

bool Rooms::append (uint8_t subnum, aux::StringRef name, util::TempVal target)
{
   if (m_count >= MAX_ROOMS)
      return false ;

   uint8_t free = 0 ;
   for (; free < MAX_ROOMS; ++free)
      if (m_items[free].m_name.empty())
         break ;

   util::Eeprom::Room e ;
   e.subnum = subnum ;
   e.name = name ;
   e.target = target ;
   util::EEPROM.rooms()[free] = e ;

   _append_room (free, subnum, name, target) ;

   return true ;
}

bool Rooms::remove (uint8_t subnum, aux::StringRef name)
{
   uint8_t idx = _find_room (subnum, name) ;
   if (idx == 0xff)
      return false ;

   if (m_on_removed)
      m_on_removed (idx) ;

   auto& item = m_items[idx] ;

   item.m_ui->show (false) ;
   item.m_ui->clear() ;

   delete item.m_ui ;
   item.m_ui = nullptr ;
   item.m_name.reset() ;

   util::Eeprom::Room e = util::EEPROM.rooms()[idx] ;
   e.name.reset() ;
   util::EEPROM.rooms()[idx] = e ;

   --m_count ;

   return true ;
}

bool Rooms::target (uint8_t subnum, aux::StringRef name, util::TempVal target)
{
   uint8_t idx = _find_room (subnum, name) ;
   if (idx == 0xff)
      return false ;

   auto& room = m_items[idx] ;

   bool regulated = target.is_defined() ;

   if
      (
         regulated == room.m_regulated &&
         (!regulated || room.m_target == target)
      )
      return true ;

   room.m_target = target ;
   room.m_ui->update (ui::RoomData::Target{ target }) ;

   if (!regulated)
      room.m_ui->update (ui::RoomData::State::Inactive) ;

   if (room.m_regulated && !regulated && m_on_removed)
      m_on_removed (idx) ;

   if (room.m_regulated && m_on_target_changed)
      m_on_target_changed (idx, target) ;

   room.m_regulated = regulated ;

   util::Eeprom::Room e = util::EEPROM.rooms()[idx] ;
   e.regulated = regulated ;
   e.target = target ;
   util::EEPROM.rooms()[idx] = e ;

   return true ;
}

void Rooms::mark_nodata (uint8_t idx)
{
   auto& r = m_items[idx] ;
   r.m_nodata = true ;
   if (r.m_ui)
      r.m_ui->update (ui::RoomData::State::NoData) ;
}

void Rooms::_update (uint8_t subnum, aux::String name, util::TempVal temp)
{
   uint8_t idx = _find_room (subnum, range (name)) ;
   if (idx == 0xff)
      return ;

   auto& room = m_items[idx] ;

   room.m_cur = temp ;
   room.m_nodata = false ;
   room.m_ui->update (ui::RoomData::Current{ temp }) ;

   if (room.m_regulated)
   {
      if (abs (room.m_cur - room.m_target) < 0.5_t)
         room.m_ui->update (ui::RoomData::State::Ok) ;
      else
         room.m_ui->update (room.m_cur < room.m_target ? ui::RoomData::State::Low : ui::RoomData::State::High) ;
   }

   if (room.m_regulated && m_on_temp_changed)
      m_on_temp_changed (idx, temp) ;
}

void Rooms::_append_room (uint8_t idx, uint8_t subnum, aux::String name, util::TempVal target)
{
   static auto _visual_size = ui::RoomDataItem::visual_size() ;
   static const uint16_t y_offs = 29,
                         x_offs =  (peripherals::Display::WIDTH - (_visual_size.x * 3 - 2)) / 2 ;

   const uint8_t row = m_count / 3,
                 col = m_count % 3 ;

   const bool regulated = target.is_defined() ;

   m_items[idx] =
   {
      subnum, name, target, util::TempVal::undefined(), regulated,
      new ui::RoomDataItem
      {
         { x_offs + col * (_visual_size.x - 1), y_offs + row * (_visual_size.y - 1) },
         name, regulated ? ui::RoomData::State::NoData : ui::RoomData::State::Inactive, target
      }
   } ;

   m_items[idx].m_ui->show() ;

   ++m_count ;
}

uint8_t Rooms::_find_room (uint8_t subnum, aux::StringRef name) const
{
   for (uint8_t idx = 0; idx < sizeof (m_items) / sizeof (*m_items); ++idx)
   {
      if (m_items[idx].m_name.empty())
         continue ;

      if (m_items[idx].m_subnum == subnum && m_items[idx].m_name == name)
         return idx ;
   }

   return 0xff ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
