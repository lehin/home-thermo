////////////////////////////////////////////////////////////////////////////////////////////
//
// boot-checker.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "boot-checker.h"
#include "util/eeprom.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

BootChecker BOOTCHECKER ;

////////////////////////////////////////////////////////////////////////////////////////////

BootChecker::BootChecker (void) :
   m_pending_checks{ Checks::Opentherm | Checks::Mqtt }
{
}

void BootChecker::initialize (void)
{
   m_ot_present_connection =
      hw::otcomm::LINK.signal_present().connect
         (
            [] (bool present)
            {
               if (!present)
                  return ;

               BOOTCHECKER._set_ok (Checks::Opentherm) ;
               hw::otcomm::LINK.signal_present().disconnect (BOOTCHECKER.m_ot_present_connection) ;
            }
         ) ;

   m_mqtt_state_connection =
      MQTTCLIENT.signal_state_changed().connect
         (
            [] (auto state)
            {
               if (state != MqttClient::State::Ready)
                  return ;

               BOOTCHECKER._set_ok (Checks::Mqtt) ;
               MQTTCLIENT.signal_state_changed().disconnect (BOOTCHECKER.m_mqtt_state_connection) ;
            }
         ) ;
}

void BootChecker::_set_ok (Checks check)
{
   m_pending_checks &= ~check ;
   if (!m_pending_checks)
      util::EEPROM.boot_flag() = util::BootFlag::Normal ;
}


////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
