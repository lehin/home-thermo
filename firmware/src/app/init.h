////////////////////////////////////////////////////////////////////////////////////////////
//
// init.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Initializer INIT ;

////////////////////////////////////////////////////////////////////////////////////////////

class Initializer
{
public:
   void do_initialize (void) ;

private:
   template<typename _Class, _Class*> friend class Init ;

   // InitBase interface
   typedef void (*Action) (void) ;

   struct ListItem
   {
      Action m_fn ;
      ListItem* m_next ;
   } ;

   template<Action _fn>
      class RegImpl
      {
      public:
         static void do_register (void)
         {
            s_item.m_next = s_actions ;
            s_actions = &s_item ;
         }

      private:
         static ListItem s_item ;
      } ;

   template<Action _fn>
      static void register_action (void)
      {
         RegImpl<_fn>::do_register() ;
      }

private:
   static ListItem* s_actions ;
} ;

template<Initializer::Action _fn>
   Initializer::ListItem Initializer::RegImpl<_fn>::s_item { _fn } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Class, _Class *_instance>
   class Init
   {
      static void _do_init (void) { _instance->initialize() ; }

   protected:
      Init (void)
      {
         Initializer::register_action<_do_init>() ;
      }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
