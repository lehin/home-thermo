////////////////////////////////////////////////////////////////////////////////////////////
//
// mqtt.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "mqtt.h"
#include "util/eeprom.h"
#include "util/utf-8.h"
#include "mqtt-client.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

Mqtt MQTT ;

////////////////////////////////////////////////////////////////////////////////////////////

void Mqtt::initialize (void)
{
   auto s = settings() ;
   if (s.m_address.empty() || !s.m_port)
      return ;

   uint8_t idx = 0 ;

   for_each_subscription
      (
         [&idx] (auto topic, auto key, auto tempval)
         {
            auto& parser =
               MQTTCLIENT.add_subscription
                  (
                     topic,
                     [idx] (auto const& vals) { MQTT._on_publish (idx, vals) ; }
                  ) ;

            parser.collect (range (key)) ;
            parser.collect (range (tempval)) ;

            ++idx ;
         }
      ) ;

   MQTTCLIENT.connect (s.m_address, s.m_port) ;
}

void Mqtt::setup (aux::StringRef server, uint16_t port)
{
   auto parms = *util::EEPROM.mqtt() ;
   parms.server = server ;
   parms.port = port ;
   util::EEPROM.mqtt() = parms ;
}

Mqtt::Settings Mqtt::settings (void) const
{
   auto parms = *util::EEPROM.mqtt() ;
   return { parms.server, parms.port } ;
}

bool Mqtt::add_subscription (aux::StringRef topic, aux::StringRef key, aux::StringRef tempval)
{
   for (uint8_t n = 0; n < util::Eeprom::MAX_SUBSCRIPTIONS; ++n)
   {
      auto s = *util::EEPROM.subscriptions()[n] ;
      if (s.topic)
         continue ;

      s.topic = topic ;
      s.key = key ;
      s.tempval = tempval ;
      util::EEPROM.subscriptions()[n] = s ;
      return true ;
   }

   return false ;
}

bool Mqtt::remove_subscription (uint8_t num)
{
   for (uint8_t n = 0; n < util::Eeprom::MAX_SUBSCRIPTIONS; ++n)
   {
      auto s = *util::EEPROM.subscriptions()[n] ;
      if (!s.topic || num--)
         continue ;

      s.topic.reset() ;
      s.key.reset() ;
      s.tempval.reset() ;
      util::EEPROM.subscriptions()[n] = s ;
      return true ;
   }

   return false ;
}

void Mqtt::for_each_subscription (const SubscriptionHandler& handler)
{
   for (uint8_t n = 0; n < util::Eeprom::MAX_SUBSCRIPTIONS; ++n)
   {
      auto s = *util::EEPROM.subscriptions()[n] ;
      if (s.topic)
         handler (s.topic, s.key, s.tempval) ;
   }
}

void Mqtt::_on_publish (uint8_t idx, const app::json::ValsCollector& vals)
{
   aux::String key = vals[0].m_str ;

   if (!util::is_ascii_string (range (key)))
      key = util::utf8_decode (range (key)) ;

   m_temp_signal (idx, key, util::TempVal{ (float)atof (vals[1].m_str.c_str()) }) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
