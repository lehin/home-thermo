////////////////////////////////////////////////////////////////////////////////////////////
//
// regulator.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "regulator.h"
#include "rooms.h"
#include "outdoor.h"
#include "boiler.h"
#include "peripherals/rtc.h"
#include "util/eeprom.h"
#include "aux/ll-scheduler.h"
#include <math.h>

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static constexpr unsigned RECALC_PERIOD_SECONDS = 300 ;
   static constexpr unsigned SAVE_PERIOD_SECONDS = 3600 ;
   static constexpr unsigned NO_ROOM_DATA_TIMEOUT = 1800 ;
}

////////////////////////////////////////////////////////////////////////////////////////////

Regulator REGULATOR ;

////////////////////////////////////////////////////////////////////////////////////////////

Regulator::Regulator (void) :
   m_locks{ Locks::None },
   m_integral{ 0 },
   m_outdoor_t{ NAN },
   m_boiler_t{ NAN },
   m_using_fallback_boiler_t{ false },
   m_in_saturation{ false },
   m_seconds_to_recalc{ 0 },
   m_seconds_to_save{ 0 }
{
}

void Regulator::initialize (void)
{
   auto eep = *util::EEPROM.regulator() ;

   m_pid = eep.pid ;

   if (isnan (m_pid.p))
      m_pid.p = 15 ;

   if (isnan (m_pid.i))
      m_pid.i = 5.0/3600 ;

   if (isnan (m_pid.d))
      m_pid.d = 1 * 3600 ;

   m_minmax = eep.minmax ;

   if (m_minmax.minimum == 0xff)
      m_minmax.minimum = 30 ;

   if (m_minmax.maximum == 0xff)
      m_minmax.maximum = 85 ;

   m_fallback_boiler_temp = eep.fallback_boiler_temp ;
   if (m_fallback_boiler_temp == 0xff)
      m_fallback_boiler_temp = 60 ;

   float _i = util::EEPROM.regI() ;
   if (!isnan (_i))
      m_integral = _i ;

   peripherals::RTC.signal_seconds_tick().connect ([] { REGULATOR._seconds_tick() ; }) ;

   ROOMS.on_temp_changed
      (
         [] (auto idx, auto t)
         {
            REGULATOR._on_room_temperature (idx, static_cast<float>(t)) ;
         }
      ) ;

   ROOMS.on_target_changed
      (
         [] (auto idx, auto t)
         {
            REGULATOR._on_room_target (idx, static_cast<float>(t)) ;
         }
      ) ;

   ROOMS.on_removed
      (
         [] (auto idx)
         {
            REGULATOR._on_room_removed (idx) ;
         }
      ) ;

   OUTDOOR.signal_temp_changed().connect
      (
         [] (auto t)
         {
            if (t >= -50_t && t <= 50_t)
               REGULATOR._on_outdoor_temperature (static_cast<float>(t)) ;
         }
      ) ;

   BOILER.on_running
      (
         [] (auto r)
         {
            REGULATOR._set_lock (Locks::ByBoiler, !r.m_ch) ;
         }
      ) ;

   BOILER.on_mode_changed
      (
         [] (auto mode)
         {
            REGULATOR._set_lock (Locks::ByMode, mode != Boiler::Mode::Winter) ;
         }
      ) ;

   aux::LLSCHED.schedule
      (
         []
         {
            REGULATOR._set_lock (Locks::ByMode, BOILER.get_mode() != Boiler::Mode::Winter) ;
            REGULATOR._set_lock (Locks::ByRoom | Locks::ByOutdoor, true) ;
         },
         PSTR ("REGULATOR INIT")
      ) ;
}

void Regulator::reset (void)
{
   util::EEPROM.regI() = m_integral = 0 ;

   for (auto& r : m_rooms)
   {
      r.m_err = r.m_target - r.m_cur ;
      r.m_diff = 0 ;
   }

   m_in_saturation = false ;

   if (!is_locked())
      _recalc_boiler_t() ;
}

Regulator::Pid Regulator::pid (void) const
{
   return { m_pid.p, m_pid.i * 3600, m_pid.d / 3600 } ;
}

bool Regulator::pid (Pid s)
{
   if (isnan (s.p) || isnan (s.i) || isnan (s.d))
      return false ;

   s.i /= 3600 ;
   s.d *= 3600 ;
   m_pid = s ;
   util::EEPROM.regulator() = { m_pid, m_minmax, m_fallback_boiler_temp } ;

   return true ;
}

bool Regulator::minmax (const MinMax& mm)
{
   if (!mm.minimum || !mm.maximum || !mm.minimum || mm.maximum >= 100 || mm.maximum <= mm.minimum)
      return false ;

   m_minmax = mm ;
   util::EEPROM.regulator() = { m_pid, m_minmax, m_fallback_boiler_temp } ;

   return true ;
}

bool Regulator::fallback_boiler_temp (uint8_t t)
{
   if (t < m_minmax.minimum || t > m_minmax.maximum)
      return false ;

   m_fallback_boiler_temp = t ;
   util::EEPROM.regulator() = { m_pid, m_minmax, m_fallback_boiler_temp } ;

   _set_boiler_fallback() ;

   return true ;
}

void Regulator::_on_room_temperature (uint8_t num, float t)
{
   RoomData *room = nullptr ;
   for (auto& r : m_rooms)
   {
      if (r.m_num != num)
         continue ;

      r.m_cur = t ;
      room = &r ;

      break ;
   }

   if (!room)
   {
      _append_room (num) ;
      room = &m_rooms.back() ;
   }

   room->m_no_data_timeout = NO_ROOM_DATA_TIMEOUT ;
   _set_lock (Locks::ByRoom, false) ;
   _recalc_boilder_by_temp_change (*room) ;
}

void Regulator::_on_room_target (uint8_t num, float t)
{
   for (auto& r : m_rooms)
   {
      if (r.m_num != num)
         continue ;

      r.m_target = t ;
      r.m_err = r.m_target - r.m_cur ;
   }

   _recalc_boiler_t() ;
}

void Regulator::_on_room_removed (uint8_t num)
{
   bool found = false ;

   for (auto it = m_rooms.begin(); it != m_rooms.end(); ++it)
      if (it->m_num == num)
      {
         m_rooms.erase (it) ;
         found = true ;
         break ;
      }

   if (!found)
      return ;

   if (m_rooms.empty())
      _set_lock (Locks::ByRoom) ;
   else
      _recalc_boiler_t() ;
}

void Regulator::_append_room (uint8_t num)
{
   auto& r = ROOMS.get (num) ;

   auto t = static_cast<float> (r.m_target) ;
   auto c = static_cast<float> (r.m_cur) ;

   m_rooms.push_back ({ num, t, c }) ;
}

void Regulator::_set_lock (Locks lck, bool set)
{
   auto newlcks = m_locks ;

   if (set)
      newlcks |= lck ;
   else
      newlcks &= lck ;

   if (newlcks == m_locks)
      return ;

   if ((m_locks & Locks::ByMode) && !(newlcks & Locks::ByMode))
   {
      m_boiler_t = NAN ;
      reset() ;
   }

   m_locks = newlcks ;

   if (m_locks != Locks::None)
      _set_boiler_fallback() ;
   else
   {
      m_seconds_to_recalc = RECALC_PERIOD_SECONDS ;
      m_seconds_to_save = SAVE_PERIOD_SECONDS ;
      _recalc_boiler_t() ;
   }
}

void Regulator::_set_boiler_fallback (void)
{
   if (m_locks & (Locks::ByRoom | Locks::ByOutdoor | Locks::ByUser))
      _set_boiler_t (NAN) ;
}

void Regulator::_on_outdoor_temperature (float t)
{
   m_outdoor_t = t ;
   _set_lock (Locks::ByOutdoor, false) ;
   _recalc_boiler_t() ;
}

void Regulator::_recalc_boilder_by_temp_change (RoomData& room)
{
   auto err = room.m_target - room.m_cur ;

   if (room.m_time_stable)
      room.m_diff = m_pid.d / room.m_time_stable * (err - room.m_err) ;

   room.m_err = err ;
   room.m_time_stable = 0 ;

   _recalc_boiler_t() ;
}

void Regulator::_recalc_boiler_t (void)
{
   if (m_locks != Locks::None && m_locks != Locks::ByBoiler)
      return ;

   aux::debug_assert( !m_rooms.empty() && !isnan (m_outdoor_t), PSTR("REGULATOR NO DATA")) ;

   m_seconds_to_recalc = RECALC_PERIOD_SECONDS ;

   m_err = 0 ;
   m_diff = 0 ;

   float target = 0 ;
   uint8_t cnt = 0 ;

   for (const auto& r : m_rooms)
   {
      if (!r.m_no_data_timeout)
         continue ;

      m_err += r.m_err ;
      m_diff += r.m_diff ;
      target += r.m_target ;
      ++cnt ;
   }

   aux::debug_assert( cnt != 0, PSTR("REGULATOR ROOM CNT")) ;

   m_err /= cnt ;
   m_diff /= cnt ;
   target /= cnt ;

   if (m_on_calc_pid_values)
      m_on_calc_pid_values (m_err, m_diff, m_integral) ;

   float t = (target - m_outdoor_t + 35) ;
   t += m_integral + m_diff ;
   t += m_pid.p * m_err ;

   m_in_saturation = true ;

   if (t < m_minmax.minimum)
      t = m_minmax.minimum ;
   else if (t > m_minmax.maximum)
      t = m_minmax.maximum ;
   else
      m_in_saturation = false ;

   _set_boiler_t (t) ;
}

void Regulator::_set_boiler_t (float t)
{
   bool fallback = false ;
   if (isnan (t))
   {
      fallback = true ;
      t = m_fallback_boiler_temp ;
   }

   if (fallback == m_using_fallback_boiler_t && fabs (t - m_boiler_t) < 0.01f)
      return ;

   m_boiler_t = t ;
   m_using_fallback_boiler_t = fallback ;

   static bool _scheduled = false ;
   if (_scheduled)
      return ;

   _scheduled = true ;

   aux::LLSCHED.schedule
      (
         []
         {
            _scheduled = false ;
            BOILER.ch_temp (util::TempVal{ REGULATOR.m_boiler_t }, REGULATOR.m_using_fallback_boiler_t) ;
         },
         PSTR("BOILER CH TEMP")
      ) ;
}

void Regulator::_check_save_runtime_data (void)
{
   m_seconds_to_save = SAVE_PERIOD_SECONDS ;

   float i = util::EEPROM.regI() ;

   if (isnan (i) || fabs (i - m_integral) > 1)
      util::EEPROM.regI() = m_integral ;
}

void Regulator::_seconds_tick (void)
{
   if
      (
         (m_locks & (Locks::ByRoom | Locks::ByUser | Locks::ByMode)) ||
         m_in_saturation
      )
      return ;

   float err = 0 ;
   uint8_t cnt = 0 ;

   for (auto& r : m_rooms)
   {
      if (!r.m_no_data_timeout)
         continue ;

      if (--r.m_no_data_timeout)
      {
         ++cnt ;
         err += r.m_err ;

         ++r.m_time_stable ;
      }
      else
      {
         ROOMS.mark_nodata (r.m_num) ;

         r.m_time_stable = 0 ;
         r.m_diff = 0 ;
      }
   }

   if (!cnt)
      _set_lock (Locks::ByRoom) ;

   if (is_locked())
      return ;

   m_integral += m_pid.i * err / cnt ;

   if (!m_seconds_to_recalc || !--m_seconds_to_recalc)
      _recalc_boiler_t() ;

   if (!m_seconds_to_save || !--m_seconds_to_save)
      _check_save_runtime_data() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
