////////////////////////////////////////////////////////////////////////////////////////////
//
// json-parser.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/string.h"
#include "util/strconv.h"
#include "aux/vector.h"

#include "util/client-sink.h"

namespace app
{
   namespace json
   {

////////////////////////////////////////////////////////////////////////////////////////////

enum class ValType : uint8_t{ Null, String, Numeric } ;

////////////////////////////////////////////////////////////////////////////////////////////

struct Context
{
   enum class Type : uint8_t{ Value, Field } ;

   virtual void array_begin (aux::StringRef name) = 0 ;
   virtual Type array_end (void) = 0 ;
   virtual void object_begin (aux::StringRef name) = 0 ;
   virtual Type object_end (void) = 0 ;
   virtual void value (ValType type, aux::StringRef name, aux::StringRef val) = 0 ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class Analyzer
{
public:
   Analyzer (Context& ctx) ;

   void reset (void) ;

   Analyzer& operator<< (char ch) ;

private:
   typedef bool (Analyzer::*CharSink) (char ch) ;
   void _set_sink (CharSink sink) { m_sink = sink ; }

   bool _pre_entity (char ch) ;
   bool _post_entity (char ch) ;
   bool _string_val (char ch) ;
   bool _numeric_val (char ch) ;
   bool _pre_field_name (char ch) ;
   bool _field_name (char ch) ;
   bool _field_separator (char ch) ;

   void _array_begin (void) ;
   void _array_end (void) ;
   void _object_begin (void) ;
   void _object_end (void) ;
   void _on_value (ValType type) ;

private:
   Context& m_context ;
   Context::Type m_ctxtype ;
   CharSink m_sink ;
   aux::String m_name, m_value ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class Path
{
public:
   Path (aux::StringRef str) : m_str{ str } {}

   template<typename _Sink>
      _Sink parse (_Sink sink)
      {
         const char *name = m_str.begin(),
                    *end = name ;

         bool arr = false ;
         int8_t idx = -1 ;

         for (const char *p = m_str.begin(); p != m_str.end(); ++p, end = arr ? end : p)
         {
            switch (*p)
            {
               case '[':
                  arr = true ;
                  break ;

               case ']':
                  if (arr)
                     idx = util::atoib (end + 1, p) ;
                  break ;

               case '.':
                  sink (aux::range (name, end), idx) ;
                  ++(name = p) ;
                  idx = -1 ;
                  arr = false ;
                  break ;
            }
         }

         sink (aux::range (name, end), idx) ;

         return sink ;
      }

private:
   aux::StringRef m_str ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class ValsCollector : private Context
{
   ValsCollector (const ValsCollector&) = delete ;
   ValsCollector& operator= (const ValsCollector&) = delete ;

public:
   ValsCollector (void) ;
   ~ValsCollector (void) ;
   ValsCollector (ValsCollector&& org) ;

   void reset (void) ;
   void collect (aux::StringRef path) ;
   void collect (const char *path) ;
   bool is_valid (void) const { return m_is_valid ; }

   void dump_collect_tree (util::ClientSink& sink) const ;

   ValsCollector& operator<< (char ch) { m_analyzer << ch ; return *this ; }

   struct Value
   {
      ValType m_type ;
      aux::String m_str ;
   } ;

   const Value& operator[] (int8_t idx) const { return m_values[idx] ; }

private:
   virtual void array_begin (aux::StringRef name) ;
   virtual Type array_end (void) ;
   virtual void object_begin (aux::StringRef name) ;
   virtual Type object_end (void) ;
   virtual void value (ValType type, aux::StringRef name, aux::StringRef val) ;

   struct CollectTreeItem ;
   CollectTreeItem* _find_in_collect_tree (aux::StringRef name) const ;

   void _check_increment_index (void) ;
   Type _object_array_end (bool (*comp) (int8_t)) ;

private:
   Analyzer m_analyzer ;

   typedef aux::Vector<Value> Values ;
   Values m_values ;

   struct CollectTreeItem
   {
      CollectTreeItem (aux::StringRef name, int8_t idx) : m_name{ name }, m_idx{ idx } {}

      ~CollectTreeItem (void)
      {
         if (m_child) delete m_child ;
         if (m_sibling) delete m_sibling ;
      }

      CollectTreeItem *m_sibling = nullptr,
                      *m_child = nullptr ;

      int8_t m_validx = -1 ;

      aux::String m_name ;
      int8_t m_idx ;
   } ;

   CollectTreeItem *m_collect ;

   struct StackItem
   {
      int8_t m_idx ;
      aux::String m_name ;
      CollectTreeItem *m_item ;
   } ;

   typedef aux::Vector<StackItem> CtxStack ;
   CtxStack m_ctx_stack ;

   bool m_is_valid ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace json
} // namespace app
