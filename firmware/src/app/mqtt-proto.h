////////////////////////////////////////////////////////////////////////////////////////////
//
// mqtt-proto.c
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/string.h"
#include "aux/list.h"
#include "lan/lan-socket.h"

namespace app
{
   namespace mqtt
   {

////////////////////////////////////////////////////////////////////////////////////////////

enum class PktType : uint8_t
{
   Connect = 1, ConnACK, Publish, PubACK, PubRec, PubRel, PubComp,
   Subscribe, SubACK, Unsubscribe, UnsubACK,
   PingReq, PingResp, Disconnect,
   _Count
} ;

inline constexpr uint8_t operator| (PktType type, uint8_t flags) { return (uint8_t)type << 4 | flags ; }

////////////////////////////////////////////////////////////////////////////////////////////

class String
{
public:
   String (void) = default ;
   String (const aux::String& str) : m_str{ str } {}

   size_t size (void) const ;
   void write (hw::rak439::TxStream& tx) const ;
   bool read (size_t remaining, hw::rak439::RxStream& is) ;

   const aux::String& str (void) const { return m_str ; }

private:
   aux::String m_str ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class StringP
{
public:
   StringP (const char* pstr) : m_pstr{ pstr } {}

   size_t size (void) const ;
   void write (hw::rak439::TxStream& tx) const ;
   bool read (size_t, hw::rak439::RxStream&) { return false ; }

private:
   const char *m_pstr ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class PacketId
{
public:
   PacketId (bool generate = true) : m_id{ generate ? ++s_current_id : 0 } {}

   size_t size (void) const { return sizeof (m_id) ; }
   void write (hw::rak439::TxStream& tx) const ;
   bool read (size_t remaining, hw::rak439::RxStream& is) ;

   uint16_t id (void) const { return m_id ; }

private:
   uint16_t m_id ;

   static uint16_t s_current_id ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class Message
{
public:
   size_t size (void) const { return m_str.size() ; }
   void write (hw::rak439::TxStream& tx) const ;
   bool read (size_t remaining, hw::rak439::RxStream& is) ;

   const aux::String& str (void) const { return m_str ; }

private:
   aux::String m_str ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class FixedHdr
{
public:
   FixedHdr (void) = default ;

   constexpr FixedHdr (PktType type, uint8_t flags) :
      m_byte1{ type | flags },
      m_remaining_size{ 0 }
   {
   }

   PktType pkt_type (void) const { return (PktType)(m_byte1 >> 4) ; }

   size_t remaining_size (void) const { return m_remaining_size ; }
   void remaining_size (size_t sz) { m_remaining_size = sz ; }

   size_t size (void) const ;
   void write (hw::rak439::TxStream& tx) const ;
   bool read (hw::rak439::RxStream& rx) ;

private:
   uint8_t m_byte1 ;
   size_t m_remaining_size ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp>
   class Plain
   {
   public:
      Plain (void) = default ;
      Plain (const _Tp& val) : m_val{ val } {}

      size_t size (void) const { return sizeof (m_val) ; }
      void write (hw::rak439::TxStream& tx) const { tx << m_val ; }

      bool read (size_t remaining, hw::rak439::RxStream& is)
      {
         if (remaining < size())
            return false ;

         is >> m_val ;
         return true ;
      }

      const _Tp& value (void) const { return m_val ; }

   private:
      _Tp m_val ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp>
   class List : public aux::List<_Tp>
   {
      typedef aux::List<_Tp> base_class ;

   public:
      size_t size (void) const
      {
         size_t res = 0 ;
         for (auto& val : *this)
            res += val.size() ;
         return res ;
      }

      void write (hw::rak439::TxStream& tx) const
      {
         for (auto& val : *this)
            val.write (tx) ;
      }

      bool read (size_t remaining, hw::rak439::RxStream& is)
      {
         while (remaining)
         {
            _Tp val ;
            if (!val.read (remaining, is))
               return false ;

            remaining -= val.size() ;
            base_class::push_back (val) ;
         }

         return true ;
      }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename ... _Tail>
   class Sequence ;

template<typename _Type, typename ... _Tail>
   class Sequence<_Type, _Tail...>
   {
   public:
      typedef _Type value_type ;
      typedef Sequence<_Tail...> tail_type ;

   public:
      Sequence (void) = default ;

      template<typename ... _Args>
         Sequence (const value_type& p, const _Args& ... tail) : m_field{ p }, m_tail{ tail... } {}

      value_type& field (void) { return m_field ; }
      const value_type& field (void) const { return m_field ; }

      tail_type& tail (void) { return m_tail ; }
      const tail_type& tail (void) const { return m_tail ; }

      size_t size (void) const { return m_field.size() + m_tail.size() ; }

      void write (hw::rak439::TxStream& tx) const
      {
         m_field.write (tx) ;
         m_tail.write (tx) ;
      }

      bool read (size_t remaining, hw::rak439::RxStream& is)
      {
         if (!m_field.read (remaining, is))
            return false ;

         return m_tail.read (remaining - m_field.size(), is) ;
      }

   private:
      value_type m_field ;
      tail_type m_tail ;
   } ;


template<>
   class Sequence<>
   {
   public:
      Sequence (void) = default ;

      size_t size (void) const { return 0 ; }
      void write (hw::rak439::TxStream& tx) const {}
      bool read (size_t, hw::rak439::RxStream&) { return true ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

struct PacketSink
{
   virtual void operator() (const struct ConnACK& pkt) const = 0 ;
   virtual void operator() (const struct SubACK& pkt) const = 0 ;
   virtual void operator() (const struct PingResp& pkt) const = 0 ;
   virtual void operator() (const struct Publish& pkt) const = 0 ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<PktType _type, uint8_t _flags, typename ... _Contents>
   class Packet
   {
      typedef Sequence<FixedHdr, _Contents...> Contents ;

   protected:
      typedef Packet base_class_ ;

   public:
      template<typename ... _Args>
         constexpr Packet (const _Args& ... args) :
            m_contents{ { _type, _flags }, args... }
         {
         }

      typename Contents::tail_type& contents (void) { return m_contents.tail() ; }
      const typename Contents::tail_type& contents (void) const { return m_contents.tail() ; }

   protected:
      Contents m_contents ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<PktType _type, uint8_t _flags, typename ... _Contents>
   class Outgoing : public Packet<_type,_flags,_Contents...>
   {
      typedef Packet<_type,_flags,_Contents...> base_class ;

   protected:
      typedef Outgoing base_class_ ;

   public:
      template<typename ... _Args>
         constexpr Outgoing (const _Args& ... args) : base_class{ args... } {}

      void send (lan::Socket& sock)
      {
         if (!sock.is_valid())
            return ;

         base_class::m_contents.field().remaining_size (base_class::m_contents.tail().size()) ;

         auto tx = sock.send_stream (base_class::m_contents.size()) ;
         base_class::m_contents.write (tx) ;
      }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

class InboundBase
{
public:
   virtual ~InboundBase (void) = default ;

   virtual void sink (const PacketSink& sink) const = 0 ;

   static InboundBase* from_stream (hw::rak439::RxStream& rx) ;

protected:
   typedef InboundBase* (*CreateFn) (void) ;
   static void register_type (PktType type, CreateFn fn) ;

   virtual bool from_stream (const FixedHdr& hdr, hw::rak439::RxStream& rx) = 0 ;

private:
   static CreateFn s_creators[] ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Result, PktType _type, uint8_t _flags, typename ... _Contents>
   class Inbound : public Packet<_type,_flags,_Contents...>, InboundBase
   {
      typedef Packet<_type,_flags,_Contents...> base_class ;

      struct Registrar
      {
         Registrar (void)
         {
            register_type
               (
                  _type,
                  [] (void) -> InboundBase* { return new _Result{} ; }
               ) ;
         }

         void dummy (void) const {}
      } ;

      static Registrar s_registrar ;

   protected:
      typedef Inbound base_class_ ;

   public:
      template<typename ... _Args>
         constexpr Inbound (const _Args& ... args) :
            base_class{ args... }
         {
            s_registrar.dummy() ;
         }

      virtual void sink (const PacketSink& sink) const
      {
         sink (*reinterpret_cast<const _Result*> (this)) ;
      }

   protected:
      virtual bool from_stream (const FixedHdr& hdr, hw::rak439::RxStream& rx)
      {
         base_class::m_contents.field() = hdr ;
         return base_class::m_contents.tail().read (hdr.remaining_size(), rx) ;
      }
   } ;


template<typename _Result, PktType _type, uint8_t _flags, typename ... _Contents>
   typename Inbound<_Result,_type,_flags,_Contents...>::Registrar
      Inbound<_Result,_type,_flags,_Contents...>::s_registrar ;

////////////////////////////////////////////////////////////////////////////////////////////

struct ConnectFlags
{
   bool reserved : 1,
        clean_sess : 1,
        will_flag : 1 ;
   uint8_t will_qos : 2 ;
   bool will_retain : 1,
        has_password : 1,
        has_user_name : 1 ;

   constexpr ConnectFlags (void) :
      reserved{ false }, clean_sess{ false },
      will_flag{ false }, will_qos{ 0 }, will_retain{ false },
      has_password{ false }, has_user_name{ false }
   {
   }
} ;

static_assert (sizeof (ConnectFlags) == 1, "ConnectFlags has wrong size!") ;

struct ConnectVarHdr
{
   uint16_t m_proto_name_len = htons (4) ;
   char m_proto_name[4] = { 'M', 'Q', 'T', 'T' } ;
   uint8_t m_proto_level = 4 ;
   ConnectFlags m_flags ;
   uint16_t m_keep_alive = 0 ;

   constexpr ConnectVarHdr (ConnectFlags flags = {}, uint16_t keep_alive = 0) :
      m_flags{ flags }, m_keep_alive{ htons (keep_alive) } {}
} ;

struct Connect : Outgoing< PktType::Connect, 0, Plain<ConnectVarHdr>, StringP >
{
   typedef base_class_ base_class ;
   Connect (const char *pclientid = nullptr, uint16_t keep_alive = 0) :
      base_class{ ConnectVarHdr{ {}, keep_alive }, pclientid } {}
} ;

////////////////////////////////////////////////////////////////////////////////////////////

using Subscribe = Outgoing< PktType::Subscribe, 2, PacketId, List< Sequence<String, Plain<uint8_t> > > > ;
using PingReq = Outgoing< PktType::PingReq, 0> ;

////////////////////////////////////////////////////////////////////////////////////////////

struct ConnACKData
{
   uint8_t sp,
           ret ;
} ;

static_assert (sizeof (ConnACKData) == 2, "ConnACKData has wrong size!") ;

struct ConnACK : Inbound< ConnACK, PktType::ConnACK, 0, Plain<ConnACKData> >
{
   ConnACK (void) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct SubACK : Inbound< SubACK, PktType::SubACK, 0, PacketId, Plain<uint8_t> >
{
   typedef base_class_ base_class ;
   SubACK (void) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct PingResp : Inbound< PingResp, PktType::PingResp, 0>
{
   PingResp (void) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct Publish : Inbound< Publish, PktType::Publish, 0, String, Message >
{
   typedef base_class_ base_class ;
   Publish (void) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace mqtt
} // namespace app
