////////////////////////////////////////////////////////////////////////////////////////////
//
// rooms.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "init.h"
#include "aux/array.h"
#include "ui/room-data.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Rooms ROOMS ;

////////////////////////////////////////////////////////////////////////////////////////////

class Rooms : Init<Rooms,&ROOMS>
{
public:
   static constexpr uint8_t MAX_ROOMS = 6 ;

public:
   Rooms (void) ;

   void initialize (void) ;

   bool append (uint8_t subnum, aux::StringRef name, util::TempVal target) ;
   bool remove (uint8_t subnum, aux::StringRef name) ;
   bool target (uint8_t subnum, aux::StringRef name, util::TempVal target) ;

   struct Data
   {
      uint8_t m_subnum ;
      aux::String m_name ;

      util::TempVal m_target,
                    m_cur ;

      bool m_regulated : 1,
           m_nodata : 1 ;
   } ;

   const Data& get (uint8_t idx) const { return m_items[idx] ; }
   void mark_nodata (uint8_t idx) ;

   template<typename _Fn>
      void for_each_room (_Fn fn)
      {
         for (auto& r : m_items)
            if (!r.m_name.empty())
               fn (r) ;
      }

   typedef void (*OnTempChanged) (uint8_t idx, util::TempVal t) ;
   void on_temp_changed (OnTempChanged fn) { m_on_temp_changed = fn ; }
   void on_target_changed (OnTempChanged fn) { m_on_target_changed = fn ; }

   typedef void (*OnRemoved) (uint8_t idx) ;
   void on_removed (OnRemoved fn) { m_on_removed = fn ; }

private:
   void _update (uint8_t subnum, aux::String name, util::TempVal temp) ;
   void _append_room (uint8_t idx, uint8_t subnum, aux::String name, util::TempVal target) ;
   uint8_t _find_room (uint8_t subnum, aux::StringRef name) const ;

private:
   struct Room : Data
   {
      Room (void) = default ;

      Room (uint8_t subnum, aux::String name, util::TempVal target, util::TempVal cur, bool reg, ui::RoomDataItem *ui) :
         Data{ subnum, name, target, cur, reg, false },
         m_ui{ ui }
      {
      }

      ui::RoomDataItem *m_ui = nullptr ;
   } ;

   Room m_items[MAX_ROOMS] ;
   uint8_t m_count ;

   OnTempChanged m_on_temp_changed = nullptr ;
   OnTempChanged m_on_target_changed = nullptr ;
   OnRemoved m_on_removed = nullptr ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
