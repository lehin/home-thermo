////////////////////////////////////////////////////////////////////////////////////////////
//
// mqtt-proto.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "mqtt-proto.h"
#include "hw/rak439/rxstream.h"
#include "hw/rak439/txstream.h"

namespace app
{
   namespace mqtt
   {

////////////////////////////////////////////////////////////////////////////////////////////

uint16_t PacketId::s_current_id = 0 ;

////////////////////////////////////////////////////////////////////////////////////////////

size_t String::size (void) const
{
   return m_str.length() + sizeof (uint16_t) ;
}

void String::write (hw::rak439::TxStream& tx) const
{
   tx << htons (m_str.size()) ;
   for (auto ch : m_str)
      tx << ch ;
}

bool String::read (size_t remaining, hw::rak439::RxStream& is)
{
   uint16_t sz = is.read() ;
   sz = ntohs (sz) ;

   m_str = aux::String{ (uint8_t)sz, (uint8_t)sz } ;

   if (remaining < size())
      return false ;

   is >> range (m_str) ;
   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

size_t StringP::size (void) const
{
   return strlen_P (m_pstr) + sizeof (uint16_t) ;
}

void StringP::write (hw::rak439::TxStream& tx) const
{
   uint16_t len = strlen_P (m_pstr) ;
   tx << htons (len) ;

   for (const char *p = m_pstr; len; --len)
      tx << pgm_read_byte (p++) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

void PacketId::write (hw::rak439::TxStream& tx) const
{
   tx << htons (m_id) ;
}

bool PacketId::read (size_t remaining, hw::rak439::RxStream& is)
{
   if (remaining < size())
      return false ;

   is >> m_id ;
   m_id = ntohs (m_id) ;
   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

void Message::write (hw::rak439::TxStream& tx) const
{
   tx.write (range (m_str)) ;
}

bool Message::read (size_t remaining, hw::rak439::RxStream& is)
{
   m_str = aux::String{ (uint8_t)remaining, (uint8_t)remaining } ;
   is >> range (m_str) ;
   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

size_t FixedHdr::size (void) const
{
   return 2 + (m_remaining_size > 0x7f) ;
}

void FixedHdr::write (hw::rak439::TxStream& tx) const
{
   tx << m_byte1 ;
   bool second = m_remaining_size > 0x7f ;
   tx << (uint8_t) ((m_remaining_size & 0x7f) | (second ? 0x80 : 0)) ;
   if (second)
      tx << (uint8_t) (m_remaining_size >> 7) ;
}

bool FixedHdr::read (hw::rak439::RxStream& rx)
{
   rx >> m_byte1 ;

   uint8_t byte = rx.read() ;
   m_remaining_size = byte & 0x7f ;
   if (!(byte & 0x80))
      return true ;

   rx >> byte ;
   if ((byte & 0x80))
      return false ; // too long message

   m_remaining_size += byte * 128u ;
   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

InboundBase::CreateFn InboundBase::s_creators[(uint8_t)PktType::_Count] = { nullptr } ;

InboundBase* InboundBase::from_stream (hw::rak439::RxStream& rx)
{
   FixedHdr hdr ;
   if (!hdr.read (rx))
      return nullptr ;

   auto type = hdr.pkt_type() ;
   if ((uint8_t)type >= sizeof (s_creators) / sizeof (*s_creators))
      return nullptr ;

   auto creator = s_creators[(uint8_t)type] ;
   if (!creator)
      return nullptr ;

   auto ret = creator() ;
   if (!ret->from_stream (hdr, rx))
   {
      delete ret ;
      return nullptr ;
   }

   return ret ;
}

void InboundBase::register_type (PktType type, CreateFn fn)
{
   s_creators[(uint8_t)type] = fn ;
}

////////////////////////////////////////////////////////////////////////////////////////////

ConnACK::ConnACK (void) {}
SubACK::SubACK (void) : base_class{ false } {}
PingResp::PingResp (void) {}
Publish::Publish (void) {}

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace mqtt
} // namespace app
