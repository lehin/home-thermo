////////////////////////////////////////////////////////////////////////////////////////////
//
// boiler.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "init.h"
#include "util/tempval.h"
#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////////////////

namespace ui
{
   class BoilerChStat ;
   class BoilerDhwStat ;
   class BoilerTempItem ;
   class BoilerFlameStat ;
   class CtrlModeItem ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Boiler BOILER ;

////////////////////////////////////////////////////////////////////////////////////////////

class Boiler : Init<Boiler, &BOILER>
{
public:
   enum class Mode : uint8_t { Off, Winter, Summer, Standby, _Count } ;

public:
   void initialize (void) ;

   void ch_temp (util::TempVal t, bool fallback = true) ;
   util::TempVal ch_temp (void) const ;

   bool dhw_temp (util::TempVal t) ;
   util::TempVal dhw_temp (void) const ;

   bool standby_temp (util::TempVal t) ;
   util::TempVal standby_temp (void) const { return m_standby_t ; }

   bool set_mode (Mode mode) ;
   Mode get_mode (void) const { return m_mode ; }

   typedef void (*ModeCallback) (Mode) ;
   void on_mode_changed (ModeCallback fn) { m_on_mode_changed = fn ; }

   struct Running
   {
      bool m_ch : 1,
           m_dhw : 1 ;
   } ;

   typedef void (*RunningCallback) (Running) ;
   void on_running (RunningCallback fn) { m_on_running = fn ; }

private:
   bool _switch_mode (Mode mode, bool init = false) ;
   bool _set_dhw (util::TempVal t) ;
   bool _set_standby (util::TempVal t) ;

private:
   ui::BoilerFlameStat *m_flame = nullptr ;
   ui::BoilerTempItem *m_actual_t = nullptr ;

   ui::BoilerChStat *m_ch = nullptr ;
   ui::BoilerDhwStat *m_dhw = nullptr ;
   ui::BoilerTempItem *m_ch_target = nullptr ;
   ui::BoilerTempItem *m_dhw_target = nullptr ;
   ui::CtrlModeItem *m_mode_switch = nullptr ;

   Mode m_mode = Mode::Off ;
   util::TempVal m_standby_t = 20_t ;

   ModeCallback m_on_mode_changed = nullptr ;
   RunningCallback m_on_running = nullptr ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
