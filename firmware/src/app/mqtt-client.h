////////////////////////////////////////////////////////////////////////////////////////////
//
// mqtt-client.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/string.h"
#include "aux/signal.h"
#include "json-parser.h"
#include "lan/lan-socket.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace mqtt
{
   struct ConnACK ;
   struct SubACK ;
   struct PingResp ;
   struct Publish ;
}

////////////////////////////////////////////////////////////////////////////////////////////

extern class MqttClient MQTTCLIENT ;

////////////////////////////////////////////////////////////////////////////////////////////

class MqttClient
{
public:
   MqttClient (void) ;

   typedef aux::function<void (const json::ValsCollector& vals)> TopicEventHandler ;
   json::ValsCollector& add_subscription (aux::String topic, TopicEventHandler&& evthandler) ;

   void connect (aux::String server, uint16_t port = 1883) ;

   enum class State : uint8_t{ Uninitialized, Resolving, Connecting, Subscribing, Ready, Error, Lost } ;
   State state (void) const { return m_state ; }
   const char* state_description (void) const ;

   typedef aux::Signal<void(State)> SigStateChanged ;
   SigStateChanged& signal_state_changed (void) { return m_signal_state_changed ; }

   uint16_t reconnections_count (void) const { return m_reconnections_count ; }
   void reset_reconnections_count (void) { m_reconnections_count = 0 ; }

private:
   void _set_state (State s) ;
   void _initialize_connection (void) ;
   void _schedule_reconnect (bool fastreconnect = false) ;
   void _on_connection_error (bool fastreconnect = false) ;
   void _connect (void) ;
   void _subscribe (void) ;

   void _on_receive (const hw::rak439::Sock::ReceiveData& rec) ;

   void _on_conn_ack (const mqtt::ConnACK& ack) ;
   void _on_sub_ack (const mqtt::SubACK& ack) ;
   void _on_ping_response (const mqtt::PingResp& ping) ;
   void _on_publish (const mqtt::Publish& pkt) ;

   void _on_seconds_tick (void) ;
   void _on_keep_alive (void) ;

private:
   aux::String m_server ;
   uint32_t m_server_ip ;
   uint16_t m_port ;
   uint16_t m_subscribe_id ;

   struct Flags
   {
      bool m_connect_on_network_ready : 1,
           m_keep_alive_pending : 1 ;
   } ;

   Flags m_flags ;
   uint8_t m_reconnect_timeout ;
   uint16_t m_reconnections_count ;
   uint16_t m_keep_alive_remaining ;

   lan::Socket* m_sock ;

   struct Topic
   {
      aux::String m_name ;
      TopicEventHandler m_fn ;
      json::ValsCollector m_parser ;
   } ;

   aux::List<Topic> m_topics ;

   State m_state ;
   SigStateChanged m_signal_state_changed ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
