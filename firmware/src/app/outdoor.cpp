////////////////////////////////////////////////////////////////////////////////////////////
//
// outdoor.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "outdoor.h"
#include "mqtt.h"
#include "util/eeprom.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

Outdoor OUTDOOR ;

////////////////////////////////////////////////////////////////////////////////////////////

Outdoor::Outdoor (void) :
   m_settings{ 0xff, {} },
   m_temperature{ util::TempVal::undefined() }
{
}

void Outdoor::initialize (void)
{
   m_settings = settings() ;

   if (m_settings.m_subnum == 0xff || m_settings.m_key.empty())
      return ;

   MQTT.temp_signal().connect
      (
         [] (uint8_t subnum, aux::String key, util::TempVal t)
         {
            if
               (
                  subnum == OUTDOOR.m_settings.m_subnum &&
                  key == OUTDOOR.m_settings.m_key
               )
               OUTDOOR._set_temperature (t) ;
         }
      ) ;
}

Outdoor::Settings Outdoor::settings (void) const
{
   auto e = *util::EEPROM.outdoor() ;
   return { e.subnum, e.key } ;
}

bool Outdoor::setup (const Settings& settings)
{
   if (settings.m_subnum == 0xff || settings.m_key.empty())
      return false ;

   auto e = *util::EEPROM.outdoor() ;
   e.subnum = settings.m_subnum ;
   e.key = settings.m_key ;
   util::EEPROM.outdoor() = e ;

   return true ;
}

void Outdoor::_set_temperature (util::TempVal t)
{
   if (t == m_temperature)
      return ;

   m_temperature = t ;
   m_signal_temp_changed (m_temperature) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
