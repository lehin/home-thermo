////////////////////////////////////////////////////////////////////////////////////////////
//
// regulator-ui.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "regulator-ui.h"
#include "regulator.h"
#include "ui/pid-visual.h"
#include "peripherals/display.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

RegulatorUi REGULATORUI ;

////////////////////////////////////////////////////////////////////////////////////////////

void RegulatorUi::initialize (void)
{
   constexpr uint16_t BASE_Y = peripherals::Display::HEIGHT - 64 + 2 ;
   constexpr uint16_t BASE_X = 168 ;

   m_e = new ui::PidVisual{ BASE_X, BASE_Y, 0.5, PSTR("E:") } ;
   m_d = new ui::PidVisual{ BASE_X, BASE_Y + 20, 2, PSTR("D:") } ;
   m_i = new ui::PidVisual{ BASE_X, BASE_Y + 40, 10, PSTR("I:") } ;

   m_e->show() ;
   m_d->show() ;
   m_i->show() ;

   REGULATOR.on_calc_pid_values
      (
         [] (float e, float d, float i)
         {
            REGULATORUI.m_e->update (-e) ;
            REGULATORUI.m_d->update (d) ;
            REGULATORUI.m_i->update (i) ;
         }
      ) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
