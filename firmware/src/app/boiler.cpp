////////////////////////////////////////////////////////////////////////////////////////////
//
// boiler.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "boiler.h"
#include "peripherals/opentherm.h"
#include "ui/boiler-items.h"
#include "ui/mode-switch.h"
#include "util/eeprom.h"

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static constexpr peripherals::Display::Color COLOR_ACTUAL_T = peripherals::Display::white() ;

   static constexpr peripherals::Display::Color COLOR_CH_TARGET = { 0xfd, 0xb1, 0x5b } ;
   static constexpr peripherals::Display::Color COLOR_DHW_TARGET = { 0x4f, 0xc2, 0xed } ;

   static constexpr peripherals::Display::Color COLOR_CH_FALLBACK = { 192, 0, 0 } ;
   static constexpr peripherals::Display::Color COLOR_INACTIVE = ui::BoilerTempItem::color_invalid() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

Boiler BOILER ;

////////////////////////////////////////////////////////////////////////////////////////////

void Boiler::initialize (void)
{
   constexpr uint16_t BASE_Y = peripherals::Display::HEIGHT - 64 ;
   constexpr uint8_t TXT_OFFS = (32 - 23) / 2 ;

   (m_flame = new ui::BoilerFlameStat{ (48 - 14) / 2, BASE_Y }) -> show() ;

   (m_actual_t = new ui::BoilerTempItem{ 0, BASE_Y + 32 + TXT_OFFS, COLOR_INACTIVE }) -> show() ;
   (new ui::BoilerDivider{ 56 }) -> show() ;

   (m_ch = new ui::BoilerChStat{ 64, BASE_Y }) -> show() ;
   (m_dhw = new ui::BoilerDhwStat{ 64, BASE_Y + 32 }) -> show() ;
   (m_ch_target = new ui::BoilerTempItem{ 104, BASE_Y + TXT_OFFS, COLOR_INACTIVE } ) -> show() ;
   (m_dhw_target = new ui::BoilerTempItem{ 104, BASE_Y + 32 + TXT_OFFS, COLOR_INACTIVE } ) -> show() ;

   (new ui::BoilerDivider{ 160 }) -> show() ;
   (new ui::BoilerDivider{ peripherals::Display::WIDTH - 72 }) -> show() ;

   (m_mode_switch = new ui::CtrlModeItem{ peripherals::Display::WIDTH - 64, BASE_Y + 8 }) -> show() ;

   using peripherals::OPENTHERM ;

   OPENTHERM.initialize() ;

   OPENTHERM.set_running_mode_handler
      (
         [] (auto mode)
         {
            Running r
            {
               mode & peripherals::OpenTherm::Mode::Ch,
               mode & peripherals::OpenTherm::Mode::Dhw
            } ;

            BOILER.m_ch->update (r.m_ch) ;
            BOILER.m_dhw->update (r.m_dhw) ;

            if (BOILER.m_on_running)
               BOILER.m_on_running (r) ;
         }
      ) ;

   OPENTHERM.set_flame_active_handler
      (
         [] (bool active) { BOILER.m_flame->update (active) ; }) ;
   OPENTHERM.set_temp_handler
      (
         [] (peripherals::OpenTherm::TempKind kind, util::TempVal t)
         {
            switch (kind)
            {
               case peripherals::OpenTherm::TempKind::Ch:
                  BOILER.m_ch_target->update (t) ;
                  break ;

               case peripherals::OpenTherm::TempKind::Dhw:
                  util::EEPROM.boiler() = { BOILER.m_mode, t, BOILER.m_standby_t } ;
                  BOILER.m_dhw_target->update (t) ;
                  break ;

               case peripherals::OpenTherm::TempKind::Actual:
                  BOILER.m_actual_t->update (t) ;
                  break ;
            }
         }
      ) ;

   auto settings = *util::EEPROM.boiler() ;

   _set_standby (settings.standby) ;
   _switch_mode (settings.mode, true) ;
}

void Boiler::ch_temp (util::TempVal t, bool fallback)
{
   if (m_mode != Mode::Winter)
      return ;

   peripherals::OPENTHERM.ch_temp (t) ;
   m_ch_target->set_color (fallback ? COLOR_CH_FALLBACK : COLOR_CH_TARGET) ;
}

util::TempVal Boiler::ch_temp (void) const
{
   return peripherals::OPENTHERM.ch_temp() ;
}

bool Boiler::dhw_temp (util::TempVal t)
{
   return _set_dhw (t) ;
}

util::TempVal Boiler::dhw_temp (void) const
{
   return peripherals::OPENTHERM.dhw_temp() ;
}

bool Boiler::standby_temp (util::TempVal t)
{
   if (!_set_standby (t))
      return false ;

   util::EEPROM.boiler() = { m_mode, dhw_temp(), t } ;
   return true ;
}

bool Boiler::set_mode (Mode mode)
{
   if (!_switch_mode (mode))
      return false ;

   util::EEPROM.boiler() = { mode, dhw_temp(), m_standby_t } ;
   return true ;
}

bool Boiler::_switch_mode (Mode mode, bool init)
{
   if (mode >= Mode::_Count)
      return false ;

   if (mode == m_mode && !init)
      return true ;

   m_mode = mode ;
   m_mode_switch->update (m_mode) ;

   if (mode == Mode::Winter || mode == Mode::Summer)
      peripherals::OPENTHERM.reset_currents() ;

   if (m_mode != Mode::Winter)
   {
      m_ch_target->set_color (COLOR_INACTIVE) ;

      if (m_mode != Mode::Standby)
         m_ch_target->update (util::TempVal::undefined()) ;
      else
         peripherals::OPENTHERM.ch_temp (m_standby_t) ;
   }

   if (m_mode != Mode::Winter && m_mode != Mode::Summer)
   {
      m_dhw_target->set_color (COLOR_INACTIVE) ;
      m_dhw_target->update (util::TempVal::undefined()) ;
   }
   else
   {
      m_dhw_target->set_color (COLOR_DHW_TARGET) ;
      _set_dhw ((*util::EEPROM.boiler()).dhw) ;
   }

   if (m_mode == Mode::Off)
      m_actual_t->set_color (COLOR_INACTIVE) ;
   else
      m_actual_t->set_color (COLOR_ACTUAL_T) ;

   peripherals::OPENTHERM.set_mode
      (
         (m_mode != Mode::Winter && m_mode != Mode::Standby ? peripherals::OpenTherm::Mode::Ch : peripherals::OpenTherm::Mode::Off) |
         (m_mode != Mode::Winter && m_mode != Mode::Summer ? peripherals::OpenTherm::Mode::Dhw : peripherals::OpenTherm::Mode::Off)
      ) ;

   if (m_on_mode_changed)
      m_on_mode_changed (m_mode) ;

   return true ;
}

bool Boiler::_set_dhw (util::TempVal t)
{
   if (m_mode != Mode::Winter && m_mode != Mode::Summer)
      return false ;

   if (!t || t <= 0_t || t >= 100_t)
      return false ;

   peripherals::OPENTHERM.dhw_temp (t) ;
   return true ;
}

bool Boiler::_set_standby (util::TempVal t)
{
   if (!t || t <= 0_t || t >= 100_t)
      return false ;

   m_standby_t = t ;

   if (m_mode == Mode::Standby)
      peripherals::OPENTHERM.ch_temp (m_standby_t) ;

   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
