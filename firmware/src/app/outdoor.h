////////////////////////////////////////////////////////////////////////////////////////////
//
// outdoor.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "init.h"
#include "aux/string.h"
#include "aux/signal.h"
#include "util/tempval.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Outdoor OUTDOOR ;

////////////////////////////////////////////////////////////////////////////////////////////

class Outdoor : Init<Outdoor,&OUTDOOR>
{
public:
   Outdoor (void) ;

   void initialize (void) ;

   struct Settings
   {
      uint8_t m_subnum ;
      aux::String m_key ;
   } ;

   Settings settings (void) const ;
   bool setup (const Settings& settings) ;

   util::TempVal temperature (void) const { return m_temperature ; }

   typedef aux::Signal<void (util::TempVal t)> SignalTempChanged ;
   SignalTempChanged& signal_temp_changed (void) { return m_signal_temp_changed ; }

private:
   void _set_temperature (util::TempVal t) ;

private:
   Settings m_settings ;
   util::TempVal m_temperature ;
   SignalTempChanged m_signal_temp_changed ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
