////////////////////////////////////////////////////////////////////////////////////////////
//
// mqtt.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "init.h"
#include "json-parser.h"
#include "aux/string.h"
#include "aux/signal.h"
#include "lan/lan-socket.h"
#include "util/tempval.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Mqtt MQTT ;

////////////////////////////////////////////////////////////////////////////////////////////

class Mqtt : Init<Mqtt,&MQTT>
{
public:
   void initialize (void) ;

   void setup (aux::StringRef server, uint16_t port) ;

   struct Settings
   {
      aux::String m_address ;
      uint16_t m_port ;
   } ;

   Settings settings (void) const ;

   bool add_subscription (aux::StringRef topic, aux::StringRef key, aux::StringRef tempval) ;
   bool remove_subscription (uint8_t num) ;

   typedef aux::function<void (aux::String topic, aux::String key, aux::String tempval)> SubscriptionHandler ;
   void for_each_subscription (const SubscriptionHandler& handler) ;

   typedef aux::Signal<void (uint8_t subnum, aux::String key, util::TempVal t)> TempSignal ;
   TempSignal& temp_signal (void) { return m_temp_signal ; }

private:
   void _on_publish (uint8_t idx, const app::json::ValsCollector& vals) ;

private:
   TempSignal m_temp_signal ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
