////////////////////////////////////////////////////////////////////////////////////////////
//
// tcp-server.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "tcp-server.h"
#include "lan-service.h"
#include "aux/minmax.h"
#include <util/delay.h>

namespace lan
{

////////////////////////////////////////////////////////////////////////////////////////////

uint16_t TcpServer::m_total_connections ;

void TcpServer::initialize (void)
{
   m_listener.on_incoming ([this] { _on_incoming_connection() ; }) ;
   SERVICE.signal_network_state().connect ([this] (auto s) { _on_network_state (s) ; }) ;
}

void TcpServer::_on_network_state (lan::WmiState state)
{
   switch (state)
   {
      case lan::WmiState::Connected:
         if (m_listener.is_valid())
            break ;

         m_listener.open (lan::Socket::Type::Tcp) ;
         m_listener.bind (m_port) ;
         m_listener.listen (m_backlog) ;

         break ;

      case lan::WmiState::Shutdown:
      {
         m_listener.close() ;

         auto it = m_clients.begin() ;
         while (it != m_clients.end())
         {
            auto itcur = it++ ;
            itcur->close() ;
         }

         break ;
      }

      default:;
   }
}

void TcpServer::_on_incoming_connection (void)
{
   auto peer = m_listener.accept() ;
   m_listener.listen (m_backlog) ;

   if (!peer.is_valid())
      return ;

   ++m_total_connections ;

   auto it = m_clients.insert (m_clients.end(), aux::forward (peer)) ;

   it->on_close ([this,it] { _on_client_close (it) ; }) ;

   if (m_on_accepted)
      m_on_accepted (*it) ;
}

void TcpServer::_on_client_close (Clients::iterator it)
{
   --m_total_connections ;

   if (m_on_closed)
      m_on_closed (*it) ;

   m_clients.erase (it) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace lan
