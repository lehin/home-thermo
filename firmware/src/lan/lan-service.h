////////////////////////////////////////////////////////////////////////////////////////////
//
// lan-service.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "wmi-state.h"
#include "aux/list.h"
#include "aux/ring-buffer.h"
#include "aux/signal.h"
#include "hw/rak439/sock.h"
#include "hw/rak439/wmi-events.h"

namespace lan
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Service SERVICE ;

class Socket ;

////////////////////////////////////////////////////////////////////////////////////////////

class Service
{
public:
   Service (void) ;

   void smart_config (void) ;
   void connect (void) ;
   void reconnect (void) ;
   void shutdown (void) ;
   bool is_connected (void) { return m_state.m_network_ready && !m_state.m_network_disconnected ; }

   uint16_t wlan_loss_count (void) { return m_wlan_loss_count ; }
   void reset_wlan_loss_count (void) { m_wlan_loss_count = 0 ; }

   typedef aux::Signal<void (WmiState state)> SigNetworkState ;
   SigNetworkState& signal_network_state (void) { return m_signal_network_state ; }

   void register_socket (Socket *s) ;
   void unregister_socket (Socket *s) ;

   uint32_t dns_server (void) { return is_connected() ? m_dns_server : 0 ; }

private:
   using ReceiveData = hw::rak439::Sock::ReceiveData ;

   void _initialize_wlan (void) ;
   void _do_connect (bool init) ;
   void _on_connected (void) ;
   void _on_disconnected (void) ;
   void _on_smart_config_done (const hw::rak439::evts::EasyConfig& e) ;
   void _on_network_ready (void) ;
   void _on_incoming_connection (uint32_t sock) ;
   void _on_sock_close (uint32_t sock) ;
   void _on_sock_receive (const ReceiveData& rcv) ;
   void _check_wlan_disconnected_timeout (void) ;
   void _cancel_wlan_disconnected_timeout (void) ;
   void _on_check_wlan_disconnected_timeout (void) ;

private:
   struct State
   {
      bool m_wlan_initialized : 1 ;
      bool m_network_ready : 1 ;
      bool m_network_disconnected : 1 ;
      bool m_device_on : 1 ;
   } ;

   State m_state ;

   typedef aux::List<Socket*> Sockets ;
   Sockets m_sockets ;

   uint16_t m_wlan_loss_count ;
   uint32_t m_wlan_disconnected_timeout_schedule ;

   SigNetworkState m_signal_network_state ;
   uint32_t m_dns_server ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace lan
