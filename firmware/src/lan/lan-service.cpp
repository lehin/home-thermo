////////////////////////////////////////////////////////////////////////////////////////////
//
// lan-service.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "lan-service.h"
#include "lan-socket.h"
#include "aux/scheduler.h"
#include "aux/ll-scheduler.h"
#include "aux/minmax.h"
#include "hw/led.h"
#include "hw/watchdog.h"

#include "hw/rak439/hci.h"
#include "hw/rak439/wmi.h"
#include "hw/rak439/wmi-commands.h"
#include "hw/rak439/wmi-events.h"

#include "util/eeprom.h"
#include "misc/rtc-scheduler.h"

#include <util/delay.h>

namespace lan
{

////////////////////////////////////////////////////////////////////////////////////////////

Service SERVICE ;

////////////////////////////////////////////////////////////////////////////////////////////

Service::Service (void) :
   m_state{ false, false, false, false },
   m_wlan_loss_count{ 0 },
   m_wlan_disconnected_timeout_schedule{ 0 },
   m_dns_server{ 0 }
{
}

void Service::smart_config (void)
{
   if (m_state.m_device_on)
   {
      m_state.m_device_on = false ;
      _cancel_wlan_disconnected_timeout() ;
   }

   _initialize_wlan() ;

   hw::LED.off() ;
   hw::rak439::WMI.command (pgmcmd (hw::rak439::cmds::EasyConfig{})) ;
   hw::LED.blink (250, 250) ;
}

void Service::connect (void)
{
   if (m_state.m_device_on)
      return ;

   _initialize_wlan() ;

   hw::LED.off() ;
   hw::rak439::HCI.initialize() ;

   m_state.m_device_on = true ;
}

void Service::reconnect (void)
{
   aux::LLSCHED.schedule ([] { SERVICE.shutdown() ; }, PSTR ("LAN SHUTDOWN")) ;
   aux::LLSCHED.schedule
      (
         []
         {
            hw::rak439::DEVICE.reset() ;
            SERVICE.connect() ;
         },
         PSTR ("LAN RECONN")
      ) ;
}

void Service::shutdown (void)
{
   if (!m_state.m_device_on)
      return ;

   m_state.m_device_on = false ;

   _cancel_wlan_disconnected_timeout() ;
   _on_disconnected() ;

   hw::rak439::HCI.shutdown() ;
}

void Service::register_socket (Socket *s)
{
   m_sockets.push_back (s) ;
}

void Service::unregister_socket (Socket *s)
{
   m_sockets.erase (m_sockets.find (s)) ;
}

void Service::_initialize_wlan (void)
{
   if (m_state.m_wlan_initialized)
      return ;

   m_state.m_wlan_initialized = true ;

   hw::rak439::WMI.on_event<hw::rak439::evts::Ready>
      (
         [] (const auto&)
         {
            aux::LLSCHED.schedule ([] { SERVICE._do_connect (true) ; }, PSTR ("LAN EVT READY")) ;
         }
      ) ;

   hw::rak439::WMI.on_event<hw::rak439::evts::Connect>
      (
         [] (const auto&)
         {
            aux::LLSCHED.schedule ([] { SERVICE._on_connected() ; }, PSTR ("LAN EVT CONN")) ;
         }
      ) ;

   hw::rak439::WMI.on_event<hw::rak439::evts::Disconnect>
      (
         [] (const auto&)
         {
            SERVICE.m_state.m_network_disconnected = true ;
            aux::LLSCHED.schedule ([] { SERVICE._on_disconnected() ; }, PSTR ("LAN EVT DIS")) ;
         }
      ) ;

   hw::rak439::WMI.on_event<hw::rak439::evts::EasyConfig> ([] (const hw::rak439::evts::EasyConfig& e) { SERVICE._on_smart_config_done (e) ; }) ;

   hw::rak439::Sock::dhcp_callback
      (
         [] (bool ok)
         {
            if (!ok)
               return ;

            aux::LLSCHED.schedule ([] { SERVICE._on_network_ready() ; }, PSTR ("LAN EVT DHCP")) ;
         }
      ) ;

   hw::rak439::Sock::listen_callback
      (
         [] (uint32_t sock)
         {
            static uint32_t _sock ;
            _sock = sock ;
            aux::LLSCHED.schedule ([] { SERVICE._on_incoming_connection (_sock) ; }, PSTR ("LAN EVT INCOMING")) ;
         }
      ) ;
   hw::rak439::Sock::close_callback ([] (uint32_t sock) { SERVICE._on_sock_close (sock) ; }) ;
   hw::rak439::Sock::receive_callback ([] (const ReceiveData& rcv) { SERVICE._on_sock_receive (rcv) ; }) ;
}

namespace
{
   void _normalize_eep_struct (uint8_t *data, size_t size)
   {
      for (; size; ++data, --size)
         if (*data == 255)
            *data = 0 ;
   }

   template<typename _Tp> inline
      void _normalize_eep_struct (_Tp& p)
         { _normalize_eep_struct (reinterpret_cast<uint8_t*> (&p), sizeof (_Tp)) ; }
}

void Service::_do_connect (bool init)
{
   hw::LED.blink (50, 950) ;
   _check_wlan_disconnected_timeout() ;

   util::Eeprom::WlanCreds creds = util::EEPROM.wlan_creds() ;
   _normalize_eep_struct (creds)  ;

   if (!creds.m_ssid[0])
      smart_config() ;
   else
   {
      if (init)
      {
         hw::rak439::Sock::init_stack_offload() ;
//         hw::rak439::WMI.command (hw::rak439::cmds::PowerMode{ hw::rak439::cmds::Power::Reduced }) ;
      }

      if (creds.m_passwd[0])
         hw::rak439::WMI.command (hw::rak439::cmds::SetPassword{ creds.m_ssid, creds.m_passwd }) ;

      hw::rak439::WMI.command
         (
            hw::rak439::cmds::Connect
            {
               creds.m_ssid,
               creds.m_bssid,
               creds.m_channel
            }
         ) ;
   }
}

void Service::_on_connected (void)
{
   _on_disconnected() ;

   util::Eeprom::HostName hostname = util::EEPROM.host_name() ;
   _normalize_eep_struct (hostname)  ;

   if (!*hostname.str)
      strcpy_P (hostname.str, PSTR("rak439")) ;

   hw::rak439::Sock::ip_config_dhcp (hostname.str) ;
   hw::LED.blink (50, 150) ;
}

void Service::_on_disconnected (void)
{
   if (!m_state.m_network_ready && m_state.m_device_on)
      return ;

   if (m_state.m_network_ready)
      ++m_wlan_loss_count ;

   m_state.m_network_ready = false ;
   m_state.m_network_disconnected = false ;

   if (m_state.m_device_on)
      hw::LED.blink (50, 950) ;
   else
      hw::LED.off() ;

   m_signal_network_state (m_state.m_device_on ? WmiState::Disconnected : WmiState::Shutdown) ;

   if (m_state.m_device_on)
      _check_wlan_disconnected_timeout() ;
}

void Service::_on_smart_config_done (const hw::rak439::evts::EasyConfig& e)
{
   if (e.m_result)
   {
      aux::LLSCHED.schedule
         (
            [] { hw::rak439::WMI.command (pgmcmd (hw::rak439::cmds::EasyConfig{})) ; },
            PSTR ("LAN SMCONFIG DONE")
         ) ;

      return ;
   }

   util::Eeprom::WlanCreds creds{ { 0 }, { 0 }, { 0 }, 0 } ;

   const char *ptr = e.m_easydata ;
   auto len = strnlen (ptr, sizeof (creds.m_passwd)) ;
   memcpy (creds.m_passwd, ptr, len) ;

   ptr += len + 1 ;

   if (ptr[0] != 0x01 && (len = strnlen (ptr, sizeof (creds.m_ssid))) > 0)
      memcpy (creds.m_ssid, ptr, len) ;

   static constexpr auto BSSID_LEN = sizeof (creds.m_bssid) ;

   memcpy (creds.m_bssid, e.m_bssid, BSSID_LEN) ;

   if (e.m_bssid[BSSID_LEN] && (len = strnlen (e.m_bssid + BSSID_LEN + 1, sizeof (creds.m_ssid))) > 0)
   {
      creds.m_channel = e.m_bssid[BSSID_LEN] ;

      if (!creds.m_ssid[0])
         memcpy (creds.m_ssid, e.m_bssid + BSSID_LEN + 1, len) ;
   }

   util::EEPROM.wlan_creds() = creds ;

   hw::LED.off() ;
   hw::rak439::HCI.shutdown() ;

   util::EEPROM.boot_flag() = util::BootFlag::SoftReset ;
   aux::SCHED.schedule (hw::reboot, 100) ;
}

void Service::_on_network_ready (void)
{
   if (m_state.m_network_ready)
      return ;

   _cancel_wlan_disconnected_timeout() ;

   hw::rak439::IpConfigResult ip ;
   if (hw::rak439::Sock::ip_config_query (ip) != -1)
      m_dns_server = ip.m_dnsrv1 ;

   m_state.m_network_ready = true ;
   m_state.m_network_disconnected = false ;

   hw::LED.on() ;

   m_signal_network_state (WmiState::Connected) ;
}

void Service::_on_incoming_connection (uint32_t sock)
{
   for (auto s : m_sockets)
      if (sock == s->raw_handle())
      {
         s->handle_incoming_connection() ;
         break ;
      }
}

void Service::_on_sock_close (uint32_t sock)
{
   for (auto s : m_sockets)
      if (sock == s->raw_handle())
      {
         s->handle_close () ;
         break ;
      }
}

void Service::_on_sock_receive (const ReceiveData& rcv)
{
   for (auto s : m_sockets)
      if (rcv.m_sock == s->raw_handle())
      {
         s->handle_receive (rcv) ;
         break ;
      }
}

void Service::_check_wlan_disconnected_timeout (void)
{
   if (m_wlan_disconnected_timeout_schedule)
      return ;

   static constexpr util::Time WLAN_DISCONNECTED_TIMEOUT { 0, 10, 0 } ;

   m_wlan_disconnected_timeout_schedule =
      misc::RTCSCHED.schedule ([] { SERVICE._on_check_wlan_disconnected_timeout() ; }, WLAN_DISCONNECTED_TIMEOUT) ;
}

void Service::_cancel_wlan_disconnected_timeout (void)
{
   if (!m_wlan_disconnected_timeout_schedule)
      return ;

   misc::RTCSCHED.cancel (m_wlan_disconnected_timeout_schedule) ;
   m_wlan_disconnected_timeout_schedule = 0 ;
}

void Service::_on_check_wlan_disconnected_timeout (void)
{
   m_wlan_disconnected_timeout_schedule = 0 ;

   if (m_state.m_network_ready || !m_state.m_device_on)
      return ;

   reconnect() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace lan
