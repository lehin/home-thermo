////////////////////////////////////////////////////////////////////////////////////////////
//
// tcp-server.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "wmi-state.h"
#include "lan-socket.h"
#include "aux/list.h"
#include "aux/function.h"
#include "aux/forward.h"

namespace lan
{

////////////////////////////////////////////////////////////////////////////////////////////

class TcpServer
{
   typedef aux::List<Socket> Clients ;

public:
   TcpServer (uint16_t port, uint8_t backlog = 1) : m_port{ port }, m_backlog{ backlog } {}

   void initialize (void) ;

   typedef aux::function<void (Socket& client)> ClientCallback ;
   void on_accepted (ClientCallback&& handler) { m_on_accepted = aux::forward (handler) ; }
   void on_closed (ClientCallback&& handler) { m_on_closed = aux::forward (handler) ; }

   bool is_valid_client (Socket& client) const ;

   static uint16_t total_connections (void) { return m_total_connections ; }

private:
   void _on_network_state (lan::WmiState state) ;
   void _on_incoming_connection (void) ;
   void _on_client_close (Clients::iterator it) ;

private:
   const uint16_t m_port ;
   const uint8_t m_backlog ;
   Socket m_listener ;
   Clients m_clients ;
   ClientCallback m_on_accepted, m_on_closed ;

   static uint16_t m_total_connections ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace lan
