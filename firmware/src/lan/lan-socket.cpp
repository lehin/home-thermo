////////////////////////////////////////////////////////////////////////////////////////////
//
// lan-socket.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "lan-socket.h"
#include "lan-service.h"
#include "aux/ll-scheduler.h"

namespace lan
{

////////////////////////////////////////////////////////////////////////////////////////////

Socket::Socket (Type type)
{
   SERVICE.register_socket (this) ;
   open (type) ;
}

Socket::Socket (void) :
   m_sock{ 0 }
{
   SERVICE.register_socket (this) ;
}

Socket::Socket (Socket&& rhs) :
   m_type{ rhs.m_type },
   m_sock{ rhs.m_sock }
{
   SERVICE.register_socket (this) ;

   rhs.m_sock = 0 ;
   m_on_incoming = aux::forward (rhs.m_on_incoming) ;
   m_on_close = aux::forward (rhs.m_on_close) ;
   m_on_receive = aux::forward (rhs.m_on_receive) ;
}

Socket::~Socket (void)
{
   close() ;
   SERVICE.unregister_socket (this) ;
}

Socket Socket::accept (void)
{
   return { Type::Tcp, hw::rak439::Sock::accept (m_sock) } ;
}

bool Socket::open (Type type)
{
   m_sock = hw::rak439::Sock::open (m_type = type) ;
   return is_valid() ;
}

bool Socket::bind (uint16_t port)
{
   return !hw::rak439::Sock::bind (m_sock, port) ;
}

bool Socket::listen (uint8_t backlog)
{
   return !hw::rak439::Sock::listen (m_sock, backlog) ;
}

bool Socket::connect (const Address& addr)
{
   return !hw::rak439::Sock::connect (m_sock, addr) ;
}

void Socket::close (void)
{
   if (!is_valid())
      return ;

   hw::rak439::Sock::close (m_sock) ;
   m_sock = 0 ;

   if (m_on_close)
      m_on_close() ;
}

void Socket::send (const void *buf, uint16_t len)
{
   auto begin = reinterpret_cast<const uint8_t*> (buf) ;

   auto tx = hw::rak439::Sock::send (m_sock, m_type, len) ;
   tx << aux::range (begin, begin + len) ;
}

void Socket::sendto (const void *buf, uint16_t len, const Address& addr)
{
   auto begin = reinterpret_cast<const uint8_t*> (buf) ;

   auto tx = hw::rak439::Sock::sendto (m_sock, m_type, len, addr) ;
   tx << aux::range (begin, begin + len) ;
}

hw::rak439::TxStream Socket::send_stream (uint16_t len)
{
   return hw::rak439::Sock::send (m_sock, m_type, len) ;
}

hw::rak439::TxStream Socket::send_to_stream (uint16_t len, const Address& addr)
{
   return hw::rak439::Sock::sendto (m_sock, m_type, len, addr) ;
}

void Socket::handle_incoming_connection (void)
{
   if (m_on_incoming)
      m_on_incoming() ;
}

void Socket::handle_close (void)
{
   if (!is_valid())
      return ;

   m_sock = 0 ;

   if (m_on_close)
      m_on_close() ;
}

void Socket::handle_receive (const hw::rak439::Sock::ReceiveData& rcv)
{
   if (m_on_receive)
      m_on_receive (rcv) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace lan
