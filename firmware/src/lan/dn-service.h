////////////////////////////////////////////////////////////////////////////////////////////
//
// dn-service.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "lan-socket.h"
#include "aux/string.h"
#include "aux/list.h"
#include "aux/forward.h"
#include "aux/function.h"

namespace lan
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class DnService DNSERVICE ;

////////////////////////////////////////////////////////////////////////////////////////////

class DnService
{
public:
   typedef aux::function<void (uint16_t id, uint32_t addr)> Callback ;
   uint16_t request (aux::String name, Callback&& cb) ;
   void cancel (uint16_t request_id) ;

private:
   void _handle_pendings (void) ;
   void _handle_timeout (uint16_t id) ;
   void _handle_reply (const hw::rak439::Sock::ReceiveData& rcv) ;
   void _deliver_reply (void) ;

private:
   struct Request
   {
      aux::String m_name ;
      Callback m_cb ;
      uint16_t m_id ;

      Request (aux::String name = {}, Callback&& cb = {}) :
         m_name{ name }, m_cb{ aux::forward (cb) }, m_id{ _generate_id() }
      {
      }

      static uint16_t _generate_id (void) ;
   } ;

   typedef aux::List<Request> Queue ;

   Queue m_queue ;

   uint16_t m_timeout_schedule = 0 ;
   uint32_t m_result = 0 ;
   bool m_ready = false ;
   Socket *m_sock = nullptr ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace lan
