////////////////////////////////////////////////////////////////////////////////////////////
//
// client-sink.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "client-sink.h"
#include <avr/pgmspace.h>
#include <string.h>

#define DECLARE_PGM_STR(a,s) \
   static const char a[] PROGMEM = s

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

ClientSink::ClientSink (Holder h) :
   m_sock{ h.m_sock }
{
}

void ClientSink::commit (void)
{
   if (m_buf.empty())
      return ;

   m_sock.send (m_buf.begin(), m_buf.size()) ;
   m_buf.clear() ;
}

void ClientSink::_write (divider)
{
   for (int8_t n = 0; n < 10; ++n)
      _write ('-') ;

   _write (endl{}) ;
}

void ClientSink::_write (Result p)
{
   DECLARE_PGM_STR (_ok, "ok") ;
   DECLARE_PGM_STR (_unknown, "unknown") ;
   DECLARE_PGM_STR (_error, "error") ;

   static const char* const PROGMEM _responses[] PROGMEM { _ok, _unknown, _error } ;

   uint8_t code = static_cast<uint8_t> (p) ;
   operator<< ('0' + code) << ' ' ;

   auto str = reinterpret_cast<const char*> (pgm_read_word (_responses + code)) ;
   write (pstr{ str }) ;

   _write (endl{}) ;
}

void ClientSink::_write (const char *str)
{
   while (*str)
   {
      if (m_buf.full())
         commit() ;

      m_buf.push_back (*str++) ;
   }
}

void ClientSink::_write (pstr str)
{
   while (char ch = pgm_read_byte (str.s++))
   {
      if (m_buf.full())
         commit() ;

      m_buf.push_back (ch) ;
   }
}

void ClientSink::_write_raw (const void *data, uint8_t sz)
{
   if (m_buf.size() + sz > m_buf.capacity())
      commit() ;

   memcpy (m_buf.end(), data, sz) ;
   m_buf.resize (m_buf.size() + sz) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
