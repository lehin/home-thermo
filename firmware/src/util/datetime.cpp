////////////////////////////////////////////////////////////////////////////////////////////
//
// datetime.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "datetime.h"
#include "strconv.h"

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

long Date::total_days (void) const
{
   int8_t m = m_month ;
   int y = m_year ;
   if (m < 3)
   {
      m += 12 ;
      --y ;
   }

   int A = y / 100 ;
   int B = 2 - A + A / 4 ;
   long C = 365.25 * y ;

   return C + (long)(30.6001 * (m + 1)) + m_day + 1720994 + B + 1 ;
}

Date Date::from_days (long days)
{
   long A = days ;

   if (days >= 2299161l)
   {
      long a = (days - 1867216.25) / 36524.25 ;
      A += 1 + a - a / 4 ;
   }

   long B = A + 1524 ;
   long C = (B - 122.1) / 365.25 ;
   long D = 365.25 * C ;
   long E = (B - D) / 30.6001 ;

   uint8_t m = (uint8_t)(E < 14 ? E - 1 : E - 13) ;

   Date res =
   {
      (uint8_t)(days % 7 + 1),
      (uint8_t)(B - D - (long)(30.6001 * E)),
      m,
      (uint16_t)(m > 2 ? C - 4716 : C - 4715)
   } ;

   return res ;
}

char *Date::to_string (char *buf, bool weekday) const
{
   if (weekday)
   {
      buf = weekday_str (buf) ;
      *buf++ = ' ' ;
   }

   buf = util::itoa (m_day, buf, true, 10) ;
   *buf++ = '/' ;
   buf = util::itoa (m_month, buf, true, 10) ;
   *buf++ = '/' ;
   buf = util::itoa (m_year, buf, true, 1000) ;

   return buf ;
}

char *Date::weekday_str (char *buf) const
{
   static const char _weekdays[] PROGMEM = "MonTueWedThuFriSatSun" ;

   const char *src = _weekdays + (m_weekday - 1) * 3 ;
   for (int8_t n = 0; n < 3; ++n)
      *buf++ = pgm_read_byte (src++) ;

   return buf ;
}

////////////////////////////////////////////////////////////////////////////////////////////

long Time::total_seconds (void) const
{
   return m_hour * 3600ul + m_min * 60 + m_sec ;
}

Time Time::from_seconds (long seconds)
{
   return
   {
      (uint8_t) (seconds % 60),
      (uint8_t) ((seconds / 60) % 60),
      (uint8_t) (seconds / 3600)
   } ;
}

char *Time::to_string (char *buf, bool seconds) const
{
   buf = util::itoa (m_hour, buf, true, 10) ;
   *buf++ = ':' ;
   buf = util::itoa (m_min, buf, true, 10) ;

   if (seconds)
   {
      *buf++ = ':' ;
      buf = util::itoa (m_sec, buf, true, 10) ;
   }

   return buf ;
}

Time Time::from_string (const aux::StringRef& s)
{
   static constexpr Time null_time{ 0, 0, 0 } ;

   Time res{ 0, 0, 0 } ;

   const char *end ;

   res.m_hour = util::atoib (s.begin(), s.end(), &end) ;
   if (end == s.end() || *end != ':')
      return null_time ;

   res.m_min = util::atoib (++end, s.end(), &end) ;

   if (end != s.end() && *end != ' ')
   {
      if (*end != ':')
         return null_time ;

      res.m_sec = util::atoib (++end, s.end(), &end) ;
   }

   return res ;
}

////////////////////////////////////////////////////////////////////////////////////////////

int32_t DateTime::operator- (const DateTime& rhs) const
{
   return (m_dt.total_days() - rhs.m_dt.total_days()) * Time::day_seconds() + (m_tm - rhs.m_tm) ;
}

char *DateTime::to_string (char *buf, bool weekday) const
{
   buf = m_dt.to_string (buf, weekday) ;
   *buf++ = ',' ;
   *buf++ = ' ' ;
   buf = m_tm.to_string (buf) ;
   return buf ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
