////////////////////////////////////////////////////////////////////////////////////////////
//
// eeprom.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>
#include <avr/eeprom.h>

#include "eeprom-proxy.h"
#include "boot-flag.h"
#include "boot-stats.h"
#include "datetime.h"
#include "tempval.h"
#include "peripherals/temp-sensor.h"
#include "app/rooms.h"
#include "app/regulator.h"
#include "app/boiler.h"
#include "aux/debug-assert.h"

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Eeprom EEPROM ;
extern class EepromStrPool EESTRPOOL ;

////////////////////////////////////////////////////////////////////////////////////////////

class EepromStrPool
{
   #ifdef DEBUG_ASSERT
   static_assert( sizeof (aux::DebugAssertInfo) == 32, "EEPROM string pool conflicts with debug_assert feature!" ) ;
   #endif

   // last 32 bytes of eeprom my be occupied by debug_assert feature
   static constexpr uint16_t END_GAP = 32 ;

public:
   static constexpr uint16_t START = (E2END + 1 - 1024) ;
   static constexpr uint16_t SIZE = E2END + 1 - START - END_GAP ;

public:
   uint8_t add (const aux::StringRef& s) ;
   void remove (uint8_t idx) ;
   void update (uint8_t idx, const aux::StringRef& s) ;
   aux::String get (uint8_t idx) ;

   uint8_t size (uint8_t idx) ;
   char* fill (uint8_t idx, char *buf) ;

private:
   uint8_t _count (void) ;
   void _count (uint8_t cnt) ;
   uint16_t _index_item (uint8_t idx) ;
   uint16_t _address (uint8_t idx) ;
   void _address (uint8_t idx, uint16_t addr) ;
   uint8_t _find_free (void) ;
   void _add_impl (uint8_t idx, const aux::StringRef& s) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class EepromString
{
public:
   EepromString (void) : m_id{ (uint8_t)-1 } {}

   operator aux::String (void) const { return get() ; }

   template<typename _Tp>
      EepromString& operator= (const _Tp& s) { set (s) ; return *this ; }

   aux::String get (void) const ;

   uint8_t size (void) const { return EESTRPOOL.size (m_id) ; }
   char* fill (char *buf) { return EESTRPOOL.fill (m_id, buf) ; }

   void set (const aux::String& s) { set ({ s.begin(), s.end() }) ; }
   void set (const aux::StringRef& s) ;
   void reset (void) ;

   explicit operator bool (void) const { return size() != 0 ; }

private:
   uint8_t m_id ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class EepromProxyMutable
{
   friend class Eeprom ;
   static void init (void) ;

   static constexpr uint16_t BEGIN = 2048 ;
   static constexpr uint16_t END = EepromStrPool::START ;

public:
   template<typename _Tp>
      static void eep_read (uint16_t addr, _Tp& data)
      {
         EepromExchange::read (addr + s_blk_start + 1, data) ;
      }

   template<typename _Tp>
      static void eep_write (uint16_t addr, const _Tp& data)
      {
         EepromExchange::write (addr + s_blk_start + 1, data) ;
         _on_eep_written() ;
      }

   template<typename _Tp>
      static void eep_reset (uint16_t addr, const _Tp&)
      {
         EepromExchange::reset (addr + s_blk_start + 1, sizeof (_Tp)) ;
         _on_eep_written() ;
      }

private:
   static void _on_eep_written (void) ;

private:
   static uint16_t s_blk_start ;
   static uint8_t s_write_cnt ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Struct, typename _MemType>
   constexpr uint16_t eeprom_member_addr (_MemType _Struct::*_Member)
      { return (uint16_t) & (((const _Struct*)nullptr) ->* _Member) ; }

template<typename _Struct, typename _MemType>
   constexpr EepromProxy<_MemType> eeprom_proxy (_MemType _Struct::*_Member)
      { return EepromProxy<_MemType> { eeprom_member_addr (_Member) } ; }

#define EEPROM_MEMBER(m) \
   constexpr decltype (eeprom_proxy (&Data::m)) m (void) { return eeprom_proxy (&Data::m) ; }

template<typename _Struct, typename _MemType>
   constexpr EepromProxy<_MemType,EepromProxyMutable> eeprom_proxy_mutable (_MemType _Struct::*_Member)
      { return EepromProxy<_MemType,EepromProxyMutable> { eeprom_member_addr (_Member) } ; }

#define EEPROM_MEMBER_MUTABLE(m) \
   constexpr decltype (eeprom_proxy_mutable (&Mutable::m)) m (void) { return eeprom_proxy_mutable (&Mutable::m) ; }

////////////////////////////////////////////////////////////////////////////////////////////

class Eeprom
{
public:
   enum ModbusSingles : uint8_t
   {
      Generic,
      _Count = 16
   } ;

   struct WlanCreds
   {
      static constexpr uint8_t MAX_SSID_LEN = 32 ;
      static constexpr uint8_t MAX_PSK_LEN = 64 ;

      char m_ssid[MAX_SSID_LEN] ;
      char m_passwd[MAX_PSK_LEN] ;
      uint8_t m_bssid[6] ;
      uint8_t m_channel ;
   } ;

   struct HostName
   {
      static constexpr uint8_t MAX_LEN = 32 ;
      char str[MAX_LEN] ;
   } ;

   struct NtpParms
   {
      EepromString ntp_server ;
      int32_t ntp_offset ;
      Time sync_period ;
   } ;

   struct Room
   {
      uint8_t subnum ;
      EepromString name ;
      util::TempVal target ;
      bool regulated ;
   } ;

   static constexpr auto MAX_ROOMS = app::Rooms::MAX_ROOMS ;

   struct Mqtt
   {
      EepromString server ;
      uint16_t port ;
   } ;

   struct Subscription
   {
      EepromString topic,
                   key,
                   tempval ;
   } ;

   static constexpr uint8_t MAX_SUBSCRIPTIONS = 4 ;

   struct Outdoor
   {
      uint8_t subnum ;
      EepromString key ;
   } ;

   struct Regulator
   {
      app::Regulator::Pid pid ;
      app::Regulator::MinMax minmax ;
      uint8_t fallback_boiler_temp ;
   } ;

   struct Boiler
   {
      app::Boiler::Mode mode ;
      util::TempVal dhw, standby ;
   } ;

private:
   struct Data
   {
      BootFlag boot_flag ;
      BootStatistics boot_stats ;
      WlanCreds wlan_creds ;
      HostName host_name ;

      const char CLEANUP_MARKER[0] ;

      NtpParms ntp_parms ;

      Mqtt mqtt ;
      Subscription subscriptions[MAX_SUBSCRIPTIONS] ;
      Room rooms[MAX_ROOMS] ;
      Outdoor outdoor ;
      Regulator regulator ;
      Boiler boiler ;

      bus::OneWire::DEVID tsens_addr ;
      uint8_t modbus_addrs[ModbusSingles::_Count] ;
   } ;

   struct Mutable
   {
      float regI ;
   } ;

public:
   EEPROM_MEMBER (boot_flag) ;
   EEPROM_MEMBER (boot_stats) ;
   EEPROM_MEMBER (wlan_creds) ;
   EEPROM_MEMBER (host_name) ;
   EEPROM_MEMBER (ntp_parms) ;
   EEPROM_MEMBER (rooms) ;
   EEPROM_MEMBER (mqtt) ;
   EEPROM_MEMBER (outdoor) ;
   EEPROM_MEMBER (regulator) ;
   EEPROM_MEMBER (boiler) ;
   EEPROM_MEMBER (subscriptions) ;
   EEPROM_MEMBER (tsens_addr) ;
   EEPROM_MEMBER (modbus_addrs) ;

   EEPROM_MEMBER_MUTABLE (regI) ;

   static constexpr uint16_t SIZE = sizeof (Data) ;
   static constexpr size_t MUTABLE_SIZE = sizeof (Mutable) ;

   static_assert( SIZE <= EepromProxyMutable::BEGIN, "EEPROM contents is too large!" ) ;

   Eeprom (void) ;

   void clear_all (void) ;
   void clear_mutable (void) ;

private:
   void _clear_range (size_t begin, size_t end) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
