////////////////////////////////////////////////////////////////////////////////////////////
//
// client-sink.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "pstr-holder.h"
#include "lan/lan-socket.h"
#include "aux/array.h"
#include "aux/range.h"
#include "aux/string.h"

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

struct endl
{
} ;

struct divider
{
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class ClientSink
{
   using pstr = util::pstr ;

   typedef uint16_t BUFFER_SIZE_TYPE ;
   static constexpr BUFFER_SIZE_TYPE BUFFER_SIZE = 1022 ;

public:
   enum class Result : uint8_t { Ok, Unknown, Error } ;

   class Holder
   {
      lan::Socket& m_sock ;
      Holder (lan::Socket& sock) : m_sock{ sock } {}
      friend class ClientSink ;
   } ;

public:
   ClientSink (lan::Socket& sock) : m_sock{ sock } {}
   ClientSink (Holder h) ;
   ~ClientSink (void) { commit() ; }

   template<typename _Head, typename ... _Args>
      ClientSink& write (const _Head& head, const _Args& ... args)
         { _write (head) ; write (args...) ; return *this ; }

   lan::Socket& sock (void) const { return m_sock ; }

   void write (void) {}

   template<typename _Arg>
      ClientSink& operator<< (const _Arg& arg)
         { _write (arg) ; return *this ; }

   void commit (void) ;

   Holder hold (void) { return m_sock ; }

private:
   template<typename _Tp>
      void _write (const _Tp& p)
         { _write_raw (&p, sizeof (p)) ; }

   template<typename _It>
      void _write (const aux::Range<_It>& r)
         { for (auto& a : r) _write (a) ; }

   void _write (const aux::String& str) { _write (range (str)) ; }
   void _write (const char *str) ;
   void _write (pstr str) ;
   void _write (endl) { write ('\r', '\n') ; }
   void _write (divider) ;
   void _write (Result p) ;

   void _write_raw (const void *data, uint8_t sz) ;

private:
   lan::Socket& m_sock ;
   aux::ArrayImpl<uint8_t, BUFFER_SIZE_TYPE, BUFFER_SIZE> m_buf ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
