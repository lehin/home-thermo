////////////////////////////////////////////////////////////////////////////////////////////
//
// tempval.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/string.h"
#include <stdint.h>

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

class Fixed
{
   explicit constexpr Fixed (int16_t raw) : m_raw{ raw } {}

public:
   Fixed (void) {}

   constexpr Fixed (int8_t integer, int8_t frac) :
      m_raw{ (int16_t)integer * 100 + (integer < 0 ? -frac : frac) }
   {
   }

   explicit constexpr Fixed (float f) :
      m_raw{ static_cast<int16_t> (f * 100 + (f >= 0 ? 0.5 : -0.5)) }
   {
   }

   friend inline bool operator< (const Fixed& lhs, const Fixed& rhs) { return lhs.m_raw < rhs.m_raw ; }
   friend inline bool operator<= (const Fixed& lhs, const Fixed& rhs) { return lhs.m_raw <= rhs.m_raw ; }
   friend inline bool operator> (const Fixed& lhs, const Fixed& rhs) { return lhs.m_raw > rhs.m_raw ; }
   friend inline bool operator>= (const Fixed& lhs, const Fixed& rhs) { return lhs.m_raw >= rhs.m_raw ; }
   friend inline bool operator== (const Fixed& lhs, const Fixed& rhs) { return lhs.m_raw == rhs.m_raw ; }
   friend inline bool operator!= (const Fixed& lhs, const Fixed& rhs) { return lhs.m_raw != rhs.m_raw ; }

   friend Fixed operator+ (const Fixed& lhs, const Fixed& rhs) { return from_raw (lhs.m_raw + rhs.m_raw) ; }
   friend Fixed operator- (const Fixed& lhs, const Fixed& rhs) { return from_raw (lhs.m_raw - rhs.m_raw) ; }

   friend inline Fixed& operator+= (Fixed& lhs, const Fixed& rhs) { lhs.m_raw += rhs.m_raw ; return lhs ; }
   friend inline Fixed& operator-= (Fixed& lhs, const Fixed& rhs) { lhs.m_raw -= rhs.m_raw ; return lhs ; }

   inline Fixed operator- (void) { return from_raw (-m_raw) ; }

   Fixed round (int8_t digits) const ;

   int8_t integer (void) const { return m_raw / 100 ; }
   int8_t frac (void) const { return m_raw % 100 ; }

   explicit operator float (void) const { return m_raw / 100.0f ; }
   explicit operator int16_t (void) const { return m_raw ; }

   aux::StringRef format (bool sign = false, bool low_prec = false) const ;

   static constexpr Fixed undefined (void) { return Fixed{ (int16_t)0x8000 } ; }
   static constexpr Fixed from_raw (int16_t raw) { return Fixed{ raw } ; }

   Fixed abs (void) const { return from_raw (m_raw < 0 ? -m_raw : m_raw) ; }

   bool is_defined (void) const { return *this != undefined() ; }
   explicit operator bool (void) const { return is_defined() ; }

private:
   int16_t m_raw ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

inline Fixed abs (Fixed val) { return val.abs() ; }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util

////////////////////////////////////////////////////////////////////////////////////////////

inline constexpr util::Fixed operator "" _f (long double f)
   { return util::Fixed::from_raw ((int16_t)(f * 100)) ; }

inline constexpr util::Fixed operator "" _f (unsigned long long degree)
   { return util::Fixed::from_raw ((int16_t)degree * 100) ; }

////////////////////////////////////////////////////////////////////////////////////////////
