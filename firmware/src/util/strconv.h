////////////////////////////////////////////////////////////////////////////////////////////
//
// strconv.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/string.h"
#include "bus/one-wire.h"
#include <stdint.h>

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

aux::String dev_id_to_string (const bus::OneWire::DEVID& id) ;
bool dev_id_from_string (bus::OneWire::DEVID& id, const aux::StringRef& s) ;

inline bool dev_id_from_string (bus::OneWire::DEVID& id, const aux::String& s)
   { return dev_id_from_string (id, range (s)) ; }

char *itoa (uint8_t n, char *dst, bool leadings = false, uint8_t div = 100) ;
char *itoa (uint16_t n, char *dst, bool leadings = false, uint16_t div = 10000) ;
char* itoa (uint32_t n, char *dst, bool leadings = false, uint32_t div = 1000000000ul) ;
char* itoa (int32_t n, char *dst, bool leadings = false, uint32_t div = 1000000000ul) ;
char* itoah (uint8_t n, char *dst) ;
char* itoah (uint16_t n, char *dst) ;
char* itoah (int32_t n, char *dst) ;
char* itoah (uint32_t n, char *dst) ;

uint8_t atoib (const char *s, const char *end, const char **err = nullptr) ;
uint16_t atoiw (const char *s, const char *end, const char **err = nullptr) ;
int32_t atol (const char *s, const char *end, const char **err = nullptr) ;
uint32_t atoul (const char *s, const char *end, const char **err = nullptr) ;
uint8_t ahtoib (const char *s, const char *end, const char **err = nullptr) ;
uint16_t ahtoiw (const char *s, const char *end, const char **err = nullptr) ;
int16_t atoi (const char *s, const char *end, const char **err = nullptr) ;
float atof (const char *s, const char *end) ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
