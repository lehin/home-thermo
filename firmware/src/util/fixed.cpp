////////////////////////////////////////////////////////////////////////////////////////////
//
// tempval.cpp
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "fixed.h"
#include "strconv.h"

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

Fixed Fixed::round (int8_t digits) const
{
   if (!is_defined())
      return undefined() ;

   int16_t exp = 1 ;

   for (digits = 2 - digits; digits > 0; --digits)
      exp *= 10 ;

   return from_raw ((m_raw + (m_raw < 0 ? -1 : 1) * exp / 2) / exp * exp) ;
}

aux::StringRef Fixed::format (bool sign, bool low_prec) const
{
   static constexpr char _undefined[] = "--.--" ;
   static constexpr char _undefined_sign[] = " --.--" ;

   static constexpr char _undefined_low[] = "--.-" ;
   static constexpr char _undefined_sign_low[] = " --.-" ;

   if (!is_defined())
   {
      auto s = low_prec ?
         (sign ? _undefined_sign_low : _undefined_low) :
         (sign ? _undefined_sign : _undefined) ;

      return { s, s + strlen (s) } ;
   }

   static char buf[16] ;

   char *ptr = buf ;

   auto val = m_raw ;
   if (val < 0)
   {
      *ptr++ = '-' ;
      val = -val ;
   }
   else if (val > 0 && sign)
      *ptr++ = '+' ;

   if (low_prec)
      val += 5 ;

   ptr = itoa ((uint8_t)(val / 100), ptr) ;
   *ptr++ = '.' ;
   ptr = itoa ((uint8_t)((val % 100) / (low_prec ? 10 : 1)), ptr, true, low_prec ? 1 : 10) ;
   return { buf + 0, ptr } ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
