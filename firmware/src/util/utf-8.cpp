////////////////////////////////////////////////////////////////////////////////////////////
//
// utf-8.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "utf-8.h"
#include <avr/pgmspace.h>

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   struct Utf8Decoder
   {
   public:
      uint16_t operator() (uint8_t ch)
      {
         switch (m_state)
         {
            case State::Regular :
            {
               m_cnt = _symbol_length (ch) ;

               if (m_cnt == 1)
                  return ch ;
               else if (!m_cnt || m_cnt > 3)
                  m_state = State::Skipping ;
               else
               {
                  m_val = ch & ((0b10000000 >> m_cnt) - 1) ;
                  m_state = State::Accumulating ;
               }
               break ;
            }

            case State::Accumulating:
               if ((ch & 0b11000000) == 0b10000000)
                  m_val = (m_val << 6) | (ch & 0b00111111) ;
               else
               {
                  m_state = State::Skipping ;
                  m_cnt = 1 ;
               }
               break ;

            case State::Skipping:
               break ;
         }

         if (--m_cnt)
            return 0 ;

         if (m_state == State::Skipping)
            m_val = 0xffff ;

         m_state = State::Regular ;

         return m_val ;
      }

   private:
      static uint8_t _symbol_length (uint8_t ch)
      {
         if (!(ch & 0x80))
            return 1 ;

         if ((ch & 0b11100000) == 0b11000000) return 2 ;
         if ((ch & 0b11110000) == 0b11100000) return 3 ;
         if ((ch & 0b11111000) == 0b11110000) return 4 ;
         if ((ch & 0b11111100) == 0b11111000) return 5 ;
         if ((ch & 0b11111110) == 0b11111100) return 6 ;

         return 0 ;
      }

   private:
      enum class State{ Regular, Accumulating, Skipping } ;
      State m_state = State::Regular ;

      uint16_t m_val ;
      uint8_t m_cnt ;
   } ;
}

////////////////////////////////////////////////////////////////////////////////////////////

bool is_ascii_string (aux::StringRef s)
{
   for (auto ch : s)
      if (ch & 0x80)
         return false ;

   return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static uint8_t _character_length (aux::StringRef s)
   {
      uint8_t res = 0 ;

      Utf8Decoder d ;
      for (auto ch : s) if (d (ch)) ++res ;

      return res ;
   }
}

aux::String utf8_decode (aux::StringRef s)
{
   static const char koi8_rus[] PROGMEM =
   {
     0x3f, 0xb3, 0x3f, 0x3f, 0x3f, 0x3f, 0x3f, 0x3f, 0x3f, 0x3f, 0x3f, 0x3f,
     0x3f, 0x3f, 0x3f, 0x3f, 0xe1, 0xe2, 0xf7, 0xe7, 0xe4, 0xe5, 0xf6, 0xfa,
     0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef, 0xf0, 0xf2, 0xf3, 0xf4, 0xf5,
     0xe6, 0xe8, 0xe3, 0xfe, 0xfb, 0xfd, 0xff, 0xf9, 0xf8, 0xfc, 0xe0, 0xf1,
     0xc1, 0xc2, 0xd7, 0xc7, 0xc4, 0xc5, 0xd6, 0xda, 0xc9, 0xca, 0xcb, 0xcc,
     0xcd, 0xce, 0xcf, 0xd0, 0xd2, 0xd3, 0xd4, 0xd5, 0xc6, 0xc8, 0xc3, 0xde,
     0xdb, 0xdd, 0xdf, 0xd9, 0xd8, 0xdc, 0xc0, 0xd1, 0x3f, 0xa3, 0x0a
   } ;

   auto dstlen = _character_length (s) ;
   aux::String res{ dstlen, dstlen } ;
   auto dst = res.begin() ;

   Utf8Decoder decoder ;
   for (auto ch : s)
   {
      auto d = decoder (ch) ;
      if (!d)
         continue ;

      if (d < 128)
         *dst = (char)d ;
      else if (d >= 0x400 && d < 0x400 + sizeof(koi8_rus))
         *dst = pgm_read_byte (&koi8_rus[d-0x400]) ;
      else
         *dst = '?' ;

      ++dst ;
   }

   return res ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   struct Utf8Encoder
   {
      template<typename _Sink>
         void operator() (uint16_t utfc, _Sink sink) const
         {
            switch (utf_length (utfc))
            {
               case 1:
                  sink ((char)utfc) ;
                  break ;

               case 2:
                  sink ((char)((utfc >> 6) | 0b11000000)) ;
                  sink ((char)((utfc & 0b00111111) | 0b10000000)) ;
                  break ;

               case 3:
                  sink ((char)((utfc >> 12) | 0b11100000)) ;
                  sink ((char)(((utfc >> 6) & 0b00111111) | 0b10000000)) ;
                  sink ((char)((utfc & 0b00111111) | 0b10000000)) ;
                  break ;
            }
         }

      uint8_t utf_length (unsigned short utfc) const
      {
         if (utfc < 0x80)
            return 1 ;
         else if (utfc < 0x800)
            return 2 ;

         return 3 ;
      }
   } ;
}

aux::String utf8_encode (aux::StringRef s)
{
   static const uint16_t utf8_rus[] PROGMEM =
   {
      0x2500, 0x2502, 0x250C, 0x2510, 0x2514, 0x2518, 0x251C, 0x2524, 0x252C, 0x2534, 0x253C, 0x2580, 0x2584, 0x2588, 0x258C, 0x2590,
      0x2591, 0x2592, 0x2593, 0x2320, 0x25A0, 0x2219, 0x221A, 0x2248, 0x2264, 0x2265, 0x00A0, 0x2321, 0x00B0, 0x00B2, 0x00B7, 0x00F7,
      0x2550, 0x2551, 0x2552, 0x0451, 0x2553, 0x2554, 0x2555, 0x2556, 0x2557, 0x2558, 0x2559, 0x255A, 0x255B, 0x255C, 0x255D, 0x255E,
      0x255F, 0x2560, 0x2561, 0x0401, 0x2562, 0x2563, 0x2564, 0x2565, 0x2566, 0x2567, 0x2568, 0x2569, 0x256A, 0x256B, 0x256C, 0x00A9,
      0x044E, 0x0430, 0x0431, 0x0446, 0x0434, 0x0435, 0x0444, 0x0433, 0x0445, 0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E,
      0x043F, 0x044F, 0x0440, 0x0441, 0x0442, 0x0443, 0x0436, 0x0432, 0x044C, 0x044B, 0x0437, 0x0448, 0x044D, 0x0449, 0x0447, 0x044A,
      0x042E, 0x0410, 0x0411, 0x0426, 0x0414, 0x0415, 0x0424, 0x0413, 0x0425, 0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E,
      0x041F, 0x042F, 0x0420, 0x0421, 0x0422, 0x0423, 0x0416, 0x0412, 0x042C, 0x042B, 0x0417, 0x0428, 0x042D, 0x0429, 0x0427, 0x042A
   } ;

   Utf8Encoder encoder ;

   uint8_t utflen = 0 ;

   for (uint8_t ch : s)
      if (ch < 0x80)
         ++utflen ;
      else
         utflen += encoder.utf_length (pgm_read_word (&utf8_rus[ch-0x80])) ;

   aux::String res{ utflen, utflen } ;

   auto *dst = res.begin() ;
   for (uint8_t ch : s)
      if (ch < 0x80)
         *dst++ = ch ;
      else
         encoder
            (
               pgm_read_word (&utf8_rus[ch-0x80]),
               [&dst] (char ch) { *dst++ = ch ; }
            ) ;

   return res ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util

