////////////////////////////////////////////////////////////////////////////////////////////
//
// datetime.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/string.h"

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

struct Date
{
   uint8_t m_weekday,
           m_day,
           m_month ;
   uint16_t m_year ;

   int operator- (const Date& rhs) const
      { return (int)(total_days() - rhs.total_days()) ; }

   bool operator== (const Date& rhs) const
      { return m_day == rhs.m_day && m_month == rhs.m_month && m_year == rhs.m_year ; }

   bool operator!= (const Date& rhs) const
      { return !operator== (rhs) ; }

   long total_days (void) const ;
   static Date from_days (long days) ;

   char *to_string (char *buf, bool weekday = false) const ;
   char *weekday_str (char *buf) const ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct Time
{
   uint8_t m_sec,
           m_min,
           m_hour ;

   long operator- (const Time& rhs) const
      { return total_seconds() - rhs.total_seconds() ; }

   bool operator== (const Time& rhs) const
      { return m_sec == rhs.m_sec && m_min == rhs.m_min && m_hour == rhs.m_hour ; }

   bool operator!= (const Time& rhs) const
      { return !operator== (rhs) ; }

   long total_seconds (void) const ;
   static Time from_seconds (long seconds) ;

   static constexpr long day_seconds (void) { return 24 * 3600l ; }

   char *to_string (char *buf, bool seconds = true) const ;

   static Time from_string (const aux::StringRef& s) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct DateTime
{
   Time m_tm ;
   Date m_dt ;

   int32_t operator- (const DateTime& rhs) const ;

   bool operator== (const DateTime& rhs) const
      { return m_tm == rhs.m_tm && m_dt == rhs.m_dt ; }

   bool operator!= (const DateTime& rhs) const
      { return !operator== (rhs) ; }

   char *to_string (char *buf, bool weekday = false) const ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
