////////////////////////////////////////////////////////////////////////////////////////////
//
// strconv.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "strconv.h"
#include <math.h>

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   inline char _to_hex (uint8_t val)
   {
      val &= 0x0F ;
      return (val < 10 ? '0' : 'A' - 10) + val;
   }

   inline uint8_t _from_hex (char ch)
   {
      if (ch >= '0' && ch <= '9')
         return ch - '0' ;

      if (ch >= 'A' && ch <= 'F')
         return ch - 'A' + 10 ;

      return (uint8_t)-1 ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

aux::String dev_id_to_string (const bus::OneWire::DEVID& id)
{
   aux::String res{ 16, 16 } ;

   auto dst = res.begin() ;
   for (uint8_t byte = 0; byte < 8; ++byte)
   {
      *dst++ = _to_hex (id.m_data[7-byte] >> 4) ;
      *dst++ = _to_hex (id.m_data[7-byte] & 0x0F) ;
   }

   return res ;
}

bool dev_id_from_string (bus::OneWire::DEVID& id, const aux::StringRef& s)
{
   auto src = s.begin() ;

   for (uint8_t byte = 0; byte < 8; ++byte)
   {
      if (src == s.end()) return false ;
      uint8_t hi = _from_hex (*src++) ;
      if (hi == (uint8_t)-1) return false ;
      if (src == s.end()) return false ;
      uint8_t lo = _from_hex (*src++) ;
      if (lo == (uint8_t)-1) return false ;

      id.m_data[7-byte] = (hi << 4) | lo ;
   }

   return true ;
}

char *itoa (uint8_t n, char *dst, bool leadings, uint8_t div)
{
   for (; div; div /= 10)
   {
      auto dig = n / div ;

      if (dig || div == 1)
         leadings = true ;

      if (leadings)
         *dst++ = dig + '0' ;

      n %= div ;
   }

   return dst ;
}

char *itoa (uint16_t n, char *dst, bool leadings, uint16_t div)
{
   for (; div; div /= 10)
   {
      auto dig = n / div ;

      if (dig || div == 1)
         leadings = true ;

      if (leadings)
         *dst++ = dig + '0' ;

      n %= div ;
   }

   return dst ;
}

char* itoa (uint32_t n, char *dst, bool leadings, uint32_t div)
{
   for (; div; div /= 10)
   {
      auto dig = n / div ;

      if (dig || div == 1)
         leadings = true ;

      if (leadings)
         *dst++ = dig + '0' ;

      n %= div ;
   }

   return dst ;
}

char* itoa (int32_t n, char *dst, bool leadings, uint32_t div)
{
   if (n < 0)
   {
      *dst++ = '-' ;
      n *= -1 ;
   }

   for (; div; div /= 10)
   {
      auto dig = n / div ;

      if (dig || div == 1)
         leadings = true ;

      if (leadings)
         *dst++ = dig + '0' ;

      n %= div ;
   }

   return dst ;
}

char* itoah (uint8_t n, char *dst)
{
   for (uint8_t quad = 8; quad; quad -= 4)
      *dst++ = _to_hex (n >> (quad - 4)) ;

   return dst ;
}

char* itoah (uint16_t n, char *dst)
{
   for (uint8_t quad = 16; quad; quad -= 4)
      *dst++ = _to_hex (n >> (quad - 4)) ;

   return dst ;
}

char* itoah (uint32_t n, char *dst)
{
   for (uint8_t quad = 32; quad; quad -= 4)
      *dst++ = _to_hex (n >> (quad - 4)) ;

   return dst ;
}

char* itoah (int32_t n, char *dst)
{
   return itoah ((uint32_t)n, dst) ;
}

uint8_t atoib (const char *s, const char *end, const char **err)
{
   return atoiw (s, end, err) ;
}

uint16_t atoiw (const char *s, const char *end, const char **err)
{
   uint16_t res = 0 ;
   for (; s != end && *s >= '0' && *s <= '9'; ++s)
   {
      res *= 10 ;
      res += *s - '0' ;
   }

   if (err)
      *err = s ;

   return res ;
}

int32_t atol (const char *s, const char *end, const char **err)
{
   if (s == end)
      return 0 ;

   int8_t mult = 1 ;
   if (*s == '+') ++s ;
   else if (*s == '-')
   {
      mult = -1 ;
      ++s ;
   }

   int32_t res = 0 ;
   for (; s != end && *s >= '0' && *s <= '9'; ++s)
   {
      res *= 10 ;
      res += *s - '0' ;
   }

   if (err)
      *err = s ;

   return res * mult ;
}

uint32_t atoul (const char *s, const char *end, const char **err)
{
   if (s == end)
      return 0 ;

   uint32_t res = 0 ;
   for (; s != end && *s >= '0' && *s <= '9'; ++s)
   {
      res *= 10u ;
      res += *s - '0' ;
   }

   if (err)
      *err = s ;

   return res ;
}

uint8_t ahtoib (const char *s, const char *end, const char **err)
{
   return ahtoiw (s, end, err) ;
}

uint16_t ahtoiw (const char *s, const char *end, const char **err)
{
   uint16_t res = 0 ;
   for (; s != end ; ++s)
   {
      if (*s >= '0' && *s <= '9')
      {
         res <<= 4 ;
         res += *s - '0' ;
      }
      else if (*s >= 'A' && *s <= 'F')
      {
         res <<= 4 ;
         res += *s - 'A' + 10 ;
      }
      else if (*s >= 'a' && *s <= 'f')
      {
         res <<= 4 ;
         res += *s - 'a' + 10 ;
      }
      else
         break ;
   }

   if (err)
      *err = s ;

   return res ;
}

int16_t atoi (const char *s, const char *end, const char **err)
{
   if (s == end)
      return 0 ;

   int8_t mult = 1 ;
   if (*s == '+') ++s ;
   else if (*s == '-')
   {
      mult = -1 ;
      ++s ;
   }

   int16_t res = 0 ;
   for (; s != end && *s >= '0' && *s <= '9'; ++s)
   {
      res *= 10 ;
      res += *s - '0' ;
   }

   if (err)
      *err = s ;

   return res * mult ;
}

float atof (const char *s, const char *end)
{
   if (s == end)
      return NAN ;

   bool negative = *s == '-' ;
   if (negative)
      ++s ;

   if (s == end)
      return NAN ;

   float result = 0 ;

   for (; s != end && *s >= '0' && *s <= '9'; ++s)
   {
      result *= 10 ;
      result += *s - '0' ;
   }

   if (s == end || *s != '.')
      return !negative ? result : -result ;

   ++s ;

   float scale = 0.1 ;

   for (; s != end && *s >= '0' && *s <= '9'; ++s, scale /= 10)
      result += scale * (*s - '0') ;

   return !negative ? result : -result ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
