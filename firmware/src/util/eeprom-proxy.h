////////////////////////////////////////////////////////////////////////////////////////////
//
// eeprom-proxy.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>
#include <avr/eeprom.h>

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

struct EepromExchange
{
   template<typename _Tp>
      static void read (uint16_t addr, _Tp& val)
      {
         eeprom_read_block (&val, (const void*)addr, sizeof (_Tp)) ;
      }

   template<typename _Tp>
      static void write (uint16_t addr, const _Tp& val)
      {
         eeprom_update_block (&val, (void*)addr, sizeof (_Tp)) ;
      }

   static void read (uint16_t addr, uint8_t& val)
   {
      val = eeprom_read_byte ((const uint8_t*)addr) ;
   }

   static void write (uint16_t addr, uint8_t val)
   {
      eeprom_update_byte ((uint8_t*)addr, val) ;
   }

   static void read (uint16_t addr, uint16_t& val)
   {
      val = eeprom_read_word ((const uint16_t*)addr) ;
   }

   static void write (uint16_t addr, uint16_t val)
   {
      eeprom_update_word ((uint16_t*)addr, val) ;
   }

   static void read (uint16_t addr, uint32_t& val)
   {
      val = eeprom_read_dword ((const uint32_t*)addr) ;
   }

   static void write (uint16_t addr, uint32_t val)
   {
      eeprom_update_dword ((uint32_t*)addr, val) ;
   }

   static void reset (uint16_t addr, size_t size)
   {
      while (size--)
         eeprom_write_byte ((uint8_t*)addr++, 255) ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct EepromProxyBase
{
   template<typename _Tp>
      static void eep_read (uint16_t addr, _Tp& data)
      {
         EepromExchange::read (addr, data) ;
      }

   template<typename _Tp>
      static void eep_write (uint16_t addr, const _Tp& data)
      {
         EepromExchange::write (addr, data) ;
      }

   template<typename _Tp>
      static void eep_reset (uint16_t addr, const _Tp&)
      {
         EepromExchange::reset (addr, sizeof (_Tp)) ;
      }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp, typename _Base>
   class ValueProxy : _Base
   {
      typedef _Base base_class ;

      union Convertor
      {
         _Tp m_val ;
         uint8_t m_bytes[sizeof (_Tp)] ;
      } ;

   public:
      explicit constexpr ValueProxy (uint16_t addr) : m_addr { addr } {}

      operator _Tp (void) const { return operator*() ; }

      _Tp operator* (void) const
      {
         _Tp result ;
         base_class::eep_read (m_addr, result) ;
         return result ;
      }

      const ValueProxy& operator= (const _Tp& val) const
      {
         base_class::eep_write (m_addr, val) ;
         return *this ;
      }

      constexpr uint16_t address (void) const { return m_addr ; }

      void reset (void) const { base_class::eep_reset (m_addr, _Tp{}) ; }

   private:
      const uint16_t m_addr ;
   } ;

template<typename _Tp, typename _Base>
   class ArrayProxy
   {
      typedef ValueProxy<_Tp,_Base> value_proxy ;

   public:
      explicit constexpr ArrayProxy (uint16_t addr) : m_addr { addr } {}

      value_proxy operator[] (uint8_t idx) const { return value_proxy{ m_addr + idx * sizeof (_Tp) } ; }
      value_proxy operator* (void) const { return operator[](0) ; }

   private:
      const uint16_t m_addr ;
   } ;

template<typename _Tp, typename _Base = EepromProxyBase>
   class EepromProxy : public ValueProxy<_Tp,_Base>
   {
      typedef ValueProxy<_Tp,_Base> base_class ;

   public:
      explicit constexpr EepromProxy (uint16_t addr) : base_class { addr } {}

      using base_class::operator= ;
   } ;

template<typename _Tp, unsigned sz, typename _Base>
   class EepromProxy<_Tp[sz],_Base> : public ArrayProxy<_Tp,_Base>
   {
      typedef ArrayProxy<_Tp,_Base> base_class ;

   public:
      explicit constexpr EepromProxy (uint16_t addr) : base_class { addr } {}
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
