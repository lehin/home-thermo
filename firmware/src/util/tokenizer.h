////////////////////////////////////////////////////////////////////////////////////////////
//
// tokenizer.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/string.h"

namespace util
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace impl
{
   struct Tokenizer
   {
      template<typename _Sink>
         bool operator() (const aux::StringRef& s, _Sink sink) const
         {
            const char *ptr = _skip_ws (s.begin(), s.end()) ;
            while (ptr != s.end())
            {
               const char *end = _find_ws (ptr, s.end()) ;
               if (!sink (aux::range (ptr, end)))
                  return false ;

               ptr = _skip_ws (end, s.end()) ;
            }

            return true ;
         }

      const char *_skip_ws (const char *ptr, const char *end) const ;
      const char *_find_ws (const char *ptr, const char *end) const ;
   } ;
}

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Sink> inline
   bool tokenize (const aux::StringRef& s, _Sink sink)
      { return impl::Tokenizer{} (s, sink) ; }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace util
