////////////////////////////////////////////////////////////////////////////////////////////
//
// rtc.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "bus/i2c.h"
#include "aux/signal.h"
#include "util/datetime.h"
#include <avr/interrupt.h>

ISR (PCINT2_vect) ;

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Rtc RTC ;

////////////////////////////////////////////////////////////////////////////////////////////

class Rtc
{
public:
   using Date = util::Date ;
   using Time = util::Time ;
   using DateTime = util::DateTime ;

public:
   void initialize (void) ;
   void enable (void) ;
   void disable (void) ;

   Time time (void) ;
   Date date (void) ;

   void time (const Time& tm) ;
   void date (const Date& dt) ;

   DateTime date_time (void) { return { time(), date() } ; }
   DateTime start_time (void) { return m_startup ; }

   typedef aux::Signal<void(void)> SigSecondsTick ;
   SigSecondsTick& signal_seconds_tick (void) { return m_signal_seconds_tick ; }

private:
   void _on_data_read (uint8_t errno, bus::I2c::Rx& rx) ;
   void _read_data (void) ;

   static void _seconds_tick (void) ;

private:
   struct ChipMem
   {
      struct Time
      {
         uint8_t m_sec : 4,
                 m_sec10 : 3 ;

         bool m_hold : 1 ;

         uint8_t m_min : 4,
                 m_min10 : 3,
                 m_reserved1 : 1 ;

         uint8_t m_hour : 4,
                 m_hour10 : 2,
                 m_reserved2 : 2 ;
      } m_tm ;

      struct Date
      {
         uint8_t m_weekday : 3,
                 m_reserved3 : 5 ;

         uint8_t m_day : 4,
                 m_day10 : 2,
                 m_reserved4 : 2 ;

         uint8_t m_mon : 4,
                 m_mon10 : 1,
                 m_reserved5 : 3 ;

         uint8_t m_year : 4,
                 m_year10 : 4 ;
      } m_dt ;
   } ;

   static_assert( sizeof (ChipMem) == 7, "Wrong size of RTC RAM struct!" ) ;

   ChipMem m_mem ;
   DateTime m_startup ;
   bool m_initialized = false ;

   SigSecondsTick m_signal_seconds_tick ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
