////////////////////////////////////////////////////////////////////////////////////////////
//
// opentherm.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "opentherm-als.h"
#include "aux/signal.h"

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class OpenTherm OPENTHERM ;

////////////////////////////////////////////////////////////////////////////////////////////

class OpenTherm
{
public:
   OpenTherm (void) ;

   void initialize (void) ;
   void reset_currents (void) ;

   void ch_temp (util::TempVal t) ;
   util::TempVal ch_temp (void) const { return m_temp_ch ; }

   void dhw_temp (util::TempVal t) ;
   util::TempVal dhw_temp (void) const { return m_temp_dhw ; }

   util::TempVal actual_temp (void) const { return m_actual_t ; }
   bool flame_active (void) const { return m_flame_active ; }

   enum class Mode : uint8_t { Off = 0, Ch = 1, Dhw = 2 } ;
   void set_mode (Mode mode) ;
   Mode get_mode (void) const { return m_running_mode ; }

   typedef aux::function<void(bool,uint8_t,uint8_t)> CmdResult ;
   void reset (CmdResult&& handler) ;
   void fault_data (CmdResult&& handler) ;

   typedef void (*ModeHandler) (Mode mode) ;
   void set_running_mode_handler (ModeHandler fn) { m_running_mode_handler = fn ; }

   typedef void (*FlameActiveHandler) (bool active) ;
   void set_flame_active_handler (FlameActiveHandler fn) { m_flame_active_handler = fn ; }

   enum class TempKind : uint8_t { Ch, Dhw, Actual } ;
   typedef void (*TempHandler) (TempKind kind, util::TempVal t) ;
   void set_temp_handler (TempHandler fn) { m_temp_handler = fn ; }

   uint8_t status_byte (void) const { return m_status_byte ; }
   util::TempVal actual_temperature (void) { return m_actual_t ; }

private:
   void _user_command (const opentherm::Message& msg, CmdResult&& handler) ;
   bool _set_temperature (util::TempVal t, util::TempVal& var, TempKind kind) ;
   void _set_running_mode (Mode mode) ;
   void _set_flame_active (bool active) ;
   void _ctrl_ch_setpoint (void) ;
   void _read_dhw_setpoint (void) ;
   void _on_status (const opentherm::Message& msg) ;
   void _on_ch_temp_set (const opentherm::Message& msg) ;
   void _on_dhw_setpoint_read (const opentherm::Message& msg) ;
   void _on_actual_temp_read (const opentherm::Message& msg) ;

private:
   uint8_t m_status_byte ;

   Mode m_running_mode ;
   bool m_flame_active ;

   util::TempVal m_temp_ch,
                 m_temp_dhw ;

   util::TempVal m_actual_t ;

   bool m_ch_setpoint_pending ;

   ModeHandler m_running_mode_handler ;
   FlameActiveHandler m_flame_active_handler ;
   TempHandler m_temp_handler ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

inline OpenTherm::Mode operator| (OpenTherm::Mode lhs, OpenTherm::Mode rhs)
   { return (OpenTherm::Mode) ((uint8_t)lhs | (uint8_t)rhs) ; }

inline bool operator& (OpenTherm::Mode lhs, OpenTherm::Mode rhs)
   { return ((uint8_t)lhs & (uint8_t)rhs) != 0 ; }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
