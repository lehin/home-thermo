////////////////////////////////////////////////////////////////////////////////////////////
//
// unified-device.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#if !defined (SERIALDEBUG)

#include "device-server.h"

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

class UnifiedDeviceBase : public DeviceBase
{
public:
   static constexpr uint8_t CommErr = 0x80 ;

protected:
   UnifiedDeviceBase (uint8_t poll_period, uint16_t eeprom_addr) ;

public:
   typedef void (*SigStateChanged) (uint8_t state, uint8_t changed) ;
   void on_state_changed (SigStateChanged fn) { m_signal_state_changed = fn ; }

   uint8_t state (void) const { return m_state ; }
   bool is_active (void) const { return !(m_state & CommErr) ; }

protected:
   uint8_t bus_addr (void) const { return m_bus_addr ; }
   void bus_addr (uint8_t addr) ;

   void control (uint8_t outputs) ;
   void set_state (uint8_t state) ;

   virtual void command (const aux::StringRef& cmd, util::ClientSink&& client) ;
   virtual void reboot (util::ClientSink&& client) ;

private:
   virtual void poll_impl (void) ;
   virtual void reset (void) {}

   void _poll_result (bus::Modbus::Result result, const bus::Modbus::Data& data) ;

private:
   const uint16_t m_eeprom_addr ;
   uint8_t m_bus_addr ;
   uint8_t m_state ;
   uint8_t m_errors ;
   bool m_indeterminate ;

   SigStateChanged m_signal_state_changed ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tag>
   using UnifiedDevice = StaticDevice<_Tag,UnifiedDeviceBase> ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals

////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined (SERIALDEBUG)
