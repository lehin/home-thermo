////////////////////////////////////////////////////////////////////////////////////////////
//
// temp-sensor.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "temp-sensor.h"
#include "util/eeprom.h"
#include "aux/scheduler.h"
#include "controller/system-master.h"

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

TempSensor TEMPSENSOR ;

////////////////////////////////////////////////////////////////////////////////////////////

bus::OneWire::DEVID TempSensor::m_id = bus::OneWire::DEVID::null() ;
int16_t TempSensor::m_temperature = -1 ;

TempSensor::TempSensor (void) :
   base_class{ (uint16_t)6000 }
{
   m_id = util::EEPROM.tsens_addr() ;
}

void TempSensor::command (const aux::StringRef& str, util::ClientSink&& client)
{
   auto it = str.begin() ;
   auto cmd = *it++ ;

   for (; it != str.end() && *it != ' '; ++it) ;
   for (; it != str.end() && *it == ' '; ++it) ;

   char buf[16] { 0 } ;

   switch (cmd)
   {
      case 'i': // ID
         if (it == str.end())
         {
            client << util::dev_id_to_string (m_id) << util::endl{} << util::ClientSink::Result::Ok ;
         }
         else
         {
            bool was_null = m_id.is_null() ;

            bool ok = util::dev_id_from_string (m_id, aux::range (it, str.end())) ;
            if (ok)
            {
               util::EEPROM.tsens_addr() = m_id ;
               if (was_null)
                  continue_poll() ;
            }

            client << (ok ? util::ClientSink::Result::Ok : util::ClientSink::Result::Error) ;
         }
         break ;

      case 't': // temperature
      {
         auto t = m_temperature ;
         char *it = buf ;

         if (t < 0)
         {
            *it++ = '-' ;
            t = -t ;
         }

         it = util::itoa ((uint8_t)(t >> 4), it) ;
         *it++ = '.' ;
         it = util::itoa ((uint8_t)(((t & 0b1111) * 100 + 8) / 16), it, true, 10) ;
         strcpy_P (it, PSTR("°C")) ;
         while (*++it) ;

         client << aux::range (buf + 0, it) << util::endl{} << util::ClientSink::Result::Ok ;
         break ;
      }

      case 'p': // precision
      {
         bool ok = false ;

         if (it != str.end())
         {
            auto prec = util::atoib (it, str.end()) ;
            if (prec < 9 || prec > 12)
               ok = false ;
            else
            {
               uint8_t byte = ((prec - 9) << 5) | 0x1F ;

               ok =
                  bus::ONEWIRE.reset() &&
                  bus::ONEWIRE.match_rom (m_id) &&
                  bus::ONEWIRE.send ('\x4E') &&
                  bus::ONEWIRE.send ('\0') &&
                  bus::ONEWIRE.send ('\0') &&
                  bus::ONEWIRE.send (byte) &&
                  bus::ONEWIRE.reset() &&
                  bus::ONEWIRE.match_rom (m_id) &&
                  bus::ONEWIRE.send ('\x48') ;
            }
         }

         client << (ok ? util::ClientSink::Result::Ok : util::ClientSink::Result::Error) ;

         break ;
      }

      default:
         client << util::ClientSink::Result::Error ;
   }
}

void TempSensor::poll_impl (void)
{
   if (m_id.is_null())
      return ;

   if
      (
         bus::ONEWIRE.reset() &&
         bus::ONEWIRE.match_rom (m_id) &&
         bus::ONEWIRE.send ('\x44')
      )
      aux::SCHED.schedule ([] { TEMPSENSOR._read_measurement() ; }, 1000) ;
   else
      continue_poll() ;
}

void TempSensor::_read_measurement (void)
{
   uint8_t lsb, msb ;

   if
      (
         bus::ONEWIRE.reset() &&
         bus::ONEWIRE.match_rom (m_id) &&
         bus::ONEWIRE.send ('\xBE') &&
         bus::ONEWIRE.read (&lsb) &&
         bus::ONEWIRE.read (&msb)
      )
      m_temperature = (int16_t)msb << 8 | lsb ;

   continue_poll() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
