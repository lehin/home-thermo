////////////////////////////////////////////////////////////////////////////////////////////
//
// device-server.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "lan/lan-socket.h"
#include "bus/modbus.h"
#include "util/client-sink.h"
#include "util/strconv.h"
#include <avr/pgmspace.h>

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class DeviceServer DEVSERVER ;

class DeviceList ;

////////////////////////////////////////////////////////////////////////////////////////////

class DeviceServer
{
   friend class DeviceBase ;

public:
   void start (void) ;
   void stop (void) ;
   bool is_running (void) { return m_flags.m_poll_enabled ; }

   DeviceList devices (void) ;

private:
   void _schedule (void) ;
   void _poll (void) ;

private:
   class DeviceBase *m_devices = nullptr ;

   struct Flags
   {
      bool m_poll_enabled : 1,
           m_poll_scheduled : 1 ;
   } m_flags = { false, false } ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class DeviceBase
{
   friend class DeviceList ;

   DeviceBase (const DeviceBase&) = delete ;
   int operator= (const DeviceBase&) = delete ;

protected:
   DeviceBase (uint16_t poll_period) ;
   ~DeviceBase (void) ;

public:
   void poll (void) ;

   virtual const char *name (void) const = 0 ;
   virtual void command (const aux::StringRef& cmd, util::ClientSink&& client) = 0 ;
   virtual void reboot (util::ClientSink&& client) {}

protected:
   virtual void poll_impl (void) = 0 ;
   void continue_poll (void) { m_poll_counter = m_poll_period ; }
   void set_poll_period (uint16_t period) { m_poll_period = period ; }

private:
   DeviceBase *m_prev,
              *m_next ;

   uint16_t m_poll_period ;
   uint16_t m_poll_counter ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

#define DECLARE_DEVICE_TAG(_Name) \
   struct _Name \
   { \
      static const char *name (void) \
      { \
         static const char _name[] PROGMEM = #_Name ; \
         return _name ; \
      } \
   }

template<typename _Tag, typename _Base = DeviceBase>
   class StaticDevice : public _Base
   {
      typedef _Base base_class ;
      typedef _Tag tag_type ;

   protected:
      typedef StaticDevice base_class_ ;

   public:
      template<typename ... _Args>
         StaticDevice (_Args ... args) :
            base_class{ args... }
         {
         }

      virtual const char *name (void) const { return tag_type::name() ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

class DeviceList
{
   friend class DeviceServer ;

private:
   DeviceList (DeviceBase *head) : m_head{ head } {}

public:
   class iterator
   {
   public:
      typedef DeviceBase value ;
      typedef value* pointer ;
      typedef value& reference ;

   public:
      iterator (DeviceBase *ptr) : m_ptr{ ptr } {}

      pointer operator-> (void) const { return m_ptr ; }
      reference operator* (void) const { return *m_ptr ; }

      iterator& operator++ (void) { m_ptr = m_ptr->m_next ; return *this ; }
      iterator& operator-- (void) { m_ptr = m_ptr->m_prev ; return *this ; }

      iterator operator++ (int) { iterator ret = *this ; ++*this ; return ret ; }
      iterator operator-- (int) { iterator ret = *this ; --*this ; return ret ; }

      friend inline bool operator== (const iterator& lhs, const iterator& rhs) { return lhs.m_ptr == rhs.m_ptr ; }
      friend inline bool operator!= (const iterator& lhs, const iterator& rhs) { return lhs.m_ptr != rhs.m_ptr ; }

   private:
      DeviceBase *m_ptr ;
   } ;

   iterator begin (void) const { return m_head ; }
   iterator end (void) const { return nullptr ; }

   bool empty (void) const { return !m_head ; }

   iterator find (const aux::StringRef& name) const ;

private:
   DeviceBase *m_head ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
