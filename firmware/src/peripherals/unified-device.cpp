////////////////////////////////////////////////////////////////////////////////////////////
//
// unified-device.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#if !defined (SERIALDEBUG)

#include "unified-device.h"
#include "util/eeprom.h"

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

UnifiedDeviceBase::UnifiedDeviceBase (uint8_t poll_period, uint16_t eeprom_addr) :
   DeviceBase{ poll_period },
   m_eeprom_addr{ eeprom_addr },
   m_state{ m_eeprom_addr == 255 ? CommErr : (uint8_t)0 },
   m_errors{ 0 },
   m_indeterminate{ true },
   m_signal_state_changed{ nullptr }
{
   util::EepromExchange::read (m_eeprom_addr, m_bus_addr) ;
}

void UnifiedDeviceBase::bus_addr (uint8_t addr)
{
   auto oldaddr = m_bus_addr ;

   util::EepromExchange::write (m_eeprom_addr, m_bus_addr = addr) ;

   if (oldaddr != 255 && m_bus_addr != 255)
      bus::MODBUS.request (oldaddr, 66, [addr] (bus::Modbus::Tx& tx) { tx << addr ; }) ;

   if (oldaddr == 255)
      continue_poll() ;

   if (m_bus_addr == 255)
      set_state (CommErr) ;
}

void UnifiedDeviceBase::control (uint8_t outputs)
{
   bus::MODBUS.request (bus_addr(), 65, [outputs] (bus::Modbus::Tx& tx){ tx << outputs ; }) ;
}

void UnifiedDeviceBase::set_state (uint8_t state)
{
   auto changed = state ^ m_state ;

   m_state = state ;

   if (m_indeterminate)
   {
      m_indeterminate = false ;
      changed = (uint8_t)-1 ;
   }

   if (changed && m_signal_state_changed)
      m_signal_state_changed (m_state, changed) ;
}

namespace
{
   static void _report_byte (util::ClientSink&& client, bus::Modbus::Result result, const bus::Modbus::Data& data)
   {
      if (result != bus::Modbus::Result::Ok || data.size() < sizeof (uint8_t))
         client << util::ClientSink::Result::Error ;
      else
      {
         char buf[3] ;
         auto end = util::itoa (*data.begin(), buf) ;
         client << aux::range (buf + 0, end) << util::endl{} << util::ClientSink::Result::Ok ;
      }
   }
}

void UnifiedDeviceBase::command (const aux::StringRef& str, util::ClientSink&& client)
{
   auto it = str.begin() ;
   auto cmd = *it++ ;

   for (; it != str.end() && *it != ' '; ++it) ;
   for (; it != str.end() && *it == ' '; ++it) ;

   char buf[4] { 0 } ;

   switch (cmd)
   {
      case 'a': // address
         if (it == str.end())
         {
            char *end = util::itoa (bus_addr(), buf) ;
            client << aux::range (buf + 0, end) << util::endl{} << util::ClientSink::Result::Ok ;
         }
         else
         {
            bus_addr (util::atoib (it, str.end())) ;
            client << util::ClientSink::Result::Ok ;
         }
         break ;

      case 's': // state
         client << aux::range (buf + 0, util::itoa (state(), buf)) << util::endl{} << util::ClientSink::Result::Ok ;
         break ;

      case 'v': // device firmware version
         bus::MODBUS.request
            (
               bus_addr(), 17, {},
               [h = client.hold()] (bus::Modbus::Result result, const bus::Modbus::Data& data)
               {
                  _report_byte (util::ClientSink{ h }, result, data) ;
               }
            ) ;
         break ;

      case 'b': // boot count
         bus::MODBUS.request
            (
               bus_addr(), 8, {},
               [h = client.hold()] (bus::Modbus::Result result, const bus::Modbus::Data& data)
               {
                  _report_byte (util::ClientSink{ h }, result, data) ;
               }
            ) ;
         break ;

      case 'c': // control
         if (it == str.end())
            client << util::ClientSink::Result::Error ;
         else
         {
            control (util::atoib (it, str.end())) ;
            client << util::ClientSink::Result::Ok ;
         }
         break ;

      default:
      {
         bus::MODBUS.request
            (
               bus_addr(),
               67,
               [str] (bus::Modbus::Tx& tx) { tx << str ; },
               [h = client.hold()] (bus::Modbus::Result result, const bus::Modbus::Data& data)
               {
                  util::ClientSink c{ h } ;

                  if (result != bus::Modbus::Result::Ok)
                     c << util::ClientSink::Result::Error ;
                  else
                     c << aux::range (data.begin(), data.end()) << util::endl{} << util::ClientSink::Result::Ok ;
               }
            ) ;
      }
   }
}

void UnifiedDeviceBase::reboot (util::ClientSink&& client)
{
   bus::MODBUS.request
      (
         bus_addr(), 127, {},
         [h = client.hold()] (bus::Modbus::Result result, const bus::Modbus::Data& data)
         {
            util::ClientSink{ h } << (result == bus::Modbus::Result::Ok ? util::ClientSink::Result::Ok : util::ClientSink::Result::Error) ;
         }
      ) ;
}

void UnifiedDeviceBase::poll_impl (void)
{
   if (m_bus_addr != 255)
      bus::MODBUS.request
         (
            m_bus_addr, 7, {},
            [this] (bus::Modbus::Result result, const bus::Modbus::Data& data)
            {
               _poll_result (result, data) ;
            },
            false
         ) ;
}

void UnifiedDeviceBase::_poll_result (bus::Modbus::Result result, const bus::Modbus::Data& data)
{
   switch (result)
   {
      case bus::Modbus::Result::Ok:
         m_errors = 0 ;
         if (data.size() <= 0)
            break ;

         if (m_state & CommErr)
            reset() ;

         set_state (*data.begin()) ;

      case bus::Modbus::Result::IoError:
         if (m_errors == 5)
            set_state (m_state | CommErr) ;
         else
            ++m_errors ;
         break ;

      default:;
   }

   continue_poll() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals

////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined (SERIALDEBUG)
