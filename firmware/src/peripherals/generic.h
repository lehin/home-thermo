////////////////////////////////////////////////////////////////////////////////////////////
//
// cmn-valve.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "unified-device.h"

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class GenericDevice GENERICDEVICE ;

////////////////////////////////////////////////////////////////////////////////////////////

DECLARE_DEVICE_TAG (generic) ;

class GenericDevice : public UnifiedDevice<generic>
{
   typedef base_class_ base_class ;

public:
   GenericDevice (void) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
