////////////////////////////////////////////////////////////////////////////////////////////
//
// display.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "display.h"
#include "hw/gpio.h"
#include "hw/spi.h"
#include "aux/scheduler.h"

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   using FontDesc = Display::FontDesc ;

   #include "fonts/digits-16x23.inc"
   #include "fonts/digits-32x48.inc"
   #include "fonts/font-9x15.inc"

   #include "fonts/font-5x7.inc"
   #include "fonts/font-7x14.inc"
   #include "fonts/font-8x13.inc"
   #include "fonts/font-9x18.inc"
   #include "fonts/font-10x16.inc"
   #include "fonts/font-12x16.inc"
   #include "fonts/font-12x24.inc"
   #include "fonts/font-24x26.inc"
   #include "fonts/font-31x33.inc"
   #include "fonts/font-7x10-bold.inc"
}

////////////////////////////////////////////////////////////////////////////////////////////

Display DISPLAY ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   /////////////////////////////////////////////////////////////////////////////////////////

   template<typename _Pin, _Pin& _pin>
      struct LockPin
      {
         LockPin (const LockPin&) = delete ;
         LockPin& operator= (const LockPin&) = delete ;

         LockPin (void) { hw::SPI.flush() ; _pin.reset() ; }
         ~LockPin (void) { hw::SPI.flush() ; _pin.set() ; }
      } ;

   /////////////////////////////////////////////////////////////////////////////////////////

   class CmdCmn
   {
      struct SelectPin : LockPin<decltype(hw::gpio::DDC),hw::gpio::DDC> {} ;

   public:
      constexpr CmdCmn (uint8_t code) : m_code{ code } {}

   protected:
      void send (void) const
      {
         SelectPin _sel ;
         hw::SPI.send (pgm_read_byte (&m_code)) ;
      }

   private:
      const uint8_t m_code ;
   } ;

   /////////////////////////////////////////////////////////////////////////////////////////

   template<typename _Tp = void, typename ... _Tail>
      struct CmdArgs : CmdArgs<_Tail...>
      {
         typedef CmdArgs<_Tail...> base_args ;

         void send (_Tp p, _Tail ... tail) const
         {
            hw::SPI.send (hw::Spi::msb{}, p) ;
            base_args::send (tail...) ;
         }
      } ;

   template<>
      struct CmdArgs<void>
      {
         void send (void) const {}
      } ;

   /////////////////////////////////////////////////////////////////////////////////////////

   template<typename ... _Args>
      class Cmd : CmdCmn, CmdArgs<_Args...>
      {
         typedef CmdArgs<_Args...> base_args ;

      public:
         constexpr Cmd (uint8_t code) : CmdCmn{ code } {}

         void operator() (_Args ... args) const
         {
            send() ;
            base_args::send (args...) ;
         }
      } ;

   /////////////////////////////////////////////////////////////////////////////////////////

   namespace cmds
   {
      constexpr Cmd<> PROGMEM sleep_out{ 0x11 } ;
      constexpr Cmd<> PROGMEM norm_mode{ 0x13 } ;
      constexpr Cmd<> PROGMEM inverse_off{ 0x20 } ;
      constexpr Cmd<> PROGMEM disp_on{ 0x29 } ;
      constexpr Cmd<uint16_t,uint16_t> PROGMEM col_addr_set{ 0x2A } ;
      constexpr Cmd<uint16_t,uint16_t> PROGMEM page_addr_set{ 0x2B } ;
      constexpr Cmd<> PROGMEM write_mem{ 0x2C } ;
      constexpr Cmd<uint8_t> PROGMEM mem_access{ 0x36 } ;
      constexpr Cmd<uint16_t> PROGMEM vscoll_addr{ 0x37 } ;
      constexpr Cmd<uint8_t> PROGMEM pixel_fmt{ 0x3A } ;
   }

   /////////////////////////////////////////////////////////////////////////////////////////

}

////////////////////////////////////////////////////////////////////////////////////////////

struct Display::ChipSelect : LockPin<decltype(hw::gpio::DCS),hw::gpio::DCS> {} ;

////////////////////////////////////////////////////////////////////////////////////////////

Display::Display (void)
{
   color (white()) ;
   bkgnd (black()) ;
   select_font (Font::Regular) ;
}

void Display::initialize (void)
{
   _init() ;
   clear() ;
}

namespace
{
   template<typename _Tp>
      static _Tp pgm_read_data (const _Tp *src)
      {
         _Tp res ;

         auto _s = reinterpret_cast<const uint8_t*> (src) ;
         auto _d = reinterpret_cast<uint8_t*> (&res) ;

         for (uint8_t n = 0; n < sizeof (_Tp); ++n)
            *_d++ = pgm_read_byte (_s++) ;

         return res ;
      }
}

void Display::select_font (Font fnt)
{
   static const FontDesc fonts[static_cast<uint8_t> (Font::_Count)] PROGMEM =
      { Font_9x15_Desc, Digits_16x23_Desc } ; //, Digits_32x48_Desc, Font_5x7_Desc } ;

   m_cur_font = pgm_read_data (fonts + static_cast<uint8_t> (fnt)) ;
}

void Display::clear (void) const
{
   clear (0, 0, WIDTH, HEIGHT) ;
}

void Display::clear (uint16_t x, uint16_t y, uint16_t w, uint16_t h) const
{
   ChipSelect _cs ;

   _window (x, y, w, h) ;

   for (uint16_t x = 0; x < w; ++x)
      for (uint16_t y = 0; y < h; ++y)
         _pixel_bkgnd() ;
}

void Display::fill (uint16_t x, uint16_t y, uint16_t w, uint16_t h) const
{
   ChipSelect _cs ;

   _window (x, y, w, h) ;

   for (uint16_t x = 0; x < w; ++x)
      for (uint16_t y = 0; y < h; ++y)
         _pixel() ;
}

void Display::hline (uint16_t x, uint16_t y, uint16_t len) const
{
   fill (x, y, len, 1) ;
}

void Display::vline (uint16_t x, uint16_t y, uint16_t len) const
{
   fill (x, y, 1, len) ;
}

void Display::box (uint16_t x, uint16_t y, uint16_t w, uint16_t h) const
{
   hline (x, y, w) ;
   hline (x, y + h - 1, w) ;
   vline (x, y, h) ;
   vline (x + w -1, y, h) ;
}

void Display::putc (uint16_t x, uint16_t y, char ch) const
{
   ChipSelect _cs ;
   _putc (ch, x, y) ;
}

void Display::putsp (uint16_t x, uint16_t y, const char *s) const
{
   ChipSelect _cs ;

   const auto metrics = font_metrics() ;

   while (char ch = pgm_read_byte (s++))
   {
      if (x + metrics.w > WIDTH)
      {
         x = 0 ;
         y += metrics.h ;
      }

      _putc (ch, x, y) ;
      x += metrics.w ;
   }
}

void Display::puts (uint16_t x, uint16_t y, const char *s) const
{
   ChipSelect _cs ;

   const auto metrics = font_metrics() ;

   while (char ch = *(s++))
   {
      if (x + metrics.w > WIDTH)
      {
         x = 0 ;
         y += metrics.h ;
      }

      _putc (ch, x, y) ;
      x += metrics.w ;
   }
}

void Display::puts (uint16_t x, uint16_t y, const aux::String& str) const
{
   ChipSelect _cs ;

   const auto metrics = font_metrics() ;

   for (char ch : str)
   {
      if (x + metrics.w > WIDTH)
      {
         x = 0 ;
         y += metrics.h ;
      }

      _putc (ch, x, y) ;
      x += metrics.w ;
   }
}

void Display::puts (uint16_t x, uint16_t y, const aux::StringRef& str) const
{
   ChipSelect _cs ;

   const auto metrics = font_metrics() ;

   for (char ch : str)
   {
      if (x + metrics.w > WIDTH)
      {
         x = 0 ;
         y += metrics.h ;
      }

      _putc (ch, x, y) ;
      x += metrics.w ;
   }
}

void Display::image (uint16_t x, uint16_t y, const Image& imgp, bool inactive) const
{
   auto img = _image_from_progmem (imgp) ;

   ChipSelect _cs ;
   _window (x, y, img.w, img.h) ;

   switch (img.bpp)
   {
      case 4: _image4bpp (x, y, img, inactive) ; break ;
      case 8: _image8bpp (x, y, img, inactive) ; break ;
      case 16: _image16bpp (x, y, img, inactive) ; break ;
   }
}

Display::Metrics Display::image_metrics (const Image& imgp) const
{
   auto img = _image_from_progmem (imgp) ;
   return { img.w, img.h } ;
}

void Display::_init (void) const
{
   ChipSelect _cs ;

   cmds::sleep_out() ;
   cmds::norm_mode() ;
   cmds::vscoll_addr (0) ;
   cmds::pixel_fmt (0x05) ;
   cmds::disp_on() ;
   cmds::inverse_off() ;
   cmds::mem_access (0xE0) ;
}

void Display::_window (uint16_t x, uint16_t y, uint16_t w, uint16_t h) const
{
   cmds::col_addr_set (x, x + w - 1) ;
   cmds::page_addr_set (y, y + h -1) ;
   cmds::write_mem() ;
}

void Display::_putc (char ch, uint16_t x, uint16_t y) const
{
   const uint8_t *base = m_cur_font.m_data + (static_cast<uint8_t> (ch) - m_cur_font.m_first_glyph) * m_cur_font.m_glyph_size ;
   const auto& metrics = m_cur_font.m_metrics ;

   _window (x, y, metrics.w, metrics.h) ;

   for (uint8_t j = 0; j < metrics.h; ++j)
   {
      uint8_t extra_offs = j / 8 * metrics.w,
              shift = j % 8 ;

      for (uint8_t i = 0; i < metrics.w; i++)
      {
         uint8_t fdata = pgm_read_byte (base + i + extra_offs) ;
         fdata & (1 << shift) ? _pixel() : _pixel_bkgnd() ;
      }
   }
}

inline __attribute__ ((always_inline))
   void Display::_pixel (void) const
   {
      hw::SPI.send (hw::Spi::msb{}, m_clr) ;
   }

inline __attribute__ ((always_inline))
   void Display::_pixel_bkgnd (void) const
   {
      hw::SPI.send (hw::Spi::msb{}, m_bkgnd) ;
   }

namespace
{
   struct PaletteWriter
   {
      const uint8_t *m_pal ;
      const bool m_inactive ;
      const uint16_t m_bkgnd ;

      PaletteWriter& operator<< (uint8_t idx)
      {
         uint16_t pixel = pgm_read_word (m_pal + idx * 2u) ;

         if (!m_inactive)
            hw::SPI.send (hw::Spi::msb{}, pixel) ;
         else
            hw::SPI.send (hw::Spi::msb{}, Display::Color::from_word (pixel) - Display::Color::from_word (m_bkgnd) < 128 ? m_bkgnd : Display::inactive().as_word()) ;

         return *this ;
      }
   } ;
}

void Display::_image4bpp (uint16_t x, uint16_t y, const Image& img, bool inactive) const
{
   PaletteWriter writer { img.pal, inactive, m_bkgnd } ;

   auto end = img.data + (size_t)img.w * img.h / 2 ;
   for (auto cur = img.data; cur < end ; ++cur)
   {
      uint8_t pair = pgm_read_byte (cur) ;
      writer << (pair & 0x0F) << (pair >> 4) ;
   }
}

void Display::_image8bpp (uint16_t x, uint16_t y, const Image& img, bool inactive) const
{
   PaletteWriter writer { img.pal, inactive, m_bkgnd } ;

   auto end = img.data + (size_t)img.w * img.h ;
   for (auto cur = img.data; cur < end ; ++cur)
      writer << pgm_read_byte (cur) ;
}

void Display::_image16bpp (uint16_t x, uint16_t y, const Image& img, bool inactive) const
{
   auto end = img.data + (size_t)img.w * img.h * 2 ;

   for (auto cur = img.data ; cur < end; cur += 2)
   {
      uint16_t pixel = pgm_read_word (cur) ;

      if (!inactive)
         hw::SPI.send (hw::Spi::msb{}, pixel) ;
      else
         hw::SPI.send (hw::Spi::msb{}, Display::Color::from_word (pixel) - Display::Color::from_word (m_bkgnd) < 128 ? m_bkgnd : Display::inactive().as_word()) ;
   }
}

Display::Image Display::_image_from_progmem (const Image& imgp) const
{
   Image ret ;
   memcpy_P (&ret, &imgp, sizeof (Image)) ;
   return ret ;
}

////////////////////////////////////////////////////////////////////////////////////////////

Display::CurFont::CurFont (const FontDesc& org) :
   FontDesc{ org },
   m_glyph_size{ static_cast<uint8_t> (org.m_metrics.w * ((org.m_metrics.h + 7) / 8)) }
{
}

////////////////////////////////////////////////////////////////////////////////////////////

uint16_t Display::Color::as_word (void) const
{
   union
   {
      struct
      {
         uint8_t r : 5, g : 6, b : 5 ;
      } rgb ;
      uint16_t word ;
   } cvt
      {
         {
            static_cast<uint8_t> (r >> 3),
            static_cast<uint8_t> (g >> 2),
            static_cast<uint8_t> (b >> 3)
         }
      } ;

   return cvt.word ;
}

Display::Color Display::Color::from_word (uint16_t word)
{
   union
   {
      uint16_t word ;
      struct
      {
         uint8_t r : 5, g : 6, b : 5 ;
      } rgb ;
   } cvt { word } ;

   return
      {
         static_cast<uint8_t> (cvt.rgb.r << 3),
         static_cast<uint8_t> (cvt.rgb.g << 2),
         static_cast<uint8_t> (cvt.rgb.b << 3)
      } ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
