////////////////////////////////////////////////////////////////////////////////////////////
//
// i2c-device.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "i2c-device.h"
#include "aux/shared-ptr.h"

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

I2cDeviceBase::I2cDeviceBase (uint8_t addr, uint8_t poll_period) :
   DeviceBase{ poll_period },
   I2cProto{ addr },
   m_state{ 0 },
   m_errors{ 0 },
   m_indeterminate{ true },
   m_signal_state_changed{ nullptr }
{
}

namespace
{
   static void _report_byte (util::ClientSink&& client, uint8_t errno, bus::I2c::Rx& rx)
   {
      if (errno)
         client << util::ClientSink::Result::Error ;
      else
      {
         if (rx)
         {
            char buf[3] ;
            auto end = util::itoa (rx.read<uint8_t>(), buf) ;
            client << aux::range (buf + 0, end) << util::endl{} ;
         }

         client << util::ClientSink::Result::Ok ;
      }
   }
}

void I2cDeviceBase::request_simple (util::ClientSink& client, CmdCode code)
{
   request
      (
         [code] (auto& tx) { tx << code ; },
         [h = client.hold()] (uint8_t errno, bus::I2c::Rx& rx)
         {
            _report_byte (util::ClientSink{ h }, errno, rx) ;
         }
      ) ;
}

void I2cDeviceBase::control (uint8_t outputs)
{
   bus::I2C.request (address(), 0, [outputs] (auto& tx) { tx << CmdCode::Control << outputs ; }) ;
}

void I2cDeviceBase::set_state (uint8_t state)
{
   auto changed = state ^ m_state ;

   m_state = state ;

   if (m_indeterminate)
   {
      m_indeterminate = false ;
      changed = (uint8_t)-1 ;
   }

   if (changed)
      state_changed (changed) ;
}

void I2cDeviceBase::command (const aux::StringRef& str, util::ClientSink&& client)
{
   auto it = str.begin() ;
   auto cmd = *it++ ;

   for (; it != str.end() && *it != ' '; ++it) ;
   for (; it != str.end() && *it == ' '; ++it) ;

   char buf[4] { 0 } ;

   switch (cmd)
   {
      case 'a': // address
      {
         char *end = util::itoa (address(), buf) ;
         client << aux::range (buf + 0, end) << util::endl{} << util::ClientSink::Result::Ok ;
         break ;
      }

      case 's': // state
         client << aux::range (buf + 0, util::itoa (state(), buf)) << util::endl{} << util::ClientSink::Result::Ok ;
         break ;

      case 'v': // device firmware version
         request_simple (client, CmdCode::Version) ;
         break ;

      case 'b': // boot count
         request_simple (client, CmdCode::BootCnt) ;
         break ;

      case 'c': // control
         if (it == str.end())
            client << util::ClientSink::Result::Error ;
         else
         {
            control (util::atoib (it, str.end())) ;
            client << util::ClientSink::Result::Ok ;
         }
         break ;

      default:
      {
         aux::String s{ str } ;

         request
            (
               [s] (auto& tx) { tx << CmdCode::Extended << range (s) ; },
               [h = client.hold()] (uint8_t errno, bus::I2c::Rx& rx)
               {
                  util::ClientSink c{ h } ;

                  if (!errno)
                     if (rx)
                        rx >> errno ;
                     else
                        errno = CommErr ;

                  if (errno)
                     c << util::ClientSink::Result::Error ;
                  else
                  {
                     if (rx)
                        c << rx.as_range() << util::endl{} ;

                     c << util::ClientSink::Result::Ok ;
                  }
               }
            ) ;
      }
   }
}

void I2cDeviceBase::reboot (util::ClientSink&& client)
{
   request_simple (client, CmdCode::Reboot) ;
}

void I2cDeviceBase::state_changed (uint8_t changed)
{
   if (m_signal_state_changed)
      m_signal_state_changed (m_state, changed) ;
}

void I2cDeviceBase::poll_impl (void)
{
   request
      (
         [] (auto& tx) { tx << CmdCode::Status ; },
         [this] (uint8_t errno, bus::I2c::Rx& rx)
         {
            if (!errno && rx)
            {
               m_errors = 0 ;
               if (m_state & CommErr)
                  reset() ;

               set_state (rx.read<uint8_t>()) ;
            }
            else
            {
               if (m_errors < 3)
                  ++m_errors ;
               else
                  set_state (m_state | CommErr) ;
            }

            continue_poll() ;
         }
      ) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
