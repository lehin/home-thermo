////////////////////////////////////////////////////////////////////////////////////////////
//
// opentherm-als.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "hw/ot-comm.h"
#include "util/tempval.h"

namespace peripherals
{
   namespace opentherm
   {

////////////////////////////////////////////////////////////////////////////////////////////

enum class MsgType : uint8_t
{
   ReadData = 0, WriteData, InvalidData,
   ReadACK = 4, WriteACK, DataInvalid, UnknownDataID
} ;

////////////////////////////////////////////////////////////////////////////////////////////

enum class DataId : uint8_t
{
   StatusFlags = 0, CtrlSetPoint = 1, SlaveConfig = 3, RemoteCmd = 4, FaultData = 5,
   BoilerTemp = 25, DhwTemp = 26, DhwSetPoint = 56
} ;

////////////////////////////////////////////////////////////////////////////////////////////

enum MasterStatus : uint8_t
{
   Default = 0x00,
   ChEn = 0x01, DhwEn = 0x02, CoolingEn = 0x04, OtcActive = 0x08,
   Ch2En = 0x10
} ;

enum SlaveStatus : uint8_t
{
   FaultInd = 0x01, ChMode = 0x02, DhwMode = 0x04, FlameStat = 0x08,
   CoolingStat = 0x10, Ch2Mode = 0x20, DiagEvt = 0x40
} ;

enum SlaveCfg : uint8_t
{
   DhwPresent = 0x01, CtrlType = 0x02, CoolingCfg = 0x04, DhwCfg = 0x08,
   PumpCtrl = 0x10, Ch2Present = 0x20
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class Message
{
   typedef hw::otcomm::Link::Message Raw ;

   class Proxy
   {
      friend class Message ;

      const uint8_t *m_vals ;
      Proxy (const uint8_t *vals) : m_vals{ vals } {}

   public:
      uint8_t operator[] (uint8_t idx) const { return m_vals[idx] ; }

      uint8_t as_uint8_t (void) const { return m_vals[0] ; }
      uint16_t as_uint16_t (void) const { return (uint16_t)m_vals[0] << 8 | m_vals[1] ; }
      util::TempVal as_temp (void) const { return util::TempVal::from_raw ( as_uint16_t() * 100ul / 256 ) ; }

      operator uint8_t (void) const { return as_uint8_t() ; }
      operator uint16_t (void) const { return as_uint16_t() ; }
      operator util::TempVal (void) const { return as_temp() ; }
   } ;

public:
   Message (const Raw& raw) : m_raw{ raw } {}

   Message (MsgType t, DataId id) :
      m_raw{ 0, (uint8_t)t, 0, (uint8_t)id, { 0, 0 } } {}

   Message (MsgType t, DataId id, uint8_t val1, uint8_t val2) :
      m_raw{ 0, (uint8_t)t, 0, (uint8_t)id, { val1, val2 } } {}

   Message (MsgType t, DataId id, uint8_t val) :
      m_raw{ 0, (uint8_t)t, 0, (uint8_t)id, { val, 0 } } {}

   Message (MsgType t, DataId id, uint16_t val) :
      m_raw{ 0, (uint8_t)t, 0, (uint8_t)id, { (uint8_t)(val >> 8), (uint8_t)(val & 0xff) } } {}

   Message (MsgType t, DataId id, util::TempVal temp) :
      Message{ t, id, (uint16_t)(static_cast<int16_t> (temp) * 256l / 100) } {}

   operator const Raw& (void) const { return m_raw ; }

   MsgType type (void) const { return static_cast<MsgType> (m_raw.type) ; }
   DataId id (void) const { return static_cast<DataId> (m_raw.id) ; }
   Proxy data (void) const { return m_raw.vals ; }

private:
   Raw m_raw ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace opentherm
} // namespace peripherals
