////////////////////////////////////////////////////////////////////////////////////////////
//
// i2c-proto.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "i2c-proto.h"
#include "util/strconv.h"

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static void _sink_wrapper (uint8_t addr, bus::I2c::Sink& sink, uint8_t errno, bus::I2c::Rx& rx, uint8_t reqsz)
   {
      if (errno)
         sink (errno, rx) ;
      else
      {
         uint8_t size = 0 ;
         if (rx)
            rx >> size ;
         else
            sink (I2cProto::CommErr, rx) ;

         if (size == 0xff)
            bus::I2C.request_continue
               (
                  addr, reqsz, {},
                  [a = addr, s = aux::forward (sink), reqsz] (uint8_t errno, bus::I2c::Rx& rx) mutable
                  {
                     _sink_wrapper (a, s, errno, rx, reqsz) ;
                  }
               ) ;
         else if (size > rx.as_range().size())
            bus::I2C.request_continue
               (
                  addr, size + 1, {},
                  [a = addr, s = aux::forward (sink), size] (uint8_t errno, bus::I2c::Rx& rx) mutable
                  {
                     _sink_wrapper (a, s, errno, rx, size + 1) ;
                  }
               ) ;
         else
            sink (errno, rx) ;
      }
   } ;
}

void I2cProto::request (Source&& cmd, bus::I2c::Sink&& sink)
{
   if (!sink)
      bus::I2C.request (m_addr, 0, aux::forward (cmd)) ;
   else
      bus::I2C.request
         (
            m_addr, 1, aux::forward (cmd),
            [a = m_addr, s = aux::forward (sink)] (uint8_t errno, bus::I2c::Rx& rx) mutable
            {
               _sink_wrapper (a, s, errno, rx, 1) ;
            }
         ) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
