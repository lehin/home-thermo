////////////////////////////////////////////////////////////////////////////////////////////
//
// display.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/string.h"

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Display DISPLAY ;

////////////////////////////////////////////////////////////////////////////////////////////

class Display
{
   struct ChipSelect ;

public:
   static constexpr uint16_t WIDTH = 320 ;
   static constexpr uint16_t HEIGHT = 240 ;

   struct Color
   {
      uint8_t r,g,b ;

      friend inline bool operator== (const Color& lhs, const Color& rhs)
         { return lhs.r == rhs.r && lhs.g == rhs.g && lhs.b == rhs.b ; }

      friend inline int16_t operator- (const Display::Color& lhs, const Display::Color& rhs)
         { return abs ((int16_t)lhs.r - rhs.r) + abs ((int16_t)lhs.g - rhs.g) + abs ((int16_t)lhs.b - rhs.b) ; }

      uint16_t as_word (void) const ;

      static Color from_word (uint16_t word) ;
   } ;

   static constexpr Color black (void) { return { 0,0,0 } ; }
   static constexpr Color red (void) { return { 255,0,0 } ; }
   static constexpr Color green (void) { return { 0,255,0 } ; }
   static constexpr Color blue (void) { return { 0,0,255 } ; }
   static constexpr Color white (void) { return { 255,255,255 } ; }
   static constexpr Color inactive (void) { return { 127, 127, 127 } ; }

   struct Metrics { uint8_t w, h ; } ;

   struct FontDesc
   {
      Metrics m_metrics ;
      uint8_t m_first_glyph ;
      const uint8_t *m_data ;
   } ;

   struct Image
   {
      uint8_t w, h ;
      uint8_t bpp ;
      const uint8_t *data ;
      const uint8_t *pal = nullptr ;
   } ;

public:
   Display (void) ;

   void initialize (void) ;

   enum class Font : uint8_t{ Regular, Digits, _Count } ;

   void select_font (Font fnt) ;
   Metrics font_metrics (void) const { return m_cur_font.m_metrics ; }

   void color (const Color& clr) { m_clr = clr.as_word() ; }
   void bkgnd (const Color& clr) { m_bkgnd = clr.as_word() ; }
   void clear (void) const ;
   void clear (uint16_t x, uint16_t y, uint16_t w, uint16_t h) const ;
   void fill (uint16_t x, uint16_t y, uint16_t w, uint16_t h) const ;
   void hline (uint16_t x, uint16_t y, uint16_t len) const ;
   void vline (uint16_t x, uint16_t y, uint16_t len) const ;
   void box (uint16_t x, uint16_t y, uint16_t w, uint16_t h) const ;
   void putc (uint16_t x, uint16_t y, char ch) const ;
   void putsp (uint16_t x, uint16_t y, const char *s) const ;
   void puts (uint16_t x, uint16_t y, const char *s) const ;
   void puts (uint16_t x, uint16_t y, const aux::String& str) const ;
   void puts (uint16_t x, uint16_t y, const aux::StringRef& str) const ;
   void opaque_text (bool o = true) { m_opaque_text = o ; }

   void image (uint16_t x, uint16_t y, const Image& imgp, bool inactive = false) const ;
   Metrics image_metrics (const Image& imgp) const ;

private:
   void _init (void) const ;
   void _window (uint16_t x, uint16_t y, uint16_t w, uint16_t h) const ;
   void _putc (char ch, uint16_t x, uint16_t y) const ;
   void _pixel (void) const ;
   void _pixel_bkgnd (void) const ;
   void _image4bpp (uint16_t x, uint16_t y, const Image& img, bool inactive) const ;
   void _image8bpp (uint16_t x, uint16_t y, const Image& img, bool inactive) const ;
   void _image16bpp (uint16_t x, uint16_t y, const Image& img, bool inactive) const ;
   Image _image_from_progmem (const Image& imgp) const ;

private:
   uint16_t m_clr, m_bkgnd ;

   struct CurFont : FontDesc
   {
      uint8_t m_glyph_size ;
      CurFont (const FontDesc& org) ;
      CurFont (void) = default ;
   } ;

   CurFont m_cur_font ;
   bool m_opaque_text = true ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
