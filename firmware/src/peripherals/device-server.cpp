////////////////////////////////////////////////////////////////////////////////////////////
//
// device-server.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "device-server.h"
#include "aux/scheduler.h"
#include "hw/watchdog.h"
#include <string.h>

#define POLL_PERIOD_MS (10)

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

DeviceServer DEVSERVER ;

////////////////////////////////////////////////////////////////////////////////////////////

void DeviceServer::start (void)
{
   m_flags.m_poll_enabled = true ;
   _schedule() ;
}

void DeviceServer::stop (void)
{
   m_flags.m_poll_enabled = false ;
}

DeviceList DeviceServer::devices (void)
{
   return m_devices ;
}

void DeviceServer::_schedule (void)
{
   if (m_flags.m_poll_scheduled)
      return ;

   m_flags.m_poll_scheduled = true ;
   aux::SCHED.schedule ([] { DEVSERVER._poll() ; }, POLL_PERIOD_MS) ;
}

void DeviceServer::_poll (void)
{
   m_flags.m_poll_scheduled = false ;

   if (!m_flags.m_poll_enabled)
      return ;

   for (auto& dev : devices())
      dev.poll() ;

   _schedule() ;

   hw::WDT.reset_extra() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

DeviceList::iterator DeviceList::find (const aux::StringRef& name) const
{
   for (auto it = begin(); it != end(); ++it)
      if (!strncmp_P (name.begin(), it->name(), name.size()))
         return it ;

   return end() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

DeviceBase::DeviceBase (uint16_t poll_period) :
   m_prev{ nullptr },
   m_next{ DEVSERVER.m_devices },
   m_poll_period{ poll_period },
   m_poll_counter{ 1 }
{
   DEVSERVER.m_devices = this ;
}

DeviceBase::~DeviceBase (void)
{
   if (m_prev)
      m_prev->m_next = m_next ;
   else
      DEVSERVER.m_devices = m_next ;

   if (m_next)
      m_next->m_prev = m_prev ;
}

void DeviceBase::poll (void)
{
   if (m_poll_counter && !--m_poll_counter)
      poll_impl() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
