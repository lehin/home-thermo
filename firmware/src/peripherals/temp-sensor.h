////////////////////////////////////////////////////////////////////////////////////////////
//
// temp-sensor.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "device-server.h"
#include "bus/one-wire.h"

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class TempSensor TEMPSENSOR ;

////////////////////////////////////////////////////////////////////////////////////////////

DECLARE_DEVICE_TAG(tsens) ;

class TempSensor : public StaticDevice<tsens>
{
   typedef base_class_ base_class ;

public:
   TempSensor (void) ;

private:
   virtual void command (const aux::StringRef& cmd, util::ClientSink&& client) ;
   virtual void poll_impl (void) ;

   void _read_measurement (void) ;

private:
   // хз, с какого перепугу, но компилятору гораздо легче, если эти члены статические
   static bus::OneWire::DEVID m_id ;
   static int16_t m_temperature ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
