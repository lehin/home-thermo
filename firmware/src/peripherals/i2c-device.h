////////////////////////////////////////////////////////////////////////////////////////////
//
// i2c-device.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "device-server.h"
#include "i2c-proto.h"

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

class I2cDeviceBase : public DeviceBase, public I2cProto
{
   enum class CmdCode : uint8_t { Status, BootCnt, Version, Reboot, Extended, Control } ;

protected:
   I2cDeviceBase (uint8_t addr, uint8_t poll_period) ;

public:
   typedef void (*SigStateChanged) (uint8_t state, uint8_t changed) ;
   void on_state_changed (SigStateChanged fn) { m_signal_state_changed = fn ; }

   uint8_t state (void) const { return m_state ; }
   bool is_active (void) const { return !(m_state & CommErr) ; }

protected:
   void request_simple (util::ClientSink& client, CmdCode code) ;
   void control (uint8_t outputs) ;
   void set_state (uint8_t state) ;

   virtual void command (const aux::StringRef& cmd, util::ClientSink&& client) ;
   virtual void reboot (util::ClientSink&& client) ;
   virtual void state_changed (uint8_t changed) ;

private:
   virtual void poll_impl (void) ;
   virtual void reset (void) {}

private:
   uint8_t m_state ;
   uint8_t m_errors ;
   bool m_indeterminate ;

   SigStateChanged m_signal_state_changed ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tag>
   using I2cDevice = StaticDevice<_Tag,I2cDeviceBase> ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
