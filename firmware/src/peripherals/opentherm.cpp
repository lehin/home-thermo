////////////////////////////////////////////////////////////////////////////////////////////
//
// opentherm.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "opentherm.h"

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

OpenTherm OPENTHERM ;

////////////////////////////////////////////////////////////////////////////////////////////

using namespace opentherm ;

OpenTherm::OpenTherm (void) :
   m_status_byte{ 0 },
   m_running_mode{ Mode::Off },
   m_temp_ch{ util::TempVal::undefined() },
   m_temp_dhw{ util::TempVal::undefined() },
   m_actual_t { -127_t },
   m_ch_setpoint_pending{ false },
   m_running_mode_handler{ nullptr },
   m_flame_active_handler{ nullptr },
   m_temp_handler{ nullptr }
{
}

void OpenTherm::initialize (void)
{
   hw::otcomm::LINK.initialize ([] (auto& msg) { OPENTHERM._on_status (msg) ; }) ;
   hw::otcomm::LINK.send (opentherm::Message{ MsgType::ReadData,  DataId::SlaveConfig }) ;
   _read_dhw_setpoint() ;
}

void OpenTherm::reset_currents (void)
{
   m_temp_ch = util::TempVal::undefined() ;
   m_temp_dhw = util::TempVal::undefined() ;
}

void OpenTherm::ch_temp (util::TempVal t)
{
   if (!_set_temperature (t, m_temp_ch, TempKind::Ch))
      return ;

   if (hw::otcomm::LINK.is_present())
      _ctrl_ch_setpoint() ;
   else
      m_ch_setpoint_pending = true ;
}

void OpenTherm::dhw_temp (util::TempVal t)
{
   if (m_temp_dhw != t)
      hw::otcomm::LINK.send
         (
            opentherm::Message{ MsgType::WriteData, DataId::DhwSetPoint, t },
            [] (auto&) { OPENTHERM._read_dhw_setpoint() ; }
         ) ;
}

void OpenTherm::set_mode (Mode mode)
{
   hw::otcomm::LINK.mode ((uint8_t)mode ^ 3) ;
}

void OpenTherm::reset (CmdResult&& handler)
{
   _user_command (opentherm::Message{ MsgType::WriteData, DataId::RemoteCmd, (uint8_t)1 }, aux::forward (handler)) ;
}

void OpenTherm::fault_data (CmdResult&& handler)
{
   _user_command (opentherm::Message{ MsgType::ReadData, DataId::FaultData }, aux::forward (handler)) ;
}

void OpenTherm::_user_command (const opentherm::Message& msg, CmdResult&& handler)
{
   hw::otcomm::LINK.send
      (
         msg,
         [ h = aux::forward (handler), wr = msg.type() == opentherm::MsgType::WriteData ] (const opentherm::Message& msg)
         {
            const auto ack = (wr ? MsgType::WriteACK : MsgType::ReadACK) ;
            h (msg.type() == ack, msg.data()[0], msg.data()[1]) ;
         }
      ) ;
}

bool OpenTherm::_set_temperature (util::TempVal t, util::TempVal& var, TempKind kind)
{
   if (var == t)
      return false ;

   var = t ;

   if (m_temp_handler)
      m_temp_handler (kind, t) ;

   return true ;
}

void OpenTherm::_set_running_mode (Mode mode)
{
   if (m_running_mode == mode)
      return ;

   m_running_mode = mode ;

   if (m_running_mode_handler)
      m_running_mode_handler (m_running_mode) ;
}

void OpenTherm::_set_flame_active (bool active)
{
   if (m_flame_active == active)
      return ;

   m_flame_active = active ;

   if (m_flame_active_handler)
      m_flame_active_handler (m_flame_active) ;
}

void OpenTherm::_ctrl_ch_setpoint (void)
{
   hw::otcomm::LINK.send
      (
         opentherm::Message{ MsgType::WriteData, DataId::CtrlSetPoint, m_temp_ch },
         [] (auto& msg) { OPENTHERM._on_ch_temp_set (msg) ; }
      ) ;
}

void OpenTherm::_read_dhw_setpoint (void)
{
   hw::otcomm::LINK.send
      (
         opentherm::Message{ MsgType::ReadData,  DataId::DhwSetPoint },
         [] (auto& msg) { OPENTHERM._on_dhw_setpoint_read (msg) ; }
      ) ;
}

void OpenTherm::_on_status (const Message& msg)
{
   if (msg.type() != MsgType::ReadACK)
      return ;

   if (m_ch_setpoint_pending)
   {
      m_ch_setpoint_pending = false ;
      _ctrl_ch_setpoint() ;
      return ;
   }

   uint8_t raw = msg.data()[1] ;

   auto mode = (raw & 0x02 ? Mode::Ch : Mode::Off) | (raw & 0x04 ? Mode::Dhw : Mode::Off) ;
   _set_running_mode (mode) ;
   _set_flame_active (raw & 0x08) ;

   m_status_byte = msg.data()[1] ;

   hw::otcomm::LINK.send
      (
         opentherm::Message{ MsgType::ReadData, mode & Mode::Dhw ? DataId::DhwTemp : DataId::BoilerTemp },
         [] (auto& msg) { OPENTHERM._on_actual_temp_read (msg) ; }
      ) ;
}

void OpenTherm::_on_ch_temp_set (const opentherm::Message& msg)
{
   aux::debug_assert( msg.type() == MsgType::WriteACK, PSTR("BOILER CH SETPOINT") ) ;
}

void OpenTherm::_on_actual_temp_read (const opentherm::Message& msg)
{
   auto t = msg.type() == MsgType::ReadACK ? msg.data().as_temp() : util::TempVal::undefined() ;
   _set_temperature (t, m_actual_t, TempKind::Actual) ;
}

void OpenTherm::_on_dhw_setpoint_read (const opentherm::Message& msg)
{
   auto t = msg.type() == MsgType::ReadACK ? msg.data().as_temp() : util::TempVal::undefined() ;
   _set_temperature (t, m_temp_dhw, TempKind::Dhw) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
