////////////////////////////////////////////////////////////////////////////////////////////
//
// rtc.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "rtc.h"
#include "aux/ll-scheduler.h"
#include "util/strconv.h"
#include "hw/gpio.h"
#include "hw/pcint.h"
#include <avr/pgmspace.h>

static constexpr uint8_t CHIP = 0x68 ;

namespace peripherals
{

////////////////////////////////////////////////////////////////////////////////////////////

Rtc RTC ;

////////////////////////////////////////////////////////////////////////////////////////////

void Rtc::initialize (void)
{
   bus::I2C.request (CHIP, 0, [] (bus::I2c::Tx& tx) { tx << '\x07' << '\x10' ; }) ;
   _read_data() ;

   enable() ;
}

void Rtc::enable (void)
{
   hw::PCINT.enable (hw::Pcint::Tag<5,&_seconds_tick>{}) ;
}

void Rtc::disable (void)
{
   hw::PCINT.disable (hw::Pcint::Tag<5,&_seconds_tick>{}) ;
}

Rtc::Time Rtc::time (void)
{
   return
   {
      (uint8_t)(m_mem.m_tm.m_sec + m_mem.m_tm.m_sec10 * 10),
      (uint8_t)(m_mem.m_tm.m_min + m_mem.m_tm.m_min10 * 10),
      (uint8_t)(m_mem.m_tm.m_hour + m_mem.m_tm.m_hour10 * 10)
   } ;
}

Rtc::Date Rtc::date (void)
{
   return
   {
      m_mem.m_dt.m_weekday,
      (uint8_t)(m_mem.m_dt.m_day + m_mem.m_dt.m_day10 * 10),
      (uint8_t)(m_mem.m_dt.m_mon + m_mem.m_dt.m_mon10 * 10),
      (uint16_t)(m_mem.m_dt.m_year + m_mem.m_dt.m_year10 * 10 + 2000)
   } ;
}

void Rtc::time (const Time& tm)
{
   ChipMem::Time dst
   {
      (uint8_t)(tm.m_sec % 10), (uint8_t)(tm.m_sec / 10), false,
      (uint8_t)(tm.m_min % 10), (uint8_t)(tm.m_min / 10), 0,
      (uint8_t)(tm.m_hour % 10), (uint8_t)(tm.m_hour / 10), 0
   } ;

   bus::I2C.request
      (
         CHIP, 0,
         [dst] (bus::I2c::Tx& tx) { tx << '\x00' << dst ; }
      ) ;
}

void Rtc::date (const Date& dt)
{
   auto y = dt.m_year - 2000 ;

   ChipMem::Date dst
   {
      (uint8_t)(dt.total_days() % 7 + 1), 0,
      (uint8_t)(dt.m_day % 10), (uint8_t)(dt.m_day / 10), (uint8_t)0,
      (uint8_t)(dt.m_month % 10), (uint8_t)(dt.m_month / 10), (uint8_t)0,
      (uint8_t)(y % 10), (uint8_t)(y / 10)
   } ;

   bus::I2C.request
      (
         CHIP, 0,
         [dst] (bus::I2c::Tx& tx) { tx << '\x03' << dst ; }
      ) ;
}

void Rtc::_on_data_read (uint8_t errno, bus::I2c::Rx& rx)
{
   static bool _entered = false ;

   if (errno || _entered)
      return ;

   _entered = true ;

   rx >> m_mem ;

   if (!m_initialized)
   {
      m_initialized = true ;
      m_startup = RTC.date_time() ;
   }

   aux::LLSCHED.schedule
      (
         []
         {
            _entered = false ;
            RTC.m_signal_seconds_tick() ;
         },
         PSTR ("RTC READ")
      ) ;
}

void Rtc::_read_data (void)
{
   bus::I2C.request
      (
         CHIP, sizeof (m_mem),
         [] (bus::I2c::Tx& tx) { tx << '\x00' ; },
         [] (uint8_t errno, bus::I2c::Rx& rx) { RTC._on_data_read (errno, rx) ; }
      ) ;
}

void Rtc::_seconds_tick (void)
{
   RTC._read_data() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripherals
