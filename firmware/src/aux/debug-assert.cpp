////////////////////////////////////////////////////////////////////////////////////////////
//
// debug-assert.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "debug-assert.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <string.h>
#include "hw/gpio.h"
#include "hw/uart.h"
#include "hw/watchdog.h"
#include <util/delay.h>

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEBUG_ASSERT

static DebugAssertInfo* const DEBUG_ASSERT_EEPROM = (DebugAssertInfo*)(E2END + 1 - sizeof (DebugAssertInfo)) ;

DebugAssertInfo read_debug_assert_info (void)
{
   DebugAssertInfo res ;
   eeprom_read_block (&res, DEBUG_ASSERT_EEPROM, sizeof (res)) ;
   return res ;
}

void write_debug_assert_info (const char *ctx)
{
   DebugAssertInfo info = { { 0 }, 0 } ;

   if (ctx)
   {
      if (eeprom_read_byte ((uint8_t*)E2END))
         return ;

      strncpy_P (info.m_ctx, ctx, sizeof (info.m_ctx)) ;
   }

   eeprom_update_block (&info, DEBUG_ASSERT_EEPROM, sizeof (info)) ;
}

void commit_debug_assert_info (void)
{
   if (eeprom_read_byte ((uint8_t*)DEBUG_ASSERT_EEPROM))
      eeprom_update_byte ((uint8_t*)E2END, true) ;
}

#endif

////////////////////////////////////////////////////////////////////////////////////////////

void debug_assert (bool result, const char *ctx)
{
   if (result)
      return ;

   cli() ;

   #ifdef DEBUG_ASSERT
   write_debug_assert_info (ctx) ;

   #endif

   // marker for bootloader
   eeprom_write_byte (0, 2) ;
   eeprom_busy_wait() ;

   hw::reboot() ;

/*   for (;;)
   {
      hw::gpio::LED.turn() ;
      _delay_ms (50) ;
      hw::gpio::LED.turn() ;
      _delay_ms (50) ;
   }*/
}


////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
