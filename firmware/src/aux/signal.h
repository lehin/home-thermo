////////////////////////////////////////////////////////////////////////////////////////////
//
// signal.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "function.h"
#include "list.h"

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Signature>
   class Signal
   {
      Signal (const Signal&) = delete ;
      Signal& operator= (const Signal&) = delete ;

      typedef function<_Signature> Subscriber ;
      typedef List<Subscriber> Subscription ;

   public:
      typedef typename Subscription::iterator Connection ;

   public:
      Signal (void) {}

      template<typename _Func>
         Connection connect (const _Func& fn) { return m_subscription.insert (m_subscription.end(), fn) ; }

      void disconnect (Connection c) { m_subscription.erase (c) ; }

      template<typename ... _Args>
         void operator() (const _Args ... args) const
         {
            auto it = m_subscription.begin() ;
            while (it != m_subscription.end())
            {
               auto itcur = it++ ;
               (*itcur) (args ...) ;
            }
         }

   private:
      Subscription m_subscription ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
