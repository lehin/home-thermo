////////////////////////////////////////////////////////////////////////////////////////////
//
// vector.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "alloc.h"

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp, typename _Sz, _Sz _min_alloc = 4>
   class VectorImpl
   {
   public:
      typedef _Sz size_type ;
      typedef _Sz difference_type ;
      typedef _Sz index_type ;
      typedef _Tp value_type ;
      typedef value_type* pointer_type ;
      typedef value_type& reference_type ;
      typedef const value_type& const_reference_type ;

      template<typename _ItVal>
         class iterator_base
         {
         public:
            typedef _ItVal value_type ;
            typedef value_type& reference ;
            typedef value_type* pointer ;

         private:
            friend class VectorImpl ;
            template<typename> friend class iterator_base ;

            iterator_base (_ItVal *p) : m_p{ p } {}

         public:
            iterator_base (void) = default ;

            template<typename _ItVal2>
               iterator_base (iterator_base<_ItVal2> org) : m_p{ org.m_p } {}

            template<typename _ItVal2>
               iterator_base& operator= (iterator_base<_ItVal2> rhs)
                  { m_p = rhs.m_p ; return *this ; }

            reference operator* (void) const { return *m_p ; }
            pointer operator-> (void) const { return m_p ; }

            iterator_base& operator++ (void) { ++m_p ; return *this ; }
            iterator_base operator++ (int) { return m_p++ ; }

            iterator_base& operator-- (void) { --m_p ; return *this ; }
            iterator_base operator-- (int) { return m_p-- ; }

            iterator_base operator+ (difference_type diff) const { return m_p + diff ; }
            iterator_base operator- (difference_type diff) const { return m_p - diff ; }

            iterator_base& operator+= (difference_type diff) { m_p += diff ; return *this ; }
            iterator_base& operator-= (difference_type diff) { m_p -= diff ; return *this ; }

            template<typename _Lhs, typename _Rhs>
               friend inline difference_type operator- (const iterator_base<_Lhs>& lhs, const iterator_base<_Rhs>& rhs)
                  { return lhs.m_p - rhs.m_p ; }

            template<typename _Lhs, typename _Rhs>
               friend inline bool operator< (const iterator_base<_Lhs>& lhs, const iterator_base<_Rhs>& rhs)
                  { return lhs.m_p < rhs.m_p ; }

            template<typename _Lhs, typename _Rhs>
               friend inline bool operator> (const iterator_base<_Lhs>& lhs, const iterator_base<_Rhs>& rhs)
                  { return lhs.m_p > rhs.m_p ; }

            template<typename _Lhs, typename _Rhs>
               friend inline bool operator<= (const iterator_base<_Lhs>& lhs, const iterator_base<_Rhs>& rhs)
                  { return lhs.m_p <= rhs.m_p ; }

            template<typename _Lhs, typename _Rhs>
               friend inline bool operator>= (const iterator_base<_Lhs>& lhs, const iterator_base<_Rhs>& rhs)
                  { return lhs.m_p >= rhs.m_p ; }

            template<typename _Lhs, typename _Rhs>
               friend inline bool operator== (const iterator_base<_Lhs>& lhs, const iterator_base<_Rhs>& rhs)
                  { return lhs.m_p == rhs.m_p ; }

            template<typename _Lhs, typename _Rhs>
               friend inline bool operator!= (const iterator_base<_Lhs>& lhs, const iterator_base<_Rhs>& rhs)
                  { return lhs.m_p != rhs.m_p ; }

         private:
            _ItVal* m_p ;
         } ;

         typedef iterator_base<value_type> iterator ;
         typedef iterator_base<const value_type> const_iterator ;

   public:
      ~VectorImpl (void)
      {
         _destroy() ;
      }

      VectorImpl (size_type reserved = _min_alloc)
      {
         _alloc_buffer (reserved) ;
      }

      template<typename _Tp2, size_type _Sz2>
         VectorImpl (const VectorImpl<_Tp2, size_type, _Sz2>& org) :
            VectorImpl (org.size())
         {
            for (auto& v : org)
               new (m_end++) value_type{ v } ;
         }

      VectorImpl (VectorImpl&& org) :
         m_begin{ org.m_begin },
         m_end{ org.m_end },
         m_beyond{ org.m_beyond }
      {
         org.m_begin = org.m_end = org.m_beyond = nullptr ;
      }

      template<typename _Tp2, size_type _Sz2>
         VectorImpl& operator= (const VectorImpl<_Tp2, size_type, _Sz2>& org)
         {
            if (m_begin == org.m_begin)
               return *this ;

            _destroy() ;
            _alloc_buffer (org.size()) ;

            for (auto& v : org)
               new (m_end++) value_type{ v } ;

            return *this ;
         }

      VectorImpl& operator= (VectorImpl&& org)
      {
         if (m_begin == org.m_begin)
            return *this ;

         _destroy() ;
         m_begin = org.m_begin ;
         m_end = org.m_end ;
         m_beyond = org.m_beyond ;
         org.m_begin = org.m_end = org.m_beyond = nullptr ;

         return *this ;
      }

      size_type size (void) const { return m_end - m_begin ; }
      bool empty (void) const { return m_end == m_begin ; }

      void clear (void)
      {
         for (auto& v : *this)
            v.value_type::~value_type() ;

         m_end = m_begin ;
      }

      void resize (size_type new_size)
      {
         auto new_end = m_begin + new_size ;
         if (new_end == m_end)
            return ;

         if (new_end < m_end)
            for (auto& val : range (new_end, m_end))
               val.value_type::~value_type() ;
         else
         {
            if (_check_resize (new_end - m_end))
               new_end = m_begin + new_size ;

            for (auto& val : range (m_end, new_end))
               new (&val) value_type{} ;
         }

         m_end = new_end ;
      }

      iterator begin (void) { return m_begin ; }
      iterator end (void) { return m_end ; }

      const_iterator begin (void) const { return m_begin ; }
      const_iterator end (void) const { return m_end ; }

      reference_type front (void) { return *m_begin ; }
      const_reference_type front (void) const { return *m_begin ; }

      reference_type back (void) { return *(m_end-1) ; }
      const_reference_type back (void) const { return *(m_end-1) ; }

      void push_back (const value_type& val)
      {
         _check_resize (1) ;
         new (m_end++) value_type{ val } ;
      }

      value_type pop_back (void)
      {
         value_type ret = *--m_end ;
         m_end->value_type::~value_type() ;
         return ret ;
      }

      void erase (iterator it)
      {
         it->value_type::~value_type() ;

         for (++it; it != end(); ++it)
         {
            new ( &*(it - 1) ) value_type{ *it } ;
            it->value_type::~value_type() ;
         }

         --m_end ;
      }

      reference_type operator[] (size_type idx) { return m_begin[idx] ; }
      const_reference_type operator[]  (size_type idx) const { return m_begin[idx] ; }

   private:
      void _alloc_buffer (size_type sz)
      {
         if (!sz)
            m_begin = m_end = m_beyond = nullptr ;
         else
         {
            m_begin = m_end = (value_type*)allocate_heap_memory (sz * sizeof (value_type)) ;
            m_beyond = m_begin + sz ;
         }
      }

      bool _check_resize (size_type sz)
      {
         if (m_end + sz <= m_beyond)
            return false ;

         _do_resize (max (size() + sz, (m_beyond - m_begin) * 5u / 4u )) ;
         return true ;
      }

      void _do_resize (size_type sz)
      {
         value_type *b = m_begin, *e = m_end ;
         _alloc_buffer (sz) ;

         for (auto& v : aux::range (b,e))
         {
            new (m_end++) value_type{ v } ;
            v.value_type::~value_type() ;
         }

         free (b) ;
      }

      void _destroy (void)
      {
         if (!m_begin)
            return ;

         clear() ;

         free (m_begin) ;
      }

   private:
      value_type *m_begin, *m_end, *m_beyond ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp, size_t _min_alloc = 4>
   using Vector = VectorImpl<_Tp, size_t, _min_alloc> ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
