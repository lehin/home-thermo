////////////////////////////////////////////////////////////////////////////////////////////
//
// ll-scheduler.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ring-buffer.h"
#include "debug-assert.h"
#include <util/atomic.h>
#include <avr/io.h>

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class LowLatencySched LLSCHED ;

////////////////////////////////////////////////////////////////////////////////////////////

class LowLatencySched
{
   typedef void (*Slot) (void) ;

public:
   enum class Priority : uint8_t{ High, Regular, _Count } ;
   void schedule (const Slot& slot, const char *ctx, Priority priority = Priority::Regular) ;
   void schedule_high (const Slot& slot, const char *ctx) { schedule (slot, ctx, Priority::High) ; }

   void process (Priority priority = Priority::Regular) ;
   void process_no_sleep (Priority priority = Priority::Regular) ;

   template<typename _Cond>
      void wait (_Cond cond, Priority priority = Priority::High)
      {
         while (!cond())
            process_no_sleep (priority) ;
      }

private:
   struct Item
   {
      Slot m_slot ;
      const char *m_ctx ;
   } ;

   typedef RingBuffer<24,Item> Queue ;

   Queue m_pending [(uint8_t)Priority::_Count] ;
} ;


inline
   void LowLatencySched::schedule (const Slot& slot, const char *ctx, Priority priority)
   {
      ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
      {
         debug_assert( m_pending[(uint8_t)priority] . push_back ({ slot, ctx }), ctx ) ;
      }
   }

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Cond>
   inline void wait (_Cond cond, LowLatencySched::Priority priority = LowLatencySched::Priority::High)
      { LLSCHED.wait (cond, priority) ; }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
