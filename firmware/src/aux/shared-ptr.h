////////////////////////////////////////////////////////////////////////////////////////////
//
// shared-ptr.h
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "debug-assert.h"

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

template<class _Tp>
   class shared_ptr
   {
   public:
      typedef _Tp value_type ;
      typedef _Tp& reference ;
      typedef _Tp* pointer ;

   private:
      struct Wrapped : value_type
      {
         Wrapped (const Wrapped&) = delete ;
         int operator= (const Wrapped&) = delete ;

         template<typename ... _Args>
            Wrapped (_Args ... args) :
               value_type{ args... }
            {
            }

         uint8_t m_refcnt = 1 ;
      } ;

      shared_ptr (Wrapped *wrapped) : m_value{ wrapped } {}

   public:
      shared_ptr (void) : m_value{ nullptr } {}
      ~shared_ptr (void) { _unreference (m_value) ; }

      shared_ptr (const shared_ptr& rhs) :
         m_value{ rhs.m_value }
      {
         _reference (m_value) ;
      }

      shared_ptr (shared_ptr&& rhs) :
         m_value{ rhs.m_value }
      {
         rhs.m_value = nullptr ;
      }

      shared_ptr& operator= (const shared_ptr& rhs)
      {
         _reference (rhs.m_value) ;
         _unreference (m_value) ;
         m_value = rhs.m_value ;
         return *this ;
      }

      shared_ptr& operator= (shared_ptr&& rhs)
      {
         _unreference (m_value) ;

         m_value = rhs.m_value ;
         rhs.m_value = nullptr ;

         return *this ;
      }

      void reset (void)
      {
         _unreference (m_value) ;
         m_value = nullptr ;
      }

      pointer operator-> (void) const { debug_assert( m_value, PSTR("SHAREDPTRVAL") ) ; return m_value ; }
      reference operator* (void) const { return *operator->() ; }

      explicit operator bool (void) const { return m_value != nullptr ; }

      template<typename ... _Args>
         static shared_ptr create (_Args ... args)
         {
            return shared_ptr{ new Wrapped{ args... } } ;
         }

      friend inline bool operator== (const shared_ptr& lhs, const shared_ptr& rhs) { return lhs.m_wrapped == rhs.m_wrapped ; }
      friend inline bool operator!= (const shared_ptr& lhs, const shared_ptr& rhs) { return lhs.m_wrapped != rhs.m_wrapped ; }
      friend inline bool operator< (const shared_ptr& lhs, const shared_ptr& rhs) { return lhs.m_wrapped < rhs.m_wrapped ; }
      friend inline bool operator> (const shared_ptr& lhs, const shared_ptr& rhs) { return lhs.m_wrapped > rhs.m_wrapped ; }
      friend inline bool operator<= (const shared_ptr& lhs, const shared_ptr& rhs) { return lhs.m_wrapped <= rhs.m_wrapped ; }
      friend inline bool operator>= (const shared_ptr& lhs, const shared_ptr& rhs) { return lhs.m_wrapped >= rhs.m_wrapped ; }

   private:
      void _reference (Wrapped *value)
      {
         if (value)
            ++value->m_refcnt ;
      }

      void _unreference (Wrapped *value)
      {
         if (value && !--value->m_refcnt)
            delete value ;
      }

   private:
      Wrapped *m_value ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
