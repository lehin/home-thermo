////////////////////////////////////////////////////////////////////////////////////////////
//
// function.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "forward.h"

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace func
{
   template<typename _Result, typename ... _Args>
      struct Caller
      {
         virtual _Result call (_Args... args) = 0 ;
         virtual void destroy (void) = 0 ;
         virtual Caller* clone (void) const = 0 ;
      } ;

   template<typename _Binder, typename _Result, typename ... _Args>
      struct CallerImpl : Caller<_Result,_Args...>
      {
         typedef Caller<_Result,_Args...> caller_class ;
         typedef CallerImpl this_type ;

         _Binder m_fn ;
         CallerImpl (_Binder fn) : m_fn { fn } {}

         virtual _Result call (_Args... args) { return m_fn (args...) ; }
         virtual void destroy (void) { delete this ; }
         virtual caller_class* clone (void) const { return new this_type{ m_fn } ; }

         static caller_class* create (_Binder fn) { return new CallerImpl{ fn } ; }
      } ;

   struct true_type{} ;
   struct false_type{} ;

   template<typename _Tp> _Tp declval (void) ;

   template<typename _From, typename _To>
      class is_convertibe
      {
         template<typename _To1>
            static void _test_aux (_To1) ;

         template<typename _From1, typename _To1, typename = decltype (_test_aux<_To1> (declval<_From1>()))>
            static true_type _test (int) ;

         template<typename _From1, typename _To1>
            static false_type _test (...) ;

      public:
         typedef decltype (_test<_From,_To> (0)) type ;
      } ;

   template<typename _fn_convertible, typename _Binder, typename _Result, typename ... _Args>
      struct CallerImplSelectImpl
      {
         typedef CallerImpl<_Binder, _Result, _Args...> type ;

         static Caller<_Result,_Args...>* create (_Binder fn) { return type::create (fn) ; }
      } ;

   template<typename _Binder, typename _Result, typename ... _Args>
      struct CallerImplSelectImpl<true_type, _Binder, _Result, _Args...>
      {
         typedef CallerImpl<_Result (*) (_Args...), _Result, _Args...> type ;

         static Caller<_Result,_Args...>* create (_Binder fn) { return type::create (fn) ; }
      } ;

} // namespace func

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Signature>
   class function ;

template<typename _Result, typename... _Args>
   class function<_Result(_Args...)>
   {
      typedef func::Caller<_Result,_Args...> Caller ;

   public:
      template<typename _Fn>
         function (_Fn fn) :
            m_caller
               {
                  func::CallerImplSelectImpl
                     <
                        typename func::is_convertibe<_Fn,_Result (*) (_Args...)>::type,
                        _Fn, _Result, _Args...
                     > ::create (fn)
               }
         {
         }

      function (void) :
         m_caller { nullptr }
      {
      }

      function (const function& org) :
         m_caller { org.m_caller->clone() }
      {
      }

      function (function&& org) :
         m_caller { org.m_caller }
      {
         org.m_caller = nullptr ;
      }

      ~function (void)
      {
         _destroy() ;
      }

      function& operator= (const function& rhs)
      {
         _destroy() ;
         m_caller = rhs.m_caller->clone() ;
         return *this ;
      }

      function& operator= (function&& rhs)
      {
         _destroy() ;

         m_caller = rhs.m_caller ;
         rhs.m_caller = nullptr ;

         return *this ;
      }

      _Result operator() (_Args... args) const
      {
         if (!m_caller)
            return _Result() ;

         return m_caller->call (args...) ;
      }

      bool is_null (void) const
      {
         return m_caller == nullptr ;
      }

      operator bool (void) const
      {
         return !is_null() ;
      }

   private:
      void _destroy (void)
      {
         if (m_caller)
            m_caller->destroy() ;
      }

   private:
      Caller *m_caller ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
