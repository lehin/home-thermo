////////////////////////////////////////////////////////////////////////////////////////////
//
// ll-scheduler.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "ll-scheduler.h"
#include "hw/watchdog.h"
#include <avr/sleep.h>
#include <util/atomic.h>

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

LowLatencySched LLSCHED ;

////////////////////////////////////////////////////////////////////////////////////////////

void LowLatencySched::process (Priority priority)
{
   Slot slot ;

   while (true)
   {
      Slot slot = nullptr ;
      const char *ctx = nullptr ;

      cli() ;

      for (uint8_t n = 0; n <= (uint8_t)priority; ++n)
         if (!m_pending[n].empty())
         {
            auto item = m_pending[n].pop_front() ;
            slot = item.m_slot ;
            ctx = item.m_ctx ;
            break ;
         }

      if (!slot)
      {
         sleep_enable() ;
         sei() ;
         sleep_cpu() ;
         sleep_disable() ;
         return ;
      }

      hw::Wdt::Context _wctx{ ctx } ;
      sei() ;
      slot() ;
   }
} ;

void LowLatencySched::process_no_sleep (Priority priority)
{
   Slot slot ;

   while (true)
   {
      Slot slot = nullptr ;
      const char *ctx = nullptr ;

      ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
      {
         for (uint8_t n = 0; n <= (uint8_t)priority; ++n)
            if (!m_pending[n].empty())
            {
               auto item = m_pending[n].pop_front() ;
               slot = item.m_slot ;
               ctx = item.m_ctx ;
               break ;
            }

         if (!slot)
            return ;
      }

      hw::Wdt::Context _wctx{ ctx } ;
      slot() ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
