#include <avr/interrupt.h>
#include <util/delay.h>
#include "aux/ll-scheduler.h"
#include "aux/scheduler.h"
#include "hw/watchdog.h"
#include "hw/led.h"
#include "hw/reset-line.h"
#include "lan/lan-service.h"
#include "peripherals/device-server.h"
#include "peripherals/rtc.h"
#include "controller/system-master.h"
#include "misc/rtc-scheduler.h"
#include "misc/ntp-client.h"
#include "app/init.h"

////////////////////////////////////////////////////////////////////////////////////////////

extern "C"
{
   void __cxa_pure_virtual(void)
   {
      abort() ;
   }

   void __cxa_deleted_virtual(void)
   {
      abort() ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

static bool check_reset (void)
{
   bool reset = !hw::gpio::MISO.read() ;

   for (uint8_t n = 0; reset && n < 100; ++n)
   {
      if (hw::gpio::MISO.read())
         reset = false ;

      _delay_ms (5) ;
   }

   return reset ;
}

////////////////////////////////////////////////////////////////////////////////////////////

static void _initialize (void)
{
   if (check_reset())
   {
      hw::LED.on() ;

      aux::SCHED.schedule
         (
            []
            {
               hw::LED.off() ;
               lan::SERVICE.smart_config() ;
            },
            50
         ) ;
   }
   else
   {
      hw::LED.blink (20, 20) ;

      bus::ONEWIRE.reset_chip() ;

      peripherals::RTC.initialize() ;
      peripherals::DEVSERVER.start() ;

      misc::NTPCLIENT.initialize() ;
      misc::RTCSCHED.initialize() ;

      lan::SERVICE.connect() ;
      controller::SYSMASTER.initialize() ;

      app::INIT.do_initialize() ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

int main (int srgc, const char *argv[])
{
   aux::commit_debug_assert_info() ;

   hw::WDT.enable() ;
   sei() ;

   hw::RSTLINE.activate (2, _initialize) ;

   while (true)
   {
      hw::WDT.reset() ;
      aux::LLSCHED.process() ;
   }
}
