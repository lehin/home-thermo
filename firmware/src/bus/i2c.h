////////////////////////////////////////////////////////////////////////////////////////////
//
// i2c.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "hw/i2c.h"
#include "aux/list.h"

namespace bus
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class I2c I2C ;

////////////////////////////////////////////////////////////////////////////////////////////

class I2c
{
public:
   typedef hw::I2c::Tx Tx ;
   typedef hw::I2c::Rx Rx ;

public:
   typedef aux::function<void (Tx&)> Source ;
   typedef aux::function<void (uint8_t errno, Rx&)> Sink ;

   void request (uint8_t addr, uint8_t receive, Source&& src, Sink&& sink = {})
      { _request_impl (addr, receive, src, sink, false) ; }

   void request_continue (uint8_t addr, uint8_t receive, Source&& src, Sink&& sink = {})
      { _request_impl (addr, receive, src, sink, true) ; }

private:
   void _request_impl (uint8_t addr, uint8_t receive, Source& src, Sink& sink, bool continuation) ;
   void _post_top_request (void) ;
   void _handle_sent (uint8_t errno) ;
   void _handle_received (uint8_t errno, Rx& rx) ;


private:
   struct Request
   {
      uint8_t m_addr ;
      Source m_source ;
      Sink m_sink ;

      uint8_t m_recsize ;
   } ;

   typedef aux::List<Request> Queue ;
   Queue m_queue ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace bus
