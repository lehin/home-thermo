////////////////////////////////////////////////////////////////////////////////////////////
//
// one-wire.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "one-wire.h"
#include "i2c.h"
#include "aux/ll-scheduler.h"

namespace bus
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   enum Status : uint8_t { ONEWB = 0x01, PPD = 0x02, SD = 0x04, LL = 0x08, RST = 0x10, SBR = 0x20, TSB = 0x40, DIR = 0x80 } ;

   enum Cmd1Wire : uint8_t
   {
      CHIP = 0x18,

      CMD_CHIP_RESET = 0xF0,
      CMD_SET_READ_POINTER = 0xE1,
      CMD_1WIRE_RESET = 0xB4,
      CMD_1WIRE_WRITE = 0xA5,
      CMD_1WIRE_TRIPLET = 0x78,
      CMD_1WIRE_WRITE_BIT = 0x87,
      CMD_1WIRE_READ = 0x96,

      READ_PTR_STATUS = 0xF0,
      READ_PTR_DATA = 0xE1,
      READ_PTR_CONFIG = 0xC3
   } ;
}

////////////////////////////////////////////////////////////////////////////////////////////

OneWire ONEWIRE ;

////////////////////////////////////////////////////////////////////////////////////////////

void OneWire::reset_chip (void)
{
   I2C.request (CHIP, 0, [] (I2c::Tx& tx) { tx << CMD_CHIP_RESET ; }) ;
}

bool OneWire::reset (void)
{
   I2C.request (CHIP, 1, [] (I2c::Tx& tx) { tx << CMD_1WIRE_RESET ; }, _receive_chip_state_static) ;

   if (!_onewb_wait())
      return false ;

   return (m_chip_status & PPD) != 0 ;
}

namespace
{
   inline bool _get_bit (const OneWire::DEVID& id, uint8_t bit)
      {  return ( id.m_data[ bit/8 ] & (1 << (bit % 8)) ) != 0 ; }

   inline void _set_bit (OneWire::DEVID& id, uint8_t bit, bool val)
   {
      if (val)
         id.m_data[ bit/8 ] |= (1 << (bit % 8)) ;
      else
         id.m_data[ bit/8 ] &= ~(1 << (bit % 8)) ;
   }
}

bool OneWire::discovery (IdSink&& id_sink)
{
   int8_t lastpos = -1 ;
   DEVID id ;

   do
   {
      if (!reset())
         return false ;

      if (!_send_byte (0xF0))
         return false ;

      int8_t curpos = -1 ;

      for (int8_t bit = 0; bit < 64; ++bit)
      {
         uint8_t dir ;
         if (bit < lastpos)
            dir = _get_bit (id, bit) ? 0x80 : 0x00 ;
         else
            dir = bit > lastpos ? 0x00 : 0x80 ;

         I2C.request (CHIP, 1, [dir] (I2c::Tx& tx) { tx << CMD_1WIRE_TRIPLET << dir ; }, _receive_chip_state_static) ;

         if (!_onewb_wait())
            return false ;

         switch ((m_chip_status >> 5) & 0x03)
         {
            case 0:
               _set_bit (id, bit, (m_chip_status & DIR) != 0) ;

               if (curpos < 0 && bit > lastpos)
                  curpos = bit ;

               break ;

            case 1: _set_bit (id, bit, true) ; break ;
            case 2: _set_bit (id, bit, false) ; break ;
            case 3: return false ;
         }
      }

      lastpos = curpos ;

      id_sink (id) ;
   } while (lastpos >= 0) ;

   return true ;
}

OneWire::BitResult OneWire::bit_timeslot (bool one)
{
   I2C.request (CHIP, 1, [one] (I2c::Tx& tx) { tx << CMD_1WIRE_WRITE_BIT << (one ? '\x80' : '\0') ; }, _receive_chip_state_static) ;

   if (!_onewb_wait())
      return BitResult::Error ;

   return m_chip_status & SBR ? BitResult::One : BitResult::Zero ;
}

bool OneWire::match_rom (const DEVID& id)
{
   return send ('\x55') && send (id) ;
}

bool OneWire::read (uint8_t *byte)
{
   I2C.request (CHIP, 1, [] (I2c::Tx& tx) { tx << CMD_1WIRE_READ ; }, _receive_chip_state_static) ;
   if (!_onewb_wait())
      return false ;

   I2C.request (CHIP, 1, [] (I2c::Tx& tx) { tx << CMD_SET_READ_POINTER << READ_PTR_DATA ; }, _receive_data_byte_static) ;

   m_state = State::Pending ;
   aux::wait ([]() { return ONEWIRE.m_state != State::Pending ; }) ;

   *byte = m_received_byte ;

   return m_state == State::Ok ;
}

bool OneWire::_send_byte (uint8_t byte)
{
   I2C.request (CHIP, 1, [byte] (I2c::Tx& tx) { tx << CMD_1WIRE_WRITE << byte ; }, _receive_chip_state_static) ;
   return _onewb_wait() ;
}

bool OneWire::_simple_wait (void)
{
   m_state = State::Pending ;
   aux::wait ([]() { return ONEWIRE.m_state != State::Pending ; }) ;

   return m_state == State::Ok ;
}

bool OneWire::_onewb_wait (void)
{
   do
   {
      m_state = State::Pending ;

      aux::wait ([]() { return ONEWIRE.m_state != State::Pending ; }) ;

      if (m_state == State::Error)
         return false ;

      if (!(m_chip_status & ONEWB))
         break ;
      else
         I2C.request (CHIP, 1, {}, _receive_chip_state_static) ;
   } while (true) ;

   return true ;
}

inline __attribute__ ((always_inline))
   void OneWire::_receive_chip_state (uint8_t errno, I2c::Rx& rx)
   {
      m_chip_status = _receive_byte_impl (errno, rx) ;
   }

inline __attribute__ ((always_inline))
   void OneWire::_receive_data_byte (uint8_t errno, I2c::Rx& rx)
   {
      m_received_byte = _receive_byte_impl (errno, rx) ;
   }

uint8_t OneWire::_receive_byte_impl (uint8_t errno, I2c::Rx& rx)
{
   uint8_t byte ;

   if (errno)
      m_state = State::Error ;
   else
   {
      rx >> byte ;
      m_state = State::Ok ;
   }

   return byte ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace bus
