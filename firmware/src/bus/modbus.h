////////////////////////////////////////////////////////////////////////////////////////////
//
// modbus.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#if !defined (SERIALDEBUG)

#include "hw/modbus.h"
#include "aux/function.h"
#include "aux/list.h"

namespace bus
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Modbus MODBUS ;

////////////////////////////////////////////////////////////////////////////////////////////

class Modbus
{
public:
   typedef hw::Modbus::Tx Tx ;
   typedef hw::Modbus::RecBytes Data ;

   enum class Result : uint8_t { Ok, IoError, OpError } ;

   struct LastError
   {
      uint8_t m_addr, m_opcode ;

      LastError (uint8_t addr, uint8_t opcode) : m_addr{ addr }, m_opcode{ opcode } {}
      LastError (void) : m_addr{ 0 }, m_opcode{ 0 } {}
   } ;

public:
   typedef aux::function<void (Tx&)> Source ;
   typedef aux::function<void (Result, const Data&)> Sink ;

   void request (uint8_t addr, uint8_t opcode, Source&& src = {}, Sink&& sink = {}, bool retry = true) ;
   void broadcast (uint8_t opcode, Source&& src = {}) ;

   LastError last_error (void) { return m_last_error ; }
   void reset_last_error (void) { m_last_error = {} ; }

private:
   void _post_top_request (void) ;
   void _handle_reply (bool ok, const Data& data) ;
   void _handle_broadcast (void) ;

private:
   struct Request
   {
      uint8_t m_addr ;
      uint8_t m_opcode ;
      Source m_source ;
      Sink m_sink ;
      uint8_t m_attempts ;

      Request(void) {}

      Request (uint8_t addr, uint8_t opcode, Source&& source, Sink&& sink, uint8_t attempts) :
         m_addr{ addr }, m_opcode{ opcode }, m_source{ aux::forward (source) }, m_sink{ aux::forward (sink) }, m_attempts{ attempts } {}
   } ;

   typedef aux::List<Request> Queue ;
   Queue m_queue ;

   LastError m_last_error ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace bus

////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined (SERIALDEBUG)
