////////////////////////////////////////////////////////////////////////////////////////////
//
// modbus.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "modbus.h"
#include "aux/ll-scheduler.h"

#if !defined (SERIALDEBUG)

static constexpr uint8_t DEVICE_TIMEOUT = 100 ;

namespace bus
{

////////////////////////////////////////////////////////////////////////////////////////////

Modbus MODBUS ;

////////////////////////////////////////////////////////////////////////////////////////////

void Modbus::request (uint8_t addr, uint8_t opcode, Source&& src, Sink&& sink, bool retry)
{
   bool first = m_queue.empty() ;

   static constexpr uint8_t REQUEST_ATTEMPTS = 5 ;
   static constexpr uint8_t REQUEST_ATTEMPTS_NO_RETRY = 1 ;

   m_queue.push_back ({ addr, opcode, aux::forward (src), aux::forward (sink), retry ? REQUEST_ATTEMPTS : REQUEST_ATTEMPTS_NO_RETRY }) ;

   if (first)
      aux::LLSCHED.schedule_high ([] { MODBUS._post_top_request() ; }, PSTR ("MODBUS REQ")) ;
}

void Modbus::broadcast (uint8_t opcode, Source&& src)
{
   request (0, opcode, aux::forward (src), {}, false) ;
}

void Modbus::_post_top_request (void)
{
   if (m_queue.empty())
      return ;

   auto& top = *m_queue.begin() ;
   auto tx = hw::MODBUS.tx() ;

   tx << top.m_addr << top.m_opcode ;

   if (top.m_source)
      top.m_source (tx) ;

   if (top.m_addr)
   {
      if (tx.send())
         hw::MODBUS.receive
            (
               [] (bool ok, const hw::Modbus::RecBytes& data) { MODBUS._handle_reply (ok, data) ; },
               DEVICE_TIMEOUT
            ) ;
      else
      {
         uint8_t err[3] = { top.m_addr, (uint8_t) (top.m_opcode | 0x80), 255 } ;
         _handle_reply (false, aux::range (err)) ;
      }
   }
   else // broadcast
   {
      if (!tx.send ([] { MODBUS._handle_broadcast() ; } ))
         _handle_broadcast() ;
   }
}

void Modbus::_handle_reply (bool ok, const Data& data)
{
   if (m_queue.empty())
      return ;

   Result res = ok ? Result::Ok : Result::IoError ;

   if (res == Result::Ok)
      if
         (
            data.size() > 1 &&
            data.begin()[0] == m_queue.front().m_addr &&
            (data.begin()[1] & 0x7F) == m_queue.front().m_opcode
         )
         res = !(data.begin()[1] & 0x80) ? Result::Ok : Result::OpError ;
      else
      {
         hw::MODBUS.receive
            (
               [] (bool ok, const hw::Modbus::RecBytes& data) { MODBUS._handle_reply (ok, data) ; },
               DEVICE_TIMEOUT
            ) ;

         return ;
      }

   auto& request = m_queue.front() ;

   if (!ok)
      m_last_error = LastError{ request.m_addr, request.m_opcode } ;

   if (ok || !--request.m_attempts)
   {
      auto& sink = request.m_sink ;

      if (sink)
         sink (res, aux::range (data.begin() + 2, data.end())) ;

      m_queue.erase (m_queue.begin()) ;
   }

   if (!m_queue.empty())
      aux::LLSCHED.schedule_high ([] { MODBUS._post_top_request() ; }, PSTR ("MODBUS REPLY")) ;
}

void Modbus::_handle_broadcast (void)
{
   if (m_queue.empty())
      return ;

   m_queue.erase (m_queue.begin()) ;

   if (!m_queue.empty())
      aux::LLSCHED.schedule_high ([] { MODBUS._post_top_request() ; }, PSTR ("MODBUS BCAST")) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace bus

////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined (SERIALDEBUG)
