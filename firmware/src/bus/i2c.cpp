////////////////////////////////////////////////////////////////////////////////////////////
//
// i2c.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "i2c.h"
#include "aux/ll-scheduler.h"

namespace bus
{

////////////////////////////////////////////////////////////////////////////////////////////

I2c I2C ;

////////////////////////////////////////////////////////////////////////////////////////////

void I2c::_request_impl (uint8_t addr, uint8_t receive, Source& src, Sink& sink, bool continuation)
{
   bool first = m_queue.empty() ;

   if (!continuation)
      m_queue.push_back ({ addr, aux::forward (src), aux::forward (sink), receive }) ;
   else
      m_queue.push_front ({ addr, aux::forward (src), aux::forward (sink), receive }) ;

   if (first)
      aux::LLSCHED.schedule_high ([] { I2C._post_top_request() ; }, PSTR ("I2C BUS REQ")) ;
}

void I2c::_post_top_request (void)
{
   if (m_queue.empty())
      return ;

   auto& top = *m_queue.begin() ;

   if (!top.m_source)
      _handle_sent (0) ;
   else
   {
      auto tx = hw::I2C.tx (top.m_addr, [] (uint8_t errno) { I2C._handle_sent (errno) ; }) ;
      top.m_source (tx) ;
      tx.send() ;
   }
}

void I2c::_handle_sent (uint8_t errno)
{
   auto& top = *m_queue.begin() ;
   if (!errno && top.m_recsize)
      hw::I2C.rx (top.m_addr, top.m_recsize, [] (uint8_t errno, Rx& rx) { I2C._handle_received (errno, rx) ; }) ;
   else
   {
      auto rx = hw::I2C.rx_buf() ;
      _handle_received (errno, rx) ;
   }
}

void I2c::_handle_received (uint8_t errno, Rx& rx)
{
   auto it = m_queue.begin() ;

   if (it->m_sink)
      it->m_sink (errno, rx) ;

   m_queue.erase (it) ;

   if (!m_queue.empty())
      aux::LLSCHED.schedule_high ([] { I2C._post_top_request() ; }, PSTR ("I2C BUS REC")) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace bus
