////////////////////////////////////////////////////////////////////////////////////////////
//
// one-wire.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "i2c.h"
#include "aux/function.h"

namespace bus
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class OneWire ONEWIRE ;

////////////////////////////////////////////////////////////////////////////////////////////

class OneWire
{
public:
   struct DEVID
   {
      uint8_t m_data[8] ;

      DEVID (void){}

      constexpr DEVID (uint8_t i1, uint8_t i2, uint8_t i3, uint8_t i4, uint8_t i5, uint8_t i6, uint8_t i7, uint8_t i8) :
         m_data{ i1, i2, i3, i4, i5, i6, i7, i8 }
      {
      }

      bool is_null (void) const
      {
         for (auto byte : m_data)
            if (byte != 0xFF)
               return false ;

         return true ;
      }

      static constexpr DEVID null (void) { return { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF } ; }
   } ;

public:
   void reset_chip (void) ;
   bool reset (void) ;

   typedef aux::function<void (const DEVID& id)> IdSink ;
   bool discovery (IdSink&& id_sink) ;

   enum class BitResult { Zero, One, Error } ;
   BitResult bit_timeslot (bool one = true) ;

   bool match_rom (const DEVID& id) ;

   bool send (uint8_t byte) { return _send_byte (byte) ; }

   template<typename _Tp>
      bool send (const _Tp& p)
      {
         for (uint8_t i = 0; i < sizeof (_Tp); ++i)
            if (!send (reinterpret_cast<const uint8_t*>(&p)[i]))
               return false ;

         return true ;
      }

   bool read (uint8_t *byte) ;

private:
   bool _send_byte (uint8_t byte) ;
   bool _simple_wait (void) ;
   bool _onewb_wait (void) ;
   void _receive_chip_state (uint8_t errno, I2c::Rx& rx) ;
   void _receive_data_byte (uint8_t errno, I2c::Rx& rx) ;
   uint8_t _receive_byte_impl (uint8_t errno, I2c::Rx& rx) ;

   static void _receive_chip_state_static (uint8_t errno, I2c::Rx& rx) { ONEWIRE._receive_chip_state (errno, rx) ; }
   static void _receive_data_byte_static (uint8_t errno, I2c::Rx& rx) { ONEWIRE._receive_data_byte (errno, rx) ; }

private:
   enum class State : uint8_t { Ok, Error, Pending } ;
   State m_state = State::Ok ;

   uint8_t m_chip_status = 0 ;
   uint8_t m_received_byte ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace bus
