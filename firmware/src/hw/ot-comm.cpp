////////////////////////////////////////////////////////////////////////////////////////////
//
// ot-comm.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "ot-comm.h"
#include "gpio.h"
#include "watchdog.h"
#include "aux/minmax.h"
#include "aux/ll-scheduler.h"
#include <string.h>
#include <util/atomic.h>

////////////////////////////////////////////////////////////////////////////////////////////

ISR (TIMER2_COMPA_vect)
{
   hw::otcomm::TRANSMITTER._on_compa_timer() ;
}

ISR (TIMER2_COMPB_vect)
{
   hw::otcomm::TRANSMITTER._on_compb_timer() ;
}

ISR (TIMER1_CAPT_vect)
{
   hw::otcomm::RECEIVER._on_icp() ;
}

ISR (TIMER1_COMPB_vect)
{
   hw::otcomm::RECEIVER._on_timeout() ;
}

ISR (TIMER3_COMPA_vect)
{
   hw::otcomm::LINK._on_timer() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace hw
{
   namespace otcomm
   {

////////////////////////////////////////////////////////////////////////////////////////////

Transmitter TRANSMITTER ;
Receiver RECEIVER ;
Link LINK ;

////////////////////////////////////////////////////////////////////////////////////////////

Transmitter::Transmitter (void)
{
   OCR2A = 156 ; // ~ period - 1 mS
   OCR2B = 78 ; // ~ half of period - 500 uS
   TCCR2A = _BV(WGM21) ; // CTC mode ;
   TIMSK2 = _BV(OCIE2A) | _BV(OCIE2B) ;
}

void Transmitter::transmit (const uint8_t *bytes)
{
   memcpy (m_bytes, bytes, BUFFER_SIZE) ;

   RECEIVER.disable() ;

   // running transmission
   TCNT2 = 0 ;

   m_cur = -1 ; // we need to send start bit first
   m_mask = 0 ;
   m_stop = false ;

   TCCR2B = _BV(CS20) | _BV(CS22) ; // CTC mode, clk/128
}

void Transmitter::_stop (void)
{
   TCCR2B = 0 ;
   RECEIVER.enable() ;
}

void Transmitter::_load_latch (void)
{
   if (m_cur != -1 && m_cur != BUFFER_SIZE)
   {
      m_mask = 0x80 ;
      m_latch = m_bytes[m_cur] ;
   }
   else
   {
      m_mask = 0x1 ;
      m_latch = 1 ;

      if (m_cur == BUFFER_SIZE)
         m_stop = true ;
   }

   ++m_cur ;
}

inline __attribute__ ((always_inline))
   void Transmitter::_on_compa_timer (void)
   {
      gpio::OTO.turn() ;
   }

inline __attribute__ ((always_inline))
   void Transmitter::_on_compb_timer (void)
   {
      if (m_stop)
         _stop() ;
      else
      {
         if (!m_mask)
            _load_latch() ;

         if (m_latch & m_mask)
            gpio::OTO.set() ;
         else
            gpio::OTO.reset() ;

         m_mask >>= 1 ;
      }
   }

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static constexpr uint8_t ERROR_FACTOR (uint8_t a) { return a/5 ; }

   static constexpr uint8_t HALF_PERIOD = 10 ; // 500 uS
   static constexpr uint8_t FULL_PERIOD =  HALF_PERIOD * 2 ; // 1 mS

   static constexpr uint8_t HALF_ERROR = ERROR_FACTOR (HALF_PERIOD) ;
   static constexpr uint8_t FULL_ERROR = ERROR_FACTOR (FULL_PERIOD) ;

   static constexpr uint8_t HALF_LEFT = HALF_PERIOD - HALF_ERROR ;
   static constexpr uint8_t HALF_RIGHT = HALF_PERIOD + HALF_ERROR ;

   static constexpr uint8_t FULL_LEFT = FULL_PERIOD - FULL_ERROR ;
   static constexpr uint8_t FULL_RIGHT = FULL_PERIOD + FULL_ERROR ;
}

Receiver::Receiver (void) :
   m_icp_t0{ 0 },
   m_idle{ true },
   m_half_edge{ false },
   m_on_receive{ nullptr }
{
}

void Receiver::enable (void)
{
   TIFR1 |= _BV(ICF1) ;
   TIMSK1 |= _BV(ICIE1) ;
}

void Receiver::disable (void)
{
   ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
   {
      TIMSK1 &= ~_BV(ICIE1) ;
      _stop() ;
   }
}

Receiver::EdgeKind Receiver::_on_capture (uint16_t interval, uint16_t tm, bool positive)
{
   if (m_idle)
   {
      OCR1B = tm + FULL_RIGHT ;
      TIFR1 = _BV(OCF1B) ;
      TIMSK1 |= _BV(OCIE1B) ;

      m_idle = false ;
      m_half_edge = false ;

      _first_bit() ;
   }
   else
   {
      if (interval >= HALF_LEFT && interval <= HALF_RIGHT)
      {
         if (m_half_edge)
            return EdgeKind::Invalid ;

         m_half_edge = true ;
         return EdgeKind::Skip ;
      }
      else if (interval >= FULL_LEFT)
      {
         m_half_edge = false ;

         if (m_cur < BUFFER_SIZE)
         {
            if (!positive)
               m_bytes[m_cur] |= m_mask ;

            if (!(m_mask >>= 1))
            {
               ++m_cur ;
               m_mask = 0x80 ;
            }
         }
         else
         {
            _stop() ;

            if (!positive && m_on_receive) // check for stop bit
               m_on_receive() ;

            return EdgeKind::Final ;
         }
      }
      else
         return EdgeKind::Invalid ;

      OCR1B = tm + FULL_RIGHT ;
   }

   return EdgeKind::Resync ;
}

void Receiver::_first_bit (void)
{
   memset (&m_bytes, 0, sizeof (m_bytes)) ;
   m_cur = 0 ;
   m_mask = 0x80 ;
}

void Receiver::_stop (void)
{
   TIMSK1 &= ~_BV(OCIE1B) ;
   TCCR1B &= ~_BV(ICES1) ;
   m_idle = true ;
}

inline __attribute__ ((always_inline))
   void Receiver::_on_icp (void)
   {
      uint16_t tm = ICR1 ;

      uint16_t interval ;
      if (tm >= m_icp_t0)
         interval = tm - m_icp_t0 ;
      else
         interval = (uint32_t)tm - m_icp_t0 ;

      switch (_on_capture (interval, tm, (TCCR1B & _BV(ICES1)) != 0))
      {
         case EdgeKind::Resync:
            m_icp_t0 = tm ;

         case EdgeKind::Skip:
            TCCR1B ^= _BV(ICES1) ; // change ICP edge
            break ;

         case EdgeKind::Invalid:
         case EdgeKind::Final:
            break ;
      }
   }

inline __attribute__ ((always_inline))
   void Receiver::_on_timeout (void)
   {
      _stop() ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static constexpr uint16_t TIMER_ONE_SECOND = 19531 ;
   static constexpr uint16_t DURATION_MS (uint16_t ms) { return (uint32_t)ms * TIMER_ONE_SECOND / 1000 ; }
}

void Link::initialize (OnReply&& onstatus)
{
   TCCR3B = _BV(CS30) | _BV(CS32) ; // clk / 1024
   RECEIVER.on_receive ([] { LINK._on_receive() ; }) ;
   m_onstatus = onstatus ;
   _ping() ;
}

void Link::send (Message m, OnReply&& onreply)
{
   auto bytes = reinterpret_cast<const uint8_t*> (&m) ;

   // calculating parity bit
   m.parity = 0 ;

   uint8_t mask = 0x40 ;
   for (uint8_t i = 0; i < otcomm::BUFFER_SIZE; ++i)
   {
      for (; mask; mask >>= 1)
         if (bytes[i] & mask)
            m.parity ^= 1 ;

      mask = 0x80 ;
   }

   m_pendings.push_back ({ m, aux::forward (onreply) }) ;
}

void Link::_do_send (void)
{
   TRANSMITTER.transmit (reinterpret_cast<const uint8_t*> (&m_pendings.front().m_msg)) ;
   m_t0 = TCNT3 ;

   m_state = State::WaitReply ;
   _set_timer (m_t0 + DURATION_MS (870)) ;
}

void Link::_handle_received (void)
{
   aux::debug_assert (!m_pendings.empty(), PSTR("OT rec. context")) ;

   auto bytes = RECEIVER.bytes() ;
   bool parity = false ;

   for (uint8_t i = 0; i < otcomm::BUFFER_SIZE; ++i)
      for (uint8_t mask = 1; mask; mask <<= 1)
         if (bytes[i] & mask)
            parity = !parity ;

   if (parity)
      return ;

   _set_present (true) ;

   m_state = State::Pause ;
   _set_timer (TCNT3 + DURATION_MS (105)) ;

   auto it = m_pendings.begin() ;

   if (it->m_onreply)
      it->m_onreply (*reinterpret_cast<const Message*> (bytes)) ;

   m_pendings.erase (it) ;

   WDT.reset_extra() ;
}

void Link::_on_receive (void)
{
   static uint8_t transaction_id ;
   transaction_id = m_transaction_id ;

   aux::LLSCHED.schedule_high
      (
         []
         {
            if (transaction_id == LINK.m_transaction_id)
               LINK._handle_received() ;
         },
         PSTR ("OT LINK REC")
      ) ;
}

void Link::_set_timer (uint16_t tm)
{
   OCR3A = tm ;
   TIFR3 |= _BV(OCF3A) ;
   TIMSK3 |= _BV (OCIE3A) ;

   ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
      ++m_transaction_id ;
}

void Link::_ping (void)
{
   send
      (
         m_status,
         [] (auto& msg)
         {
            if (LINK.m_onstatus)
               LINK.m_onstatus (msg) ;
         }
      ) ;

   _do_send() ;
}

void Link::_check_pendings (void)
{
   if (!m_pendings.empty())
      _do_send() ;
   else
      _ping() ;
}

void Link::_set_present (bool present)
{
   if (present)
      m_commstat.m_error_pending = false ;
   else
      ++m_retrains_counter ;

   if (present == m_commstat.m_present)
      return ;

   if (!present)
   {
      if (!m_commstat.m_error_pending)
      {
         m_commstat.m_error_pending = true ;
         m_commstat.m_not_present_delay = 3 ;
      }

      if (m_commstat.m_not_present_delay && --m_commstat.m_not_present_delay)
         return ;
   }

   m_commstat.m_present = present ;
   m_signal_present (present) ;
}

inline __attribute__ ((always_inline))
   void Link::_on_timer (void)
   {
      TIMSK3 &= ~_BV(OCIE3A) ;

      static uint8_t transaction_id ;
      transaction_id = LINK.m_transaction_id ;

      aux::LLSCHED.schedule_high
         (
            []
            {
               if (transaction_id != LINK.m_transaction_id)
                  return ;

               switch (LINK.m_state)
               {
                  case State::WaitReply: LINK._set_present (false) ; LINK._do_send() ; break ;
                  case State::Pause: LINK._check_pendings() ; break ;
               }
            },
            PSTR ("OT LINK TIMER")
         ) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace otcomm
} // namespace hw
