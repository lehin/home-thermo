////////////////////////////////////////////////////////////////////////////////////////////
//
// uart.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#if defined (SERIALDEBUG)

#include "aux/ring-buffer.h"
#include "aux/range.h"
#include "aux/buf-guard.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

ISR (USART0_RX_vect) ;
ISR (USART0_UDRE_vect) ;

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Uart UART ;

////////////////////////////////////////////////////////////////////////////////////////////

class Uart
{
   static constexpr uint8_t RECBUFSIZE = 8 ;
   static constexpr uint8_t SNDBUFSIZE = 16 ;

   typedef void (*EvtHandler) (void) ;
   typedef aux::RingBuffer<RECBUFSIZE,uint8_t> ReceiveBuffer ;
   typedef aux::RingBuffer<SNDBUFSIZE,uint8_t> SendBuffer ;

public:
   typedef aux::BufferGuard<ReceiveBuffer> RecBufAccessor ;

public:
   Uart (void) ;

   void send (uint8_t byte) { _push (byte) ; }

   void send_str (const char *s) ;
   void send_pstr (const char *s) ;

   template<typename _Tp>
      void send (const _Tp& data)
      {
         send_range (aux::range ((const uint8_t*)&data, (const uint8_t*)(&data + 1))) ;
      }

   template<typename _Tp, typename... _Args>
      void send_multiple (const _Tp& data, const _Args& ... args)
      {
         send (data) ;
         send_multiple (args...) ;
      }

   template<typename _Range>
      void send_range (const _Range& range)
      {
         for (auto& val : range)
            send (val) ;
      }

   void receive_handler (EvtHandler handler)
   {
      m_receive_handler = handler ;
   }

   RecBufAccessor buffer (void) { return m_recbuf ; }

private:
   void send_multiple (void) {}
   void _push (uint8_t byte) ;

   friend void ::USART0_RX_vect (void) ;
   friend void ::USART0_UDRE_vect (void) ;

   void _handle_received (void) ;
   void _on_receive (void) ;
   void _on_buffer_empty (void) ;

private:
   ReceiveBuffer m_recbuf ;
   SendBuffer m_sendbuf ;
   bool m_transmitter_active ;
   EvtHandler m_receive_handler ;
   volatile bool m_receive_handle_scheduled ;
   volatile bool m_data_received ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw

#endif // SERIALDEBUG
