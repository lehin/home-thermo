////////////////////////////////////////////////////////////////////////////////////////////
//
// gpio.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <avr/io.h>

namespace hw
{
   namespace gpio
   {

////////////////////////////////////////////////////////////////////////////////////////////

#define PORTDEF(name) \
   struct Portdef##name \
   { \
      static volatile uint8_t& in (void) { return PIN##name ; } \
      static volatile uint8_t& out (void) { return PORT##name ; } \
      static volatile uint8_t& ddr (void) { return DDR##name ; } \
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

enum class Dir { In, Out } ;

template<int _num, Dir _dir = Dir::In, int _init = 1>
   struct PinD
   {
      static constexpr uint8_t mask (void) { return _BV(_num) ; }
      static constexpr uint8_t ddr (void) { return _dir == Dir::Out ? mask() : 0 ; }
      static constexpr uint8_t init (void) { return _init ? mask() : 0 ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _PinD = void, typename ... _Tail>
   struct PortPins : PortPins<_Tail...>
   {
      typedef PortPins<_Tail...> base_class ;

      static constexpr uint8_t ddr (void)
      {
         return (base_class::ddr() & ~_PinD::mask()) | _PinD::ddr() ;
      }

      static constexpr uint8_t init (void)
      {
         return (base_class::init() & ~_PinD::mask()) | _PinD::init() ;
      }
   } ;

template<>
   struct PortPins<void>
   {
      static constexpr uint8_t ddr (void) { return 0 ; }
      static constexpr uint8_t init (void) { return 0xFF ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template< typename _PortDef, typename ... _PinsDef >
   class Port : public _PortDef
   {
   public:
      using _PortDef::in ;
      using _PortDef::out ;
      using _PortDef::ddr ;

      Port (void)
      {
         out() = PortPins<_PinsDef...>::init() ;
         ddr() = PortPins<_PinsDef...>::ddr() ;
      }

      template<int _num>
         struct Pin
         {
            typedef Port Owner ;
            static constexpr int num = _num ;
            static constexpr uint8_t mask = _BV(num) ;

            template<int _set> __attribute__ ((always_inline))
               void set (void) const
               {
                  if (_set)
                     Owner::out() |= mask ;
                  else
                     Owner::out() &= ~mask ;
               }

            __attribute__ ((always_inline))
               void set (void) const { set<1>() ; }

            __attribute__ ((always_inline))
               void reset (void) const { set<0>() ; }

            __attribute__ ((always_inline))
               void turn (void) const { Owner::out() ^= _BV(num) ; }

            __attribute__ ((always_inline))
               bool read (void) const { return (Owner::in() & _BV(num)) != 0 ; }

            __attribute__ ((always_inline))
               bool stat (void) const { return (Owner::out() & _BV(num)) != 0 ; }

            __attribute__ ((always_inline))
               void as_input (void) const { Owner::ddr() &= ~PinD<num>::mask() ; }

            __attribute__ ((always_inline))
               void as_output (void) const { Owner::ddr() |= PinD<num>::mask() ; }
         } ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

PORTDEF (A) ;
PORTDEF (B) ;
PORTDEF (C) ;
PORTDEF (D) ;

using PortA = Port<PortdefA> ;

using PortB =
   Port
      <
         PortdefB,
         PinD<1, Dir::Out, 0>,
         PinD<2, Dir::Out, 1>,
         PinD<3, Dir::Out, 1>,
         PinD<4, Dir::Out, 1>,
         PinD<6, Dir::In, 1>
      > ;

using PortC =
   Port
      <
         PortdefC,
         PinD<0, Dir::Out, 0>,
         PinD<1, Dir::In, 0>,
         PinD<2, Dir::Out, 1>,
         PinD<3, Dir::Out, 1>,
         PinD<4, Dir::In, 0>,
         PinD<5, Dir::In, 0>
      > ;

using PortD =
   Port
      <
         PortdefD,
         PinD<0, Dir::In, 1>,
         PinD<1, Dir::Out, 1>,
         PinD<2, Dir::In, 0>,
         PinD<3, Dir::Out, 1>,
         PinD<4, Dir::Out, 1>,
         PinD<6, Dir::In, 0>,
         PinD<7, Dir::Out, 0>
      > ;

////////////////////////////////////////////////////////////////////////////////////////////

constexpr PortB::Pin<1> LED ;
constexpr PortB::Pin<2> DCS ;
constexpr PortB::Pin<3> DDC ;
constexpr PortB::Pin<4> RTS ;
constexpr PortB::Pin<5> MOSI ;
constexpr PortB::Pin<6> MISO ;
constexpr PortB::Pin<7> SCK ;

constexpr PortC::Pin<0> SCL ;
constexpr PortC::Pin<1> SDA ;
constexpr PortC::Pin<2> WLCS ;
constexpr PortC::Pin<3> RSTP ;
constexpr PortC::Pin<4> WLIRQ ;
constexpr PortC::Pin<5> SQW ;

constexpr PortD::Pin<0> RXD ;
constexpr PortD::Pin<1> TXD ;
constexpr PortD::Pin<2> MISOP ;
constexpr PortD::Pin<3> MOSIP ;
constexpr PortD::Pin<4> SCKP ;
constexpr PortD::Pin<6> OTI ;
constexpr PortD::Pin<7> OTO ;

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace gpio
} // namespace hw
