////////////////////////////////////////////////////////////////////////////////////////////
//
// pcint.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "pcint.h"
#include "aux/ll-scheduler.h"
#include <util/atomic.h>

ISR (PCINT2_vect)
{
   static bool _scheduled = false ;

   if (_scheduled)
      return ;

   _scheduled = true ;

   aux::LLSCHED.schedule_high
      (
         []
         {
            _scheduled = false ;
            hw::PCINT._on_interrupt() ;
         },
         PSTR("PIN CHANGE")
      ) ;
}

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

Pcint PCINT ;

////////////////////////////////////////////////////////////////////////////////////////////

Pcint::Pcint (void) :
   m_current{ PINC },
   m_mask{ 0 }
{
   PCICR |= _BV(PCIE2) ;
}

uint8_t Pcint::_register_handler (uint8_t mask, Handler fn)
{
   m_items.push_back ({ mask, fn }) ;
   PCMSK2 |= mask ;
   return m_items.size() - 1 ;
}

void Pcint::_enable (uint8_t idx)
{
   m_mask |= m_items[idx].m_mask ;
}

void Pcint::_disable (uint8_t idx)
{
   m_mask &= ~m_items[idx].m_mask ;
}

inline __attribute__ ((always_inline))
   void Pcint::_on_interrupt (void)
   {
      uint8_t val = PINC ;
      uint8_t changed = (m_current ^ val) & m_mask ;

      m_current = val ;

      if (!changed)
         return ;

      for (const auto& i : m_items)
         if ((changed & i.m_mask) && !(m_current & i.m_mask))
            i.m_fn() ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
