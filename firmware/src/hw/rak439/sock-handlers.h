////////////////////////////////////////////////////////////////////////////////////////////
//
// sock-handlers.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "sock.h"

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

struct SockResponseData
{
   uint32_t m_type ;
   uint32_t m_sock ;
   uint32_t m_error ;
} ;

typedef Event<0x9007,SockResponseData> SockResponse ;

////////////////////////////////////////////////////////////////////////////////////////////

class SockHandlerBase
{
public:
   static void handle_response (const SockResponse& event) ;

   SockHandlerBase (SockCmdType type) ;
   ~SockHandlerBase (void) ;

   SockCmdType type (void) const { return m_type ; }

protected:
   virtual bool is_waiting (const SockResponse& resp) const ;
   virtual void process (const SockResponse& resp) = 0 ;

private:
   const SockCmdType m_type ;

   SockHandlerBase *m_prev,
                    *m_next ;

   static SockHandlerBase *s_head ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class SockHandler : public SockHandlerBase
{
public:
   SockHandler (SockCmdType type, uint32_t sock) : SockHandlerBase{ type }, m_sock{ sock } {}

   uint32_t retcode (void) const ;

protected:
   virtual bool is_waiting (const SockResponse& resp) const ;
   virtual void process (const SockResponse& resp) ;

private:
   const uint32_t m_sock ;
   uint32_t m_error = 0 ;
   bool m_ready = false ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class SockOpenHandler : public SockHandlerBase
{
public:
   SockOpenHandler (void) : SockHandlerBase{ SockCmdType::Open } {}

   uint32_t sock (void) const ;

protected:
   virtual void process (const SockResponse& resp) ;

private:
   uint32_t m_sock ;
   bool m_ready = false ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class SockListenerHandler : public SockHandlerBase
{
   using Callback = Sock::SimplCallback ;

public:
   SockListenerHandler (void) : SockHandlerBase{ SockCmdType::Listen } {}

   static void callback (Callback cb) { m_callback = cb ; }

protected:
   virtual bool is_waiting (const SockResponse& resp) const ;
   virtual void process (const SockResponse& resp) ;

private:
   static Callback m_callback ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class SockCallbackHandler : public SockHandlerBase
{
   using Callback = Sock::SimplCallback ;

public:
   SockCallbackHandler (SockCmdType type) : SockHandlerBase{ type } {}

   static void callback (Callback cb) { m_callback = cb ; }

protected:
   virtual void process (const SockResponse& resp) ;

private:
   static Callback m_callback ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class SockDhcpHandler : public SockHandlerBase
{
   using Callback = Sock::DhcpCallback ;

public:
   SockDhcpHandler (void) : SockHandlerBase{ SockCmdType::IpConfig } {}

   static void callback (Callback cb) { m_callback = cb ; }

protected:
   virtual bool is_waiting (const SockResponse& resp) const ;
   virtual void process (const SockResponse& resp) ;

private:
   static Callback m_callback ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class SockIpConfigHandler : public SockHandlerBase
{
public:
   SockIpConfigHandler (IpConfigResult& res) :
      SockHandlerBase{ SockCmdType::IpConfig }, m_result { res } {}

   uint32_t retcode (void) const ;

protected:
   virtual void process (const SockResponse& resp) ;

private:
   IpConfigResult& m_result ;
   uint32_t m_error = 0 ;
   bool m_ready = false ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
