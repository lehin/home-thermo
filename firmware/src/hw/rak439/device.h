////////////////////////////////////////////////////////////////////////////////////////////
//
// device.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

extern class Device DEVICE ;

////////////////////////////////////////////////////////////////////////////////////////////

class Device
{
public:
   struct Cs
   {
      Cs (const Cs&) = delete ;
      Cs& operator= (const Cs&) = delete ;

      Cs (void) ;
      ~Cs (void) ;
   } ;

public:
   typedef void (*VoidFn) (void) ;
   void enable_interrupts (VoidFn interrupt_handler) ;
   void disable_interrupts (void) ;
   void reset (void) ;

private:
   static void _on_wlirq (void) ;

private:
   VoidFn m_completion = nullptr ;
   VoidFn m_interrupt_handler = nullptr ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
