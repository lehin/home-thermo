////////////////////////////////////////////////////////////////////////////////////////////
//
// sock.cpp
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "sock.h"
#include "sock-handlers.h"
#include "sock-commands.h"
#include "wmi-events.h"
#include "rxstream.h"
#include "hci.h"
#include "aux/forward.h"

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

bool Sock::m_initialized ;
Sock::ReceiveCallback Sock::m_receive_callback ;

void Sock::dhcp_callback (DhcpCallback cb)
{
   static SockDhcpHandler dhcp_handler ;
   dhcp_handler.callback (cb) ;
}

void Sock::listen_callback (SimplCallback cb)
{
   static SockListenerHandler listener_handler ;
   listener_handler.callback (cb) ;
}

void Sock::close_callback (SimplCallback cb)
{
   static SockCallbackHandler close_handler{ SockCmdType::Close } ;
   close_handler.callback (cb) ;
}

void Sock::receive_callback (ReceiveCallback cb)
{
   _check_initialized() ;
   m_receive_callback = cb ;
}

void Sock::init_stack_offload (void)
{
   _check_initialized() ;
   WMI.command (pgmcmd (sock::StackInit{})) ;
}

void Sock::ip_config_dhcp (const char *hostname)
{
   _check_initialized() ;
   WMI.command (sock::DhcpIpConfig{ hostname }) ;
}

int32_t Sock::ip_config_query (IpConfigResult& res)
{
   _check_initialized() ;

   WMI.command (sock::IpConfig{}) ;
   return SockIpConfigHandler{ res }.retcode() ;
}

uint32_t Sock::open (SockType type)
{
   _check_initialized() ;
   WMI.command (sock::Open{ type }) ;
   return SockOpenHandler{}.sock() ;
}

int Sock::bind (uint32_t sock, uint16_t port)
{
   _check_initialized() ;
   return _standard_command (sock, sock::Bind{ sock, SockAddr{ 0, port } }) ;
}

int Sock::listen (uint32_t sock, int backlog)
{
   _check_initialized() ;
   return _standard_command (sock, sock::Listen{ sock, backlog }) ;
}

uint32_t Sock::accept (uint32_t sock)
{
   _check_initialized() ;
   return _standard_command (sock, sock::Accept{ sock }) ;
}

int32_t Sock::connect (uint32_t sock, const SockAddr& addr)
{
   _check_initialized() ;
   return _standard_command (sock, sock::Connect{ sock, addr }) ;
}

void Sock::close (uint32_t sock)
{
   _check_initialized() ;
   WMI.command (sock::Close{ sock }) ;
}

TxStream Sock::send (uint32_t sock, SockType type, uint16_t len)
{
   return _send_impl (sock, type, len, SockAddr{}) ;
}

TxStream Sock::sendto (uint32_t sock, SockType type, uint16_t len, const SockAddr& addr)
{
   return _send_impl (sock, type, len, addr) ;
}

TxStream Sock::_send_impl (uint32_t sock, SockType type, uint16_t len, const SockAddr& addr)
{
   _check_initialized() ;

   struct SockSend
   {
      uint32_t m_sock ;
      uint16_t m_length ;
      uint16_t m_reserved ;
      uint32_t m_flags ;
      SockAddr m_addr ;
      uint16_t m_addrlen ;
   } ;

   static constexpr uint8_t HDR_SIZES[] = { 0, 64 /* TCP headroom */, 44 /* UDP headroom */ } ;
   uint8_t cust_hdr_size = HDR_SIZES[(uint8_t)type] ;

   auto tx = WMI.send (Wmi::MsgType::Data, Wmi::HdrType::Dot3, 12 + cust_hdr_size + len) ;

   tx.fill (12) ;

   SockSend ss{ sock, len, 0,  0, addr, addr.m_addr ? sizeof (addr) : 0 } ;
   tx.write (ss) ;
   tx.fill (cust_hdr_size - sizeof (SockSend)) ;

   return aux::forward (tx) ;
}

void Sock::_check_initialized (void)
{
   if (m_initialized)
      return ;

   m_initialized = true ;

   WMI.on_event<SockResponse> (SockHandlerBase::handle_response) ;
   WMI.on_receive (_on_data_received) ;
}

uint32_t Sock::_standard_command_result (uint32_t sock, SockCmdType type)
{
   return SockHandler{ type, sock } . retcode() ;
}

void Sock::_on_data_received (RxStream& rx)
{
   if (!m_receive_callback)
      return ;

   struct RecvHdr
   {
      uint32_t m_sock ;
      SockAddr m_addr ;
      uint16_t m_socklen ;
      uint16_t m_reserved ;
      uint32_t m_reassembly_info ;
   } ;

   auto hdr = rx.read<RecvHdr>() ;

   m_receive_callback ({ hdr.m_sock, hdr.m_addr, rx }) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
