////////////////////////////////////////////////////////////////////////////////////////////
//
// rxstream.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/debug-assert.h"
#include "aux/range.h"
#include "hw/spi.h"

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

class RxStream
{
   friend class Hci ;
   RxStream (uint16_t size) : m_unread{ size } {}

   RxStream (const RxStream&) = delete ;
   RxStream& operator= (const RxStream&) = delete ;

   struct Proxy
   {
      RxStream& m_this ;
      Proxy (RxStream& this_) : m_this{ this_ } {}

      template<typename _Tp>
         operator _Tp (void) const
            { return m_this.read<_Tp>() ; }
   } ;

   struct SkipBase
   {
      const uint16_t m_bytes ;
      SkipBase (uint16_t bytes) : m_bytes{ bytes } {}

      uint16_t bytes (void) const { return m_bytes ; }
   } ;

public:
   template<uint16_t _Bytes>
      struct Skip : SkipBase
      {
         constexpr Skip (void) : SkipBase{ _Bytes } {}
      } ;

public:
   ~RxStream (void) ;

   RxStream& operator>> (const SkipBase& s) { skip (s.bytes()) ; return *this ; }

   template<typename  _Tp>
      RxStream& operator>> (_Tp& p) ;

   template<typename  _It>
      RxStream& operator>> (const aux::Range<_It>& r) ;

   template<typename  _It>
      RxStream& operator>> (aux::Range<_It>& r) ;

   Proxy read (void) { return *this ; }

   template<typename _Tp>
      _Tp read (void) ;

   void skip (uint16_t bytes) ;
   uint16_t unread (void) const { return m_unread ; }

private:
   template<typename  _Range>
      void _read_range (const _Range& r) ;

private:
   uint16_t m_unread ;
} ;


template<typename  _Tp> inline
   RxStream& RxStream::operator>> (_Tp& p)
   {
      aux::debug_assert( m_unread >= sizeof (p), PSTR("Rx Packet Size 2") ) ;
      hw::SPI.receive (hw::Spi::lsb{}, p) ;
      m_unread -= sizeof (p) ;
      return *this ;
   }

template<typename  _It> inline
   RxStream& RxStream::operator>> (const aux::Range<_It>& r)
   {
      _read_range (r) ;
      return *this ;
   }

template<typename  _It> inline
   RxStream& RxStream::operator>> (aux::Range<_It>& r)
   {
      _read_range (r) ;
      return *this ;
   }

template<typename _Tp> inline
   _Tp RxStream::read (void)
   {
      _Tp res ;
      operator>> (res) ;
      return res ;
   }

template<typename  _Range> inline
   void RxStream::_read_range (const _Range& r)
   {
      aux::debug_assert( m_unread >= r.size() * sizeof (typename _Range::value_type), PSTR("Rx Packet Size 3") ) ;
      hw::SPI.receive_range (hw::Spi::lsb{}, r) ;
      m_unread -= r.size() * sizeof (typename _Range::value_type) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
