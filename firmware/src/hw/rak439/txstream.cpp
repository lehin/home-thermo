////////////////////////////////////////////////////////////////////////////////////////////
//
// txstream.cpp
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "txstream.h"

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

TxStream::~TxStream (void)
{
   fill (m_unsent) ;
}

void TxStream::fill (uint16_t size, uint8_t val)
{
   aux::debug_assert( m_unsent >= size, PSTR("Tx Packet Size") ) ;
   m_unsent -= size ;
   for (; size; --size)
      hw::SPI.send (val) ;
}

void TxStream::_generic_write (const void *buf, uint16_t size)
{
   aux::debug_assert( m_unsent >= size, PSTR("Tx Packet Size") ) ;

   m_unsent -= size ;

   auto first = reinterpret_cast<const uint8_t*> (buf) ;
   hw::SPI.send_range (hw::Spi::lsb{}, aux::range (first, first + size)) ;
}

void TxStream::_generic_write_p (const void *buf, uint16_t size)
{
   aux::debug_assert( m_unsent >= size, PSTR("Tx Packet Size") ) ;

   m_unsent -= size ;

   auto first = reinterpret_cast<const uint8_t*> (buf) ;

   for (auto& b : aux::range (first, first + size))
      hw::SPI.send (pgm_read_byte (&b)) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
