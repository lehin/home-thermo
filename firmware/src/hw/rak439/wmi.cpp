////////////////////////////////////////////////////////////////////////////////////////////
//
// wmi.cpp
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "wmi.h"
#include "hci.h"
#include "aux/forward.h"

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

Wmi WMI ;

////////////////////////////////////////////////////////////////////////////////////////////

struct Wmi::CtrlHeader
{
   uint16_t event_id ;
   uint16_t info1 ;
   uint16_t reserved ;
} ;

struct Wmi::DataHeader
{
   int8_t m_rssi ;

   struct
   {
      uint8_t m_msg_type : 2;
      uint8_t m_tid : 3 ;
      uint8_t m_ap_flag : 1 ;
      uint8_t m_hdr_type : 2 ;
   } m_info ;

   struct
   {
      uint16_t m_seqno : 12 ;
      uint16_t m_a_mspdu : 1 ;
      uint16_t m_meta_version : 3 ;
   } m_info2 ;

   uint16_t m_reserved ;

   DataHeader (void) {}

   DataHeader (MsgType msgtype, HdrType hdrtype) :
      m_rssi{ 0 },
      m_info { static_cast<uint8_t> (msgtype), 0, 0, static_cast<uint8_t> (hdrtype) },
      m_info2{ 0, 0, 5 },
      m_reserved{ 0 }
   {
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

TxStream Wmi::send (MsgType msgtype, HdrType hdrtype, uint16_t msgsize)
{
   static_assert( sizeof (DataHeader) == 6, "Wrong WMI data header declaration!" ) ;

   DataHeader hdr{ msgtype, hdrtype } ;

   auto tx = HCI.send_packet (msgsize + sizeof (hdr), 2) ;
   tx.write (hdr) ;

   return aux::forward (tx) ;
}

void Wmi::_check_initialized (void)
{
   if (m_initialized)
      return ;

   m_initialized = true ;
   HCI.set_wmi_packet_handler ([] (RxStream& rx) { WMI._handle_wmi_packet (rx) ; }) ;
   HCI.set_data_packet_handler ([] (RxStream& rx) { WMI._handle_data_packet (rx) ; }) ;
}

void Wmi::_handle_wmi_packet (RxStream& rx)
{
   auto h = rx.read<CtrlHeader>() ;
   EvtHandler::handle (h.event_id, rx) ;
}

void Wmi::_handle_data_packet (RxStream& rx)
{
   auto hdr = rx.read<DataHeader>() ;

   if (hdr.m_info.m_hdr_type || hdr.m_info2.m_a_mspdu || hdr.m_info2.m_meta_version)
      return ;

   if (m_receive_handler)
      m_receive_handler (rx) ;
}

TxStream Wmi::_send_header (uint16_t cmdid, uint16_t cmdsize)
{
   TxStream tx = HCI.send_packet (cmdsize + sizeof (CtrlHeader), 1) ;

   tx << CtrlHeader{ cmdid, 0, 0 } ;

   return aux::forward (tx) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

const Wmi::EvtHandler *Wmi::EvtHandler::s_head ;

bool Wmi::EvtHandler::handle (uint16_t event_id, RxStream& rx)
{
   for (auto h = s_head; h; h = h->m_next)
      if (h->m_event_id == event_id)
      {
         h->handle_event (rx) ;
         return true ;
      }

   return false ;
}

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
