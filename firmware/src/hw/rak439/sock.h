////////////////////////////////////////////////////////////////////////////////////////////
//
// sock.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "wmi.h"

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

enum class SockCmdType : uint8_t
{
   Open, Close, Connect, Bind, Listen, Accept, Select,
   SetSockOpt, GetSockOpt, Errno, IpConfig, Ping, StackInit,
   StackMisc, Ping6, Ip6Config, IpConfigDhcpPool, Ip6ConfigRouterPrefix,
   SetTcpExpBackoffRetry, IpSetIp6Status, IpDhcpRelease
} ;

////////////////////////////////////////////////////////////////////////////////////////////

enum class SockType { Tcp = 1, Udp = 2 } ;

////////////////////////////////////////////////////////////////////////////////////////////

struct SockAddr
{
   uint16_t m_port ;
   uint16_t m_family ;
   uint32_t m_addr ;

   static constexpr uint32_t ANY = 0 ;

   SockAddr (uint32_t addr, uint16_t port) : m_port{ port }, m_family{ 2 }, m_addr{ addr } {}
   SockAddr (void) : m_port{ 0 }, m_family{ 0 }, m_addr{ 0 } {}

   inline friend bool operator< (const SockAddr& lhs, const SockAddr& rhs)
   {
      if (lhs.m_addr != rhs.m_addr)
         return lhs.m_addr < rhs.m_addr ;

      return lhs.m_port < rhs.m_port ;
   }

   inline friend bool operator== (const SockAddr& lhs, const SockAddr& rhs)
   {
      return
         lhs.m_addr == rhs.m_addr &&
         lhs.m_port == rhs.m_port ;
   }

   inline friend bool operator!= (const SockAddr& lhs, const SockAddr& rhs)
      { return !operator== (lhs, rhs) ; }

} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct IpConfigResult
{
   uint32_t m_mode ;
   uint32_t m_ipv4 ;
   uint32_t m_subnet_mask ;
   uint32_t m_gateway4 ;
   uint32_t m_dnsrv1 ;
   uint32_t m_dnsrv2 ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class Sock
{
public:
   typedef void (*DhcpCallback) (bool ok) ;
   typedef void (*SimplCallback) (uint32_t sock) ;

   struct ReceiveData
   {
      uint32_t m_sock ;
      SockAddr& m_addr ;
      RxStream& m_rx ;
   } ;

   typedef void (*ReceiveCallback) (const ReceiveData& r) ;

public:
   static void dhcp_callback (DhcpCallback cb) ;
   static void listen_callback (SimplCallback cb) ;
   static void close_callback (SimplCallback cb) ;
   static void receive_callback (ReceiveCallback cb) ;

   static void init_stack_offload (void) ;
   static void ip_config_dhcp (const char *hostname) ;
   static int32_t ip_config_query (IpConfigResult& res) ;
   static uint32_t open (SockType type) ;
   static int bind (uint32_t sock, uint16_t port) ;
   static int listen (uint32_t sock, int backlog) ;
   static uint32_t accept (uint32_t sock) ;
   static int32_t connect (uint32_t sock, const SockAddr& addr) ;
   static void close (uint32_t sock) ;
   static TxStream send (uint32_t sock, SockType type, uint16_t len) ;
   static TxStream sendto (uint32_t sock, SockType type, uint16_t len, const SockAddr& addr) ;

private:
   static void _check_initialized (void) ;

   static TxStream _send_impl (uint32_t sock, SockType type, uint16_t len, const SockAddr& addr) ;

   template<typename _Command>
      static uint32_t _standard_command (uint32_t sock, const _Command& cmd)
      {
         WMI.command (cmd) ;
         return _standard_command_result (sock, (_Command::CMDTYPE)) ;
      }

   static uint32_t _standard_command_result (uint32_t sock, SockCmdType type) ;

   static void _on_data_received (RxStream& rx) ;

private:
   static bool m_initialized ;
   static ReceiveCallback m_receive_callback ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
