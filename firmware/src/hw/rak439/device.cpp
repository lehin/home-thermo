////////////////////////////////////////////////////////////////////////////////////////////
//
// device.cpp
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "device.h"
#include "hw/gpio.h"
#include "hw/spi.h"
#include "hw/pcint.h"
#include "aux/scheduler.h"
#include "aux/ll-scheduler.h"
#include <util/atomic.h>
#include <util/delay.h>

namespace hw
{
   namespace rak439
   {

////////////////////////////////////////////////////////////////////////////////////////////

Device DEVICE ;

////////////////////////////////////////////////////////////////////////////////////////////

void Device::enable_interrupts (VoidFn interrupt_handler)
{
   m_interrupt_handler = interrupt_handler ;
   hw::PCINT.enable (hw::Pcint::Tag<4,_on_wlirq>{}) ;
}

void Device::disable_interrupts (void)
{
   hw::PCINT.disable (hw::Pcint::Tag<4,_on_wlirq>{}) ;
}

void Device::reset (void)
{
   ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
   {
      hw::gpio::RSTP.reset() ;
      _delay_us (5) ;
      hw::gpio::RSTP.set() ;
   }
}

void Device::_on_wlirq (void)
{
   DEVICE.m_interrupt_handler() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static uint8_t _cs_locks = 0 ;
}

Device::Cs::Cs (void)
{
   ATOMIC_BLOCK (ATOMIC_FORCEON)
      if (!_cs_locks++)
         gpio::WLCS.reset() ;
}

Device::Cs::~Cs (void)
{
   SPI.flush() ;

   ATOMIC_BLOCK (ATOMIC_FORCEON)
      if (!--_cs_locks)
         gpio::WLCS.set() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace rak439
} // namespace hw
