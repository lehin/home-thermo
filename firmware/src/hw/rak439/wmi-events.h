////////////////////////////////////////////////////////////////////////////////////////////
//
// wmi-events.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "wmi.h"

namespace hw
{
   namespace rak439
   {
      namespace evts
      {

////////////////////////////////////////////////////////////////////////////////////////////

struct DisconnectData
{
    uint16_t m_proto_reason ;
    uint8_t m_bssid[6] ;
    uint8_t m_reason ;
    uint8_t m_response_len ;
} ;

struct CmdErrorData
{
   uint16_t m_cmd ;
   uint8_t m_error ;
} ;

struct EasyConfigData
{
   char m_bssid[48] ;
   char m_easydata[128] ;
   uint8_t m_result ;
   uint8_t m_reserved[3] ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

typedef Event<0x1001> Ready ;
typedef Event<0x1002> Connect ;
typedef Event<0x1003,DisconnectData> Disconnect ;
typedef Event<0x1005,CmdErrorData> CmdError ;
typedef Event<0x1006> RegDomain ;
typedef Event<0x101E> WlanVer ;
typedef Event<0x9008,EasyConfigData> EasyConfig ;

////////////////////////////////////////////////////////////////////////////////////////////

      } // namespace evts
   } // namespace rak439
} // namespace hw
