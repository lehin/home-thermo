////////////////////////////////////////////////////////////////////////////////////////////
//
// wmi-commands.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "wmi.h"
#include "aux/minmax.h"

namespace hw
{
   namespace rak439
   {
      namespace cmds
      {

////////////////////////////////////////////////////////////////////////////////////////////

namespace impl
{
   inline
      void parms_string_impl (char *dest, uint8_t dest_size, uint8_t& len, const char *src)
      {
         len = (uint8_t)strnlen (src, dest_size) ;
         memcpy (dest, src, len) ;
      }

   template<typename _Tp>
      struct ArraySize ;

   template<typename _Tp, size_t _Size>
      struct ArraySize<_Tp[_Size]>
      {
         static constexpr size_t size = _Size ;
      } ;

   template<typename _Parm>
      void parms_string (_Parm& dest, uint8_t& len, const char *src)
      {
         parms_string_impl (dest, (ArraySize<_Parm>::size), len, src) ;
      }
}

////////////////////////////////////////////////////////////////////////////////////////////

struct EasyConfigParms
{
   const uint8_t m_enabled = 1 ;
} ;

struct SetPasswordParms
{
   char m_ssid[32] ;
   char m_pass[64] ;
   uint8_t m_ssid_len ;
   uint8_t m_pass_len ;

   SetPasswordParms (const char *ssid, const char *pass)
   {
      impl::parms_string (m_ssid, m_ssid_len, ssid) ;
      impl::parms_string (m_pass, m_pass_len, pass) ;
   }
} ;

struct SetChannelParms
{
   uint16_t m_freq ;

   SetChannelParms (int ch) : m_freq{ 2412 + ((uint16_t)ch - 1) * 5 } {}
} ;

struct ScanParametersParms
{
   uint16_t m_fg_start_period = 0 ;
   uint16_t m_fg_end_period = 4 ;
   uint16_t m_bg_period = 0xFFFF ;
   uint16_t m_maxact_chdwell_time = 60 ;
   uint16_t m_pas_chdwell_time = 0 ;
   uint8_t m_shortScanRatio = 3 ;
   uint8_t m_scanCtrlFlags = 0x2F ;
   uint16_t m_minact_chdwell_time = 0 ;
   uint16_t m_maxact_scan_per_ssid = 0 ;
   uint32_t m_max_dfsch_act_time = 0 ;
} ;

struct ConnectParms
{
   uint8_t m_network_type = 1 ;
   uint8_t m_dot11_auth_mode = 1 ;
   uint8_t m_auth_mode = 0x10 ;
   uint8_t m_pairwise_crypto_type = 8 ;
   uint8_t m_pairwise_crypto_len = 0 ;
   uint8_t m_group_crypto_type = 8 ;
   uint8_t m_group_crypto_len = 0 ;
   uint8_t m_ssid_len = 0 ;
   char m_ssid[32] = { 0 } ;
   uint16_t m_channel = 0 ;
   uint8_t m_bssid[6] = { 0 } ;
   uint32_t m_ctrl_flags = 0x44 ;

   ConnectParms (const char *ssid, const uint8_t *bssid = nullptr, uint8_t channel = 0)
   {
      impl::parms_string (m_ssid, m_ssid_len, ssid) ;

      if (bssid)
         memcpy (m_bssid, bssid, sizeof (m_bssid)) ;

      if (channel)
         m_channel = 2412 + ((uint16_t)channel - 1) * 5 ;
   }
} ;

struct ScanParms
{
   int32_t m_force_fg_scan = 0 ;
   int32_t m_is_legacy = 0 ;
   uint32_t m_home_dwell_time = 0 ;
   uint32_t m_force_scan_interval = 0 ;
   uint8_t m_scan_type = 0 ;
   uint8_t m_num_channels = 11 ;
   uint16_t channelList[11] { 2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447, 2452, 2457, 2462 } ;
} ;

struct ProbeSsidParms
{
   uint8_t m_entry_index = 0 ;
   uint8_t m_flag = 1 ;
   uint8_t m_ssid_len ;
   char m_ssid[32] ;

   ProbeSsidParms (const char *ssid)
   {
      impl::parms_string (m_ssid, m_ssid_len, ssid) ;
   }
} ;

enum class Power : uint8_t { Reduced = 1, Maximal } ;

struct PowerModeParms
{
   Power m_mode ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

typedef Command<0xF040,EasyConfigParms> EasyConfig ;
typedef Command<0xF048,SetPasswordParms> SetPassword ;
typedef Command<0xF042,SetChannelParms> SetChannel ;
typedef Command<0x0008,ScanParametersParms> ScanParameters ;
typedef Command<0x0001,ConnectParms> Connect ;
typedef Command<0x0007,ScanParms> Scan ;
typedef Command<0x000A,ProbeSsidParms> ProbeSsid ;
typedef Command<0x0012,PowerModeParms> PowerMode ;

////////////////////////////////////////////////////////////////////////////////////////////

      } // namespace cmds
   } // namespace rak439
} // namespace hw
