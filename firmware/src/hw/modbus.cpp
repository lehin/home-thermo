////////////////////////////////////////////////////////////////////////////////////////////
//
// // modbus.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#if !defined (SERIALDEBUG)

#include "modbus.h"
#include "aux/scheduler.h"
#include "aux/ll-scheduler.h"
#include "hw/gpio.h"

#include <avr/power.h>
#include <util/crc16.h>

static constexpr uint16_t MODBUS_DELAY_OCR0A = 78 ;

ISR (USART0_RX_vect)
{
   hw::MODBUS._on_receive() ;
}

ISR (USART0_TX_vect)
{
   hw::MODBUS._on_transmit() ;
}

ISR (USART0_UDRE_vect)
{
   hw::MODBUS._on_buffer_empty() ;
}


ISR (TIMER0_COMPA_vect)
{
   hw::MODBUS._on_rec_timeout() ;
}

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

Modbus MODBUS ;

////////////////////////////////////////////////////////////////////////////////////////////

Modbus::Modbus (void) :
   m_flags{ 0, 0, 0, 0 },
   m_statistics{ 0, 0, 0 }
{
   UBRR0 = 42 ; // 57600
   UCSR0A = _BV(U2X0) ;
   UCSR0B = _BV(RXCIE0) | _BV(TXCIE0) ;
   UCSR0C = _BV(UCSZ00) | _BV(UCSZ01) ;

   power_timer2_enable() ;

   TCCR0A = 0 ;
   TCCR0B = _BV(CS02) ; // prescaler of /256 gives 1ms at TNC2 == 78 - modbus packet delimiter at 19200+
}

void Modbus::receive (Sink sink, uint16_t timeout)
{
   ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
   {
      m_sink = sink ;
      m_flags.m_rec_started = false ;

      UCSR0B |= _BV(RXEN0) ;
   }

   if (timeout)
      m_reply_timeout_schedule = aux::SCHED.schedule ([] { MODBUS._on_reply_timeout() ; }, timeout) ;
}

Modbus::Tx Modbus::tx (void)
{
   return Tx{ m_io_buffer } ;
}

Modbus::Statistics Modbus::statistics (void) const
{
   ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
   {
      return m_statistics ;
   }
}

void Modbus::reset_statistics (void)
{
   ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
   {
      m_statistics = { 0, 0, 0 } ;
   }
}

bool Modbus::_send_packet (SendHandler handler)
{
   if (m_io_buffer.empty())
      return false ;

   m_send_handler = handler ;
   m_tx_idx = 0 ;

   ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
   {
      if (!m_flags.m_bus_interval)
         _start_transmit() ;
      else
         m_flags.m_transmit_pending = true ;
   }

   return true ;
}

void Modbus::_start_transmit (void)
{
   gpio::RTS.set() ;
   UCSR0B |= _BV(UDRIE0) | _BV(TXEN0) ;

   _send_byte() ;
}

inline
   bool Modbus::_send_byte (void)
   {
      if (m_tx_idx == m_io_buffer.size())
         return false ;

      UDR0 = m_io_buffer[m_tx_idx++] ;

      return true ;
   }

void Modbus::_on_reply_timeout (void)
{
   ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
   {
      if (m_flags.m_rec_started)
         return ;

      ++m_statistics.m_timeouts ;

      _on_pkt_received (false) ;
   }
}

void Modbus::_on_pkt_received (bool ok)
{
   UCSR0B &= ~_BV(RXEN0) ;

   m_flags.m_rec_ok = ok ;
   m_flags.m_rec_started = false ;

   _start_bus_interval (false) ;

   aux::LLSCHED.schedule_high ([] { MODBUS._complete_receive() ; }, PSTR ("MODBUS PKT REC")) ;
}

void Modbus::_complete_receive (void)
{
   uint8_t addr = 0,
           opcode = 0 ;

   RecBytes recbytes ;

   bool ok = m_flags.m_rec_ok ;

   if (ok && m_io_buffer.size() < 4)
      ok = false ;

   if (ok)
   {
      auto it_crc = m_io_buffer.end() - 2 ;

      uint16_t crc = 0 ;
      for (auto byte : aux::range (m_io_buffer.begin(), it_crc))
         crc = _crc16_update (crc, byte) ;

      ok = (crc == *reinterpret_cast<const uint16_t*> (it_crc)) ;

      if (!ok)
         ++m_statistics.m_crc_errors ;
   }

   if (ok)
   {
      recbytes = { m_io_buffer.begin(), m_io_buffer.end() - 2 } ;
      ++m_statistics.m_success ;
   }

   auto sink = m_sink ;
   m_sink = nullptr ;

   if (sink)
      sink (ok, recbytes) ;
}

void Modbus::_complete_transmit (void)
{
   auto handler = m_send_handler ;
   m_send_handler = nullptr ;

   if (handler)
      handler() ;
}

void Modbus::_start_bus_interval (bool tx)
{
   m_flags.m_bus_interval = true ;

   TCNT0 = 0 ;
   OCR0A = (tx ? 2 : 1) * MODBUS_DELAY_OCR0A ;
   TIFR0 = _BV(OCF0A) ;
   TIMSK0 = _BV(OCIE0A) ;
}

inline __attribute__ ((always_inline))
   void Modbus::_on_receive (void)
   {
      uint8_t byte = UDR0 ;

      if (m_flags.m_transmit_pending)
         return ;

      if (!m_flags.m_rec_started)
         m_io_buffer.clear() ;

      m_io_buffer.push_back (byte) ;

      TCNT0 = 0 ;

      if (m_flags.m_rec_started)
         return ;

      m_flags.m_rec_started = true ;
      m_flags.m_bus_interval = false ;

      OCR0A = MODBUS_DELAY_OCR0A ;
      TIFR0 = _BV(OCF0A) ;
      TIMSK0 = _BV(OCIE0A) ;

      aux::LLSCHED.schedule_high
         (
            []
            {
               aux::SCHED.cancel (MODBUS.m_reply_timeout_schedule) ;
               MODBUS.m_reply_timeout_schedule = 0 ;
            },
            PSTR ("MODBUS REC INT")
         ) ;
   }

inline __attribute__ ((always_inline))
   void Modbus::_on_transmit (void)
   {
      if (!m_flags.m_rec_started)
         _start_bus_interval (true) ;

      gpio::RTS.reset() ;

      aux::LLSCHED.schedule_high ([] { MODBUS._complete_transmit() ; }, PSTR ("MODBUS TRNS")) ;
   }

inline __attribute__ ((always_inline))
   void Modbus::_on_buffer_empty (void)
   {
      while (UCSR0A & _BV(UDRE0))
         if (!_send_byte())
         {
            UCSR0B &= ~(_BV(TXEN0) | _BV(UDRIE0)) ;
            break ;
         }
   }

inline __attribute__ ((always_inline))
   void Modbus::_on_rec_timeout (void)
   {
      TIMSK0 = 0 ;

      if (m_flags.m_bus_interval)
      {
         m_flags.m_bus_interval = false ;

         if (m_flags.m_transmit_pending)
         {
            m_flags.m_transmit_pending = false ;
            _start_transmit() ;
         }
      }
      else if (m_flags.m_rec_started)
         _on_pkt_received (true) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

bool Modbus::Tx::send (SendHandler handler)
{
   uint16_t crc = 0 ;
   for (auto byte : m_buf)
      crc = _crc16_update (crc, byte) ;

   write (crc) ;

   return MODBUS._send_packet (handler) ;
}

////////////////////////////////////////////////////////////////////////////////////////////


} // namespace hw

#endif // SERIALDEBUG
