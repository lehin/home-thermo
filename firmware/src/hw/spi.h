////////////////////////////////////////////////////////////////////////////////////////////
//
// spi.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/range.h"
#include <stdint.h>
#include <stddef.h>
#include <avr/io.h>
#include <util/atomic.h>

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Spi SPI ;

////////////////////////////////////////////////////////////////////////////////////////////

class Spi
{
public:
   struct msb {} ;
   struct lsb {} ;

public:
   Spi (void) ;

   void flush (void) ;

   void send (uint8_t byte) ;

   // send any struct using MSB-first order
   template<typename _Order, typename _Tp>
      void send (_Order, const _Tp& data) ;

   template<typename _Order, typename _Tp, typename... _Args>
      void send_multiple (_Order order, const _Tp& data, const _Args& ... args) ;

   template<typename _Order, typename _Range>
      void send_range (_Order order, const _Range& range) ;

   template<typename _Tp>
      void send_range (lsb, const aux::Range<_Tp*>& range) ;

   uint8_t receive (const uint8_t sending_byte = 0) ;

   template<typename _Order, typename _Tp>
      void receive (_Order, _Tp& data, const uint8_t sending_byte = 0) ;

   template<typename _Order, typename _Range>
      void receive_range (_Order order, const _Range& range, const uint8_t sending_byte = 0) ;

   template<typename _Tp>
      void receive_range (lsb, const aux::Range<_Tp*>& range, const uint8_t sending_byte = 0) ;

private:
   template<typename _Order>
      void send_multiple (_Order) {}

   void _send_bytes_generic (lsb, const uint8_t *p, size_t size) ;
   void _send_bytes_generic (msb, const uint8_t *p, size_t size) ;

   void _receive_bytes_generic (lsb, uint8_t *p, size_t size, uint8_t sending_byte) ;
   void _receive_bytes_generic (msb, uint8_t *p, size_t size, uint8_t sending_byte) ;

   void _send_bytes_2 (lsb, const uint16_t& p) ;
   void _send_bytes_2 (msb, const uint16_t& p) ;
   uint16_t _receive_bytes_2 (lsb, const uint8_t sending_byte) ;
   uint16_t _receive_bytes_2 (msb, const uint8_t sending_byte) ;

   void _receive_prepare (void) ;
   uint8_t _receive_byte (const uint8_t sending_byte) ;

   template<typename _Order, typename _Tp>
      void _receive_raw (_Order, _Tp& data, const uint8_t sending_byte) ;

   template<typename _Tp, size_t _Size>
      struct _GenericSender ;

   template<typename _Tp>
      struct _GenericSender<_Tp,1> ;

   template<typename _Tp>
      struct _GenericSender<_Tp,2> ;

   template<typename _Tp, size_t _Size>
      struct _GenericReceiver ;

   template<typename _Tp>
      struct _GenericReceiver<_Tp,1> ;

   template<typename _Tp>
      struct _GenericReceiver<_Tp,2> ;
} ;


template<typename _Tp, size_t _Size>
   struct Spi::_GenericSender
   {
      template<typename _Order> __attribute__ ((always_inline))
         void operator() (_Order order, const _Tp& p) const
            { _send_bytes_generic (order, (const uint8_t*)&p, _Size) ; }
   } ;

template<typename _Tp>
   struct Spi::_GenericSender<_Tp,1>
   {
      __attribute__ ((always_inline))
         void operator() (lsb, const _Tp& p) const { SPI.send (reinterpret_cast<const uint8_t&> (p)) ; }

      __attribute__ ((always_inline))
         void operator() (msb, const _Tp& p) const { SPI.send (reinterpret_cast<const uint8_t&> (p)) ; }
   } ;

template<typename _Tp>
   struct Spi::_GenericSender<_Tp,2>
   {
      template<typename _Order> __attribute__ ((always_inline))
         void operator() (_Order order, const _Tp& p) const
            { SPI._send_bytes_2 (order, reinterpret_cast<const uint16_t&> (p)) ; }
   } ;

template<typename _Tp, size_t _Size>
   struct Spi::_GenericReceiver
   {
      template<typename _Order> __attribute__ ((always_inline))
         void operator() (_Order order, _Tp& p, const uint8_t sending_byte) const
          { SPI._receive_bytes_generic (order, (uint8_t*)&p, _Size, sending_byte) ; }
   } ;

template<typename _Tp>
   struct Spi::_GenericReceiver<_Tp,1>
   {
      __attribute__ ((always_inline))
         void operator() (lsb, _Tp& p, const uint8_t sending_byte) const
            { reinterpret_cast<uint8_t&> (p) = SPI.receive (sending_byte) ; }

      __attribute__ ((always_inline))
         void operator() (msb, _Tp& p, const uint8_t sending_byte) const
            { reinterpret_cast<uint8_t&> (p) = SPI.receive (sending_byte) ; }
   } ;

template<typename _Tp>
   struct Spi::_GenericReceiver<_Tp,2>
   {
      template<typename _Order> __attribute__ ((always_inline))
         void operator() (_Order order, _Tp& p, const uint8_t sending_byte) const
            { reinterpret_cast<uint16_t&> (p) = SPI._receive_bytes_2 (order, sending_byte) ; }
   } ;

inline
   void Spi::flush (void)
   {
      loop_until_bit_is_set (UCSR1A, TXC1) ;
   }

inline
   void Spi::send (uint8_t byte)
   {
      loop_until_bit_is_set (UCSR1A, UDRE1) ;

      ATOMIC_BLOCK (ATOMIC_FORCEON)
      {
         UDR1 = byte ;
         UCSR1A = _BV (TXC1) ;
      }
   }

template<typename _Order, typename _Tp> __attribute__ ((always_inline))
   inline void Spi::send (_Order, const _Tp& data)
   {
      _GenericSender<_Tp,sizeof(_Tp)>{} (_Order{}, data) ;
   }

template<typename _Order, typename _Tp, typename... _Args>
   inline void Spi::send_multiple (_Order order, const _Tp& data, const _Args& ... args)
   {
      send (order, data) ;
      send_multiple (order, args...) ;
   }

template<typename _Order, typename _Range>
   inline void Spi::send_range (_Order order, const _Range& range)
   {
      for (auto& val : range)
         send (order, val) ;
   }

template<typename _Tp>
   inline void Spi::send_range (lsb, const aux::Range<_Tp*>& range)
   {
      _send_bytes_generic (lsb{}, range.begin(), range.size() * sizeof (_Tp));
   }

inline
   uint8_t Spi::receive (uint8_t sending_byte)
   {
      _receive_prepare() ;
      return _receive_byte (sending_byte) ;
   }

template<typename _Order, typename _Tp> __attribute__ ((always_inline))
   inline void Spi::receive (_Order, _Tp& data, const uint8_t sending_byte)
   {
      _receive_prepare() ;
      _GenericReceiver<_Tp,sizeof(_Tp)>{} (_Order{}, data, sending_byte) ;
   }

template<typename _Order, typename _Range>
   inline void Spi::receive_range (_Order order, const _Range& range, const uint8_t sending_byte)
   {
      _receive_prepare() ;
      for (auto& val : range)
         _receive_raw (order, val, sending_byte) ;
   }

template<typename _Tp>
   inline void Spi::receive_range (lsb, const aux::Range<_Tp*>& range, const uint8_t sending_byte)
   {
      _receive_prepare() ;
      _receive_bytes_generic (lsb{}, (uint8_t*)range.begin(), range.size() * sizeof (_Tp), sending_byte) ;
   }

inline
   void Spi::_send_bytes_2 (lsb, const uint16_t& p)
   {
      send (p) ;
      send (p >> 8) ;
   }

inline
   void Spi::_send_bytes_2 (msb, const uint16_t& p)
   {
      send (p >> 8) ;
      send (p) ;
   }

inline
   uint16_t Spi::_receive_bytes_2 (lsb, const uint8_t sending_byte)
   {
      return _receive_byte (sending_byte) | ((uint16_t)_receive_byte (sending_byte) << 8) ;
   }

inline
   uint16_t Spi::_receive_bytes_2 (msb, const uint8_t sending_byte)
   {
      return ((uint16_t)_receive_byte (sending_byte) << 8) | _receive_byte (sending_byte) ;
   }

inline
   void Spi::_receive_prepare (void)
   {
      flush() ;

      while ((UCSR1A & _BV(RXC1)))
         UDR1 ;
   }

inline
   uint8_t Spi::_receive_byte (const uint8_t sending_byte)
   {
      UDR1 = sending_byte ;
      loop_until_bit_is_set (UCSR1A, RXC1) ;
      return UDR1 ;
   }

template<typename _Order, typename _Tp>
   void Spi::_receive_raw (_Order, _Tp& data, const uint8_t sending_byte)
   {
      _GenericReceiver<_Tp,sizeof(_Tp)>{} (_Order{}, data, sending_byte) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
