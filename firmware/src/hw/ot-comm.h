////////////////////////////////////////////////////////////////////////////////////////////
//
// ot-comm.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <avr/interrupt.h>
#include "aux/function.h"
#include "aux/list.h"
#include "aux/signal.h"

ISR (TIMER2_COMPA_vect) ;
ISR (TIMER2_COMPB_vect) ;

ISR (TIMER1_CAPT_vect) ;
ISR (TIMER1_COMPB_vect) ;

ISR (TIMER3_COMPA_vect) ;

namespace hw
{
   namespace otcomm
   {

////////////////////////////////////////////////////////////////////////////////////////////

extern class Transmitter TRANSMITTER ;
extern class Receiver RECEIVER ;
extern class Link LINK ;

////////////////////////////////////////////////////////////////////////////////////////////

static constexpr uint8_t BUFFER_SIZE = 4 ;

////////////////////////////////////////////////////////////////////////////////////////////

class Transmitter
{
public:
   Transmitter (void) ;

   void transmit (const uint8_t *bytes) ;

private:
   void _stop (void) ;
   void _load_latch (void) ;

   bool _on_capture (uint16_t interval, uint16_t tm, bool positive) ;
   void _first_bit (void) ;

   friend void ::TIMER2_COMPA_vect (void) ;
   friend void ::TIMER2_COMPB_vect (void) ;

   void _on_compa_timer (void) ;
   void _on_compb_timer (void) ;

private:
   uint8_t m_bytes[BUFFER_SIZE] ;

   uint8_t m_latch ;
   uint8_t m_mask ;
   int8_t m_cur ;

   bool m_stop ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class Receiver
{
public:
   Receiver (void) ;

   void enable (void) ;
   void disable (void) ;

   const uint8_t* bytes (void) const { return m_bytes ; }

   typedef void (*OnReceive) (void) ;
   void on_receive (OnReceive fn) { m_on_receive = fn ; }

private:
   enum class EdgeKind : uint8_t { Skip, Resync, Invalid, Final } ;
   EdgeKind _on_capture (uint16_t interval, uint16_t tm, bool positive) ;

   void _first_bit (void) ;
   void _stop (void) ;

   friend void ::TIMER1_CAPT_vect (void) ;
   friend void ::TIMER1_COMPB_vect (void) ;

   void _on_icp (void) ;
   void _on_timeout (void) ;

private:
   uint16_t m_icp_t0 ;

   bool m_idle, m_half_edge ;

   uint8_t m_bytes[BUFFER_SIZE] ;
   uint8_t m_cur ;
   uint8_t m_mask ;

   OnReceive m_on_receive ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class Link
{
   enum class State : uint8_t{ WaitReply, Pause } ;

public:
   struct Message
   {
      uint8_t spare : 4,
              type: 3,
              parity : 1 ;
      uint8_t id ;
      uint8_t vals[2] ;
   } ;

   static_assert (sizeof (Message) == otcomm::BUFFER_SIZE, "OtComm message size is wrong!") ;

public:
   typedef aux::function<void(const Message&)> OnReply ;

   void initialize (OnReply&& onstatus = {}) ;
   void send (Message m, OnReply&& onreply = {}) ;
   void mode (uint8_t mode) { m_status.vals[0] = mode ; }

   typedef aux::Signal<void (bool present)> SigPresent ;
   SigPresent& signal_present (void) { return m_signal_present ; }
   bool is_present (void) const { return m_commstat.m_present ; }

   uint16_t retrains_counter (void) { return m_retrains_counter ; }
   void reset_retrains_counter (void) { m_retrains_counter = 0 ; }

private:
   void _do_send (void) ;
   void _handle_received (void) ;
   void _on_receive (void) ;

   void _set_timer (uint16_t tm) ;
   void _ping (void) ;
   void _check_pendings (void) ;
   void _set_present (bool present) ;

   friend void ::TIMER3_COMPA_vect (void) ;
   void _on_timer (void) ;

private:
   Message m_status = { 0, 0, 0, 0, { 0, 0 } } ;
   OnReply m_onstatus ;

   struct Request
   {
      Message m_msg ;
      OnReply m_onreply ;
   } ;

   typedef aux::List<Request> Pendings ;
   Pendings m_pendings ;

   State m_state ;
   uint16_t m_t0 ;
   uint8_t m_transaction_id = 0 ;

   uint16_t m_retrains_counter = 0 ;

   struct CommStat
   {
      bool m_present : 1,
           m_error_pending : 1 ;

      uint8_t m_not_present_delay : 6 ;
   } m_commstat = { false, false, 0 } ;

   SigPresent m_signal_present ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace otcomm
} // namespace hw
