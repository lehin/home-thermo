////////////////////////////////////////////////////////////////////////////////////////////
//
// pcint.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/array.h"
#include <avr/interrupt.h>

ISR (PCINT2_vect) ;

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Pcint PCINT ;

class Pcint
{
public:
   Pcint (void) ;

   typedef void (*Handler) (void) ;

   template<uint8_t _num, Handler _handler>
      class Tag
      {
         static constexpr uint8_t MASK = 1 << _num ;

      public:
         uint8_t index (void) const
         {
            if (s_idx == 0xff)
               s_idx = PCINT._register_handler (MASK, _handler) ;

            return s_idx ;
         }

      private:
         static uint8_t s_idx ;
      } ;

   template<typename _Tag>
      void enable (_Tag tag) { _enable (tag.index()) ; }

   template<typename _Tag>
      void disable (_Tag tag) { _disable (tag.index()) ; }

private:
   uint8_t _register_handler (uint8_t mask, Handler fn) ;
   void _enable (uint8_t idx) ;
   void _disable (uint8_t idx) ;

   friend void ::PCINT2_vect (void) ;
   void _on_interrupt (void) ;

private:
   struct Item
   {
      uint8_t m_mask ;
      Handler m_fn ;
   } ;

   typedef aux::Array<Item,8> Items ;
   Items m_items ;

   uint8_t m_current ;
   uint8_t m_mask ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<uint8_t _num, Pcint::Handler _handler>
      uint8_t Pcint::Tag<_num,_handler>::s_idx = 0xff ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
