////////////////////////////////////////////////////////////////////////////////////////////
//
// spi.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "spi.h"

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

Spi SPI ;

////////////////////////////////////////////////////////////////////////////////////////////

Spi::Spi (void)
{
   UCSR1C = _BV(UMSEL11) | _BV(UMSEL10) | 3 /* SPI mode */ ;
   UCSR1B = _BV(RXEN1) | _BV(TXEN1) ;

   send ((uint8_t)0) ; // without this the first flush() hangs
}

void Spi::_send_bytes_generic (lsb, const uint8_t *p, size_t size)
{
   for (size_t i = 0; i < size; ++i)
      send (p[i]) ;
}

void Spi::_send_bytes_generic (msb, const uint8_t *p, size_t size)
{
   for (int i = size - 1; i >= 0; --i)
      send (p[i]) ;
}

void Spi::_receive_bytes_generic (lsb, uint8_t *p, size_t size, uint8_t sending_byte)
{
   for (size_t i = 0; i < size; ++i)
      p[i] = _receive_byte (sending_byte) ;
}

void Spi::_receive_bytes_generic (msb, uint8_t *p, size_t size, uint8_t sending_byte)
{
   for (int i = size - 1; i >= 0; --i)
      p[i] = _receive_byte (sending_byte) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
