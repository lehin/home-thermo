////////////////////////////////////////////////////////////////////////////////////////////
//
// led.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "led.h"
#include "aux/scheduler.h"

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

Led LED ;

////////////////////////////////////////////////////////////////////////////////////////////

void Led::blink (uint16_t on, uint16_t off)
{
   _cancel() ;

   m_on_time = on ;
   m_off_time = off ;

   _switch() ;
}

void Led::_cancel (void)
{
   if (!m_schedule)
      return ;

   aux::SCHED.cancel (m_schedule) ;
   m_schedule = m_on_time = m_off_time = 0 ;
}

void Led::_switch (void)
{
   if (!m_on_time || !m_off_time)
      return ;

   gpio::LED.turn() ;

   m_schedule = aux::SCHED.schedule ([] { LED._switch() ; }, gpio::LED.stat() ? m_on_time : m_off_time) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
