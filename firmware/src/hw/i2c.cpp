////////////////////////////////////////////////////////////////////////////////////////////
//
// i2c.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "i2c.h"
#include <avr/io.h>
#include <avr/power.h>
#include "aux/ll-scheduler.h"

ISR (TWI_vect)
{
   hw::I2C._on_twi_event() ;
}

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static constexpr uint8_t _ENABLE = _BV(TWEN) | _BV(TWIE) ;

   static constexpr uint8_t START =    _ENABLE | _BV(TWINT) | _BV(TWSTA) ;
   static constexpr uint8_t STOP =     _ENABLE | _BV(TWINT) | _BV(TWSTO) ;
   static constexpr uint8_t MORE =     _ENABLE | _BV(TWINT) ;

   static constexpr uint8_t ACK = _BV(TWEA) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

I2c I2C ;

////////////////////////////////////////////////////////////////////////////////////////////

I2c::I2c (void) :
   m_address{ 0 }
{
   power_twi_enable() ;

   TWBR = 17 ; // 400KHz at 20MHz clocks
//   TWBR = 92 ; // 100KHz at 20MHz clocks
   TWSR = 0 ; // prescaler == 1 (100KHz at 20MHz clocks)
   TWCR = _ENABLE ;
}

I2c::Tx I2c::tx (uint8_t addr, SendHandler&& handler)
{
   m_address = addr << 1 ; // SLA+W
   m_io_buffer.clear() ;
   m_send_handler = aux::forward (handler) ;

   return m_io_buffer ;
}

void I2c::rx (uint8_t addr, uint8_t sz, RecHandler&& handler)
{
   m_address = (addr << 1) | 1 ; // SLA+R
   m_io_buffer.resize (sz) ;
   m_rec_handler = aux::forward (handler) ;

   TWCR = START ;
}

void I2c::_send_buffer (void)
{
   TWCR = START ;
}

void I2c::_complete_request (uint8_t errno)
{
   TWCR = STOP ;

   m_errno = errno ;

   static bool scheduled ;

   if (scheduled)
      return ;

   scheduled = true ;

   aux::LLSCHED.schedule_high
      (
         []
         {
            scheduled = false ;

            if (I2C.m_address & 1)
               I2C._on_receive_completed() ;
            else
               I2C._on_send_completed() ;
         },
         PSTR ("I2C HW REQ")
      ) ;
}

void I2c::_on_receive_completed (void)
{
   if (!m_rec_handler)
      return ;

   auto h = m_rec_handler ;
   m_rec_handler = {} ;

   Rx rx { m_io_buffer } ;
   h (m_errno, rx) ;
}

void I2c::_on_send_completed (void)
{
   if (!m_send_handler)
      return ;

   auto h = m_send_handler ;
   m_send_handler = {} ;

   h (m_errno) ;
}

inline __attribute__ ((always_inline))
   void I2c::_on_twi_event (void)
   {
      switch (TWSR & ~3)
      {
         case 0x38: // arbitration lost
            TWCR = START ;
            break ;

         case 0x08: // START issued
            TWDR = m_address ;
            TWCR = MORE ;
            break ;

         case 0x18: // SLA+W ACK
            m_retries = 0 ;
            m_io_pos = 0 ;

         case 0x28: // DATA+W ACK
            if (m_io_pos >= m_io_buffer.size())
               _complete_request (0) ;
            else
            {
               TWDR = m_io_buffer[m_io_pos++] ;
               TWCR = MORE ;
            }
            break ;

         case 0x40: // SLA+R ACK
            m_retries = 0 ;
            m_io_pos = 0 ;
            TWCR = MORE | (m_io_pos + 1 < m_io_buffer.size() ? ACK : 0) ;
            break ;

         case 0x50: // DATA+R ACK
            m_io_buffer[m_io_pos++] = TWDR ;
            TWCR = MORE | (m_io_pos < m_io_buffer.size() ? ACK : 0) ;
            break ;

         case 0x58: // DATA+R NACK
            m_io_buffer[m_io_pos] = TWDR ;
            _complete_request (0) ;
            break ;

         case 0x20: // SLA+W NACK
         case 0x48: // SLA+R NACK
            if (++m_retries)
            {
               TWCR = START ;
               break ;
            }

         case 0x30: // DATA+W NACK
         default:
            _complete_request (TWSR & ~3) ;
            break ;
      }
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
