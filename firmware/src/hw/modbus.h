////////////////////////////////////////////////////////////////////////////////////////////
//
// modbus.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#if !defined (SERIALDEBUG)

#include "aux/array.h"
#include "aux/range.h"
#include "aux/list.h"
#include "aux/debug-assert.h"
#include <avr/io.h>
#include <avr/interrupt.h>

ISR (USART0_RX_vect) ;
ISR (USART0_TX_vect) ;
ISR (USART0_UDRE_vect) ;
ISR (TIMER0_COMPA_vect) ;

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class Modbus MODBUS ;

////////////////////////////////////////////////////////////////////////////////////////////

class Modbus
{
public:
   static constexpr uint8_t BUFSIZES = 64 ;
   typedef aux::Array<uint8_t,BUFSIZES> IoBuffer ;

   typedef void (*SendHandler) (void) ;

   class Tx
   {
      friend class Modbus ;
      Tx (IoBuffer& buf) : m_buf (buf) { m_buf.clear() ; }

   public:
      template<typename  _Tp>
         Tx& operator<< (const _Tp& p)
         {
            aux::debug_assert( sizeof (_Tp) <= m_buf.capacity() - m_buf.size(), PSTR("MODBUS TX buf") ) ;

            reinterpret_cast<_Tp&> (*m_buf.end()) = p ;
            m_buf.resize (m_buf.size() + sizeof (_Tp)) ;

            return *this ;
         }

      template<typename  _It>
         Tx& operator<< (const aux::Range<_It>& r)
         {
            for (auto& item : r)
               operator<< (item) ;

            return *this ;
         }

      template<typename _Tp, typename ... _Args>
         void write (_Tp& p, _Args& ... tail)
         {
            operator<< (p) ;
            write (tail...) ;
         }

      void write (void) {}

      bool send (SendHandler handler = {}) ;

   private:
      IoBuffer& m_buf ;
   } ;

   friend class Tx ;

   struct Statistics
   {
      uint16_t m_success ;
      uint16_t m_timeouts ;
      uint16_t m_crc_errors ;
   } ;

public:
   Modbus (void) ;

   typedef aux::Range<const uint8_t*> RecBytes ;
   typedef void (*Sink) (bool ok, const RecBytes& data) ;

   void receive (Sink sink, uint16_t timeout) ;
   Tx tx (void) ;

   Statistics statistics (void) const ;
   void reset_statistics (void) ;

private:
   bool _send_packet (SendHandler handler) ;
   void _start_transmit (void) ;
   bool _send_byte (void) ;
   void _on_reply_timeout (void) ;
   void _on_pkt_received (bool ok) ;
   void _complete_receive (void) ;
   void _complete_transmit (void) ;
   void _start_bus_interval (bool tx) ;

   friend void ::USART0_RX_vect (void) ;
   friend void ::USART0_TX_vect (void) ;
   friend void ::USART0_UDRE_vect (void) ;
   friend void ::TIMER0_COMPA_vect (void) ;

   void _on_receive (void) ;
   void _on_transmit (void) ;
   void _on_buffer_empty (void) ;
   void _on_rec_timeout (void) ;

private:
   IoBuffer m_io_buffer ;
   uint8_t m_tx_idx ;
   uint16_t m_reply_timeout_schedule ;
   Sink m_sink ;
   SendHandler m_send_handler ;

   struct Flags
   {
      bool m_rec_started : 1,
           m_rec_ok : 1,
           m_bus_interval : 1,
           m_transmit_pending : 1 ;
   } m_flags ;

   Statistics m_statistics ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw

#endif // SERIALDEBUG
