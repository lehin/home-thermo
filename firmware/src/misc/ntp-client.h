////////////////////////////////////////////////////////////////////////////////////////////
//
// ntp-client.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "net-query-helper.h"
#include "lan/lan-socket.h"
#include "util/datetime.h"
#include <stdint.h>

namespace misc
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class NtpClient NTPCLIENT ;

////////////////////////////////////////////////////////////////////////////////////////////

class NtpClient
{
public:
   using StatusBase = NetQueryHelper::Status ;

   struct Stat : StatusBase
   {
      Stat (const StatusBase& base, int32_t offset) :
         StatusBase{ base }, m_offset{ offset } {}

      int32_t m_offset ;
   } ;

public:
   void setup (const aux::StringRef& server, int32_t offset, const util::Time& period) ;
   void initialize (void) { _query_helper() ; }
   bool query_now (void) { return _query_helper().do_now() ; }
   Stat status (void) { return { _query_helper().status(), m_last_offset } ; }
   util::DateTime next (void) { return _query_helper().next() ; }

private:
   void _query_send (lan::Socket& sock, const lan::Address& resolved) ;
   bool _query_receive (const hw::rak439::Sock::ReceiveData& rec) ;

   NetQueryHelper& _query_helper (void) ;

private:
   int32_t m_last_offset ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace misc
