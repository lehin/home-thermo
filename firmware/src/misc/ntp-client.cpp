////////////////////////////////////////////////////////////////////////////////////////////
//
// ntp-client.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "ntp-client.h"
#include "aux/scheduler.h"
#include "aux/ll-scheduler.h"
#include "peripherals/rtc.h"
#include "util/eeprom.h"
#include "hw/rak439/rxstream.h"

static constexpr uint16_t NTP_PORT = 123 ;

namespace misc
{

////////////////////////////////////////////////////////////////////////////////////////////

NtpClient NTPCLIENT ;

////////////////////////////////////////////////////////////////////////////////////////////

void NtpClient::setup (const aux::StringRef& server, int32_t offset, const util::Time& period)
{
   util::Eeprom::NtpParms prms = util::EEPROM.ntp_parms() ;
   prms.ntp_server = server ;
   prms.ntp_offset = offset ;
   prms.sync_period = period ;
   util::EEPROM.ntp_parms() = prms ;

   _query_helper().address (server, NTP_PORT) ;
   _query_helper().period (period) ;
}

namespace
{
   struct Packet //See RFC 4330 for Simple NTP
   {
      //WARN: We are in LE! Network is BE!
      //LSb first
      unsigned mode : 3 ;
      unsigned vn : 3 ;
      unsigned li : 2 ;

      uint8_t stratum ;
      uint8_t poll ;
      uint8_t precision ;

      //32 bits header

      uint32_t rootDelay ;
      uint32_t rootDispersion ;
      uint32_t refId ;

      uint32_t refTm_s ;
      uint32_t refTm_f ;
      uint32_t origTm_s ;
      uint32_t origTm_f ;
      uint32_t rxTm_s ;
      uint32_t rxTm_f ;
      uint32_t txTm_s ;
      uint32_t txTm_f ;
   } __attribute__ ((packed)) ;

   static constexpr util::DateTime _ntp_begin (void)
      {  return { { 0, 0, 0 }, { 1, 1, 1, 1900 } } ; }
}

void NtpClient::_query_send (lan::Socket& sock, const lan::Address& resolved)
{
   util::Eeprom::NtpParms prms = util::EEPROM.ntp_parms() ;

   Packet pkt { 0 } ;
   pkt.vn = 4 ; // Version Number : 4
   pkt.mode = 3 ; // Client mode
   pkt.txTm_s = htonl (peripherals::RTC.date_time() - _ntp_begin() - prms.ntp_offset) ;

   sock.sendto (&pkt, sizeof (pkt), resolved) ;
}

bool NtpClient::_query_receive (const hw::rak439::Sock::ReceiveData& rec)
{
   if (rec.m_rx.unread() < sizeof (Packet))
      return false ;

   Packet pkt = rec.m_rx.read() ;

   //Correct Endianness
   pkt.refTm_s = ntohl (pkt.refTm_s) ;
   pkt.refTm_f = ntohl (pkt.refTm_f) ;
   pkt.origTm_s = ntohl (pkt.origTm_s) ;
   pkt.origTm_f = ntohl (pkt.origTm_f) ;
   pkt.rxTm_s = ntohl (pkt.rxTm_s) ;
   pkt.rxTm_f = ntohl (pkt.rxTm_f) ;
   pkt.txTm_s = ntohl (pkt.txTm_s) ;
   pkt.txTm_f = ntohl (pkt.txTm_f) ;

   //Compute offset, see RFC 4330 p.13

   util::Eeprom::NtpParms prms = util::EEPROM.ntp_parms() ;

   auto now = peripherals::RTC.date_time() ;
   int32_t destTm_s = now - _ntp_begin() - prms.ntp_offset ;
   m_last_offset = ( (int64_t)( pkt.rxTm_s - pkt.origTm_s ) + (int64_t) ( pkt.txTm_s - destTm_s ) ) / 2 ;

   if (m_last_offset)
   {
      int32_t date_offset = m_last_offset / util::Time::day_seconds(),
              time_offset = m_last_offset % util::Time::day_seconds() ;

      auto secs = now.m_tm.total_seconds() + time_offset ;
      auto days = now.m_dt.total_days() + date_offset ;

      if (secs < 0)
      {
         secs += util::Time::day_seconds() ;
         --days ;
      }
      else if (secs >= util::Time::day_seconds())
      {
         secs -= util::Time::day_seconds() ;
         ++days ;
      }

      //Set time accordingly
      peripherals::RTC.time (util::Time::from_seconds (secs)) ;
      peripherals::RTC.date (util::Date::from_days (days)) ;
   }

   return true ;
}

namespace
{
   static inline aux::String _ntp_address (void)
   {
      util::Eeprom::NtpParms prms = util::EEPROM.ntp_parms() ;
      return prms.ntp_server ;
   }

   static inline util::Time _sync_period (void)
   {
      util::Eeprom::NtpParms prms = util::EEPROM.ntp_parms() ;
      return prms.sync_period ;
   }
}

NetQueryHelper& NtpClient::_query_helper (void)
{
   static NetQueryHelper s_helper
   {
      _sync_period(),
      lan::Socket::Type::Udp, _ntp_address(), 123,
      [] (auto& sock, auto resolved) { NTPCLIENT._query_send (sock, resolved) ; },
      [] (auto& rec) { return NTPCLIENT._query_receive (rec) ; }
   } ;

   return s_helper ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace misc
