////////////////////////////////////////////////////////////////////////////////////////////
//
// rtc-scheduler.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "util/datetime.h"
#include "aux/list.h"
#include "aux/function.h"

namespace misc
{

////////////////////////////////////////////////////////////////////////////////////////////

extern class RtcScheduler RTCSCHED ;

////////////////////////////////////////////////////////////////////////////////////////////

class RtcScheduler
{
   using Time = util::Time ;
   using Date = util::Date ;
   using DateTime = util::DateTime ;

public:
   void initialize (void) ;

   typedef aux::function <void(void)> Task ;

   uint32_t schedule (Task&& task, const Time& interval) ;
   uint32_t schedule (Task&& task, const DateTime& tm) ;

   void cancel (uint32_t sched) ;

   DateTime next (uint32_t sched) ;

private:
   void _rtc_tick (void) ;

private:
   struct Item
   {
      uint32_t m_id ;
      DateTime m_tm ;
      Task m_task ;
   } ;

   typedef aux::List<Item> Schedule ;
   Schedule m_schedule ;
   uint32_t m_sched_id = 0 ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace misc
