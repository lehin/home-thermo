////////////////////////////////////////////////////////////////////////////////////////////
//
// net-query-helper.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "net-query-helper.h"
#include "misc/rtc-scheduler.h"
#include "peripherals/rtc.h"
#include "aux/scheduler.h"
#include "aux/ll-scheduler.h"
#include "lan/lan-service.h"
#include "lan/dn-service.h"

namespace misc
{

////////////////////////////////////////////////////////////////////////////////////////////

NetQueryHelper::Completions NetQueryHelper::s_completions ;

NetQueryHelper::NetQueryHelper (const util::Time& period, lan::Socket::Type socktype, const aux::String& addr, uint16_t port, QueryFn queryfn, ReceiveFn recfn) :
   m_sock_type{ socktype },
   m_queryfn{ queryfn },
   m_recfn{ recfn },
   m_network_ready_delay (_network_ready_delay()),
   m_addr{ addr },
   m_port{ port },
   m_period (period),
   m_schedule{ 0 },
   m_timeout{ 0 },
   m_dns_query{ 0 },
   m_sock{ nullptr }
{
   lan::SERVICE.signal_network_state().connect ([this] (auto s) { _on_network_state (s) ; }) ;

   if (lan::SERVICE.is_connected())
      _on_network_state (lan::WmiState::Connected) ;
}

NetQueryHelper::~NetQueryHelper (void)
{
   misc::RTCSCHED.cancel (m_schedule) ;
   _complete (true) ;
}

bool NetQueryHelper::do_now (void)
{
   if (!lan::SERVICE.is_connected())
      return false ;

   misc::RTCSCHED.cancel (m_schedule) ;
   _do_query() ;

   return true ;
}

util::DateTime NetQueryHelper::next (void) const
{
   if (!m_schedule)
      return {} ;

   return misc::RTCSCHED.next (m_schedule) ;
}

void NetQueryHelper::_on_network_state (lan::WmiState state)
{
   if (state != lan::WmiState::Connected)
      _complete (true) ;
   else if (!m_schedule)
      m_schedule =
         misc::RTCSCHED.schedule
            (
               [this]{ _do_query() ; },
               util::Time{ m_network_ready_delay, 0, 0 }
            ) ;
}

void NetQueryHelper::_do_query (void)
{
   m_schedule = 0 ;

   if (!lan::SERVICE.is_connected() || m_addr.empty() || !m_port)
      return ;

   m_status.m_result = Status::Result::Pending ;
   m_status.m_tm = peripherals::RTC.date_time() ;

   m_timeout = aux::SCHED.schedule ([this] { _complete() ; }, 3000) ;
   m_dns_query = lan::DNSERVICE.request (m_addr, [this] (uint16_t id, uint32_t ip) { this->_query_dns_resolved (id, ip) ; }) ;
}

void NetQueryHelper::_query_dns_resolved (uint16_t queryid, uint32_t ip)
{
   if (queryid != m_dns_query)
      return ;

   m_dns_query = 0 ;

   if (!ip)
   {
      _complete() ;
      return ;
   }

   lan::Address resolved{ ip, m_port } ;

   m_sock = new lan::Socket{ m_sock_type } ;
   m_sock->connect (resolved) ;

   m_sock->on_close ([this] { _on_closed() ; }) ;
   m_sock->on_receive ([this] (const auto& rcv) { this->_on_receive (rcv) ; }) ;

   m_queryfn (*m_sock, resolved) ;
}

void NetQueryHelper::_complete (bool cancelled)
{
   bool first = s_completions.empty() ;

   if (!cancelled)
      m_schedule = misc::RTCSCHED.schedule ([this]{ _do_query() ; }, m_period) ;

   s_completions.push_back (this) ;

   if (first)
      aux::LLSCHED.schedule
         (
            []
            {
               for (auto this_ : s_completions)
                  this_->_complete_impl() ;

               s_completions.clear() ;
            },
            PSTR ("MQTT COMPLETE")
         ) ;
}

void NetQueryHelper::_complete_impl (void)
{
   if (m_timeout)
   {
      aux::SCHED.cancel (m_timeout) ;
      m_timeout = 0 ;
   }

   if (m_dns_query)
   {
      lan::DNSERVICE.cancel (m_dns_query) ;
      m_dns_query = 0 ;
   }

   if (m_sock)
   {
      delete m_sock ;
      m_sock = nullptr ;
   }

   if (m_status.m_result == Status::Result::Pending)
      m_status.m_result = Status::Result::Failed ;

   m_status.m_tm = peripherals::RTC.date_time() ;
}

void NetQueryHelper::_on_closed (void)
{
   if (m_sock)
      _complete() ;
}

void NetQueryHelper::_on_receive (const hw::rak439::Sock::ReceiveData& data)
{
   m_status.m_result = m_recfn (data) ? Status::Result::Ok : Status::Result::Failed ;
   _complete() ;
}

uint8_t NetQueryHelper::_network_ready_delay (void)
{
   static uint8_t s_seconds = 0 ;
   return ++s_seconds * 3 ;
}

////////////////////////////////////////////////////////////////////////////////////////////

NetQueryHelper::Status::Status (void) :
   m_result{ Result::No },
   m_tm{ { 0, 0, 0 }, { 0, 0, 0 } }
{
}

const char *NetQueryHelper::Status::result_str (void) const
{
   static const char s_no[] PROGMEM = "Not queried" ;
   static const char s_ok[] PROGMEM = "Successful" ;
   static const char s_failed[] PROGMEM = "Failed" ;
   static const char s_pending[] PROGMEM = "Pending" ;

   static const char* const s_strs[] PROGMEM { s_no, s_ok, s_failed, s_pending } ;

   return (const char*) pgm_read_word (s_strs + static_cast<uint8_t> (m_result)) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace misc
