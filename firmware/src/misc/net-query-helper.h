////////////////////////////////////////////////////////////////////////////////////////////
//
// net-query-helper.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "lan/wmi-state.h"
#include "lan/lan-socket.h"
#include "util/datetime.h"
#include "hw/rak439/sock.h"
#include "aux/list.h"

namespace misc
{

////////////////////////////////////////////////////////////////////////////////////////////

class NetQueryHelper
{
   NetQueryHelper (const NetQueryHelper&) = delete ;
   NetQueryHelper& operator= (const NetQueryHelper&) = delete ;

public:
   typedef void (*QueryFn) (lan::Socket& sock, const lan::Address& resolved) ;
   typedef bool (*ReceiveFn) (const hw::rak439::Sock::ReceiveData& data) ;
   typedef void (*Completion) (bool cancelled) ;

   struct Status
   {
      enum class Result : uint8_t { No, Ok, Failed, Pending } ;

      Result m_result ;
      util::DateTime m_tm ;

      Status (void) ;

      const char *result_str (void) const ;
   } ;

public:
   NetQueryHelper (const util::Time& period, lan::Socket::Type socktype, const aux::String& addr, uint16_t port, QueryFn queryfn, ReceiveFn recfn) ;
   ~NetQueryHelper (void) ;

   bool do_now (void) ;
   util::DateTime next (void) const ;

   const Status& status (void) const { return m_status ; }

   void address (const aux::String& address, uint16_t port)
      { m_addr = address ; m_port = port ; }

   void period (util::Time period) { m_period = period ; }

private:
   void _on_network_state (lan::WmiState state) ;

   void _do_query (void) ;
   void _query_dns_resolved (uint16_t queryid, uint32_t ip) ;
   void _complete (bool cancelled = false) ;
   void _complete_impl (void) ;

   void _on_closed (void) ;
   void _on_receive (const hw::rak439::Sock::ReceiveData& data) ;

   static uint8_t _network_ready_delay (void) ;

private:
   const lan::Socket::Type m_sock_type ;
   const QueryFn m_queryfn ;
   const ReceiveFn m_recfn ;
   const uint8_t m_network_ready_delay ;

   aux::String m_addr ;
   uint16_t m_port ;

   util::Time m_period ;

   uint32_t m_schedule ;
   uint16_t m_timeout ;
   uint16_t m_dns_query ;

   lan::Socket *m_sock ;

   Status m_status ;

   typedef aux::List<NetQueryHelper*> Completions ;
   static Completions s_completions ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace misc
