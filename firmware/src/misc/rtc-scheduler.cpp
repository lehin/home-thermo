////////////////////////////////////////////////////////////////////////////////////////////
//
// rtc-scheduler.cpp
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "rtc-scheduler.h"
#include "peripherals/rtc.h"
#include "aux/ll-scheduler.h"

namespace misc
{

////////////////////////////////////////////////////////////////////////////////////////////

RtcScheduler RTCSCHED ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static bool operator< (const util::Date& lhs, const util::Date& rhs)
   {
      if (lhs.m_year != rhs.m_year)
         return lhs.m_year < rhs.m_year ;

      if (lhs.m_month != rhs.m_month)
         return lhs.m_month < rhs.m_month ;

      return lhs.m_day < rhs.m_day ;
   }

   static bool operator< (const util::Time& lhs, const util::Time& rhs)
   {
      if (lhs.m_hour != rhs.m_hour)
         return lhs.m_hour < rhs.m_hour ;

      if (lhs.m_min != rhs.m_min)
         return lhs.m_min < rhs.m_min ;

      return lhs.m_sec < rhs.m_sec ;
   }

   static bool operator< (const util::DateTime& lhs, const util::DateTime& rhs)
   {
      if (lhs.m_dt != rhs.m_dt)
         return lhs.m_dt < rhs.m_dt ;

      return lhs.m_tm < rhs.m_tm ;
   }

   static util::DateTime operator+ (const util::DateTime& lhs, const util::Time& rhs)
   {
      long secs = lhs.m_tm.total_seconds() + rhs.total_seconds() ;
      long days = secs / util::Time::day_seconds() ;
      secs %= util::Time::day_seconds() ;

      return { util::Time::from_seconds (secs), util::Date::from_days (lhs.m_dt.total_days() + days) } ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

void RtcScheduler::initialize (void)
{
   peripherals::RTC.signal_seconds_tick().connect ([] { RTCSCHED._rtc_tick() ; }) ;
}

uint32_t RtcScheduler::schedule (Task&& task, const Time& interval)
{
   return schedule (aux::forward (task), peripherals::RTC.date_time() + interval) ;
}

uint32_t RtcScheduler::schedule (Task&& task, const DateTime& tm)
{
   Schedule::iterator ins ;

   for (ins = m_schedule.begin(); ins != m_schedule.end(); ++ins)
      if (tm < ins->m_tm)
         break ;

   m_schedule.insert (ins, Item{ ++m_sched_id, tm, aux::forward (task) }) ;
   return m_sched_id ;
}

void RtcScheduler::cancel (uint32_t sched)
{
   auto it = m_schedule.find_if ([sched] (const auto& item) { return item.m_id == sched ; }) ;
   if (it != m_schedule.end())
      m_schedule.erase (it) ;
}

util::DateTime RtcScheduler::next (uint32_t sched)
{
   auto it = m_schedule.find_if ([sched] (const auto& item) { return item.m_id == sched ; }) ;
   return it != m_schedule.end() ? it->m_tm : DateTime{ { 0, 0, 0 }, { 0, 0, 0, 0 } } ;
}

void RtcScheduler::_rtc_tick (void)
{
   auto tm = peripherals::RTC.date_time() ;

   while (!m_schedule.empty() && !(tm < m_schedule.front().m_tm))
   {
      auto it = m_schedule.begin() ;
      Task task = aux::forward (it->m_task) ;
      m_schedule.erase (it) ;

      task() ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace misc
