////////////////////////////////////////////////////////////////////////////////////////////
//
// boiler-items.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "image-item.h"
#include "rounded-temp.h"

namespace ui
{

////////////////////////////////////////////////////////////////////////////////////////////

class BoilerDivider : public ScreenItem<BoilerDivider>
{
public:
   BoilerDivider (uint16_t x) : m_x{ x } {}

   void do_clear (void) const ;
   void do_draw (void) const ;

private:
   void _draw_impl (void) const ;

private:
   const uint16_t m_x ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class BoilerTempItem : public ScreenItem<BoilerTempItem, RoundedTemp<0> >
{
public:
   static constexpr peripherals::Display::Color color_invalid (void) { return peripherals::Display::inactive() ; }

public:
   BoilerTempItem (uint16_t x, uint16_t y, peripherals::Display::Color clr) :
      m_x{ x }, m_y{ y }, m_clr{ clr } {}

   void set_color (peripherals::Display::Color clr) ;

   void do_clear (void) const ;
   void do_draw (void) const ;

private:
   struct Box { uint16_t x, y, w, h ; } ;
   Box _box (void) const ;

private:
   const uint16_t m_x, m_y ;
   peripherals::Display::Color m_clr ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct BoilerChStat : ImageItemBool
{
   BoilerChStat (uint16_t x, uint16_t y) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct BoilerDhwStat : ImageItemBool
{
   BoilerDhwStat (uint16_t x, uint16_t y) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct BoilerFlameStat : ImageItemBool
{
   BoilerFlameStat (uint16_t x, uint16_t y) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace ui
