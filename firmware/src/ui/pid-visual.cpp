////////////////////////////////////////////////////////////////////////////////////////////
//
// pid-visual.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "pid-visual.h"
#include "peripherals/display.h"

namespace ui
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static constexpr uint16_t WIDTH = 72 ;
   static constexpr uint16_t HEIGHT = 17 ;

   using namespace peripherals ;

   static constexpr Display::Color operator/ (const Display::Color& lhs, uint8_t div)
   {
      return
      {
         static_cast<uint8_t> (lhs.r / div),
         static_cast<uint8_t> (lhs.g / div),
         static_cast<uint8_t> (lhs.b / div)
      } ;
   }

   static constexpr Display::Color CLR_BKGND = Display::black() ;
   static constexpr Display::Color CLR_FRAME = { 224, 224, 224 } ;

   static constexpr Display::Color CLR_0 = { 91, 239, 91 } ;
   static constexpr Display::Color CLR_NEG = { 0x20, 0x20, 0xff } ;
   static constexpr Display::Color CLR_POS = { 0xff, 0x20, 0x20 } ;

   static constexpr Display::Color CLR_0_DARK = CLR_0 / 3 ;
   static constexpr Display::Color CLR_NEG_DARK = CLR_NEG / 3 ;
   static constexpr Display::Color CLR_POS_DARK = CLR_POS / 3 ;
}

////////////////////////////////////////////////////////////////////////////////////////////

PidVisual::PidVisual (uint16_t x, uint16_t y, float m, const char *title) :
   base_class{ .0 },
   m_x{ x },
   m_y{ y },
   m_max{ m },
   m_title{ title }
{
}

void PidVisual::do_clear (void) const
{
   DISPLAY.bkgnd (CLR_BKGND) ;
   DISPLAY.clear (m_x, m_y, WIDTH, HEIGHT) ;

   m_pos_prev = INT_MIN ;
}

namespace
{
   static Display::Color _gradient (const Display::Color& src, const Display::Color& dst, int w, int pos)
   {
      return
      {
         (uint8_t)(src.r + ((int)dst.r - (int)src.r) * pos / w),
         (uint8_t)(src.g + ((int)dst.g - (int)src.g) * pos / w),
         (uint8_t)(src.b + ((int)dst.b - (int)src.b) * pos / w)
      } ;
   }
}

void PidVisual::do_draw (void) const
{
   bool fulldraw = (m_pos_prev == INT_MIN) ;
   const Rect rc = _rect() ;

   if (fulldraw)
   {
      DISPLAY.color (CLR_FRAME) ;

      DISPLAY.putsp (m_x, m_y + (HEIGHT - DISPLAY.font_metrics().h)/2, m_title) ;

      DISPLAY.hline (rc.x, rc.y, rc.w) ;
      DISPLAY.hline (rc.x, rc.y + rc.h - 1, rc.w) ;

      DISPLAY.vline (rc.x, rc.y, rc.h) ;
      DISPLAY.vline (rc.x + rc.w - 1, rc.y, rc.h) ;
   }

   float val = value().get() ;
   if (fabs (val) > m_max)
      val = val > 0 ? m_max : -m_max ;

   const int pos_max = (rc.w - 4) / 2 ;
   const int pos = val / m_max * pos_max ;
   if (pos == m_pos_prev)
      return ;

   const int left = !fulldraw ? min (pos, m_pos_prev) : -pos_max ;
   const int right = !fulldraw ? max (pos, m_pos_prev) : pos_max ;

   for (int n = left; n <= right; ++n)
   {
      bool in_range = pos >= 0 ? (n >= 0 && n <= pos) : (n <= 0 && n >= pos) ;

      const auto& _0 = in_range ? CLR_0 : CLR_0_DARK ;
      const auto& _1 = n < 0 ? (in_range ? CLR_NEG : CLR_NEG_DARK) : (in_range ? CLR_POS : CLR_POS_DARK) ;

      DISPLAY.color (_gradient (_0, _1, pos_max, abs (n))) ;
      DISPLAY.vline (rc.x + rc.w / 2 + n, rc.y + 2, rc.h - 4) ;
   }

   m_pos_prev = pos ;
}

PidVisual::Rect PidVisual::_rect (void) const
{
   DISPLAY.select_font (Display::Font::Regular) ;
   auto title_w = DISPLAY.font_metrics().w * strlen_P (m_title) + 4 ;

   if (!((WIDTH - title_w) % 2))
      ++title_w ;

   return { m_x + title_w, m_y, WIDTH - title_w, HEIGHT } ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace ui
