////////////////////////////////////////////////////////////////////////////////////////////
//
// image-item.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "screen-item.h"
#include "peripherals/display.h"

namespace ui
{

////////////////////////////////////////////////////////////////////////////////////////////

using Image = peripherals::Display::Image ;

////////////////////////////////////////////////////////////////////////////////////////////

class ImageItemBool : public ScreenItem<ImageItemBool,bool>
{
public:
   ImageItemBool (uint16_t x, uint16_t y, const Image& img, bool initial = false) :
      base_class{ initial },
      m_x{ x }, m_y{ y }, m_img{ img } {}

   void do_clear (void) const ;
   void do_draw (void) const ;

private:
   uint16_t m_x, m_y ;
   const Image& m_img ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace ui
