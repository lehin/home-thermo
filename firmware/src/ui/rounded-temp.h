////////////////////////////////////////////////////////////////////////////////////////////
//
// rounded-temp.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "util/tempval.h"

namespace ui
{

////////////////////////////////////////////////////////////////////////////////////////////

template<uint8_t _prec>
   struct RoundedTemp : util::TempVal
   {
      RoundedTemp (void) : util::TempVal{ undefined() } {}
      RoundedTemp (const util::TempVal& val) : util::TempVal{ val.round (_prec) } {}
      RoundedTemp& operator= (const util::TempVal& val) { util::TempVal::operator= (val.round(_prec)) ; return *this ; }

      inline friend bool operator== (const RoundedTemp& lhs, const util::TempVal& rhs)
         { return static_cast<const util::TempVal&> (lhs) == rhs.round (_prec) ; }

      inline friend bool operator== (const util::TempVal& lhs, const RoundedTemp& rhs)
         { return lhs.round(0) == static_cast<const util::TempVal&> (rhs) ;
      }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace ui
