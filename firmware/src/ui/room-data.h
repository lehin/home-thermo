////////////////////////////////////////////////////////////////////////////////////////////
//
// room-data.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "screen-item.h"
#include "aux/string.h"
#include "util/tempval.h"
#include "peripherals/display.h"

namespace ui
{

////////////////////////////////////////////////////////////////////////////////////////////

struct RoomData
{
   aux::String m_name ;
   util::TempVal m_target,
                 m_current ;

   enum class State : uint8_t { Inactive, Ok, Low, High, NoData } ;
   State m_state ;

   mutable struct Flags
   {
      bool m_refresh_name : 1,
           m_refresh_current : 1,
           m_refresh_frame : 1 ;
   } m_flags ;

   RoomData (aux::String name, State state, util::TempVal target, util::TempVal cur = util::TempVal::undefined()) ;

   struct Name { aux::String v ; } ;
   struct Target { util::TempVal v ; Target (util::TempVal _v) : v{ _v.round(0) } {} } ;
   struct Current { util::TempVal v ; Current (util::TempVal _v) : v{ _v.round(1) } {} } ;

   RoomData& operator= (Name rhs) { m_name = rhs.v ; m_flags.m_refresh_name = true ; return *this ; }
   RoomData& operator= (Target rhs) { m_target = rhs.v ; m_flags.m_refresh_current = true ; return *this ; }
   RoomData& operator= (Current rhs) { m_current = rhs.v ; m_flags.m_refresh_current = true ; return *this ; }
   RoomData& operator= (State rhs) ;

   bool operator== (Name rhs) const { return m_name == rhs.v ; }
   bool operator== (Target rhs) const { return m_target == rhs.v ; }
   bool operator== (Current rhs) const { return m_current == rhs.v ; }
   bool operator== (State rhs) const { return m_state == rhs ; }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class RoomDataItem : public ScreenItem<RoomDataItem,RoomData>
{
   struct XY
   {
      uint16_t x, y ;
   } ;

   using Metrics = peripherals::Display::Metrics ;

public:
   template<typename ... _Args>
      RoomDataItem (XY pos, _Args ... args) :
         base_class{ args... },
         m_pos{ pos },
         m_sz{ _calc_size() }
      {
      }

   void do_clear (void) const ;
   void do_draw (void) const ;

   static XY visual_size (void) { return _calc_size() ; }

private:
   void _draw_frame (const Metrics& small, const Metrics& large) const ;
   void _draw_caption (const Metrics& small, const Metrics& large) const ;
   void _draw_current (const Metrics& small, const Metrics& large) const ;
   void _draw_extra (const Metrics& small, const Metrics& large) const ;

   static XY _calc_size (void) ;

private:
   const XY m_pos, m_sz ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class RoomDataFrameItem : public ScreenItem<RoomDataFrameItem>
{
public:
   RoomDataFrameItem (uint16_t y) : m_y{ y } {}

   void do_clear (void) const ;
   void do_draw (void) const ;

private:
   void _draw_impl (void) const ;

private:
   const uint16_t m_y ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace ui
