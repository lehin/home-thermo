////////////////////////////////////////////////////////////////////////////////////////////
//
// pid-visual.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "screen-item.h"
#include <stdint.h>
#include <math.h>
#include <limits.h>

namespace ui
{

////////////////////////////////////////////////////////////////////////////////////////////

template<int _prec>
   struct RoundedFloat
   {
      float m_val ;

      RoundedFloat (void) : m_val{ NAN } {}
      RoundedFloat (float val) : m_val{ _round (val) } {}
      RoundedFloat& operator= (float val) { m_val = _round (val) ; return *this ; }

      float get (void) const { return m_val ; }

      inline friend bool operator== (const RoundedFloat& lhs, const RoundedFloat& rhs)
         { return fabs (lhs.m_val - rhs.m_val) < 1/_exponent() ; }

      inline friend bool operator== (const RoundedFloat& lhs, float rhs)
         { return fabs (lhs.m_val - _round (rhs)) < 1/_exponent() ; }

      inline friend bool operator== (float lhs, const RoundedFloat& rhs)
         { return fabs (_round (lhs) - rhs.m_val) < 1/_exponent() ; }

      static float _round (float f) { return (int)(f * _exponent() + (f < 0 ? -0.5 : 0.5)) / _exponent() ; }

      static constexpr float _exponent (int prec = _prec)
         { return prec > 0 ? _exponent (prec-1) * 10 : (prec < 0 ? _exponent (prec+1) / 10 : 1) ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

class PidVisual : public ScreenItem< PidVisual, RoundedFloat<2> >
{
public:
   PidVisual (uint16_t x, uint16_t y, float m, const char *title) ;

   void do_clear (void) const ;
   void do_draw (void) const ;

private:
   struct Rect
   {
      uint16_t x, y, w, h ;
   } ;

   Rect _rect (void) const ;

private:
   const uint16_t m_x, m_y ;
   const float m_max ;
   const char * const m_title ;

   mutable int m_pos_prev = INT_MIN ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace ui
