////////////////////////////////////////////////////////////////////////////////////////////
//
// screen-item.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

namespace ui
{

////////////////////////////////////////////////////////////////////////////////////////////

class ScreenItemBase
{
public:
   ScreenItemBase (void) ;
   ~ScreenItemBase (void) ;

   void reset (void) { set_dirty() ; }
   void show (bool shown = true) ;
   bool is_shown (void) const { return m_shown ; }

   void render (void) ;
   void clear (void) ;

protected:
   void set_dirty (void) ;

   virtual void draw (void) const = 0 ;
   virtual void do_clear (void) const {}

private:
   bool m_dirty : 1,
        m_shown : 1 ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Result, typename _Tp = void>
   class ScreenItem : public ScreenItemBase
   {
   protected:
      typedef ScreenItem base_class ;

   public:
      typedef _Tp value_type ;

   public:
      template<typename ... _Args>
         ScreenItem (_Args ... args) : m_value{ args... } {}

      template<typename _Rhs>
         bool update (const _Rhs& rhs)
         {
            if (m_value == rhs)
               return false ;

            m_value = rhs ;
            set_dirty() ;

            return true ;
         }

      virtual void draw (void) const
      {
         static_cast<const _Result*>(this) -> do_draw() ;
      }

      const value_type& value (void) const { return m_value ; }

   private:
      value_type m_value ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Result>
   class ScreenItem<_Result,void> : public ScreenItemBase
   {
   protected:
      typedef ScreenItem base_class ;

   public:
      virtual void draw (void) const
      {
         static_cast<const _Result*>(this) -> do_draw() ;
      }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace ui
