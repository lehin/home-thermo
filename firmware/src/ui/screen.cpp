////////////////////////////////////////////////////////////////////////////////////////////
//
// screen.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "screen.h"
#include "screen-item.h"
#include "aux/scheduler.h"
#include "peripherals/display.h"

namespace ui
{

////////////////////////////////////////////////////////////////////////////////////////////

Screen SCREEN ;

////////////////////////////////////////////////////////////////////////////////////////////

void Screen::initialize (void)
{
   aux::SCHED.schedule
      (
         []
         {
            peripherals::DISPLAY.initialize() ;

            for (auto item : SCREEN.m_items)
               item->reset() ;

            SCREEN._schedule_update() ;
         },
         50
      ) ;
}

void Screen::register_item (ScreenItemBase *item)
{
   m_items.push_back (item) ;
}

void Screen::unregister_item (ScreenItemBase *item)
{
   m_items.erase (m_items.find (item)) ;
}

void Screen::_schedule_update (void)
{
   m_update_schedule =
      aux::SCHED.schedule
         (
            []
            {
               if (!SCREEN.m_update_schedule)
                  return ;

               SCREEN._schedule_update() ;
               SCREEN._update() ;
            },
            100
         ) ;
}

void Screen::_update (void)
{
   for (auto item : SCREEN.m_items)
      item->clear() ;

   for (auto item : SCREEN.m_items)
      item->render() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace ui
