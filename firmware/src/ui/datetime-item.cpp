////////////////////////////////////////////////////////////////////////////////////////////
//
// datetime-item.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "datetime-item.h"
#include "peripherals/display.h"

namespace ui
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   using D = peripherals::Display ;
   constexpr auto& d = peripherals::DISPLAY ;

   constexpr auto FONT = D::Font::Regular ;
   constexpr uint8_t DATE_CHARS = 10 ;
   constexpr uint8_t TIME_CHARS = 5 ;
}

////////////////////////////////////////////////////////////////////////////////////////////

DateItem::DateItem (void)
{
   d.select_font (FONT) ;
   auto m = d.font_metrics() ;
   m_x = D::WIDTH - (DATE_CHARS + TIME_CHARS + 1) * m.w ;
   m_y = (24 - m.h) / 2 ;
}

void DateItem::do_clear (void) const
{
   d.select_font (FONT) ;
   auto m = d.font_metrics() ;

   d.bkgnd (D::black()) ;
   d.clear (m_x, m_y, DATE_CHARS * m.w, m.h) ;
}

void DateItem::do_draw (void) const
{
   d.select_font (FONT) ;
   d.bkgnd (D::black()) ;
   d.color ({ 239, 239, 239 }) ;

   char buf[DATE_CHARS] ;
   auto end = value().to_string (buf, false) ;
   d.puts (m_x, m_y, aux::range (buf + 0, end)) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

TimeItem::TimeItem (void)
{
   d.select_font (FONT) ;
   auto m = d.font_metrics() ;
   m_x = D::WIDTH - TIME_CHARS * m.w ;
   m_y = (24 - m.h) / 2 ;
}

void TimeItem::do_clear (void) const
{
   d.select_font (FONT) ;
   auto m = d.font_metrics() ;

   d.bkgnd (D::black()) ;
   d.clear (m_x, m_y, TIME_CHARS * m.w, m.h) ;
}

void TimeItem::do_draw (void) const
{
   d.select_font (FONT) ;
   d.bkgnd (D::black()) ;
   d.color ({ 239, 239, 239 }) ;

   char buf[TIME_CHARS] ;
   auto end = value().to_string (buf, false) ;
   d.puts (m_x, m_y, aux::range (buf + 0, end)) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace ui
