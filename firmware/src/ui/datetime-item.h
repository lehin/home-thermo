////////////////////////////////////////////////////////////////////////////////////////////
//
// datetime-item.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "screen-item.h"
#include "util/datetime.h"

namespace ui
{

////////////////////////////////////////////////////////////////////////////////////////////

class DateItem : public ScreenItem<DateItem,util::Date>
{
public:
   DateItem (void) ;

   void do_clear (void) const ;
   void do_draw (void) const ;

private:
   uint16_t m_x, m_y ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

struct TimeRound : util::Time
{
   TimeRound (void) = default ;
   TimeRound (const util::Time& tm) : util::Time{ tm } { m_sec = 0 ; }
   TimeRound& operator= (const util::Time& tm) { util::Time::operator= (tm) ; m_sec = 0 ; return *this ; }

   inline friend bool operator== (const TimeRound& lhs, util::Time rhs)
   {
      rhs.m_sec = 0 ;
      return static_cast<const util::Time&> (lhs) == rhs ;
   }

   inline friend bool operator== (util::Time lhs, const TimeRound& rhs)
   {
      lhs.m_sec = 0 ;
      return lhs == static_cast<const util::Time&> (rhs) ;
   }
} ;

class TimeItem : public ScreenItem<TimeItem,TimeRound>
{
public:
   TimeItem (void) ;

   void do_clear (void) const ;
   void do_draw (void) const ;

private:
   uint16_t m_x, m_y ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace ui
