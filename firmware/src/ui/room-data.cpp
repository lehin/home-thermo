////////////////////////////////////////////////////////////////////////////////////////////
//
// room-data.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "room-data.h"
#include "aux/minmax.h"
#include "util/strconv.h"

namespace ui
{

////////////////////////////////////////////////////////////////////////////////////////////

RoomData::RoomData (aux::String name, State state, util::TempVal target, util::TempVal cur) :
   m_name{ name },
   m_target{ target.round(0) },
   m_current{ cur.round(1) },
   m_state{ state },
   m_flags{ true, true, true }
{
}

RoomData& RoomData::operator= (State rhs)
{
   m_state = rhs ;
   m_flags = { true, true, false } ;
   return *this ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   constexpr uint8_t PADDING_X = 4 ;
   constexpr uint8_t PADDING_Y = 2 ;
   constexpr uint8_t PADDING_Y_TEMP = 4 ;
   constexpr uint8_t BORDER = 1 ;
   constexpr uint8_t DIVIDER = 1 ;

   using D = peripherals::Display ;
   constexpr auto& d = peripherals::DISPLAY ;

   constexpr D::Color COLOR_FRAME = { 191, 191, 0 } ;
   constexpr D::Color COLOR_DIVIDER = { 191, 191, 191 } ;
   constexpr D::Color COLOR_CAPTION_BG = { 63, 63, 63 } ;
   constexpr D::Color COLOR_CAPTION_FG = D::white() ;
   constexpr D::Color COLOR_TEMP_OK = { 91, 239, 91 } ;
   constexpr D::Color COLOR_TEMP_LOW = { 0x4f, 0xc2, 0xed } ;
   constexpr D::Color COLOR_TEMP_HIGH = { 0xfd, 0xb1, 0x5b } ;
   constexpr D::Color COLOR_INACTIVE = { 127, 127, 127 } ;
   constexpr D::Color COLOR_NO_DATA = { 192, 0, 0 } ;
   constexpr D::Color COLOR_BOTTOM = { 224, 224, 0 } ;
   constexpr D::Color COLOR_BOTTOM_INACTIVE = { 127, 127, 0 } ;
}

void RoomDataItem::do_clear (void) const
{
   d.bkgnd (peripherals::Display::black()) ;
   d.clear (m_pos.x, m_pos.y, m_sz.x, m_sz.y) ;
   value().m_flags = { true, true, true } ;
}

void RoomDataItem::do_draw (void) const
{
   d.select_font (D::Font::Regular) ;
   auto small_metrics = d.font_metrics() ;
   auto capm = d.font_metrics() ;

   d.select_font (D::Font::Digits) ;
   auto large_metrics = d.font_metrics() ;

   auto& v = value() ;

   if (v.m_flags.m_refresh_frame)
   {
      v.m_flags.m_refresh_frame = false ;
      _draw_frame (small_metrics, large_metrics) ;
   }

   if (v.m_flags.m_refresh_name)
   {
      v.m_flags.m_refresh_name = false ;
      _draw_caption (small_metrics, large_metrics) ;
   }

   if (v.m_flags.m_refresh_current)
   {
      v.m_flags.m_refresh_current = false ;
      _draw_current (small_metrics, large_metrics) ;
      _draw_extra (small_metrics, large_metrics) ;
   }
}

void RoomDataItem::_draw_frame (const Metrics& small, const Metrics&) const
{
   d.color (COLOR_FRAME) ;
   d.box (m_pos.x, m_pos.y, m_sz.x, m_sz.y) ;

   d.color (COLOR_DIVIDER) ;
   d.hline (m_pos.x, m_pos.y + BORDER + small.h + 2*PADDING_Y, m_sz.x) ;
}

void RoomDataItem::_draw_caption (const Metrics& small, const Metrics& large) const
{
   d.select_font (D::Font::Regular) ;
   d.opaque_text (false) ;

   auto& v = value() ;

   d.bkgnd (COLOR_CAPTION_BG) ;
   d.clear (m_pos.x + BORDER, m_pos.y + BORDER, m_sz.x - 2*BORDER, small.h + 2*PADDING_Y) ;

   uint16_t l = min (v.m_name.length(), (m_sz.x - 2*BORDER - 2*PADDING_X) / small.w) ;
   uint16_t offs = (m_sz.x - l * small.w) / 2 ;
   d.color (v.m_state != RoomData::State::Inactive ? COLOR_CAPTION_FG : COLOR_INACTIVE) ;
   d.puts (m_pos.x + offs, m_pos.y + BORDER + PADDING_Y, v.m_name) ;

   d.bkgnd (D::black()) ;
   d.opaque_text (true) ;
}

void RoomDataItem::_draw_current (const Metrics& small, const Metrics& large) const
{
   uint16_t x = m_pos.x + BORDER + PADDING_X,
            y = m_pos.y + BORDER + small.h + 2*PADDING_Y + DIVIDER + PADDING_Y_TEMP ;

   auto& v = value() ;

   if (!v.m_current.is_defined())
   {
      d.clear (x, y, large.w * 6, large.h) ;
      return ;
   }

   d.select_font (D::Font::Digits) ;

   static constexpr D::Color _colors[]
   {
      COLOR_INACTIVE,
      COLOR_TEMP_OK, COLOR_TEMP_LOW, COLOR_TEMP_HIGH,
      COLOR_NO_DATA
   } ;

   d.color (_colors[(uint8_t)v.m_state]) ;

   const char temp0[] =
   {
      v.m_current < 0_t ? '\x0c' : '\x0d',
      static_cast<char> (abs (v.m_current).integer() / 10),
      static_cast<char> (abs (v.m_current.integer()) % 10),
      '\x0a',
      static_cast<char> (abs (v.m_current).frac() / 10),
      '\x0e'
   } ;

   d.puts (x, y, aux::range (temp0)) ;
}

namespace
{
   static char* _append_pstr (char *dst, const char *pstr)
   {
      while (char ch = pgm_read_byte (pstr++))
         *dst++ = ch ;

      return dst ;
   }

   static char* _append_str (char *dst, aux::StringRef str)
   {
      memcpy (dst, str.begin(), str.size()) ;
      return dst + str.size() ;
   }
}

void RoomDataItem::_draw_extra (const Metrics& small, const Metrics& large) const
{
   auto& v = value() ;

   char buf[11] ;
   buf[10] = ' ' ; // it's possible to have shorter string by 1 character if Tdiff = 0.0°C

   char *pos = buf ;

   if (!v.m_target.is_defined())
      pos = _append_pstr (pos, PSTR ("--")) ;
   else
      pos = util::itoa ((uint8_t)v.m_target.integer(), pos, true, 10) ;

   pos = _append_pstr (pos, PSTR ("°C/")) ;

   static const char NODIFF[] PROGMEM = "--.-" ;

   if (!v.m_current.is_defined() || !v.m_current.is_defined())
      pos = _append_pstr (pos, NODIFF) ;
   else
   {
      auto diff = v.m_current - v.m_target ;
      if (abs (diff) < 10_t)
         pos = _append_str (pos, diff.format (true, true)) ;
      else
         pos = _append_pstr (pos, NODIFF) ;
   }

   pos = _append_pstr (pos, PSTR ("°C")) ;

   d.select_font (D::Font::Regular) ;
   d.color (v.m_state != RoomData::State::Inactive ? COLOR_BOTTOM : COLOR_BOTTOM_INACTIVE) ;

   d.puts
      (
         m_pos.x + BORDER + PADDING_X,
         m_pos.y + BORDER + small.h + 2*PADDING_Y + DIVIDER + 2*PADDING_Y_TEMP + large.h,
         aux::range (buf)
      ) ;
}

RoomDataItem::XY RoomDataItem::_calc_size (void)
{
   d.select_font (D::Font::Digits) ;
   auto digits_metrics = d.font_metrics() ;

   d.select_font (D::Font::Regular) ;
   auto small_metrics = d.font_metrics() ;

   return
   {
      2u*PADDING_X + 2u*BORDER + 6*digits_metrics.w,
      2u*BORDER + DIVIDER + small_metrics.h + 2u*PADDING_Y +
         digits_metrics.h + 2u*PADDING_Y_TEMP +
         small_metrics.h + PADDING_Y
   } ;
}

////////////////////////////////////////////////////////////////////////////////////////////

void RoomDataFrameItem::do_clear (void) const
{
   d.color (D::black()) ;
   _draw_impl() ;
}

void RoomDataFrameItem::do_draw (void) const
{
   d.color ({ 128, 128, 128 }) ;
   _draw_impl() ;
}

void RoomDataFrameItem::_draw_impl (void) const
{
   d.hline (0, m_y, D::WIDTH) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace ui
