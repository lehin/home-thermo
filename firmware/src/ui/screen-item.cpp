////////////////////////////////////////////////////////////////////////////////////////////
//
// screen-item.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "screen-item.h"
#include "screen.h"

namespace ui
{

////////////////////////////////////////////////////////////////////////////////////////////

ScreenItemBase::ScreenItemBase (void) :
   m_dirty{ false },
   m_shown{ false }
{
   SCREEN.register_item (this) ;
}

ScreenItemBase::~ScreenItemBase (void)
{
   SCREEN.unregister_item (this) ;
}

void ScreenItemBase::show (bool shown)
{
   if (m_shown == shown)
      return ;

   m_shown = shown ;
   m_dirty = true ;
}

void ScreenItemBase::render (void)
{
   if (!m_shown || !m_dirty)
      return ;

   m_dirty = false ;

   draw() ;
}

void ScreenItemBase::clear (void)
{
   if (m_shown || !m_dirty)
      return ;

   m_dirty = false ;

   do_clear() ;
}

void ScreenItemBase::set_dirty (void)
{
   if (m_shown)
      m_dirty = true ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace ui
